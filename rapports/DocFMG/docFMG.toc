\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Format des fichiers d'entr\IeC {\'e}e et de sortie}{2}
\contentsline {section}{\numberline {3}Lancement de FMG}{2}
\contentsline {subsection}{\numberline {3.1}Commande}{2}
\contentsline {subsection}{\numberline {3.2}Fichiers requises}{2}
\contentsline {subsection}{\numberline {3.3}Options}{2}
\contentsline {section}{\numberline {4}Appel de la biblioth\IeC {\`e}que FMG}{4}
\contentsline {subsection}{\numberline {4.1}Arguments}{4}
\contentsline {subsection}{\numberline {4.2}Fonctions d'interface}{4}
\contentsline {subsection}{\numberline {4.3}Param\IeC {\`e}tres modifiables via les fonctions d'interface}{6}
\contentsline {subsubsection}{\numberline {4.3.1}Param\IeC {\`e}tres entiers}{6}
\contentsline {subsubsection}{\numberline {4.3.2}Param\IeC {\`e}tres doubles}{7}
\contentsline {section}{\numberline {5}Fichiers et informations de sortie}{7}
\contentsline {subsection}{\numberline {5.1}Fichiers de sortie}{7}
\contentsline {subsubsection}{\numberline {5.1.1}Mode de d\IeC {\'e}bogage}{8}
\contentsline {subsection}{\numberline {5.2}Informations imprim\IeC {\'e}es sur l'\IeC {\'e}cran}{8}
