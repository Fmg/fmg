\section{Appel de la bibliothèque FMG}

\indent Pour faire l'adaptation de maillages avec FMG depuis un autre programme, il faut appeler la fonction de la bibliothèque, \emph{libfmg2d} ou \emph{libfmg3d}, en dépendant du nombre de dimensions. Cet appel peut être fait depuis un programme en C ou en Fortran 90. 

\indent Dans les cas 2D et 3D, les prototypes sont similaires :


\indent \begin{function} int FMG\_fmglib2d(MMG5\_pMesh mmgMesh ,MMG5\_pSol mmgSol,FMG\_pInfo param, double *xk, double *yk ) \end{function}

\indent \begin{function} int FMG\_fmglib3d(MMG5\_pMesh mmgMesh ,MMG5\_pSol mmgSol,FMG\_pInfo param, double *xk, double *yk, double *zk ) \end{function}

\subsection{Arguments}

\begin{itemize}
	\item \textbf{mmgMesh : } pointeur vers la structure du maillage, contenant les coordonnées du \textbf{maillage de référence (fixe)};
	\item \textbf{mmgSol : } pointeur vers la structure de la solution
	\begin{itemize}
		\item Si l'adaptation physique \textbf{ou} l'adaptation Level Set sont activées, le vecteur de valeurs de la solution a la taille égale au nombre de points du maillage;
		\item Si l'adaptation physique \textbf{et} l'adaptation Level Set sont activées, le vecteur de valeurs de la solution a la taille égale à deux fois le nombre de points du maillage, étant la solution physique dans la première moitié, et la fonction Level Set dans la deuxième;
	\end{itemize}
	\item \textbf{param : } pointeur vers la structure des paramètres de FMG; quelques paramètres sont modifiables via des fonctions d'interface (voir ci-dessous);
	\item \textbf{xk, yk, zk : } tableaux de coordonnées du \textbf{maillage physique (déformable)}
\end{itemize}

\subsection{Fonctions d'interface}

\indent Les fonctions d'interface (fonctions API) permettent d'accéder (écriture et lecture) des structures de MMG et FMG.

\indent Les fonctions d'interface spécifiques de FMG sont les suivantes : 


\begin{itemize}
		\item Initialisation des structures du maillage, de la solution et des paramètres, dans le cas 2D : 
		\indent \begin{function} void FMG\_Init\_mesh2d(MMG5\_pMesh *mesh, MMG5\_pSol *sol,FMG\_pInfo *fmgparam) \end{function}

		\item Idem pour le cas 3D :
         \indent \begin{function} void FMG\_Init\_mesh3d(MMG5\_pMesh *mesh, MMG5\_pSol *sol,FMG\_pInfo *fmgparam) \end{function}

		\item Initialisation des paramètres (attribution des valeurs par défaut) :
        \indent \begin{function} \_FMG\_Init\_parameters(FMG\_pInfo param)   \end{function}
        
        \item Modification d'un paramètre entier (attribution de la valeur \(val\)), spécifié par l'entier \(iparam = FMG\_IPARAM\_xxxxxx\) (voir liste de paramètres ci-dessous) : 
         \indent \begin{function} int FMG\_Set\_iparameter(FMG\_pInfo param, int iparam, int val) \end{function}
         
        \item Modification d'un paramètre double (attribution de la valeur \(val\)), spécifié par l'entier \(iparam = FMG\_DPARAM\_xxxxxx\) (voir liste de paramètres ci-dessous) : 
         \indent \begin{function} int FMG\_Set\_iparameter(FMG\_pInfo param, int dparam, double val) \end{function}         
\end{itemize}

\subsection{Paramètres modifiables via les fonctions d'interface}

\subsubsection{Paramètres entiers}

\begingroup

\begin{footnotesize}

\begin{table}[H]
    \footnotesize
	\centering
	\begin{tabular}{|C{5cm}|C{2cm}|C{2cm}|C{6cm}|}
		\hline
		\textbf{\centering Nom} & \textbf{Valeurs possibles} & \textbf{Valeur par défaut} & \textbf{Description}\\
		\hline
		FMG\_IPARAM\_verbose & -10..10 & 1 & niveau de verbosité \\
		\hline
  		FMG\_IPARAM\_mem & n/-1 & -1 & met la taille de mémoire à \(n\) Mbytes ou maintient la value par défaut \\
  		\hline
  		FMG\_IPARAM\_debug & 1/0 & & active/désactive le mode de débogage \\
  		\hline
  		FMG\_IPARAM\_msh & 0/1/2 & & Lit/écrit pour gmsh visu si \(val = 1\) (out); si \(val=2\) (in/out) \\
  		\hline 
  		FMG\_IPARAM\_levelSet & 1/0 & 0 & active/désactive l'adaptation à une fonction Level Set \\
		\hline
		FMG\_IPARAM\_physAdapt & 1/0 & 0 & Active/désactive l'adaptation physique	 \\	
  		\hline
  		FMG\_IPARAM\_desSize & 1/0 & 1 & active l'adaptation basée sur l'imposition de tailles désirées / sur les gradients \\
   		\hline
  		FMG\_IPARAM\_metric & 1/0 & 0 & si l'adaptation est faite basée sur l'imposition de tailles désirées (FMG\_IPARAM\_desSize = 1), les tailles sont calculée à partir d'une métrique (\(val=1\)) ou analytiquement, avec des zones prédéfinies (\(val=0\))  \\
  		\hline
  		FMG\_IPARAM\_revert & 1/0 & 0 & active/désactive désadaptation \\
  		\hline
		FMG\_IPARAM\_smooth & 1/0 & 1 & active/désactive le lissage de la fonction Level Set (seulement dans le cas de l'adaptation basée sur les gradients)\\
		\hline
  		FMG\_IPARAM\_ijacobi\_max & n & 10 & Nombre maximal d'itérations \\
  		\hline
  		FMG\_IPARAM\_maxOptim & n & 10 & nombre maximal d'éléments à traiter dans le pas d'optimisation  du maillage (procédure pour éviter la relaxation) \\
  		\hline
  		FMG\_IPARAM\_circDom & 0/1 & 0 & domaine rectangulaire/circulaire \\
  		\hline
  		FMG\_IPARAM\_createRef & 0/1 & 0 &  crée les références des points du maillage (en tenant compte de l'option \emph{circDom}). Par défaut, les références sont seulement corrigées par rapport aux conventions de FMG  \\
  		\hline
  		FMG\_IPARAM\_nsols & 1/2 & 1 & nombre de solutions à adapter \\
  		\hline
  		FMG\_IPARAM\_solCase & 0/1..n & 0 & solution définie dans mmgSol (\(val = 0\)) ou une des \(n\) fonctions analytiques pré-définies \\
  		\hline
  		%FMG\_IPARAM\_fileID : }            /*!< n, basis for file names : dom.(fileID+ijacobi).mesh */
  %\item \textbf{FMG\_IPARAM\_imprim : }            /*!< 0/1, Turn on/off save partial meshes */
  		FMG\_IPARAM\_stepSave & n & 1 & pas d'impression de la solution (fichiers .mesh et .sol) \\
  		\hline
  		FMG\_IPARAM\_stepImprim & n & 1 & pas d'impression d'informations sur la sortie standard (fichiers .mesh et .sol) \\
  		\hline
%  		FMG\_IPARAM\_external &         /*!< 0/1, Turn on/off sizes read from external files*/
	\end{tabular}
\end{table}

\end{footnotesize}
\endgroup

\subsubsection{Paramètres doubles}
\begin{table}[H]
    \footnotesize
	\centering
	\begin{tabular}{|C{5cm}|C{2cm}|C{2cm}|C{6cm}|}
		\hline
		\textbf{\centering Nom} & \textbf{Valeurs possibles} & \textbf{Valeur par défaut} & \textbf{Description}\\
		\hline
  		FMG\_DPARAM\_delta & \(val \geq 0\) & 0.01 & demi-taille de la couche de lissage de la fonction Level Set ou de valeur minimale de la métrique \\
  		\hline
  		FMG\_DPARAM\_alpha0 & \(val \geq 0\) & 1000.0 & coefficient pour le gradient de la première solution dans mmgSol  \\
  		\hline
  		FMG\_DPARAM\_beta0 & \(val \geq 0\) & 0.0 & coefficient pour le hessien de la première solution dans mmgSol \\
  		\hline
%  \item \textbf{FMG\_DPARAM\_tau0 : }              /*!< val, Coefficient for solution 0 in the computation of omega */
  		FMG\_DPARAM\_alpha1 & \(val \geq 0\) & 1000.0 & coefficient pour le gradient de la deuxième solution dans mmgSol \\
  		\hline
  		FMG\_DPARAM\_beta1 & \(val \geq 0\) & 0.0 & coefficient pour le hessien de la deuxième solution dans mmgSol \\
  		\hline
%  \item \textbf{FMG\_DPARAM\_tau1 : }              /*!< val, Coefficient for solution 1 in the computation of omega */
  		FMG\_DPARAM\_hmin & \(val \geq 0\) & 0.0001 & valeur minimale pour les tailles dans le calcul de la métrique \\
  		\hline
   		FMG\_DPARAM\_hmax & \(val \geq 0\) & 2.0 & valeur maximale pour les tailles dans le calcul de la métrique \\
  		\hline
   		FMG\_DPARAM\_epsMet & \(val \geq 0\) & 0.0001 & limitation à l'erreur d'interpolation pour le calcul de la métrique \\
  		\hline
	\end{tabular}
\end{table}

