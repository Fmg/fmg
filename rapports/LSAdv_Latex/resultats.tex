\section{Résultats}

\subsection{Maillage initial homogène}

\indent On va d'abord faire des tests en partant de maillages homogènes. On va traiter deux cas : l'advection d'un cercle et l'oscillation de l'aile Naca. On a fait varier le nombre de pas du mouvement dans l'ensemble \(\{10,20,40\}\), et dans chaque adaptation on a fait 200 itérations de Jacobi.

\indent \textbf{Premier test} : advection d'un cercle, avec 10 pas de mouvement

\indent La figure \ref{fig:CercleT1} montre l'adaptation finale à trois pas du mouvement et le détail d'un des maillages adaptés. On observe qu'on a en effet atteint notre objectif : en partant d'un maillage adapté à une certaine position de l'objet, les points sont capables de bouger vers la nouvelle position (ou de simplement "désadapter", car on les impose une taille plus grande), et on ne voit presque aucun trace du dernier raffinement.

\indent Dans la figure \ref{fig:iter10T1Detail} on voit pourtant que le résultat n'est pas complètement optimal : dans toutes les itérations, le maillage est un peu plus raffiné dans la moitié gauche du cercle, car, comme le cercle bouge vers la droite, on a au début de chaque adaptation une concentration plus grande de points proches de cette moitié, dû à l'adaptation antérieure. De plus, comme le raffinement de la moitié droite exige le déplacement de points plus lointains, il y a dans le voisinage de la couche adaptée un nombre plus grand d'éléments anisotropes dans la direction normale au cercle.

\figCircle{1}{0}{5}{10}{10}{.15}{.2}{Cercle}{10}

\indent La figure \ref{fig:intermCercleT1}, à son tour, montre trois itérations de Jacobi dans le cinquième pas du mouvement. À partir de l'itération représentée dans la figure \ref{fig:interm150T1} le maillage na change pas de façon significative.

\figJacobi{1}{0}{50}{100}{150}{.15}{Cercle}{5}

\indent Les tests suivantes ont l'objectif de vérifier si on obtient des résultats pareils avec des discrétisations plus ou moins grossières du mouvement du cercle. Dans les tests réalisés avec les anciennes approches, on avait conclu qu'il fallait trouver un équilibre, car un grand nombre de pas conduisait très souvent à la relaxation, et un petit nombre empêchait la transition d'une adaptation à l'autre.

\clearpage
\indent \textbf{Deuxième test} : advection d'un cercle, avec 2 pas de mouvement

\figCircle{5}{0}{1}{2}{2}{.15}{.2}{Cercle}{2}

\figJacobi{5}{0}{50}{100}{200}{.15}{Cercle}{1}

\indent Même avec des positions assez séparés, on obtient une bonne adaptation dans chaque pas, mais ici le dérafinemment de la dernière position / raffinement de la position actuelle est beaucoup plus intense que le mouvement de points entre deux positions successives. Pour cette raison, la transition entre les deux adaptations est plus lente, et à la fin de 200 itérations de Jacobi on voit encore la dernière adaptation (même que faiblement).

\clearpage
\indent \textbf{Troisième et quatrième tests} : advection d'un cercle, avec 20 et 40 pas de mouvement

\figCircle{3}{0}{10}{20}{20}{.15}{.2}{Cercle}{20}

\figJacobi{3}{0}{50}{100}{150}{.15}{Cercle}{10}

\figCircle{6}{0}{20}{40}{40}{.15}{.2}{Cercle}{40}

\figJacobi{6}{0}{50}{100}{150}{.15}{Cercle}{20}

\indent Dans ces deux derniers tests, toutes les adaptations ont été exécutées complètement, sans relaxation, ce qui est un grand gain par rapport aux résultat qu'on avait. On a également obtenu des bonnes adaptations à chaque pas du mouvement, mais la différence du raffinement entre les moitiés gauche et droite du cercle est plus forte. En effet, en comparant les figures \ref{fig:iter10T1Detail}, \ref{fig:iter2T5Detail}, \ref{fig:iter20T3Detail} et \ref{fig:iter40T6Detail}, on peut conclure que ce problème est croissante avec le nombre de pas du mouvement, car les adaptations successives sont faites presque sur le même endroit. Pour cette même raison, la "mauvaise anisotropie" aux voisinages du cercle est aussi plus forte.

\clearpage
\indent \textbf{Cinquième et sixième tests} : Naca oscillant, avec 10 et 40 pas de mouvement

\figCircle{101}{0}{5}{10}{10}{.21}{.22}{Naca}{10}

\figJacobi{101}{0}{50}{100}{200}{.22}{Naca}{5}

\figCircle{102}{0}{20}{40}{40}{.2}{.22}{Naca}{40}

\figJacobi{102}{0}{50}{100}{200}{.22}{Naca}{20}

\indent Les résultats sont similaires aux obtenus dans les tests avec le cercle et, de façon générale, on a des bonnes adaptations. Observons aussi qu'on a toujours une asymétrie du raffinement, dépendant de la direction du mouvement de l'objet, et que ce problème devient plus forte avec des petits pas du mouvement.


\clearpage
\subsection{Maillage initial avec une zone plus raffinée}

\indent On va maintenant faire l'adaptation en utilisant des maillages qui sont, au début, plus raffinées dans les voisinages de l'objet. Ce type de maillage est intéressant car il permet de discrétiser un grand domaine avec un nombre réduit de points, puisque en général, dans les problèmes physiques, on n'a pas besoin d'un maillage très raffinés dans les régions les plus éloignées de l'objet.

\indent On a fait les tests dans le cas du cercle, et le maillage initial est représentée dans la figure \ref{fig:init2}. Idéalement, on veut que l'adaptation soit capable de faire la boîte raffinée accompagner le mouvement du cercle. Les résultats sont présentés dans la figure \ref{fig:CercleT4}.

\begin{figure}[!ht]
	\centering
	\includegraphics[scale=.25]{figures/init2.png}
	\caption{Fonction Level Set représentée sur un maillage initial avec une zone plus raffinée \label{fig:init2}}
\end{figure}

\figCircle{4}{0}{10}{20}{20}{.15}{.2}{Cercle}{20}

\figJacobi{4}{0}{50}{100}{200}{.15}{Cercle}{10}

\indent On peut observer que l'adaptation n'est pas capable de faire la zone raffinée accompagner suffisamment le cercle. En effet, à chaque adaptation, les éléments de cette zone bougent vers la moitié gauche du cercle (qui est plus proche), mais la moitié droite arrive rapidement aux endroits les moins raffinées du maillage, où le nombre de points est trop faible pour permettre une bonne adaptation.

\indent Afin de vérifier si les résultats sont meilleurs avec un mouvement plus lent du cercle, on a fait le même test avec 100 pas de discrétisation de l'advection, mais les résultats sont similaires (figure \ref{fig:CercleT7})

\figCircle{7}{0}{50}{100}{100}{.15}{.2}{Cercle}{100}