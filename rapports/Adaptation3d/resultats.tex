\section{Premiers résultats}

\indent On présente ci-dessous les premiers résultats obtenues avec l'algorithme d'adaptation 3D. D'abord, on a utilisé un maillage utilisé (figure \ref{fig:dom}) a environ 15000 noeuds et 75000 éléments, et après on est partis d'un maillage plus raffiné dans la région contenant l'objet (maillage obtenue avec MMG). Les tests réalisés sont des adaptations à des fonctions Level Set (figure \ref{ref:LS}). On a  implémenté le calcul de \(\omega\) soit à partir du gradient de la fonction (avec \(\alpha = 1000\)), soit en imposant une taille désirée dans chaque point du maillage (tableau \ref{tab:tailles}).

			\begin{table}[!ht]
				\centering
				\begin{tabular}{c|c|c}
				i & \(R_i\) & \(h_i\) \\ \hline
				1 & 0.025 & 0.001\\
				2 & 0.05 & 0.002\\
				3 & 0.075 & 0.005 \\
				4 & 0.1 & 0.02\\
				5 & 0.125 & 0.04\\
				6 & 0.15 & 0.08\\
				7 & \(\infty\) & 0.1
				\end{tabular}
				\caption{Définition des tailles désirées \label{tab:tailles}}
			\end{table}

\indent Quelques remarques, conclusions et difficultés concernant les résultats): 

\begin{itemize}
	\item Même avec un très grand nombre de éléments, le maillage n'est pas suffisamment fine pour une bonne adaptation : l'interpolation est grossière et on sent l'influence des bords dès les premières itérations (figures \ref{fig:sphereAdapt} à \ref{fig:holeAdapt}).
	\item Si on essaie de raffiner tout le maillage de départ, le temps d'exécution devient très grand et on a fréquemment des problèmes concernant la relaxation.
	\item L'utilisation d'un maillage plus raffiné seulement dans la région où se trouve l'objet permet d'obtenir des meilleurs résultats par rapport aux problèmes ci-dessus : la surface des objets est mieux représentée, l'influence des bords est moins forte et la variation de la taille des éléments est plus lisse (figures \ref{fig:sphereAdaptMMG} à \ref{fig:holeAdaptMMG}).   
	\item Comme le calcul de \(\omega\) à partir de l'imposition des tailles demande que la fonction Level Set soit recalculée à chaque itération, le temps d'exécution devient encore plus grande.
\end{itemize}

\subsection{Adaptation à partir d'un maillage homogène}

\begin{figure}[!ht]
	\centering
	\includegraphics[scale=.2]{figures/dom.png}
	\caption{Maillage initial \label{fig:dom}}
\end{figure}

\begin{figure}[!ht]
	\begin{subfigure}{.3\linewidth}
		\centering
		\includegraphics[scale=.2]{figures/sphereLS.png}
		\caption{Sphere \label{fig:sphereLS}}
	\end{subfigure}
	\begin{subfigure}{.3\linewidth}
		\centering
		\includegraphics[scale=.2]{figures/torusLS.png}
		\caption{Tore \label{fig:torusLS}}
	\end{subfigure}
	\begin{subfigure}{.3\linewidth}
		\centering
		\includegraphics[scale=.2]{figures/holeLS.png}
		\caption{Boîte avec un trou \label{fig:holeLS}}
	\end{subfigure}
	\caption{Fonctions Level Set utilisées \label{ref:LS}}
\end{figure}

\begin{figure}[!ht]
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/3dSphere30.png}
		\caption{Après 30 itérations; \(\omega\) calculée à partir du gradient \label{fig:sphereA}}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/3dSphereSize30.png}
		\caption{Après 30 itérations; \(\omega\) calculée à partir des tailles \label{fig:sphereB}}
	\end{subfigure}
	\caption{Adaptation à la sphère \label{fig:sphereAdapt}}
\end{figure}

\begin{figure}[!ht]
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/3dTorus30a.png}
		\caption{Vue supérieure \label{fig:torusA}}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/3dTorus30b.png}
		\caption{Vue frontale \label{fig:torusB}}
	\end{subfigure}
	\caption{Adaptation au tore après 30 itérations; \(\omega\) calculée à partir des tailles \label{fig:torusAdapt}}
\end{figure}

\begin{figure}[!ht]
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/3dHole30b.png}
		\caption{Vue supérieure \label{fig:holeA}}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/3dHole30a.png}
		\caption{Vue frontale \label{fig:holeB}}
	\end{subfigure}
	\caption{Adaptation à la boîte avec un trou après 30 itérations; \(\omega\) calculée à partir des tailles \label{fig:holeAdapt}}
\end{figure}

\clearpage
\subsection{Adaptation à partir d'un maillage raffiné autour de l'objet}

\indent Les maillages ont été obtenues avec le logiciel MMG, en optimisant le maillage de la figure \ref{fig:dom} avec une taille minimale \(hmin = 0.05\) et une gradation \(hgrad = 1.3\). Les tests ont été exécutés avec \(\alpha = 10000\), étant arrêtés par relaxation complète autour de la dixième itération.

\begin{figure}[!ht]
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/domSphere.png}
		\caption{Maillage initial (environ 15000 noeuds et 100000 éléments) \label{fig:domSphere}}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/3dMMGSphere.png}
		\caption{Après 7 itérations \label{fig:sphereMMG}}
	\end{subfigure}
	\caption{Adaptation à la sphère à partir du maillage optimisé \label{fig:sphereAdaptMMG}}
\end{figure}

\begin{figure}[!ht]
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/domTorus.png}
		\caption{Maillage initial (environ 10000 noeuds et 750000 éléments) \label{fig:domTorus}}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/3dMMGTorus.png}
		\caption{Après 10 itérations \label{fig:torusMMG}}
	\end{subfigure}
	\caption{Adaptation au tore à partir du maillage optimisé \label{fig:torusAdaptMMG}}
\end{figure}

\begin{figure}[!ht]
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/domHole.png}
		\caption{Maillage initial (environ 30000 noeuds et 1750000 éléments) \label{fig:domHole}}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[scale=.3]{figures/3dMMGHole.png}
		\caption{Après 10 itérations \label{fig:holeMMG}}
	\end{subfigure}
	\caption{Adaptation à la boîte avec un trou à partir du maillage optimisé \label{fig:holeAdaptMMG}}
\end{figure}