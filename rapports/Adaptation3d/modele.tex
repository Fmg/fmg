\section{Le modèle : formulation du problème et discrétisation en éléments finis}

\indent Le modèle d'adaptation 3D sera une extension de celui qu'on a utilisé dans le cas 2D (cf. "Récapitulatif - Adaptation à des fonctions Level Set" pour les détails). Dans ce modèle, le mouvement des points du domaine physique \((x,y)\) est décrite, par rapport au domaine de référence, par l'équation

\begin{equation}
  \label{eq:laplacien}
  \nabref \cdot \left( \omega \nabref \vecx \right) = 0
\end{equation}

\indent En tenant compte des conditions aux bords pour que le problème soit bien posé, on veut résoudre

\begin{equation}
	\label{eq:systeme}
	\begin{cases}
  		\nabref \cdot \left( \omega \nabref \vecx \right) = 0 \ \ dans \ \ \domRef \\
  		\vecx = \vecg \ \ sur \ \ \bordRef^D \\
  		\nabref \vecx \cdot \vecn = 0 \ \ sur \ \ \bordRef^N 
	\end{cases}
\end{equation}

\noindent dont la formulation faible, après l'application des conditions aux bords, s'écrit

\begin{equation}
	\iDom{\omega \nabref v \cdot \nabref \vecx} = 0
\end{equation}

\noindent avec une fonction test  \(v \in H_1^0(\domRef)\)

\subsection{La fonction \(\boldsymbol{\omega}\)}

\indent En suivant les formulations utilisées dans le développement du modèle d'adaptation 2D, on implémentera deux options pour le calcul de la fonction \(\omega\), responsable pour définir le comportement de l'adaptation : la première (\eqref{eq:omega1}), utilisée par exemple par \citep{arpaia} et \citep{arpaia}, fait les points bouger en fonction de la variation de la fonction à adapter, contrôlés par les paramètres \(\alpha\) et \(\beta\) choisis par l’utilisateur; la deuxième (\eqref{eq:omega2}), présentée par \citep{askes}, impose directement une taille désirée pour chaque élément.

\begin{equation}
  \label{eq:omega1}
  \omega(\vecx) = \sqrt{1 + \alpha ||\nabref u(\vecx)|| + \beta ||H(u)(\vecx)||}
\end{equation}

\begin{equation}
 	\label{eq:omega2}
	\omega(\vecx) = \frac{1}{\hd(\vecx)}
\end{equation}

\subsection{Discrétisation en éléments finis P1}

\indent La discrétisation en éléments finis P1 se déroule de façon complètement analogue au cas 2D. Notamment, on conclue que le système \ref{eq:systeme} se ramène à la résolution de trois systèmes linéaires indépendants et de la même forme, un pour chaque coordonnée spatiale :

\begin{equation}
	\label{eq:syst_final}
	\begin{cases}
		Kx = 0 \\
		Ky = 0 \\
		Kz = 0
	\end{cases}
\end{equation}

\noindent où les éléments de la matrice \(K\) ont la forme

\begin{equation}
  k_{ij} = \iDomh{ \omega \nabref \phii \cdot \nabref \phij }
\end{equation}

\indent La résolution du système linéaire est aussi analogue au cas 2D, étant l'actualisation de la position des points donnée par

\begin{equation}
	\begin{gathered}
		\delta_i^{[n+1]} =  \delta_i^{[n]} -\frac{1}{k_{ii}} \sum_{j=1}^N k_{ij}(x_j^{[n]}) \\
		\vecx^{[n+1]} = \vecx^{[n]} + \theta \left( \vecxi + \vecdelta^{[n+1]} - \vecx^{[n]} \right)
	\end{gathered}
\end{equation}

\noindent où \(\theta \in [0,1]\) est un coefficient de relaxation utilisée afin d'éviter des volumes négatifs.

\subsection{Quelques éléments pour le calcul de K}

\indent En tenant compte, évidemment, du fait qu'on travaille maintenant en trois dimensions et que les éléments sont des tétraèdres, les détails du calcul discret des éléments de la matrice K restent les mêmes. On remarque notamment, selon la formulation plus général présentée dans \citep{vecNormal}, que le gradient de \(\phii\) sur l'élément \(T\) sera donnée par \((\nabref \phii)^T = \frac{\normT{i}}{d!|T|}\), où \(d\) est le nombre de dimensions spatiales, \(|T|\) est le volume de \(T\) et \(\normT{i}\) est le vecteur entrant à \(T\), dans le côté opposé à \(i\), qu'on calculera en faisant le produit vectoriel de deux vecteurs de ce côté. Ainsi, le calcul de \(k_{ij}\) devient

\begin{equation}
\begin{gathered}
\begin{aligned}
	k_{ij} & = \iDomh{ \omega \nabref \phii  \cdot \nabref \phij } = \sum_{T \ni i} {\iT{ \omega \nabref \phii \cdot \nabref \phij }} = \\
	       &  = \sum_{T \ni i}
	              { 
	                     { |T|\omega^T \frac{\normT{i}\cdot \normT{j}}{(3!|T|)^2}
	                     }
	              }
	          = \sum_{T \ni i}
	              { 
	                     { \omega^T \frac{\normT{i}\cdot \normT{j}}{36|T|}
	                     }
	              }	              
\end{aligned}
\end{gathered}
\end{equation}

\noindent où \(\omega^T\) est la moyenne de \(\omega\) sur l'élément \(T\).

 