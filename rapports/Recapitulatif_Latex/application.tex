\section{Application à la fonction Level Set}


\label{sec:application}

\indent Le modèle décrit dans la section précédente sera utilisé pour adapter le maillage par rapport à une fonction Level Set (LS). Cette fonction définit une distance signée entre chaque point du maillage et la surface qu'on représente: si le point \(\vecx\) est à l'extérieur de la surface, \(LS(\vecx) > 0\); s'il est à l'intérieur, \(LS(\vecx) < 0\); et si il est sur l'interface, \(LS(\vecx) = 0\).  On cherche alors à bouger les points pour avoir une meilleure représentation de la ligne de niveau 0 de la fonction Level Set, i.e., de la surface de l'objet \citep{ducrot}. Pour illustrer notre objectif, on présente dans la figure \ref{fig:exLS} un exemple d'adaptation à une fonction Level Set :

\begin{figure}[!ht]
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[clip=true, trim = 10cm 0 10cm 0, scale=.25]{figures/LSinit.png}
		\caption{\centering Fonction Level Set représentée sur le maillage initial}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[clip=true, trim = 10cm 0 10cm 0, scale=.25]{figures/LSadapt.png}
		\caption{\centering Maillage adapté à la fonction Level Set}
	\end{subfigure}
	\caption{Exemple d'adaptation à la fonction Level Set \label{fig:exLS}}
\end{figure}

\subsection{Construction de la fonction à adapter}

\indent L'adaptation, pourtant, ne sera pas faite en considérant \(u=LS\), parce qu'on n'aurait pas de forts gradients, en tenant compte de la lisseté de cette fonction. Ainsi, \(\omega\) ne serait pas capable d'exprimer le raffinement désiré dans chaque partie du maillage. De cette façon, à partir de la fonction Level Set, on va construire une nouvelle fonction qui ait un très fort gradient sur les bords de l'objet.

\indent Idéalement, on construirait une fonction avec un saut, valant \(1\) à l'extérieur et \(-1\) à l'extérieur de l'objet, ce que fournit des très bons raffinements sur la ligne de niveau 0 de la fonction Level Set. Néanmoins, dans la résolution numérique de problèmes de la mécanique des fluides, il est intéressant d'avoir aussi un bon raffinement du maillage sur une couche autour de la surface (la couche limite de l'écoulement), où l'interaction fluide-objet n'est pas négligeable. En effet, pour avoir un calcul précis, un maillage approprié sur la couche limite est requis par la plupart des schémas numériques et logiciels pour les problèmes de fluides \citep{loseille}. Ainsi, on a cherché une fonction plus lisse.

\indent Dans un premier moment, on a défini une fonction avec une variation linéaire entre les deux étages (figure \ref{fig:atan}). Néanmoins, les tests réalisées ont fourni des résultats de mauvaise qualité : étant le gradient constant à l'intérieur de la pente, les points dans cette région (y compris les points les proches du bord de l'objet) n'avaient pas la tendance de bouger, au contraire des points proches des bordes de la pente, dû à la discontinuité du gradient de \(u\). Par conséquent, l'adaptation produisait une maillage avec deux petites couches raffinées, qui ne représentaient pas la surface de l'objet (figure \ref{fig:LSlin}).
\begin{figure}[!ht]
  \centering
  \includegraphics[clip=true, trim = 10cm 0 10cm 0, scale=.25]{figures/LSlin.png}
  \caption{Adaptation à une fonction Level Set modifié avec une variation linéaire \label{fig:LSlin}}
\end{figure}

\indent On a ainsi cherché une fonction dont le gradient est plus variable et continue afin de réduire cet effet. On a obtenu des bons résultats en utilisant

\begin{equation}
	\label{eq:atan}
	u(\vecx) = \frac{1}{1.1.107} atan \left(\frac{2LS(\vecx)}{\delta} \right)
\end{equation}

\noindent étant \(2\delta\) la taille de la couche, choisie par l'utilisateur. Cette fonction est représentée dans la figure \ref{fig:atan}.

\begin{figure}[!ht]
  \centering
  \includegraphics[scale=.4]{figures/atan.png}
  \caption{Comparaison des options pour la lissage de la fonction à adapter   \label{fig:atan}}
\end{figure}

\indent On peut jouer aussi avec le paramètre multiplicateur de \(LS(\vecx)/\delta\) à l'intérieur de la fonction tangent. Avec 1, par exemple, on obtient un résultat plus proche de la pente linéaire; avec 5 ou 10, la fonction devient plus discontinue et la couche de raffinement est plus petite. On restera ainsi avec l'option donnée par \eqref{eq:atan}.


\subsection{Choix des paramètres}

\indent Afin de chercher les paramètres qui génèrent le meilleur maillage, plusieurs tests on été réalisés : 

\begin{itemize}
	\item Cas test (objet cible): 
	\begin{itemize}
		\item Cercle;
		\item Triangle;
		\item Profil d'une aile (NACA)
	\end{itemize}
	\item Type de maillage
	\begin{itemize}
		\item Structuré;
		\item Non structuré
	\end{itemize}
\end{itemize}

\indent Les meilleurs maillages obtenus on été utilisés pour résoudre un problème d'écoulement autour de l'objet, afin de vérifier l'influence sur les résultats. Les conclusions des tests sont les suivants : 

\begin{itemize}
  \item Pour le calcul de \(\omega\), il faut plutôt utiliser le gradient de \(u\). Le hessien peut être aussi utilisé, mais il a la tendance de créer deux couches raffinées dans les bords de la région de variation de la fonction \(u\) (figure \ref{fig:alphabeta});
  \item Le raffinement d'une certaine région du maillage cause un étirement dans la direction normale à surface des éléments voisins à cette région. Ce résultat n'est pas désirable car on veut plutôt une anisotropie dans la direction de l'écoulement du fluide (figure \ref{fig:anisoNormal}) : l'erreur d'interpolation est plus petit si on a des triangles anisotropes dont le côté le plus long est dans la direction des petites dérivées de deuxième ordre de la solution \citep{rippa}. Ainsi, il est important de avoir une valeur de \(\delta\) assez grande.
  \item Néanmoins, \(\delta\) ne peut être trop grand, car la fonction arctangente s'approche de la pente linéaire, et on n'obtient pas un bon raffinement à l'intérieur de la couche.
\end{itemize}


\begin{figure}[!ht]
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[clip=true, trim = 10cm 0 10cm 0, scale=.25]{figures/alphabeta0.png}
		\caption{\(\alpha = 1000, \beta = 0\)}
	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
		\centering
		\includegraphics[clip=true, trim = 10cm 0 10cm 0, scale=.25]{figures/alphabeta1.png}
		\caption{\(\alpha = 0, \beta = 1000\)}
	\end{subfigure}
	\caption{Influence des paramètres \(\alpha\) et \(\beta\) sur l'adaptation du maillage (les deux cas ave \(\delta = 0.02\)) \label{fig:alphabeta}}
\end{figure}

\begin{figure}[!ht]
	\centering
	\includegraphics[scale=.25]{figures/anisoNormal.png}
	\caption{Détail d'un maillage obtenu avec \(\delta = 0.005\) \label{fig:anisoNormal}}
\end{figure}

\indent En tenant compte de ces conclusions, on recommande l'utilisation des paramètres suivants (on rappelle qu'on utilise toujours les normes du gradient et du hessien normalisés entre 0 et 1) : 

\begin{equation}
	\begin{gathered}
	\boldsymbol{\alpha} = 1000 \\
	\boldsymbol{\beta} = 0 \\
	\boldsymbol{\delta} = 0.01,\  0.02
	\end{gathered}
\end{equation}

\indent On peut aussi faire des adaptations successives, en faisant varier les paramètres, afin d'obtenir d'autres résultats. Par exemple, on peut faire une première adaptation avec \(\delta = 0.02\) pour obtenir une bonne couche raffinée, et après une deuxième avec \(\delta = 0.01\) pour obtenir un raffinement plus précis autour de la surface. 

\indent La figure \ref{fig:example} présente quelques exemples d'adaptation à une fonction Level Set, avec les paramètres présentés ci-dessus, sur un maillage non structuré avec environ 28000 noeuds : 

\begin{figure}[h]
	\begin{subfigure}{.5\linewidth}
    		\centering
		\includegraphics[scale=.15]{{figures/example1}.png}
		\caption{\(\delta = 0.01\)}
  	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
    		\centering
		\includegraphics[scale=.15]{{figures/example2}.png}
		\caption{\(\delta = 0.02\)}
  	\end{subfigure}
  	\caption{Exemples d'adaptation à une fonction Level Set \label{fig:example}}
\end{figure}


\subsection{Relaxation et qualité du maillage}

\indent Pendant la résolution du système linéaire, on fait, si nécessaire, une relaxation des déplacements calculés afin d'éviter le croisement des noeuds et la conséquente occurrence d'aires négatives. Néanmoins, en dépendant du maillage de départ (par exemple, un maillage déjà raffiné), la relaxation est trop contraignant et impose l'arrêt de l'algorithme lors des premières itérations. Parfois cela est provoqué par un petit nombre d'éléments.

\indent On a ainsi implémenté, dans les premières itérations, un prétraitement d'optimisation, selon le schéma suivant :

\begin{itemize}
  \item Si le numéro de l'itération actuelle n'est pas supérieur à \(iter_{optim}\) : 
  \begin{enumerate}
    \item Identifier tous les éléments qui demanderont de la relaxation;
    \item Classifier ces éléments selon un critère de qualité (mesure de l'anisotropie);
    \item Optimiser les \(n_{optim}\) éléments les plus critiques de cette liste
  \end{enumerate}   
\end{itemize}

\indent L'optimisation réalisée consiste en, itérativement, approcher chaque noeud \(i\) de l'élément concerné du barycentre des noeuds voisins de \(i\). L'optimisation est arrêtée dès qu'aucun des trois noeuds de l'élément bouge au dessus d'un seuil, ou qu'au moins un des noeuds n'arrive pas à réduire l'anisotropie au dessous d'une certaine pourcentage, par rapport au début de l'itération.

\indent La figure \ref{fig:optim} montre le résultat de son application sur un élément : 

\begin{figure}[!ht]
	\begin{subfigure}{.5\linewidth}
    		\centering
		\includegraphics[scale=.25]{{figures/optim_avant}.png}
		\caption{Avant l'optimisation}
  	\end{subfigure}
	\begin{subfigure}{.5\linewidth}
    		\centering
		\includegraphics[scale=.25]{{figures/optim_apres}.png}
		\caption{Après l'optimisation}
  	\end{subfigure}
  	\caption{Résultat de l'optimisation d'un élément \label{fig:optim}}
\end{figure}

\indent Pour que le prétraitement ne soit pas très coûteux, on a choisi \(n_{optim} = 10\) e \(iter_{optim} = 2\). Les tests réalisés montrent des très bons résultats quand l'algorithme s'arrête au début de l'exécution, et avec l'optimisation on arrive à tourner tous les itérations. Néanmoins, si appliqué sur un maillage de bonne qualité et qui ne demande pas une forte relaxation, les gains sont en général petits (ou même on perd un peu de la qualité du résultat final).