\documentclass[11pt]{beamer}
\usetheme{Boadilla}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{xcolor}
\usepackage{mvmsh}


\author[J. G. CALDAS STEINSTRAESSER]{Joao Guilherme CALDAS STEINSTRAESSER}
\title{Physical and Level Set Adaptation}
%\setbeamercovered{transparent} 
%\setbeamertemplate{navigation symbols}{} 
%\logo{} 
%\institute{} 
%\date{} 
%\subject{} 

\newcommand{\hd}{h_{des}}
\newcommand{\tM}{\mathcal{M}}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

%\begin{frame}
%\tableofcontents
%\end{frame}

\begin{frame}{Objectives}
	\begin{itemize}
		\item Modify the position of the nodes of a mesh in order to
		\begin{itemize}
			\item Capture a physical phenomena
			\item Represent a surface
		\end{itemize}
		\item Keeping : 
		\begin{itemize}
			\item The same number of points
			\item The same mesh connectivity
		\end{itemize}
		\item More precise results without increasing the memory and calculation costs
	\end{itemize}

\end{frame}



\begin{frame}{The model for mesh adaptation}

	\begin{itemize}
		\item Two different domains :
		\begin{itemize}
			  \item \textbf{Physical or real domain } (\(\dom\), \(\vecx =(x,y)\)) : deformable;
  			  \item \textbf{Referential or computational domain } (\(\domRef\), \(\vecxi =(\xi,\eta)\)) : fixed
		\end{itemize}
		
		\begin{figure}
				\begin{subfigure}{.4\linewidth}
					\centering
					\includegraphics[scale=.1]{figures/naca0.png}
					\caption{Referential domain}
				\end{subfigure}	
				\begin{subfigure}{.4\linewidth}
					\centering
					\includegraphics[scale=.1]{figures/naca1.png}
					\caption{Physical domain}
				\end{subfigure}
		\end{figure}
		
		\item We seek a function \(  \vecx = \vecx(\vecxi) \)
		\item The movement of the nodes is described by the problem
		\begin{equation}
			\label{eq:systeme}
			\begin{cases}
  			\nabref \cdot \left( \begingroup \color{red}{\omega} \endgroup \nabref \vecx \right) = 0 \ \ dans \ \ \domRef \\
  			\vecx = \vecg \ \ sur \ \ \bordRef^D \\
  			\nabref \vecx \cdot \vecn = 0 \ \ sur \ \ \bordRef^N 
			\end{cases}
		\end{equation}
	\end{itemize}

\end{frame}



\begin{frame}{Discretization and resolution of the problem}
	\begin{itemize}
		\item Discretization with P1 finite elements
		\item The weak formulation leads to the resolution of independent linear systems for each spatial coordinate :
		\begin{equation}
			\label{eq:syst_final}
			\begin{cases}
			Kx = 0 \\
			Ky = 0
			\end{cases}
		\end{equation}
		\indent where the elements of the matrix \(K\) are
		\begin{equation}
			k_{ij} = \iDomh{ \omega \nabref \phii \cdot \nabref \phij }		
		\end{equation}
		\item Integrals are evaluated in  \(\domRef\), but \(\omega\) is updated in \(\dom\)
		\item Iterative resolution of the system (Jacobi method)  
	\end{itemize}
\end{frame}


\begin{frame}{The function \(\omega\)}
	\begin{itemize}
		\item Function of \(\vecx\) containing all the informations that determine the movement of the nodes;
		\item In  general : higher values of \(\omega\) in the regions where we want higher refinement;
		\item Computation of \(\omega\) : 
		\begin{itemize}
			\item As a function of the variation of the solution to adapt : 
			\begin{equation}
  			\label{eq:omega1}
			  \omega(\vecx) = \sqrt{1 + \alpha ||\nabref u(\vecx)|| + \beta ||H(u)(\vecx)||}
			\end{equation}
			\begingroup
			\color{blue}
			\item Giving to each node a desired mesh size : 
			\begin{equation}
  				\label{eq:omega2}
				\omega(\vecx) = \frac{1}{\hd(\vecx)}
			\end{equation}
			\endgroup		 
		\end{itemize}		 
	\end{itemize}
\end{frame}


\begin{frame}[allowframebreaks]{Computation of the desired mesh sizes}
	\begin{itemize}
		\item Computation of a metric \(\tM_i\) for each point \(i\) :
		\begin{itemize}
			\item a definite positive symmetric matrix constructed in order to minimize an estimator of the interpolation error on the mesh
			\item Computed in function of the Hessian of the solution to adapt
		\end{itemize}
		\item The metric is anisotropic but we consider an isotropic case
		\framebreak
		
		\item Desired mesh size : 
		\begin{equation}
			h(\vecx_i) = \frac{1}{\sqrt{\lambda_i}}
		\end{equation}
		\indent \(\lambda_i\) : highest eigenvalue of \(\tM_i\).
		\item Metric built from a Level Set function and/or a physical solution of the problem.
		

		\begin{figure}
				\begin{subfigure}{.48\linewidth}
					\centering
					\includegraphics[clip=true, trim = 2cm 0 2cm 0, scale=.14]{figures/triLS.png}
					\caption{Level Set function}
				\end{subfigure}	
				\begin{subfigure}{.48\linewidth}
					\centering
					\includegraphics[clip=true, trim = 2cm 0 2cm 0, scale=.14]{figures/triMetrique.png}
					\caption{Associated metric}
				\end{subfigure}
		\end{figure}
		
	\end{itemize}
\end{frame}


\begin{frame} {Exemple}
	\begin{itemize}
		\item Problem : Supersonic flow around a triangle (2D)
		\item Resolution with a residual distribution method applied to the penalized Navier-Stokes equations
		\item The triangle is not discretized in the domain : it is necessary to adapt the mesh
	\end{itemize}
\end{frame}
\begin{frame}[t]{Procedure of Physical and Level Set adaptation}
	\begin{itemize}
	
	\only<1>{\item Initial mesh = referential mesh}
	\only<2>{\item Computation of the metric associated to the Level Set function}
	\only<3>	{\item Adaptation of the mesh to this metric}
	\only<4>{\item \label{item:Res} Resolution of the physical problem on the adapted mesh}
	\only<5>	{\item Computation of the intersection between the metrics of the LS function and the physical solution (e.g. the horizontal component of the velocity)}
	\only<6-7>	{\item Adaptation to this new metric (using the previous mesh as initial mesh, but always the same referential mesh)}
	\only<8>{\item Resolution of the physical problem ...}


	\begin{figure}
		\centering
		
		\includegraphics<1>[scale=.15]{{figures/TestCercle/Test1d/domInit}.png}
		\only<1>{\caption {Referential mesh}}
		
		\begin{subfigure}{.4\linewidth}
					\centering
					\includegraphics<2>[clip=true, trim = 2cm 0 2cm 0, scale=.11]{figures/TestCercle/Test1d/triLS.png}
					\only<2>{\caption{Level Set function}}
		\end{subfigure}	
		\begin{subfigure}{.4\linewidth}
					\centering
					\includegraphics<2> [clip=true, trim = 2cm 0 2cm 0, scale=.11]{figures/TestCercle/Test1d/triMetrique.png}
					\only<2>{\caption{Associated metric}}
		\end{subfigure}
		
		\includegraphics<3>[scale=.2]{{figures/TestCercle/Test1d/dom0}.png}
		\only<3>{\caption {Result of the first adaptation}}

		\includegraphics<4>[scale=.2]{{figures/TestCercle/Test1d/u0}.png}
		\only<4>{\caption {Result of the first adaptation}}

		\includegraphics<5>[scale=.2]{{figures/TestCercle/Test1d/metrique1b}.png}
		\only<5>{\caption {Intersection of the metrics (LS and velocity)}}

		\begin{subfigure}{.4\linewidth}
					\centering
					\includegraphics<6>[scale=.11]{figures/TestCercle/Test1d/domInit.png}
					\only<6>{\caption{Referential mesh}}
		\end{subfigure}	
		\begin{subfigure}{.4\linewidth}
					\centering
					\includegraphics<6> [scale=.11]{figures/TestCercle/Test1d/dom0.png}
					\only<6>{\caption{Initial mesh}}
		\end{subfigure}

		\includegraphics<7>[scale=.2]{{figures/TestCercle/Test1d/dom1}.png}
		\only<7>{\caption {Result of the second adaptation}}
		
	\end{figure}

	\end{itemize}
\end{frame}

\begin{frame}{Results}
	\begin{itemize}
		\only<1>{\item Adapted meshes}
		\only<2>{\item Horizontal component of the velocity}
		\only<3-5>{\item Contour lines of the horizontal component of the velocity}
		
		\begin{figure}
		
			\begin{subfigure}{.4\linewidth}
					\centering
					\includegraphics<1>[scale=.12]{figures/TestCercle/Test1d/dom0.png}
					\only<1>{\caption{First adaptation}}
			\end{subfigure}	
			\begin{subfigure}{.4\linewidth}
					\centering
					\includegraphics<1> [scale=.12]{figures/TestCercle/Test1d/dom1.png}
					\only<1>{\caption{Second adaptation}}
			\end{subfigure}
			\begin{subfigure}{1.\linewidth}
					\centering
					\includegraphics<1> [scale=.12]{figures/TestCercle/Test1d/dom2.png}
					\only<1>{\caption{Third adaptation}}
			\end{subfigure}
			
			
			\begin{subfigure}{.4\linewidth}
					\centering
					\includegraphics<2>[clip = true, trim = 0 3cm 0 3cm, scale=.12]{figures/TestCercle/Test1d/u0.png}
					\only<2>{\caption{First resolution}}
			\end{subfigure}	
			\begin{subfigure}{.4\linewidth}
					\centering
					\includegraphics<2> [clip = true, trim = 0 3cm 0 3cm, scale=.12]{figures/TestCercle/Test1d/u1.png}
					\only<2>{\caption{Second resolution}}
			\end{subfigure}
			\begin{subfigure}{1.\linewidth}
					\centering
					\includegraphics<2> [clip = true, trim = 0 3cm 0 3cm, scale=.12]{figures/TestCercle/Test1d/u2.png}
					\only<2>{\caption{Third resolution}}
			\end{subfigure}	

%					\includegraphics<3>[clip = true, trim = 0 3cm 0 2cm, scale=.2]{figures/TestCercle/Test1d/u0.png}
%					\only<3>{\caption{First resolution}}
%					\includegraphics<4>[clip = true, trim = 0 3cm 0 2cm, scale=.2]{figures/TestCercle/Test1d/u1.png}
%					\only<4>{\caption{First resolution}}
%					\includegraphics<5>[clip = true, trim = 0 3cm 0 2cm, scale=.2]{figures/TestCercle/Test1d/u2.png}
%					\only<5>{\caption{First resolution}}			
			
			
%			\begin{subfigure}{.4\linewidth}
%					\centering
%					\includegraphics<6>[clip = true, trim = 0 3cm 0 1cm, scale=.1]{figures/TestCercle/Test1d/contour0.png}
%					\only<6>{\caption{First resolution}}
%			\end{subfigure}	
%			\begin{subfigure}{.4\linewidth}
%					\centering
%					\includegraphics<6> [clip = true, trim = 0 3cm 0 1cm, scale=.1]{figures/TestCercle/Test1d/contour1.png}
%					\only<6>{\caption{Second resolution}}
%			\end{subfigure}
%			\begin{subfigure}{1.\linewidth}
%					\centering
%					\includegraphics<6> [clip = true, trim = 0 3cm 0 1cm, scale=.1]{figures/TestCercle/Test1d/contour2.png}
%					\only<6>{\caption{Third resolution}}
%			\end{subfigure}	

					\includegraphics<3>[clip = true, trim = 0 3cm 0 1cm, scale=.2]{figures/TestCercle/Test1d/contour0.png}
					\only<3>{\caption{First resolution}}
					\includegraphics<4>[clip = true, trim = 0 3cm 0 1cm, scale=.2]{figures/TestCercle/Test1d/contour1.png}
					\only<4>{\caption{Second resolution}}
					\includegraphics<5>[clip = true, trim = 0 3cm 0 1cm, scale=.2]{figures/TestCercle/Test1d/contour2.png}
					\only<5>{\caption{Third resolution}}					
			
		\end{figure}	
		
			
	\end{itemize}

\end{frame}

\begin{frame}
	\begin{center}
			\large{Thank you for your attention}
	\end{center}
\end{frame}

\end{document}