IF( BUILD_TESTING )
  INCLUDE( CTest )

  SET( CI_DIR  ${CMAKE_BINARY_DIR}/Tests )
  FILE( MAKE_DIRECTORY ${CI_DIR} )
  SET( CI_DIR_RESULTS  ${CI_DIR}/output )
  FILE( MAKE_DIRECTORY ${CI_DIR_RESULTS} )
  SET( CI_DIR_INPUTS  "../../testfmg" CACHE PATH "path to test meshes repository" )

  # remesh different sets of mesh/sol files with different models
  SET( FMG_TESTS
    "sphere_0"
    "sphere_1"
    "square_0"
    )
  SET( FMG_TESTS_DIR
    ${CI_DIR_INPUTS}/sphere
    ${CI_DIR_INPUTS}/sphere
    ${CI_DIR_INPUTS}/square
    )
  SET( FMG_MODEL
    0
    1
    0
    )
  SET( FMG_ALPHA
    100000
    10000
    100
    )
  SET( FMG_GAMMAG
    1
    0.1
    1
    )
  SET( FMG_BETA
    0
    0
    100
    )
  SET( FMG_OPTIONS
    "-3d"
    "-3d"
    "-circ"
    )

  LIST(LENGTH FMG_TESTS nbTests_tmp)
  MATH(EXPR nbTests "${nbTests_tmp} - 1")

  FOREACH ( test_idx RANGE ${nbTests} )
    LIST ( GET FMG_TESTS     ${test_idx} test_name )
    LIST ( GET FMG_TESTS_DIR ${test_idx} test_dir )
    LIST ( GET FMG_MODEL     ${test_idx} test_model )
    LIST ( GET FMG_ALPHA     ${test_idx} test_alpha )
    LIST ( GET FMG_GAMMAG    ${test_idx} test_gammaG )
    LIST ( GET FMG_BETA      ${test_idx} test_beta )
    LIST ( GET FMG_OPTIONS   ${test_idx} test_options )
    ADD_TEST( NAME phys_${test_name}
      COMMAND  $<TARGET_FILE:${PROJECT_NAME}>
      ${test_options} -phys dom -model ${test_model} -alpha ${test_alpha} -beta ${test_beta} -tau 0 -gammaG ${test_gammaG} -it 20 -grad -v 10
      WORKING_DIRECTORY ${test_dir} )
  ENDFOREACH()


  ###############################################################################
  #####
  #####        Tests that needs the FMG LIBRARY
  #####
  ###############################################################################

  IF ( LIBFMG_STATIC )
    SET ( lib_name ${PROJECT_NAME}_a )
  ELSEIF ( LIBFMG_SHARED )
    SET ( lib_name ${PROJECT_NAME}_so )
  ELSE ()
    MESSAGE(WARNING "You must activate the compilation of the static or"
      " shared ${PROJECT_NAME} library to compile this tests." )
  ENDIF ( )


  SET ( FMG_LIB_TESTS
    libfmg_square
    libfmg_square_time
    libfmg_square_metrics
    libfmg_quarter_sphere
    libfmg_step
    libfmg_cube
    )
  SET ( FMG_LIB_TESTS_DIR
    ${CI_DIR_INPUTS}/square
    ${CI_DIR_INPUTS}/square_time
    ${CI_DIR_INPUTS}/square_metrics
    ${CI_DIR_INPUTS}/quarter_sphere
    ${CI_DIR_INPUTS}/step
    ${CI_DIR_INPUTS}/cube
    )
  SET ( FMG_LIB_TESTS_MAIN_PATH
    ${CI_DIR_INPUTS}/square/main.c
    ${CI_DIR_INPUTS}/square_time/main.c
    ${CI_DIR_INPUTS}/square_metrics/main.c
    ${CI_DIR_INPUTS}/quarter_sphere/main.c
    ${CI_DIR_INPUTS}/step/main.c
    ${CI_DIR_INPUTS}/cube/main.c
    )

  LIST(LENGTH FMG_LIB_TESTS nbTests_tmp)
  MATH(EXPR nbTests "${nbTests_tmp} - 1")

  FOREACH ( test_idx RANGE ${nbTests} )
    LIST ( GET FMG_LIB_TESTS            ${test_idx} test_name )
    LIST ( GET FMG_LIB_TESTS_DIR        ${test_idx} test_dir )
    LIST ( GET FMG_LIB_TESTS_MAIN_PATH  ${test_idx} main_path )

    ADD_LIBRARY_TEST ( ${test_name} ${main_path} copy_fmg_headers "${lib_name}" )

    ADD_TEST ( NAME ${test_name}
      COMMAND $<TARGET_FILE:${test_name}>
      WORKING_DIRECTORY ${test_dir} )

  ENDFOREACH ( )


ENDIF()
