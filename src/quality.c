/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"

/**
 * \param mesh : pointer on the mesh structure
 * \param iel : number of the triangle
 * \return the area of the triangle iel
 *
 * Compute the signed area of the triangle iel
 *
 */

double FMG_computeArea(MMG5_pMesh mesh, int iel) {
  double area, x[4], y[4];
  int k;
  MMG5_pTria ptr;  

  ptr = &mesh->tria[iel];
  
  for (k=0;k<3;k++)
  {
    x[k] = mesh->point[ptr->v[k]].c[0];
    y[k] = mesh->point[ptr->v[k]].c[1]; 
  }
  x[3] = x[0];
  y[3] = y[0];

  area = 0;
  for (k=0;k<3;k++)
    area += x[k]*y[k+1] - y[k]*x[k+1];

  area = area/2;
  
  return(area);
}

/**
 * \param mesh : pointer on the mesh structure
 * \param iel : number of the triangle
 * \param xref : xcoordinates of the vertex
 * \param yref : ycoordinates of the vertex
 * \return the area of the triangle iel
 *
 * Compute the signed area of the triangle iel
 *
 */

double FMG_computeAreaRef(MMG5_pMesh mesh, int iel,double *xref,double *yref) {
  double area, x[4], y[4];
  int k;
  MMG5_pTria ptr;  

  ptr = &mesh->tria[iel];
  
  for (k=0;k<3;k++)
  {
    x[k] = xref[ptr->v[k]];
    y[k] = yref[ptr->v[k]]; 
  }
  x[3] = x[0];
  y[3] = y[0];

  area = 0;
  for (k=0;k<3;k++)
    area += x[k]*y[k+1] - y[k]*x[k+1];

  area = area/2;
  
  return(area);
}
/**
 * \param mesh : pointer on the mesh structure
 * \param iel : number of the triangle
 * \return the volume of the tetrahedron iel
 *
 * Compute the signed area of the triangle iel
 *
 */

double FMG_computeVolume(MMG5_pMesh mesh, int iel) {

  double a[3], b[3], c[3];
  int dim;
  MMG5_pTetra ptr;
 
  ptr = &mesh->tetra[iel];

  for (dim=0; dim<3; dim++) {
    a[dim] = mesh->point[ptr->v[1]].c[dim] - mesh->point[ptr->v[0]].c[dim];
    b[dim] = mesh->point[ptr->v[2]].c[dim] - mesh->point[ptr->v[0]].c[dim];
    c[dim] = mesh->point[ptr->v[3]].c[dim] - mesh->point[ptr->v[0]].c[dim];
  }

  return 1./6.*(  a[0]*b[1]*c[2] + a[1]*b[2]*c[0] + a[2]*b[0]*c[1]
                - a[2]*b[1]*c[0] - a[1]*b[0]*c[2] - a[0]*b[2]*c[1] );

}
/**
 * \param mesh : pointer on the mesh structure
 * \param iel : number of the triangle
 * \return the volume of the tetrahedron iel
 *
 * Compute the signed area of the triangle iel
 *
 */

double FMG_computeVolumeRef(MMG5_pMesh mesh, int iel,double* xref,double* yref,double* zref) {

  double a[3], b[3], c[3];
  int dim;
  MMG5_pTetra ptr;
 
  ptr = &mesh->tetra[iel];

  a[0] = xref[ptr->v[1]] - xref[ptr->v[0]];
  b[0] = xref[ptr->v[2]] - xref[ptr->v[0]];
  c[0] = xref[ptr->v[3]] - xref[ptr->v[0]];
  a[1] = yref[ptr->v[1]] - yref[ptr->v[0]];
  b[1] = yref[ptr->v[2]] - yref[ptr->v[0]];
  c[1] = yref[ptr->v[3]] - yref[ptr->v[0]];
  a[2] = zref[ptr->v[1]] - zref[ptr->v[0]];
  b[2] = zref[ptr->v[2]] - zref[ptr->v[0]];
  c[2] = zref[ptr->v[3]] - zref[ptr->v[0]];

  return 1./6.*(  a[0]*b[1]*c[2] + a[1]*b[2]*c[0] + a[2]*b[0]*c[1]
                - a[2]*b[1]*c[0] - a[1]*b[0]*c[2] - a[0]*b[2]*c[1] );

}
