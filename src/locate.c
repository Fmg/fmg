/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"

#define FMG_EPST -1e-9 //-1e-12 don't work

/**
 * \param mesh pointer on the initial mesh
 * \param x x coordinate of the point in the modified mesh
 * \param y y coordinate of the point in the modified mesh
 * \param z z coordinate of the point in the modified mesh
 * \param init index of the starting triangle  
 * \return index of the triangle
 *
 * Find the triangle of the reference mesh that contains the point (x,y) of the new mesh
 */

int FMG_locatePointTria3d(MMG5_pMesh mesh, double x, double y, double z, int init) {

  MMG5_pTetra  ptr,pt1;
  int          iel, jel,ip,idxTr, mvDir[4], iter, flag;
  int          i,*adja;
  double       l1, l2, l3, l4, barycoord[4], eps;

  for (ip=0; ip<4; ip++)
    barycoord[ip] = 0;

  if(!init)
    idxTr = 1;
  else
    idxTr = init;

  iter = 0;
  ++mesh->base;
  while(iter <= mesh->ne) {
    mvDir[0] = mvDir[1] = mvDir[2] = mvDir[3] = 0;
    iter++;
    ptr = &mesh->tetra[idxTr];
    ptr->flag = mesh->base;
//    printf("\n^^^ %d %lf %lf %lf",idxTr,x,y,z);
    adja = &mesh->adja[4*(idxTr-1)+1];
    
    FMG_baryCoord3d(mesh, ptr, x, y, z, barycoord);
    l1 = barycoord[0];
    l2 = barycoord[1];
    l3 = barycoord[2];
    l4 = barycoord[3];
    eps = FMG_EPST*fabs(ptr->qual);
//    printf("\n*** %lf %lf %lf %lf %e %d",l1,l2,l3,l4,eps,mesh->ne);

    if (l1<eps)
      mvDir[0] = 1;
    if (l2<eps)
      mvDir[1] = 1;
    if (l3<eps)
      mvDir[2] = 1;
    if (l4<eps)
      mvDir[3] = 1;
    if(!mvDir[0] && !mvDir[1] && !mvDir[2] && !mvDir[3])
      break;

    /*check if vertex (x,y) is equal to a triangle vertex*/
    if (fabs(1-l1)<eps || fabs(1-l2)<eps || fabs(1-l3)<eps || fabs(1-l4)<eps) {	
      // printf(" ... OK vertex!");
       break;
    }

    idxTr = 0;
    for(ip=0;ip<4; ip++) {
      if (!mvDir[ip]) continue;
      jel = adja[ip]/4;

      if (!jel) continue;
      pt1 = &mesh->tetra[jel];

      if(pt1->flag == mesh->base) continue;
      idxTr = jel;
      // if(ddebug) pt1->ref = 10;
      break;
    }

    if(ip==4) {
      for(ip=0;ip<4; ip++) {
        if (mvDir[ip]) continue;
        jel = adja[ip]/4;
        if (!jel) continue;
        pt1 = &mesh->tetra[jel];
        if(pt1->flag == mesh->base) continue;
        idxTr = jel;
        break;
      }
    }
 
    if (idxTr == 0) {
      //printf("\n!!!!!! ERROR : LOCATE NO ADJ %d tetra tested\n",iter);
      iter = mesh->ne+1;
      //abort();
    }
  }
  if (iter == (mesh->ne+1)) {
    //printf("\n !!!!!!! ERROR : LOCATE POINT TETRA --> EXHAUSTIVE RESEARCH %lf %lf %lf",x,y, z);
    flag = 0;
    for (idxTr=1;idxTr<=mesh->ne;idxTr++) {
      ptr = &mesh->tetra[idxTr];
      FMG_baryCoord3d(mesh, ptr, x, y, z, barycoord);
      l1 = barycoord[0];
      l2 = barycoord[1];
      l3 = barycoord[2];
      l4 = barycoord[3];
      eps = FMG_EPST*fabs(ptr->qual);
      if (l1 > eps && l2 > eps && l3 > eps && l4 > eps) {
        printf(" ... OK! %d",idxTr);
        flag = 1;
        break;
      }
      /*check if vertex (x,y) is equal to a triangle vertex*/
      if (fabs(1-l1)<eps || fabs(1-l2)<eps || fabs(1-l3)<eps || fabs(1-l4)<eps) {
        printf(" ... OK vertex!");
      break;
      }
      if(fabs(l1)<eps) {
        if(l2>eps && l3>eps && l4>eps) {
          printf("face1 ok\n");
          flag=1;
          break;
        }
      }
      if(fabs(l2)<eps) {
        if(l1>eps && l3>eps && l4>eps) {
          printf("face2 ok\n");
          flag=1;
          break;
        }
      }       
      if(fabs(l3)<eps) {
        if(l2>eps && l1>eps && l4>eps) {
          printf("face3 ok\n");
          flag=1;
          break;
        }
      }
      if(fabs(l4)<eps) {
        if(l2>eps && l3>eps && l1>eps) {
          printf("face4 ok\n");
          flag=1;
          break;
        }
      }
      for(i=0 ; i<4 ; i++) {
        ip = ptr->v[i];
        if (fabs(x-mesh->point[ip].c[0])<eps &&
            fabs(y-mesh->point[ip].c[1])<eps &&
            fabs(z-mesh->point[ip].c[2])<eps) {
          //    printf(" ... OK vertex!");
          flag=1;
          break;
        }
      }
      if(i<4) break;
    }
    if (!flag) {
      //printf("\n!!!!! Tetrahedron not found !!!!\n");
      //printf("\n %lf %lf %lf %lf",l1,l2,l3,eps);
      //abort();
      return(0);
    }
  }
  return idxTr; 
}


/**
 * \param mesh pointer on the initial mesh
 * \param x x coordinate of the point in the modified mesh
 * \param y y coordinate of the point in the modified mesh
 * \param init index of the starting triangle  
 * \return index of the triangle
 *
 * Find the triangle of the reference mesh that contains the point (x,y) of the new mesh
 */

int FMG_locatePointTria2d(MMG5_pMesh mesh, double x, double y, int init) {

  MMG5_pTria ptr,pt1;
  int        iel, jel, ip, idxTr,mvDir[3], iter,flag,i,*adja;
  double     l1, l2, l3, barycoord[2], eps;
  int        ddebug; 
 
  //  if(fabs(x-4.996480)<1e-6 && fabs(y+  0.117299)<1e-6) ddebug=1;
  //else
  ddebug=0;

  idxTr = init;
  if(!idxTr) idxTr = 1;

  barycoord[0] = 0;
  barycoord[1] = 0;

  iter = 0;
  ++mesh->base;
  // if(ddebug) printf("here %d\n",mesh->tetra[4955].flag);
  while(iter <= mesh->nt) {
    mvDir[0] = mvDir[1] = mvDir[2] = 0;
    iter++;
    ptr = &mesh->tria[idxTr];
    ptr->flag = mesh->base;
    adja = &mesh->adja[3*(idxTr-1)+1];
    
    FMG_baryCoord2d(mesh, *ptr, x, y, barycoord);
    l1 = barycoord[0];
    l2 = barycoord[1];
    l3 = 1-l1-l2;
    eps = FMG_EPST*fabs(ptr->qual);
    if(ddebug) printf("tr %d bary %e %e %e -- %d %d %d\n",idxTr,l1,l2,l3,
           adja[0]/3,adja[1]/3,adja[2]/3);

    if (l1<eps)
      mvDir[0] = 1;
    if (l2<eps)
      mvDir[1] = 1;
    if (l3<eps)
      mvDir[2] = 1;
    if(!mvDir[0] && !mvDir[1] && !mvDir[2]) {
      break;
    }
    idxTr = 0;
    for(ip=0;ip<3; ip++) {
      if (!mvDir[ip]) continue;
      jel = adja[ip]/3;

      if (!jel) continue;
      pt1 = &mesh->tria[jel];

      if(pt1->flag == mesh->base) continue;
      idxTr = jel;
      // if(ddebug) pt1->ref = 10;
      break;
    }

    if(ip==3) {
      for(ip=0;ip<3; ip++) {
        if (mvDir[ip]) continue;
        jel = adja[ip]/3;
        if (!jel) continue;
        pt1 = &mesh->tria[jel];
        if(pt1->flag == mesh->base) continue;
        idxTr = jel;
        break;
      }
    }
 
    if (idxTr == 0) {
      //printf("\n!!!!!! ERROR : LOCATE NO ADJ %d triangles tested\n",iter);
      iter = mesh->nt+1;
      //abort();
    }
  }
  if(ddebug) printf("exaus ?? %d %d\n",iter,mesh->nt+1);
  if (iter == (mesh->nt+1)) {
    //printf("\n !!!!!!! ERROR : LOCATE POINT TRIANGLE --> EXHAUSTIVE RESEARCH %lf %lf\n",x,y);
    /* if(ddebug)   { */
    /*   MMG2D_saveMesh(mesh,"toto.mesh"); */
    /*   exit(0); */
    /* } */
    flag=0;
    for (idxTr=1;idxTr<=mesh->nt;idxTr++) {
      ptr = &mesh->tria[idxTr];
      FMG_baryCoord2d(mesh, *ptr, x, y, barycoord);
      l1 = barycoord[0];
      l2 = barycoord[1];
      l3 = 1.-l1-l2;
      eps = FMG_EPST*fabs(ptr->qual);
      if (l1 > eps && l2 > eps && l3 > eps) {
//        printf(" ... OK!");
        flag=1;
        break;
       }
      /*check if vertex (x,y) is equal to a triangle vertex*/
      for(i=0 ; i<3 ; i++) {
        ip = ptr->v[i];
        if (fabs(x-mesh->point[ip].c[0])<eps &&
            fabs(y-mesh->point[ip].c[1])<eps) {
          printf(" ... OK vertex!\n");
          flag=1;
          break;
        }
      }
    }
     if (!flag) {
       //printf("\n!!!!! Triangle not found !!!!\n");
//   printf("\n %lf %lf %lf %lf",l1,l2,l3,eps);
       return(0);
     }
  }
  return idxTr; 
}



/**
 * \param mesh pointer on the mesh in the last Jacobi iteration
 * \param tr triangle
 * \param x x coordinate of the point
 * \param y y coordinate of the point
 * \param barycoord barycentric coordinates of point (x,y)
 * 
 * Compute barycentric coordinates of point (x,y) in the triangle tr
 */

void FMG_baryCoord2d(MMG5_pMesh mesh, MMG5_Tria tr, double x, double y, double barycoord[]) {

  int         iloc;
  MMG5_pPoint  pt1, pt2, pt3;
  double      a11,a12,a21,a22;
  double      ax,ay,bx,by,cx,cy;
  double      det;
  double      xx,yy;

  pt1 = &mesh->point[tr.v[0]];
  pt2 = &mesh->point[tr.v[1]];
  pt3 = &mesh->point[tr.v[2]];

  /*calcul des coor bary dans k*/
  det = (pt1->c[0]-pt2->c[0])*(pt1->c[1]-pt3->c[1]) -
    (pt1->c[1]-pt2->c[1])*(pt1->c[0]-pt3->c[0]);
  det = 1./(det);

  a11 = pt2->c[1]-pt3->c[1];
  a12 = -(pt2->c[0]-pt3->c[0]);
  a21 = -(pt1->c[1]-pt3->c[1]);
  a22 = pt1->c[0]-pt3->c[0];
  barycoord[0] = a11*(x-pt3->c[0]) + a12*(y-pt3->c[1]);
  barycoord[1] = a21*(x-pt3->c[0]) + a22*(y-pt3->c[1]);


  ax = pt1->c[0] - x;
  ay = pt1->c[1] - y;
  bx = pt2->c[0] - x;
  by = pt2->c[1] - y;
  cx = pt3->c[0] - x;
  cy = pt3->c[1] - y;

  xx = (pt3->c[1] - pt1->c[1])*(x-pt1->c[0]) -  (pt3->c[0] - pt1->c[0])*(y-pt1->c[1]);
  yy = -(pt2->c[1] - pt1->c[1])*(x-pt1->c[0]) +  (pt2->c[0] - pt1->c[0])*(y-pt1->c[1]);
  xx*=det;
  yy*=det;
  barycoord[0] = 1-(xx+yy);
  barycoord[1] = xx;
}



/**
 * \param mesh pointer on the mesh in the last Jacobi iteration
 * \param tr triangle
 * \param x x coordinate of the point
 * \param y y coordinate of the point
 * \param z z coordinate of the point
 * \param barycoord barycentric coordinates of point (x,y)
 * 
 * Compute barycentric coordinates of point (x,y) in the triangle tr
 */

void FMG_baryCoord3d(MMG5_pMesh mesh, MMG5_pTetra ptr, double x, double y, double z, double barycoord[]) {

  int ip, idxTr, iloc, jloc, isol;
  double xx[5], yy[5], zz[5], dx[5], dy[5], dz[5], phi[5], p[3], u[3], v[3], w[3], eps=1e-6, a, b, c, d, aux, vol;

////  xx[0] = x;
////  yy[0] = y;
////  zz[0] = z;
////
////  for (iloc=0;iloc<4;iloc++) {
////    xx[iloc+1] = mesh->point[ptr->v[iloc]].c[0];
////    yy[iloc+1] = mesh->point[ptr->v[iloc]].c[1];
////    zz[iloc+1] = mesh->point[ptr->v[iloc]].c[2];
////  }
////
////  for (iloc=0;iloc<4;iloc++) {
////    if (iloc>0) { // modifier prmier point
////      aux = xx[iloc+1];
////      xx[iloc+1] = xx[1];
////      xx[1] = aux;
////    }
////    
////    for (jloc=2; jloc<=4; jloc++) {
////      dx[jloc] = xx[jloc] - xx[1];
////      dy[jloc] = yy[jloc] - yy[1];
////      dz[jloc] = zz[jloc] - zz[1];
////    }
////
////    c = (  (dx[4]-dx[2])*(dx[2]*dy[3]-dx[3]*dy[2]) - (dx[3]-dx[2])*(dx[2]*dy[4]-dx[4]*dy[2])  )/
////        (  (dx[2]*dz[4]-dx[4]*dz[2])*(dx[2]*dy[3]-dx[3]*dy[2]) - (dx[2]*dz[3]-dx[3]*dz[2])*(dx[2]*dy[4]-dx[4]*dy[2])  );
////    b = (  dx[3]-dx[2] - c*(dx[2]*dz[3] - dx[3]*dz[2])  ) / (dx[2]*dy[3] - dx[3]*dy[2]);
////    a = (  -1. - b*dy[2] - c*dz[2] )/dx[2];
////    d = 1. - a*xx[1] - b*yy[1] - c*zz[1];
////    barycoord[iloc] = a*xx[0] + b*yy[0] + c*zz[0] + d;    
////
////    if (iloc>0) { // retourner ordre originale des points
////      aux = xx[iloc+1];
////      xx[iloc+1] = xx[1];
////      xx[1] = aux;
////    }
////
////  }

//// CALCUL ALTERNATIF
  // http://www.cdsimpson.net/2014/10/barycentric-coordinates.html
  p[0] = x;
  p[1] = y;
  p[2] = z;
  for (iloc=0; iloc<3; iloc++) {
    u[iloc] = mesh->point[ptr->v[1]].c[iloc] - mesh->point[ptr->v[0]].c[iloc];
    v[iloc] = mesh->point[ptr->v[2]].c[iloc] - mesh->point[ptr->v[0]].c[iloc];
    w[iloc] = mesh->point[ptr->v[3]].c[iloc] - mesh->point[ptr->v[0]].c[iloc];
  } 
  vol = fabs(FMG_tripleProduct(u,v,w));

  for (iloc=0; iloc<3; iloc++) {
    u[iloc] = p[iloc] - mesh->point[ptr->v[1]].c[iloc];
    v[iloc] = mesh->point[ptr->v[3]].c[iloc] - mesh->point[ptr->v[1]].c[iloc];
    w[iloc] = mesh->point[ptr->v[2]].c[iloc] - mesh->point[ptr->v[1]].c[iloc];
  } 
  barycoord[0] = FMG_tripleProduct(u,v,w)/vol;

  for (iloc=0; iloc<3; iloc++) {
    u[iloc] = p[iloc] - mesh->point[ptr->v[0]].c[iloc];
    v[iloc] = mesh->point[ptr->v[2]].c[iloc] - mesh->point[ptr->v[0]].c[iloc];
    w[iloc] = mesh->point[ptr->v[3]].c[iloc] - mesh->point[ptr->v[0]].c[iloc];
  } 
  barycoord[1] = FMG_tripleProduct(u,v,w)/vol;

  for (iloc=0; iloc<3; iloc++) {
    u[iloc] = p[iloc] - mesh->point[ptr->v[0]].c[iloc];
    v[iloc] = mesh->point[ptr->v[3]].c[iloc] - mesh->point[ptr->v[0]].c[iloc];
    w[iloc] = mesh->point[ptr->v[1]].c[iloc] - mesh->point[ptr->v[0]].c[iloc];
  } 
  barycoord[2] = FMG_tripleProduct(u,v,w)/vol;

  for (iloc=0; iloc<3; iloc++) {
    u[iloc] = p[iloc] - mesh->point[ptr->v[0]].c[iloc];
    v[iloc] = mesh->point[ptr->v[1]].c[iloc] - mesh->point[ptr->v[0]].c[iloc];
    w[iloc] = mesh->point[ptr->v[2]].c[iloc] - mesh->point[ptr->v[0]].c[iloc];
  } 
  barycoord[3] = FMG_tripleProduct(u,v,w)/vol;



//  printf("\n *** %e",fabs(barycoord[0] + barycoord[1] + barycoord[2] + barycoord[3] - 1.));
//  assert(fabs(barycoord[0] + barycoord[1] + barycoord[2] + barycoord[3] - 1.) < FMG_eps*1000);

}


double FMG_tripleProduct(double *a, double *b, double *c) {

  return a[0]*b[1]*c[2] + a[1]*b[2]*c[0] + a[2]*b[0]*c[1] -
         a[2]*b[1]*c[0] - a[1]*b[0]*c[2] - a[0]*b[2]*c[1];

}




/**
 * \param mesh pointer on the reference mesh
 * \param ia index of edge's first point
 * \param ib index of edge's second point
 * \param iela index of an element containing point ia
 * \param ielb index of an element containing point ib
 * \param maxNeig maximum number of neighbors of a point in the mesh
 * \return triangles's index 
 *
 * Find the triangle containing the boundary edge (ia-ib)
 */


int FMG_locateEdge(MMG5_pMesh mesh, int ia, int ib, int iela, int ielb, int maxNeig) {

  int canda[maxNeig+1], candb[maxNeig+1];
  int iel, iel_ant, iel_two, iloc, flag;
  MMG5_pTria ptr;

  if (iela == ielb)
    return iela;

  flag = 0;

  for (iel=2; iel<=maxNeig; iel++) {
    canda[iel] = 0;
    candb[iel] = 0;
  }

  canda[0] = 1;
  candb[0] = 1;
  canda[1] = iela;
  candb[1] = ielb;

  iel = iela;
  iel_ant = iela;      
 
  flag = 0;
  iel_two = 0;

  while (!flag) {
    
    ptr = &mesh->tria[iel];
    for (iloc=0; iloc<3; iloc++)
      if (ptr->v[iloc] == ib)
        return iel;

    for (iloc=0; iloc<3 && !flag; iloc++) {
      if (ptr->v[iloc] != ia && mesh->adja[3*(iel-1)+1+iloc]/3 != iel_ant) {
        iel_ant = iel;
        iel = mesh->adja[3*(iel-1)+1+iloc]/3;
        flag = 1;
      }
      if (iel_ant == iela)
        iel_two = iel;
      if (iel == 0) {
        iel = iela;
        iel_ant = iel_two;
      }
      
    }

    flag = 0;
  }
  return(1);
}
