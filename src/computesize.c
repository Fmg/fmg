/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"
#include "math.h"

/**
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param LS Level Set fonction for each point
 * \param ijacobi current Jacobi iteration
 *
 * Reads Level Set fonction from external file 
 */

void FMG_readLS(MMG5_pMesh mesh, double *LS, int ijacobi) {
 
  int ip;
  double trash;
  FILE *LSFile;
  char c1[200], c2[200], c3[200];

  sprintf(c1,"mshdist dom.%d.mesh LS.mesh -r 1e-3 -it 10 >> sortie_mshdist",ijacobi);
////    sprintf(c2,"mshsize dom.%d.sol",ijacobi);
  sprintf(c2,"dom.%d.sol",ijacobi); ////
  sprintf(c3,"cp dom.%d.sol.out dom.tmp",ijacobi);
  system(c1);
////    system(c2);
  FMG_readSolValues(c2,mesh->dim); ////
  system(c3);
  if ((LSFile = fopen("dom.tmp","r")) == NULL) {
    printf ("\n !!! ERROR OPENING FILE OF DESIRED SIZES \n !! Hint : use mshmet and mshsize\n");
    exit(EXIT_FAILURE);
  }
  for (ip=1; ip<=mesh->np;ip++) 
    if (fscanf(LSFile,"%lf %lf",&LS[ip],&trash) != 2) {
      printf("\n!!!!! ERROR READING SIZES FROM METRIC (point %d)!!!",ip);
      exit(EXIT_FAILURE);
  }
  fclose(LSFile);

  sprintf(c1,"rm dom.%d.sol.out dom.tmp",ijacobi);
  system(c1);

   
}

/**
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param sol pointer in the solution structure
 * \param param pointer in the parameter structure
 * \param sizes vector containg the desired sizes for each point
 * \param Hes Hessian of the solution in each point
 * \param LS Level Set fonction for each point
 *
 * Computes the metric and the desired size for each point 
 */

int FMG_computeDesiredSize2d(MMG5_pMesh mesh, MMG5_pSol sol, FMG_pInfo param, double *sizes, double *Hes, double *LS, double edge_size_min, double edge_size_max) {

  MMG5_pSol sol2;
  int ip, iloc;
  double a,b,c,h1,h2,lambda,h, aux, sizemax, sizemin, eps, cd;
  double H[3], *m2;
  double w1, w2, w3, LSeps;
  double IC1, IC2, IC3, IC4; 
  FILE *f;


  if (param->debug) {
    f = fopen("met.sol","w");
    fprintf(f,"MeshVersionFormatted 2\n\nDimension 2\n\nSolAtVertices\n%d\n1 1\n",mesh->np);
  }

  param->hmin = edge_size_max/1000.;
  param->hmax = edge_size_max;
  eps = param->epsMet;
  cd = 2.0/9.0;

  sol2 = NULL;
  MMG5_SAFE_CALLOC(sol2,1,MMG5_Sol,return 0);
  MMG5_SAFE_CALLOC(sol2->m,mesh->np+1,double,return 0);
  if(param->levelSet && param->physAdapt)
    MMG5_SAFE_CALLOC(m2,mesh->np+1,double,return 0);


  if (param->levelSet) {

    w1=0.003;
    w2=0.06;
    w3=0.15;
    IC1=1./1120;
    IC2=1./275;
    IC3=1./160;
    IC4=1./100;
    LSeps=0.0005;

    for (ip=1; ip<=mesh->np; ip++) {
   
      if (fabs(LS[ip]-LSeps) <  w1) 
      {
        sol2->m[ip] = IC1;
      }
      else if (fabs(LS[ip]-LSeps) < w2)
      {
        sol2->m[ip] = IC2;
      }
      else if (fabs(LS[ip]-LSeps) < w3)
      {
        sol2->m[ip] = IC3;
      }
      else
      {
        sol2->m[ip] = IC4;
      }
      




      //  if (fabs(LS[ip]) < .5*param->delta)
      //sol2->m[ip] = param->hmin;
      //else if (LS[ip]< param->delta)
      //  sol2->m[ip] = /*2.**/param->hmin;
      //else
      //  sol2->m[ip] =  MG_MIN(param->hmax
      //      ,param->hmin + 2*fabs(LS[ip])/**fabs(u)*/ * (param->hmax - param->hmin));

    }
  
  }

  sizemax = 0;
  sizemin = 1e9;

  if (param->physAdapt) {    

    for (ip=1; ip<=mesh->np; ip++) {

      for (iloc=0; iloc<3;iloc++)
        H[iloc] = Hes[3*ip+iloc];
    
      a = 1.;
      b = -H[0] - H[2];
      c = H[0]*H[2] - H[1]*H[1];
      h1 = (-b - sqrt(b*b-4.*a*c))/(2.*a);
      h2 = (-b + sqrt(b*b-4.*a*c))/(2.*a);
    
      if (fabs(h1) > fabs(h2))
        h = fabs(h1);
      else
        h = fabs(h2);
   
      if (cd/eps*h > 1./(param->hmax*param->hmax))
        aux = cd/eps*h;
      else
        aux = 1./(param->hmax*param->hmax);
    
      if (aux < 1./(param->hmin*param->hmin)) 
        lambda = aux;
      else
        lambda = 1./(param->hmin*param->hmin);


      if (param->levelSet)     
        m2[ip] = sol2->m[ip];
     //if(lambda>1./(param->hmin*param->hmin))
        //lambda = 1./(param->hmax*param->hmax);

      sol2->m[ip] = 1./sqrt(lambda);

      if (sol2->m[ip] > sizemax)
        sizemax = sol2->m[ip];      
      if (sol2->m[ip] < sizemin)
        sizemin = sol2->m[ip];      

    }

    if (param->levelSet)
      for (ip=1; ip<=mesh->np; ip++)
        if (m2[ip] < sol2->m[ip])
          sol2->m[ip] = m2[ip];
  
  }

 
  //Permet de choisir la transition
  mesh->info.hgrad = 2.5;
  if(!MMG5_gradsiz_iso(mesh,sol2)) printf("\nERROR LISSMET");

 
  for (ip=1; ip<=mesh->np; ip++) {
    sizes[ip] = sol2->m[ip];
    if (param->debug)
      fprintf(f,"%lf\n",sol2->m[ip]);
  }
  
  if (param->debug) {
    fprintf(f,"End");
    fclose(f);
  }
  /* sol2->size=1; */
  /* sol2->np = mesh->np; */
  /* MMG2D_saveMesh(mesh,"init.mesh"); */
  /* MMG2D_saveSol(mesh,sol2,"init.sol"); */
 
  if(param->levelSet && param->physAdapt)
    MMG5_SAFE_FREE(m2);

  MMG5_SAFE_FREE(sol2->m);
  MMG5_SAFE_FREE(sol2);

}





////////////////////////////////////////////////
////////////////////////////////////////////
/////////////////////////////////////
////////////// 3D
/////////////////////////////////////
////////////////////////////////////////////
////////////////////////////////////////////////


int FMG_computeDesiredSize3d(MMG5_pMesh mesh, MMG5_pSol sol, FMG_pInfo param, double *sizes, double *Hes, double *LS, double edge_size_min, double edge_size_max) {

  MMG5_pSol sol2;
  int ip, iloc,nroots;
  double a,b,c,d,A,B,C,h1,h2,lambda,h, aux, sizemax, sizemin, eps, cd,err;
  double H[6], p3[4], p2[3], roots[3], *m2,v[3][3];
  FILE *f;

  if (param->debug) {
    f = fopen("met.sol","w");
    fprintf(f,"MeshVersionFormatted 2\n\nDimension 3\n\nSolAtVertices\n%d\n1 1\n",mesh->np);
  }

  param->hmin = edge_size_max/1000.;
  param->hmax = edge_size_max;
  eps = param->epsMet;
  cd = 9./32.;
  err = cd/eps;

  sol2 = NULL;
  MMG5_SAFE_CALLOC(sol2,1,MMG5_Sol,return 0);
  MMG5_SAFE_CALLOC(sol2->m,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(m2,mesh->np+1,double,return 0);


  if (param->levelSet) {
 
    for (ip=1; ip<=mesh->np; ip++) {
  
      if (fabs(LS[ip]) < .5*param->delta)
        sol2->m[ip] = param->hmin;
      else if (fabs(LS[ip]) < param->delta)
        sol2->m[ip] = 2.*param->hmin;
      else
        sol2->m[ip] = param->hmax;
 //     printf("\n%d %lf %lf",ip,sol2->m[ip],LS[ip]);
    }
 
  }

  sizemax = 0;
  sizemin = 1e9;

  if (param->physAdapt) {    

    for (ip=1; ip<=mesh->np; ip++) {

      for (iloc=0; iloc<6;iloc++)
        H[iloc] = err*Hes[6*ip+iloc];
   

      nroots = MMG5_eigenv(1,H,roots,v);
      //nroots = FMG_newton(p3, p2, roots, 3);
      if(!nroots)
        printf("\n\n***** %d ROOTS : %lf %lf %lf",nroots,roots[0],roots[1],roots[2]);
   
      h = fabs(roots[0]);
      for (iloc=1;iloc<=2;iloc++)
      if (fabs(roots[iloc]) > h)
        h = fabs(roots[iloc]); 
   
      if (h > 1./(param->hmax*param->hmax))
        aux = h;
      else
        aux = 1./(param->hmax*param->hmax);
    
      if (aux < 1./(param->hmin*param->hmin)) 
        lambda = aux;
      else
        lambda = 1./(param->hmin*param->hmin);


      if (param->levelSet)     
        m2[ip] = sol2->m[ip];

      sol2->m[ip] = 1./sqrt(lambda);

      if(lambda!=1./(param->hmin*param->hmin))
        lambda = 1./(param->hmax*param->hmax);

      if (sol2->m[ip] > sizemax)
        sizemax = sol2->m[ip];      
      if (sol2->m[ip] < sizemin)
        sizemin = sol2->m[ip];      

    }

    if (param->levelSet)
      for (ip=1; ip<=mesh->np; ip++)
        if (m2[ip] < sol2->m[ip])
          sol2->m[ip] = m2[ip];
  
  }

 
  mesh->info.hgrad = 2.5;
  if (!MMG3D_gradsiz_iso(mesh,sol2)) printf("\nERROR LISSMET 3D");
  
  
  for (ip=1; ip<=mesh->np; ip++) {
    sizes[ip] = sol2->m[ip];
    /* sizes[3*ip+1] = sol2->m[ip]; */
    /* sizes[3*ip+2] = sol2->m[ip]; */
    if (param->debug)
      fprintf(f,"%lf\n",sol2->m[ip]);
  }
  
  if (param->debug) {
    fprintf(f,"End");
    fclose(f);
  }

  MMG5_SAFE_FREE(m2);
  MMG5_SAFE_FREE(sol2);

}


// CORRIGER 
int FMG_newton(double *p, double *d, double *r, int dim) {

  double EPS = FMG_eps;
  double x;
  int it, itmax = 100, root, found, flag;

  found = 0;
  for (root=0; root< dim; root++) { 
    flag = 0;
    x = r[root];
    for (it=1; it<=itmax; it++) {
      x = x - FMG_computePoly(p,3,x)/FMG_computePoly(d,3,x);
      if (fabs(FMG_computePoly(p,3,x)) < EPS) {
        found++;
        flag = 1;
        r[root] = x;
        break;
      }
    }
    if (flag)
      found++;
  }

  return found;
}

double FMG_computePoly(double *p, int dim, double x) {

  int i;
  double v;
  
  v = 0.;
  for (i=0;i<=dim;i++)
    v = p[i]*pow(x,i);

  return v;
}
