/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"
#include "math.h"
#include "inlined_functions_3d.h"
/**
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * \param element's index
 *
 * Optimize the shape of an element
 */

int FMG_optlen3d(MMG5_pMesh mesh, MMG5_pSol sol, int iel) {

  int ip, ineigh,iloc, jloc, it, ilist, lon, cnt, iv, flag, ical, iter, itermax=10, mvpt, origiel;
  int *list, jp[50], neigh[5];
  double vec[3], normvec[3], oldp[3], newp[3], rlx[4];
  MMG5_pTetra ptr;
  double eps = 1e-12, cal, cal2,calt, thresh = .95, relax;

//  for (iloc=0;iloc<4;iloc++)
//    neigh[iloc] = (mesh->adja[4*(iel-1)+1+iloc])/4;
  neigh[4] = iel;

  origiel = iel;

  MMG5_SAFE_CALLOC(list,mesh->np+1,int,return 0);
  list[0] = 0;
  if (iel > 0) 
  for (iter=1; iter<=itermax; iter++) {
  for (ineigh=4;ineigh<=4;ineigh++) {
//    iel = neigh[ineigh];
    mvpt=0;
    for (iloc=0; iloc<4; iloc++) {
      ip = mesh->tetra[iel].v[iloc];
      oldp[0] = mesh->point[ip].c[0];
      oldp[1] = mesh->point[ip].c[1];
      oldp[2] = mesh->point[ip].c[2];

      lon = MMG5_boulevolp(mesh,iel,iloc,list);
  
      newp[0] = 0.;
      newp[1] = 0.;
      newp[2] = 0.;
  
      cal = 1e9;
      ical = iel;
  
      for (ilist=0;ilist<lon;ilist++) {
        it = list[ilist]/4;
        ptr = &mesh->tetra[it];
  
//////        calt = MMG5_caltet_iso(mesh,sol,ptr->v[0],ptr->v[1],ptr->v[2],ptr->v[3]);
//        calt = MMG5_caltet_iso(mesh,sol,ptr->v[0],ptr->v[1],ptr->v[2],ptr->v[3]);
        calt = MMG5_caltet_iso(mesh,sol,ptr);
        if (calt < cal) {
          cal = calt;
          ical = it;
        }
      }

   
      cnt = 0;
      flag = 0;
      for (ilist=0;ilist<lon;ilist++) {
        ptr = &mesh->tetra[list[ilist]/4];
        for (jloc=0; jloc<4; jloc++) {
          flag = 0;
//          printf("\n* %d",ptr->v[jloc]);
          if (ptr->v[jloc] != ip) {
            if (cnt>0) {
              for (iv=0; iv<cnt; iv++)
                if (jp[iv] == ptr->v[jloc]) {
                  flag = 1;
                  break;
                }
            }
            if (!flag) {
              jp[cnt] = ptr->v[jloc];
              cnt += 1;
            }
          }
        }
      }
        // equilateral triangle
        // vec[0] = mesh->point[jp[1]].c[0] - mesh->point[jp[0]].c[0];  
        // vec[1] = mesh->point[jp[1]].c[1] - mesh->point[jp[0]].c[1]; 
  
        // normvec[0] = -vec[1]; 
        // normvec[1] = vec[0]; 
  
        // flag = 0; 
       
        // while(!flag) { 
        //   mesh->point[ip].c[0] = mesh->point[jp[0]].c[0] + .5*vec[0] + .5*sqrt(3)*normvec[0]; 
        //   mesh->point[ip].c[1] = mesh->point[jp[0]].c[1] + .5*vec[1] + .5*sqrt(3)*normvec[1]; 
        //   flag=1;   
        //   if (FMG_computeArea(mesh,iel)<eps) { 
        //     flag=0; 
        //     normvec[0] = -normvec[0]; 
        //     normvec[1] = -normvec[1]; 
        //   } 
        // } 
        // newp[0] += mesh->point[ip].c[0]/(lon); 
        // newp[1] += mesh->point[ip].c[1]/(lon); 
        // mesh->point[ip].c[0] = oldp[0]; 
        // mesh->point[ip].c[1] = oldp[1]; 
        
        /*barycenter of the points*/
        for (jloc=0; jloc<cnt; jloc++) {
          newp[0] += mesh->point[jp[jloc]].c[0]/cnt;
          newp[1] += mesh->point[jp[jloc]].c[1]/cnt;
          newp[2] += mesh->point[jp[jloc]].c[2]/cnt;
        }
  
      
  
      relax = 1.;
      while (relax > eps -.1) {
        mesh->point[ip].c[0] = oldp[0] + relax*(newp[0]-oldp[0]);
        mesh->point[ip].c[1] = oldp[1] + relax*(newp[1]-oldp[1]);
        mesh->point[ip].c[2] = oldp[2] + relax*(newp[2]-oldp[2]);

        flag = 0;

        for (ilist=0;ilist<lon;ilist++) {
          ptr = &mesh->tetra[list[ilist]/4];
//          if (MMG5_caltet_iso(mesh,sol,ptr->v[0],ptr->v[1],ptr->v[2],ptr->v[3]) < thresh*cal && !flag) {
          if (MMG5_caltet_iso(mesh,sol,ptr) < thresh*cal && !flag) {
            flag=1;
            break;
          }
        }
        
        if (flag)
          relax -= .1;
        else
          break;

      }
      if(flag) {
        mesh->point[ip].c[0] = oldp[0];
        mesh->point[ip].c[1] = oldp[1];
        mesh->point[ip].c[2] = oldp[2];
      }

      if (relax > 1. - eps)
        relax = .9;

      cal2 = cal;
      cal=1e9;
      for (ilist=0;ilist<lon;ilist++) {
        it = list[ilist]/4;
        ptr = &mesh->tetra[it];
 
        calt = MMG5_caltet_iso(mesh,sol,ptr);
        if (calt<cal) cal = calt;
      }
//      printf("\nPoint %d : qualité %lf -> %lf",ip,cal2,cal);      
      rlx[iloc] = relax + .1;

      if (sqrt(pow(mesh->point[ip].c[0]-oldp[0],2) + pow(mesh->point[ip].c[1]-oldp[1],2)) > eps)
        mvpt ++;

    }

    if (!mvpt) {
//      printf("\n          **** Element %d : Optimisation faite en %d iterations",iel,iter);
      iter = itermax;
    }

  }
  }

  MMG5_SAFE_FREE(list);
}

/**
   \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * \param x0 x coordinates of the points in the initial mesh
 * \param y0 y coordinates of the points in the initial mesh
 * \param sol0 pointer on the solution in the initial mesh
 * \param dx x deplacement of the points
 * \param dy y deplacement of the points
 * \param xprev x coordinates bfore current Jacobi iteration
 * \param yprev y coordinates bfore current Jacobi iteration
 * \param ijacobi 
 *
 * \return number of elements optimized
 *
 * Check which elements will require relaxation, optimizes them and return the number of optimized elements
 */
int FMG_optim2d(MMG5_pMesh mesh, MMG5_pSol sol, double* x0, double* y0, double* sol0,
                double *dx, double *dy, double *xprev, double *yprev,
                int ijacobi) {
  MMG5_pTria ptr;
  int        iel,ip,cnt;
  double     area_min = 0/*1e-6*/, eps = 1e-6;

  /*put the new coordinates for the points (ie add the displacement)*/
  for (ip=1; ip<=mesh->np; ip++) {
    mesh->point[ip].c[0] = x0[ip] + dx[ip];
    mesh->point[ip].c[1] = y0[ip] + dy[ip];
  }

  cnt = 0;
  /*check area and try to optimize the element if needed*/
  for (iel=1; iel<=mesh->nt ; iel++) {
    
    if (FMG_computeArea(mesh, iel) < area_min) {
      ptr = &mesh->tria[iel];
      cnt += 1;
      FMG_optlen2d(mesh,sol,iel);
    }
  }

  /*if some elements are optimized*/
  if(cnt) {
    FMG_interp2d(mesh, sol, x0, y0, sol0);
    
    if (ijacobi == 0) {
      for (ip=1; ip<=mesh->np; ip++) {
        sol0[ip] = sol->m[ip];
        x0[ip] = mesh->point[ip].c[0];
        y0[ip] = mesh->point[ip].c[1];
      }
    }

  }

  /*put the old coordinates*/
  for (ip=1; ip<=mesh->np; ip++) {
    mesh->point[ip].c[0] = xprev[ip];
    mesh->point[ip].c[1] = yprev[ip];
  }

  return(cnt);
}

/**
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * \param element's index
 *
 * Optimize the shape of an element
 */

void FMG_optlen2d(MMG5_pMesh mesh, MMG5_pSol sol, int iel) {

  int ip, ineigh,iloc, jloc, it, ilist, lon, cnt, flag, ical, iter, itermax=10, mvpt, origiel;
  int list[1000], jp[2], neigh[4];
  double vec[2], normvec[2], oldp[2], newp[2], rlx[3];
  MMG5_pTria ptr;
  double eps = 1e-12, cal, cal2,calt, thresh = .95, relax;

  for (iloc=0;iloc<3;iloc++)
    neigh[iloc] = (mesh->adja[3*(iel-1)+1+iloc])/3;
  neigh[3] = iel;

  origiel = iel;

  list[0] = 0;
  
  for (iter=1; iter<=itermax; iter++) {
    for (ineigh=3;ineigh<=3;ineigh++) {
      iel = neigh[ineigh];
      mvpt=0;
      for (iloc=0; iloc<3; iloc++) {
        ip = mesh->tria[iel].v[iloc];
        oldp[0] = mesh->point[ip].c[0];
        oldp[1] = mesh->point[ip].c[1];
  
        lon = MMG2D_boulep(mesh,iel,iloc,list);
        if(lon > 999 ) fprintf(stdout,"a lot triangles, check your mesh\n");
        newp[0] = 0.;
        newp[1] = 0.;
    
        cal = 0;
        ical = iel;
    
        for (ilist=1;ilist<=lon;ilist++) {
          it = list[ilist]/3;
          ptr = &mesh->tria[it];
    
          calt = caltri_iso_in(mesh,sol,ptr);
          if (calt > cal) {
            cal = calt;
            ical = it;
          }
        }
  
   
        for (ilist=1;ilist<=lon;ilist++) {
          ptr = &mesh->tria[list[ilist]/3];
          cnt = 0;
          for (jloc=0; jloc<3; jloc++) 
            if (ptr->v[jloc] != ip) {
              jp[cnt] = ptr->v[jloc];
              cnt += 1;
            }
   
          // equilateral triangle
          // vec[0] = mesh->point[jp[1]].c[0] - mesh->point[jp[0]].c[0];  
          // vec[1] = mesh->point[jp[1]].c[1] - mesh->point[jp[0]].c[1]; 
    
          // normvec[0] = -vec[1]; 
          // normvec[1] = vec[0]; 
    
          // flag = 0; 
         
          // while(!flag) { 
          //   mesh->point[ip].c[0] = mesh->point[jp[0]].c[0] + .5*vec[0] + .5*sqrt(3)*normvec[0]; 
          //   mesh->point[ip].c[1] = mesh->point[jp[0]].c[1] + .5*vec[1] + .5*sqrt(3)*normvec[1]; 
          //   flag=1;   
          //   if (FMG_computeArea(mesh,iel)<eps) { 
          //     flag=0; 
          //     normvec[0] = -normvec[0]; 
          //     normvec[1] = -normvec[1]; 
          //   } 
          // } 
          // newp[0] += mesh->point[ip].c[0]/(lon); 
          // newp[1] += mesh->point[ip].c[1]/(lon); 
          // mesh->point[ip].c[0] = oldp[0]; 
          // mesh->point[ip].c[1] = oldp[1]; 
          
          /*barycenter of the points*/
          newp[0] += mesh->point[jp[0]].c[0]/(2.*lon);
          newp[1] += mesh->point[jp[0]].c[1]/(2.*lon);
          newp[0] += mesh->point[jp[1]].c[0]/(2.*lon);
          newp[1] += mesh->point[jp[1]].c[1]/(2.*lon);
    
        }
    
        relax = 1.;
        while (relax > eps -.1) {
          mesh->point[ip].c[0] = oldp[0] + relax*(newp[0]-oldp[0]);
          mesh->point[ip].c[1] = oldp[1] + relax*(newp[1]-oldp[1]);
  
          flag = 0;
  
          for (ilist=1;ilist<=lon;ilist++)
            if (caltri_iso_in(mesh,sol,&mesh->tria[list[ilist]/3]) > thresh*cal && !flag) {
              flag=1;
              break;
            }
          
          if (flag)
            relax -= .1;
          else
            break;
  
        }
        if(flag) {
          mesh->point[ip].c[0] = oldp[0];
          mesh->point[ip].c[1] = oldp[1];
        }
  
        if (relax > 1. - eps)
          relax = .9;
  
        cal2 = cal;
        cal=0;
        for (ilist=1;ilist<=lon;ilist++) {
          it = list[ilist]/3;
          ptr = &mesh->tria[it];
    
          calt = caltri_iso_in(mesh,sol,ptr);
          if (calt>cal) cal = calt;
        }
  //      printf("\nPoint %d : qualité %lf -> %lf",ip,cal2,cal); 
        rlx[iloc] = relax + .1;
  
        if (sqrt(pow(mesh->point[ip].c[0]-oldp[0],2) + pow(mesh->point[ip].c[1]-oldp[1],2)) > eps)
          mvpt ++;
  
      }
  
      if (!mvpt) {
  //      printf("\n          **** Element %d : Optimisation faite en %d iterations",iel,iter);
        iter = itermax;
      }
  
    }
  }

}
