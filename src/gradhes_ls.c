/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"
#define FMG_EPS1 1e-20
#define FMG_EPS 1e-6
#define FMG_LONMAX 4096

/* solve Mx=b using Gauss */
static int FMG_gauss(int n,double m[n][n],double *x,double *b,char dbg) {
  double    nn,dd;
  int       i,j,k,ip;

  nn = m[0][0];
  for (i=0; i<n; i++) 
    for (j=0; j<n; j++)
      nn = M_MAX(nn,m[i][j]);

  if ( fabs(nn) < FMG_EPS1 ) {
    if ( dbg )  fprintf(stdout,"  %%%% Null matrix\n");
    return(0);
  }
  nn = 1.0 / nn;
  for (i=0; i<n; i++) {
    for (j=0; j<n; j++)
      m[i][j] *= nn;
    b[i] *= nn; 
  }

  /* partial pivoting, column j */
  for (j=0; j<n-1; j++) {
    /* find line pivot */
    ip = j;
    for (i=j+1; i<n; i++)
      if ( fabs(m[i][j]) > fabs(m[ip][j]) )  ip = i;

    /* swap i <-> ip */
    if ( i != ip ) {
      for (k=j; k<n; k++) {
        dd       = m[j][k];
        m[j][k]  = m[ip][k];
        m[ip][k] = dd;
      }
      dd    = b[j];
      b[j]  = b[ip];
      b[ip] = dd;
    }
    if ( fabs(m[j][j]) < FMG_EPS1 ) {
      if ( dbg )  fprintf(stdout,"  %%%% Null pivot: %e.\n",m[j][i]);
      return(0);
    }

    /* elimination */
    for (i=j+1; i<n; i++) {
      dd = m[i][j] / m[j][j];
      m[i][j] = 0.0;
      for (k=j+1; k<n; k++)
        m[i][k] -= dd * m[j][k]; 
      b[i] -= dd * b[j];
    }
  }

  if ( fabs(m[n-1][n-1]) < FMG_EPS1 ) {
    if ( dbg )  fprintf(stdout,"  %%%% Null pivot.\n");
    return(0);
  }

  x[n-1] = b[n-1] / m[n-1][n-1];
  for (i=n-2; i>=0; i--) {
    dd = 0.0;
    for (j=i+1; j<n; j++)
      dd += m[i][j] * x[j];
    x[i] = (b[i] - dd) / m[i][i];
  }

  if ( dbg ) {
    for (i=0; i<n; i++) {
      dd = 0.;
      for (j=0; j<n; j++)  dd += m[i][j] * x[j];
      if ( fabs(dd-b[i]) > FMG_EPS ) {
        fprintf(stdout,"  Ax[%d] = %f   b[%d] = %E\n",i,dd,i,b[i]);
        exit(1);
      }
    }
  }

  return(1);
}

/**
 * \param Hes hessian of the solution on the mesh points (array of size 4*mesh->np)
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * \param Grd gradient of the solution in the noeuds
 * 
 * Computes the hessian of the solution over the mesh with a least square approximation
 */

int FMG_computeHessLS_3d(double* Hes,MMG5_pMesh mesh,MMG5_pSol sol,double* Grd) {
  int ip,isol;
  double *grd,*hes;
  
  for(ip=1;ip<=mesh->np;ip++) {
    for(isol=1; isol<=sol->size; isol++) {
      grd = &Grd[3*(mesh->np+1)*(isol-1) + 3*ip];
      hes = &Hes[6*(mesh->np+1)*(isol-1) + 6*ip];
      FMG_hessLS_3d(mesh,sol,ip,isol,grd,hes);   
    }
  }
  
  return(1);
}


/**
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * \param ip index of the point
 * \param isol index of the solution
 * \param grd gradient at point ip
 * \param hes hessian at point ip (at the end)
 * 
 * Computes the hessian of the solution on point ip with a least square approximation
 */
int FMG_hessLS_3d(MMG5_pMesh mesh,MMG5_pSol sol,int ip,int isol,double* grd,double *hes) {
  MMG5_pPoint    p0,p1,ppt;
  MMG5_pTetra    pt;
  double    a[6],b[6],m[6][6],ax,ay,az;
  double    du,u,u1;
  int       i,j,kk,l,iadr,ier;
  int       nsdep,ip1,list[FMG_LONMAX+2],llist[FMG_LONMAX+2],ilist,iilist,iel,base;
  p0 = &mesh->point[ip];
  nsdep = p0->tmp;
  assert(nsdep);

  i  = 0;
  pt = &mesh->tetra[nsdep];
  if ( pt->v[1] == ip )      i = 1;
  else if ( pt->v[2] == ip ) i = 2;
  else if ( pt->v[3] == ip ) i = 3;

  ilist = MMG5_boulevolp(mesh,nsdep,i,&list[0]);
  if ( ilist < 1 )  return(0);
  /* store vertices from tetra list */
  base     = ++mesh->mark;
  iilist   = 0;
  for (i=1; i<ilist; i++) {
    kk = list[i] / 4;
    pt = &mesh->tetra[kk];
 
    for (j=0; j<4; j++) {
      if ( pt->v[j] != ip ) {
        ppt = &mesh->point[ pt->v[j] ];
        if ( ppt->flag < base ) {
          llist[iilist] = pt->v[j];
          iilist++;
          ppt->flag = base;
        }
      }
    }
  }
  memset(b,0,6*sizeof(double));
  memset(m,0,36*sizeof(double));
  u = sol->m[(isol-1)*(mesh->np+1) + ip];

  /* Hessian: Ui = U + <gradU,PPi> + 0.5*(tPPi.Hess.PPi) */
  for (i=0; i<iilist; i++) {
    ip1 =llist[i];
    p1  = &mesh->point[ip1];
    u1  = sol->m[(isol-1)*(mesh->np+1) + ip1];
    ax = p1->c[0] - p0->c[0];
    ay = p1->c[1] - p0->c[1];
    az = p1->c[2] - p0->c[2];

    a[0] = 0.5 * ax*ax;
    a[1] =       ax*ay;
    a[2] =       ax*az;
    a[3] = 0.5 * ay*ay;
    a[4] =       ay*az;
    a[5] = 0.5 * az*az;
    du   = (u1-u) - (ax*grd[0] + ay*grd[1] + az*grd[2]);

    /* M = At*A symmetric positive definite */
    for (j=0; j<6; j++)
      for (l=j; l<6; l++) {
        m[j][l] += a[j]*a[l];
        m[l][j] += a[l]*a[j];
      }

    /* c = At*b */
    for (j=0; j<6; j++)
      b[j] += a[j]*du;
  }

  /* solve m(6,6)*x(6) = b(6) */
  ier = FMG_gauss(6,m,hes,b,mesh->info.ddebug);

  if ( !ier ) {
    if ( mesh->info.ddebug )  fprintf(stdout," Ill cond'ed matrix (%d, %d).\n",ip,ier);
    return(-1);
  }

  return(1);
}

/**
 * \param Grd gradient of the solution in the noeuds
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * 
 * Computes the gradient of the solution over the mesh with a least square approximation
 */

int FMG_computeGrdLS_3d(double* Grd,MMG5_pMesh mesh,MMG5_pSol sol) {
  int ip,isol;
  double *grd;
  
  for(ip=1;ip<=mesh->np;ip++) {
    for(isol=1; isol<=sol->size; isol++) {
      grd = &Grd[3*(mesh->np+1)*(isol-1) + 3*ip];
      FMG_gradLS_3d(mesh,sol,ip,isol,grd);
    }
  }
  
  return(1);
}

/* compute gradient via least square approx 
   in:  ip = vertex num   is = sol num 
   out: grd[3] gradient */
int FMG_gradLS_3d(MMG5_pMesh mesh,MMG5_pSol sol,int ip,int isol,double* grd) {
  MMG5_pPoint    p0,p1,ppt;
  MMG5_pTetra    pt;
  double    a[6],b[3],ax,ay,az,du,u,u1,aa,bb,cc,dd,ee,ff;
  int       i,iadr,nsdep,ip1,list[FMG_LONMAX+2],ilist,llist[FMG_LONMAX+2],iilist;
  int       base,kk,j;
  int ipdebug=1153;
  p0     = &mesh->point[ip];
  nsdep  = p0->tmp;
  assert(nsdep);

  i  = 0;
  pt = &mesh->tetra[nsdep];
  if ( pt->v[1] == ip )      i = 1;
  else if ( pt->v[2] == ip ) i = 2;
  else if ( pt->v[3] == ip ) i = 3;

  ilist = MMG5_boulevolp(mesh,nsdep,i,list);
  if ( ilist < 1 )  return(0);
  /* store vertices from tetra list */
  base     = ++mesh->mark;
  iilist   = 0;
  for (i=1; i<ilist; i++) {
    kk = list[i] / 4;
    pt = &mesh->tetra[kk];
 
    for (j=0; j<4; j++) {
      if ( pt->v[j] != ip ) {
        ppt = &mesh->point[ pt->v[j] ];
        if ( ppt->flag < base ) {
          llist[iilist] = pt->v[j];
          iilist++;
          ppt->flag = base;
        }
      }
    }
  }
  memset(a,0,6*sizeof(double));
  memset(b,0,3*sizeof(double));
  u = sol->m[(isol-1)*(mesh->np+1) + ip];
 

  /* Gradient: ui = u + <grad u,PPi> */
  for (i=0; i<iilist; i++) {
    ip1 = llist[i];
    p1  = &mesh->point[ip1];
    u1  = sol->m[(isol-1)*(mesh->np+1) + ip1];
    du  = u1 - u;

    /* M = At*A symmetric definite positive */
    ax = p1->c[0] - p0->c[0];
    ay = p1->c[1] - p0->c[1];
    az = p1->c[2] - p0->c[2];
    a[0] += ax * ax;
    a[1] += ax * ay;
    a[2] += ax * az;
    a[3] += ay * ay;
    a[4] += ay * az;
    a[5] += az * az;

    /* b = At*du */
    b[0] += ax * du;
    b[1] += ay * du;
    b[2] += az * du;
  }

  /* solution of A(3,3)*grad(1,3) = b(1,3) */ 
  aa = a[3]*a[5] - a[4]*a[4];
  bb = a[4]*a[2] - a[1]*a[5];
  cc = a[1]*a[4] - a[2]*a[3];
  du = aa*a[0] + bb*a[1] + cc*a[2];
  if ( fabs(du) == 0.0 ) {
    fprintf(stdout," Invalid matrix (%E) at points %d %d. Exit\n",du,ip,ip1);
    return(0);
  }
  du = 1.0 / du;
  dd = a[0]*a[5] - a[2]*a[2];
  ee = a[1]*a[2] - a[0]*a[4];
  ff = a[0]*a[3] - a[1]*a[1];

  grd[0] = (aa*b[0] + bb*b[1] + cc*b[2]) * du;
  grd[1] = (bb*b[0] + dd*b[1] + ee*b[2]) * du;
  grd[2] = (cc*b[0] + ee*b[1] + ff*b[2]) * du;

  /* normalize */
  if ( mesh->info.ls ) {
    du = grd[0]*grd[0] + grd[1]*grd[1] + grd[2]*grd[2];
    if ( du > FMG_EPS1 ) {
      du      = 1.0 / sqrt(du);
      grd[0] *= du;
      grd[1] *= du;
      grd[2] *= du;
    }
  }
  return(1);
}

/*compute gradient via least square approx*/
int FMG_gradLS_2d(MMG5_pMesh mesh,MMG5_pSol sol,int ip,int isol,double *grd) {
  MMG5_pPoint    p0,p1,ppt;
  MMG5_pTria     pt;
  double    a[3],b[2],ax,ay,du,u,u1;
  int       i,j,kk,iadr,nsdep,ip1,base;
  int       list[FMG_LONMAX+2],llist[FMG_LONMAX+2],ilist,iilist;

  p0      = &mesh->point[ip];
  // p0->nv  = 0;
  nsdep   = p0->tmp;
  assert(nsdep);

  pt   = &mesh->tria[nsdep];
  if ( pt->v[0] == ip )      i = 0;
  else if ( pt->v[1] == ip ) i = 1;
  else                       i = 2;

  ilist = MMG2D_boulep(mesh,nsdep,i,list);
  if ( ilist < 1 )  return(0);

  /* store vertices from tria list */
  base     = ++mesh->mark;
  iilist   = 0;
  for (i=1; i<=ilist; i++) {
    kk = list[i] / 3;
    pt = &mesh->tria[kk];
 
    for (j=0; j<3; j++) {
      if ( pt->v[j] != ip ) {
        ppt = &mesh->point[ pt->v[j] ];
        if ( ppt->flag < base ) {
          llist[iilist] = pt->v[j];
          iilist++;
          ppt->flag = base;
        }
      }
    }
  }
  memset(a,0,3*sizeof(double));
  memset(b,0,2*sizeof(double));
  u = sol->m[(isol-1)*(mesh->np+1) + ip];
  
  /* Gradient: ui = u + <grad u,PPi> */
  for (i=0; i<iilist; i++) {
    ip1 = llist[i];
    p1  = &mesh->point[ip1];
    u1  = sol->m[(isol-1)*(mesh->np+1) + ip1];

    /* M = At*A symmetric definite positive */
    ax    = p1->c[0] - p0->c[0];
    ay    = p1->c[1] - p0->c[1];
    a[0] += ax * ax;
    a[1] += ax * ay;
    a[2] += ay * ay;

    /* b = At*du */
    du    = u1 - u;
    b[0] += ax * du;
    b[1] += ay * du;
    //p0->nv++;
  }

  /* solution of A(2,2)*grad(1,2) = b(1,2) */ 
  du = a[0]*a[2] - a[1]*a[1];
  if ( fabs(du) == 0.0 ) {
    fprintf(stdout," Invalid matrix (%E). Exit\n",du);
    return(0);
  }
  du  = 1.0 / du;
  //grd[0] = (a[2]*b[0] - a[1]*b[1]) * du;
  //grd[1] = (a[0]*b[1] - a[1]*b[0]) * du;
  grd[2*(mesh->np+1)*(isol-1) + 2*ip] = (a[2]*b[0] - a[1]*b[1]) * du;
  grd[2*(mesh->np+1)*(isol-1) + 2*ip+1] = (a[0]*b[1] - a[1]*b[0]) * du;

  /* normalize */
  if ( mesh->info.ls ) {
    //du = grd[0]*grd[0] + grd[1]*grd[1];
    du = grd[2*(mesh->np+1)*(isol-1) + 2*ip]*grd[2*(mesh->np+1)*(isol-1) + 2*ip];
    du = du + grd[2*(mesh->np+1)*(isol-1) + 2*ip+1]*grd[2*(mesh->np+1)*(isol-1) + 2*ip+1];
    if ( du > FMG_EPS1 ) {
      du      = 1.0 / sqrt(du);
      //grd[0] *= du;
      //grd[1] *= du;
      grd[2*(mesh->np+1)*(isol-1) + 2*ip] *= du;
      grd[2*(mesh->np+1)*(isol-1) + 2*ip+1] *= du;
    }
  }

  return(1);
}


/* least square approximation of hessian */
int FMG_hessLS_2d(MMG5_pMesh mesh,MMG5_pSol sol,int ip,int isol,double* grd,double* hes) {
  MMG5_pPoint    p0,p1,ppt,p0_bis;
  MMG5_pTria     pt;
  double    a[3],ma[6],mb[3],ax,ay;
  double    det,aa,bb,cc,dd,ee,ff;
  double    u,u1;
  int       i,j,kk,iadr,nsdep,ip1,base,i_bis,ii;
  int       list[FMG_LONMAX+2],ilist,llist[FMG_LONMAX+2],iilist,list_bis[FMG_LONMAX+2], ilist_bis;

  p0 = &mesh->point[ip];
  //if ( p0->nv < 3 )  return(-1);
  nsdep = p0->tmp;
  assert(nsdep);

  pt   = &mesh->tria[nsdep];
  if ( pt->v[0] == ip )      i = 0;
  else if ( pt->v[1] == ip ) i = 1;
  else                       i = 2;

  ilist = MMG2D_boulep(mesh,nsdep,i,list);
  if ( ilist < 1 )  return(0);
  /* store vertices from tria list */
  base     = ++mesh->mark;
  iilist   = 0;
  for (i=1; i<=ilist; i++) {
    kk = list[i] / 3;
    pt = &mesh->tria[kk];
 
    for (j=0; j<3; j++) {
      if ( pt->v[j] != ip ) {
        ppt = &mesh->point[ pt->v[j] ];
        if ( ppt->flag < base ) {
          llist[iilist] = pt->v[j];
          iilist++;
          ppt->flag = base;
        }
      }
    }
  }
  memset(ma,0,6*sizeof(double));
  memset(mb,0,3*sizeof(double));
  u = sol->m[(isol-1)*(mesh->np+1) + ip];

  if(ilist<4) return(-1); //CECILE --> pour palier le pbs des bords sous determine
  ii = iilist;

  if(iilist<4) {
    for (i_bis=0; i_bis<ii; i_bis++) {

      p0_bis = &mesh->point[llist[i_bis]];
      nsdep = p0_bis->tmp;
      assert(nsdep);

      pt   = &mesh->tria[nsdep];
      if ( pt->v[0] == llist[i_bis] )      i = 0;
      else if ( pt->v[1] == llist[i_bis] ) i = 1;
      else                       i = 2;

      ilist_bis = MMG2D_boulep(mesh,nsdep,i,list_bis);
      
      for (i=1; i<=ilist_bis; i++) {
        kk = list_bis[i] / 3;
        pt = &mesh->tria[kk];
 
        for (j=0; j<3; j++) {
          if ( pt->v[j] != ip ) {
            ppt = &mesh->point[ pt->v[j] ];
            if ( ppt->flag < base ) {
              llist[iilist] = pt->v[j];
              iilist++;
              ppt->flag = base;
            }
          }
        }
      }
    }
  }

  //if(ilist<4) return(-1); //CECILE --> pour palier le pbs des bords sous determine
  /* Hessian: Ui = U + <gradU,PPi> + 0.5*(tPPi.Hess.PPi) */
  for (i=0; i<iilist; i++) {
    ip1 = llist[i];
    p1  = &mesh->point[ip1];
    u1  = sol->m[(isol-1)*(mesh->np+1) + ip1];

    ax    = p1->c[0] - p0->c[0];
    ay    = p1->c[1] - p0->c[1];
    a[0]  = 0.5 * ax*ax;
    a[1]  =       ax*ay;
    a[2]  = 0.5 * ay*ay;
    //dd    = (u1-u) - (ax*grd[0] + ay*grd[1]);
    dd    = (u1-u) - (ax*grd[2*(mesh->np+1)*(isol-1) + 2*ip] + ay*grd[2*(mesh->np+1)*(isol-1) + 2*ip+1]);

    /* M = At*A symmetric definite positive */
    ma[0] += a[0]*a[0];
    ma[1] += a[0]*a[1];
    ma[2] += a[1]*a[1];
    ma[3] += a[0]*a[2];
    ma[4] += a[1]*a[2];
    ma[5] += a[2]*a[2];

    /* c = At*b */
    mb[0] += a[0]*dd;
    mb[1] += a[1]*dd;
    mb[2] += a[2]*dd;
  }

  if ( fabs(ma[0]) < FMG_EPS1 ) {
    if ( mesh->info.ddebug )  fprintf(stdout," Ill cond'ed matrix (%d, %E).\n",ip,ma[0]);
    return(-1);
  }

  /* direct solving */
  aa = ma[2]*ma[5] - ma[4]*ma[4];
  bb = ma[4]*ma[3] - ma[1]*ma[5];
  cc = ma[1]*ma[4] - ma[2]*ma[3];
  dd = ma[0]*aa + ma[1]*bb + ma[3]*cc;

  /* singular matrix */
  if ( fabs(dd) == 0.0 ) {
    //chk sol cte and put hmax
    for (i=1; i<=iilist; i++) {
      ip1 = llist[i];
      p1  = &mesh->point[ip1];
      u1  = sol->m[(isol-1)*(mesh->np+1) + ip1];
      if(fabs(u1-u) > 1e-6) break;
    }
    if(i<=ilist) {
      if ( mesh->info.ddebug )  fprintf(stderr," Singular matrix (%d, %E).\n",ip,dd);
      for (i=1; i<=ilist; i++) {
        ip1 = list[i];
        p1  = &mesh->point[ip1];
        u1  = sol->m[(isol-1)*(mesh->np+1) + ip1];
      }
      return(-1);
    }
    //hes[0] = hes[2] = 0; //CECILE : on met 0 et du coup il prendra hmax 
    //hes[1] = 0; 
    hes[3*(mesh->np+1)*(isol-1) + 3*ip] = hes[3*(mesh->np+1)*(isol-1) + 3*ip+2] = 0;
    hes[3*(mesh->np+1)*(isol-1) + 3*ip+1] = 0; 
    //    p0->h=1;
    return(1);
  }
  det = 1.0 / dd;
  dd = ma[0]*ma[5] - ma[3]*ma[3];
  ee = ma[1]*ma[3] - ma[0]*ma[4];
  ff = ma[0]*ma[2] - ma[1]*ma[1];

  //hes[0] = (mb[0]*aa + mb[1]*bb + mb[2]*cc) * det;
  //hes[1] = (mb[0]*bb + mb[1]*dd + mb[2]*ee) * det;
  //hes[2] = (mb[0]*cc + mb[1]*ee + mb[2]*ff) * det;
  hes[3*(mesh->np+1)*(isol-1) + 3*ip] = (mb[0]*aa + mb[1]*bb + mb[2]*cc) * det;
  hes[3*(mesh->np+1)*(isol-1) + 3*ip+1] = (mb[0]*bb + mb[1]*dd + mb[2]*ee) * det;
  hes[3*(mesh->np+1)*(isol-1) + 3*ip+2] = (mb[0]*cc + mb[1]*ee + mb[2]*ff) * det;

  //if ( !p0->b && p0->nv == 4 )  return(-1);

  // p0->h = 1;

  return(1);
}
