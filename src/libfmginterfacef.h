 INTERFACE
    SUBROUTINE FMG_SET_SOLSIZE(mesh,sol,fmgdata,np,nsol,retval)
      MMG5_DATA_PTR_T, INTENT(INOUT) :: mesh,sol
      FMG_DATA_PTR_T, INTENT(INOUT)  :: fmgdata
      INTEGER, INTENT(IN)            :: np,nsol
      INTEGER, INTENT(OUT)           :: retval
    END SUBROUTINE
    SUBROUTINE FMG_SET_SCALARSOL(met,fmgdata,s,pos,isol,adapt,retval)
      MMG5_DATA_PTR_T, INTENT(INOUT) :: met
      FMG_DATA_PTR_T, INTENT(INOUT)  :: fmgdata
      INTEGER, INTENT(IN)            :: pos,isol,adapt
      REAL(KIND=8), INTENT(IN)       :: s
      INTEGER, INTENT(OUT)           :: retval
    END SUBROUTINE
SUBROUTINE FMG_INIT_FMG2D(mesh,fmgdata,retval)
  MMG5_DATA_PTR_T, INTENT(INOUT) :: mesh
  FMG_DATA_PTR_T, INTENT(INOUT)  :: fmgdata
  INTEGER, INTENT(OUT)           :: retval
END SUBROUTINE
SUBROUTINE FMG_START_FMG2D(mesh,sol,fmgdata,retval)
  MMG5_DATA_PTR_T, INTENT(INOUT) :: mesh,sol
  FMG_DATA_PTR_T, INTENT(INOUT)  :: fmgdata
  INTEGER, INTENT(OUT)           :: retval
END SUBROUTINE
SUBROUTINE FMG_END_FMG2D(mesh,fmgdata)
  MMG5_DATA_PTR_T, INTENT(INOUT) :: mesh
  FMG_DATA_PTR_T, INTENT(INOUT)  :: fmgdata
END SUBROUTINE
END  INTERFACE

