/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/



#include "fmg.h"

/**
 * \param mesh pointer on the mesh 
 * \param dxk x displacement
 * \param dyk y displacement
 * \param xref ref x coordinates
 * \param yref ref y coordinates
 * 
 * For the point on slip boundary : update the normal and project the new position on the boundary
 */

int FMG_normalUpdate(MMG5_pMesh mesh,double *dxk,double *dyk,double *xref,double *yref) {
  MMG5_pTria         pt;
  MMG5_pPoint        pp1,pp2;
  double             p12x,p12y,p1nx,p1ny,ps;
  int                iedg,*adja,ip1,ip2,ref,ip;
  double             len,s,o[2],no[2];

  for(ip=1 ; ip<=mesh->nt ; ip++) {
    pt = &mesh->tria[ip];
    if ( !MG_EOK(pt) )  continue;
    
    adja = &mesh->adja[3*(ip-1)+1];
    for (iedg=0; iedg<3; iedg++) {
      if(adja[iedg]) continue;
      ref = pt->edg[iedg];
      ip1 = pt->v[MMG2D_iare[iedg][0]];
      ip2 = pt->v[MMG2D_iare[iedg][1]];
      pp1 = &mesh->point[ip1];
      pp2 = &mesh->point[ip2];
      /*P1P2*/
      p12x = pp2->c[0] - pp1->c[0];
      p12y = pp2->c[1] - pp1->c[1];
      /*P1Pnew1*/
      p1nx = xref[ip1]+dxk[ip1]-pp1->c[0];
      p1ny = yref[ip1]+dyk[ip1]-pp1->c[1];
      /*scalar product P1Pnew1.P1P2*/
      ps   = p1nx*p12x + p1ny*p12y;
      len = sqrt((pp1->c[0]-pp2->c[0])*(pp1->c[0]-pp2->c[0]) + (pp1->c[1]-pp2->c[1])*(pp1->c[1]-pp2->c[1]));
      if(pp1->tag & MG_BDY && !(pp1->tag & MG_CRN) && !(pp1->tag & M_DIRICHLET) &&
          (ps > 0)) { /*ps > 0 check if we are on the good edge*/
        /*compute s : parameter for moving the vertex */
        s = sqrt(p1nx*p1nx +p1ny*p1ny)/len;
        if(s < 1) {
          /*compute the new coordinates and the new normals*/
          MMG2D_bezierCurv(mesh,ip,iedg,s,o,no);
   
          dxk[ip1] = o[0] - xref[ip1]; 
          dyk[ip1] = o[1] - yref[ip1];
          pp1->n[0] = no[0];
          pp1->n[1] = no[1];

        }

      }
      /*P2Pnew2*/
      p1nx = xref[ip2]+dxk[ip2]-pp2->c[0];
      p1ny = yref[ip2]+dyk[ip2]-pp2->c[1];
      /*scalar product P2Pnew2.P2P1*/
      ps   = -(p1nx*p12x + p1ny*p12y);

      if(pp2->tag & MG_BDY && !(pp2->tag & MG_CRN) && !(pp2->tag & M_DIRICHLET) &&
          (ps > 0)) {
        /*compute s : parameter for moving the vertex */
        s = sqrt(p1nx*p1nx +p1ny*p1ny)/len;
        if(s<1) {
          /*compute the new coordinates and the new normals*/
          MMG2D_bezierCurv(mesh,ip,iedg,1.-s,o,no);

          dxk[ip2] = o[0] - xref[ip2];
          dyk[ip2] = o[1] - yref[ip2];
          pp2->ref = -1;
          pp2->n[0] = no[0];
          pp2->n[1] = no[1];
        }
      }
    }
  }

  return(1);
}

/**
 * \param K FEM matrix of coefficients
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param area0 elements' areas in the initial mesh
 * \param normal0 element's normal vectors in the initial mesh
 * \omega omega values in each point of mesh
 * 
 * Computes matrix K of the depalcement linear system
 */

void FMG_computeMatrix(double** K, MMG5_pMesh mesh, double* area0, double* normal0, double* omega){
  
  int ip, jp, iel, iloc, jloc;
  double avgOmega, area;
  double veci[2], vecj[2];
  MMG5_pTria ptr;


  for (ip=1;ip<=mesh->np;ip++)
   for (jp=1;jp<=mesh->np;jp++)  
     K[ip][jp] = 0.;
 
  for (iel=1; iel<=mesh->nt; iel++) {
    ptr = &mesh->tria[iel];
    area = area0[iel];

    avgOmega = 0;
    for (iloc=0; iloc<=2; iloc++)
      avgOmega += omega[ptr->v[iloc]]/3.;

    for (iloc=0; iloc<=2; iloc++) {
      for (jloc=0; jloc<= 2; jloc++) {
        ip = ptr->v[iloc];
        jp = ptr->v[jloc];

        veci[0] = normal0[6*iel+2*iloc];
        veci[1] = normal0[6*iel+2*iloc+1];
        vecj[0] = normal0[6*iel+2*jloc];
        vecj[1] = normal0[6*iel+2*jloc+1];
            
        K[ip][jp] += avgOmega/(4.*area)*(veci[0]*vecj[0] + veci[1]*vecj[1]);
      }
    }

  }
}


/**
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param neigSparse list of neighbor points of each point
 * \param maxNeig maximum number of neighbors for a point in the mesh
 *
 * Constructs a structure for stocking a sparse matrix 
 */

void FMG_constructSparseStruct2d(MMG5_pMesh mesh, int* neigSparse, int maxNeig) {

  int ip, jp, iloc, jloc, iel, ilist, lon=mesh->np, flag = 0, k;
  int visitedNeig[mesh->np+1], list[1000];

  for (ip=0; ip<=mesh->np;ip++) {
   visitedNeig[ip] = 0;
  }

  for (ip=1;ip<=mesh->np;ip++) {

    iel = mesh->point[ip].s;
    for (iloc=0;iloc<3;iloc++)
      if (mesh->tria[iel].v[iloc] == ip)
        break;
    lon = MMG2D_boulep(mesh,iel,iloc,list);
    if(lon>999) {
      fprintf(stdout,"a lot of neighbour, check your mesh\n");
    }
    for (jp=1;jp<=visitedNeig[0];jp++)
        visitedNeig[jp] = 0;
    visitedNeig[0] = 0;

    for (ilist=1; ilist<=lon; ilist++) {
      iel = list[ilist]/3; 
      for (jloc=0; jloc<3; jloc++) {
        flag = 0;
        jp = mesh->tria[iel].v[jloc];
        for (k=1; k<=visitedNeig[0]; k++)
          if (visitedNeig[k]==jp) {
            flag = 1;
            break;
          }
        if (jp != ip && flag == 0) {
          neigSparse[(maxNeig+1)*ip+visitedNeig[0]+1] = jp;
          visitedNeig[0] += 1;
          visitedNeig[visitedNeig[0]] = jp;
        }
      }
    }
    neigSparse[(maxNeig+1)*ip+visitedNeig[0]+1] = ip;
    neigSparse[(maxNeig+1)*ip] = visitedNeig[0]+1;
  }
}


/**
 * \param K FEM matrix of coefficients
 * \param b FEM's right-hand side
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param area0 elements' areas in the initial mesh
 * \param normal0 element's normal vectors in the initial mesh
 * \param omega omega values in each point of mesh
 * \param mu mu values in each point of mesh
 * \param lamdba lamdba values in each point of mesh
 * \param neigSparse list of neighbor points of each point
 * \param maxNeig maximum number of neighbors for a point in the mesh
 * \param elast indicates if lapalcian (0) ou linear elasticity (1) model
 *
 * Computes matrix K and the RHS of the deplacement linear system (considering the structure for stocking the sparse matrix K)
 */

int FMG_computeLinSystSparse2d(double* K, double* b, MMG5_pMesh mesh, double* area0, double* normal0, double* omega, double* mu, double* lambda, int* neigSparse, int maxNeig, int elast,MMG5_pSol sol,int ijacobi){

  MMG5_pPoint ppa,ppb;
  int ip, jp, iel, iloc, jloc, ineig;
  double avgOmega, area, avgLambda;
  double veci[2], vecj[2], veck[2], avgMu[4];
  double /**totalArea,*/*sol0,*g1,*g2,*g3;
  int    /**lonBoulep,*/ipa,ipb,ipip,*adja;
  double  len;
  double tmpFrc[2];
  MMG5_pTria ptr;

  ////// CALCUL ALTERNATIF DE b
  if (elast) {
    //MMG5_SAFE_CALLOC(totalArea,mesh->np+1,double,return 0);
    MMG5_SAFE_CALLOC(sol0,mesh->np+1,double,return 0);
    MMG5_SAFE_CALLOC(g1,2*mesh->np+2,double,return 0);
    MMG5_SAFE_CALLOC(g2,2*mesh->np+2,double,return 0);
    MMG5_SAFE_CALLOC(g3,2*mesh->np+2,double,return 0);
    //MMG5_SAFE_CALLOC(lonBoulep,mesh->np+1,int,return 0);
//    for (ip=1; ip<=mesh->np;ip++) {
//      sol0[ip] = sol->m[ip];
//      sol->m[ip] = mu[4*ip] + 2*lambda[ip];
//    }
//    FMG_computeGrd2d(g1,mesh,sol);
//    for (ip=1; ip<=mesh->np;ip++)
//      sol->m[ip] = mu[4*ip+1];
//    FMG_computeGrd2d(g2,mesh,sol);
//    for (ip=1; ip<=mesh->np;ip++)
//      g3[ip] = g1[2*ip] + g2[2*ip+1];
//    for (ip=1; ip<=mesh->np;ip++)
//      sol->m[ip] = mu[4*ip+2];
//    FMG_computeGrd2d(g1,mesh,sol);
//    for (ip=1; ip<=mesh->np;ip++)
//      sol->m[ip] = mu[4*ip+3] + 2*lambda[ip];
//    FMG_computeGrd2d(g2,mesh,sol);
//    for (ip=1; ip<=mesh->np;ip++)
//      g3[ip+mesh->np] = g1[2*ip] + g2[2*ip+1];
//    for (ip=1; ip<=mesh->np;ip++)
//      sol->m[ip] = sol0[ip];

    for (ip=1; ip<=mesh->np; ip++){
      sol0[ip] = sol->m[ip];
      sol->m[ip] = omega[ip];
    }
    FMG_computeGrd2d(g1,mesh,sol);
    for (ip=1; ip<=mesh->np;ip++){
      g3[ip] = g1[2*ip];
      g3[ip+mesh->np] = g1[2*ip+1];
      sol->m[ip] = sol0[ip];
    }

  }

  ////////


  if (elast) {
    for (ip=0;ip<2*mesh->np+2;ip++)
      b[ip] = 0.;
    if(ijacobi==1)
      for (ip=0;ip<4*maxNeig*(mesh->np+1);ip++)
        K[ip] = 0.;
  }
  else {
    for (ip=0;ip<maxNeig*(mesh->np+1);ip++)
      K[ip] = 0.;
  }
 
  for (iel=1; iel<=mesh->nt; iel++) {
    ptr = &mesh->tria[iel];
    area = area0[iel];

    if (!elast)
      avgOmega = 0;
    else {
      avgLambda = 0;
      for (ip=0; ip <4; ip++)
        avgMu[ip] = 0;
    }

    for (iloc=0; iloc<3; iloc++) {
      if (!elast)
        avgOmega += omega[ptr->v[iloc]]/3.;
      else {
        avgLambda += lambda[ptr->v[iloc]]/3.;
        for (ip=0; ip<4; ip++)
          avgMu[ip] += mu[4*ptr->v[iloc]+ip]/3.;
        }
    }

    for (iloc=0; iloc<=2; iloc++) {
      ip = ptr->v[iloc];
      if(elast) {
        //totalArea[ip] += area;
        //lonBoulep[ip] += 1;
      }
      veci[0] = normal0[6*iel+2*iloc];
      veci[1] = normal0[6*iel+2*iloc+1];

      for (jloc=0; jloc<= 2; jloc++) {
        jp = ptr->v[jloc];

        for (ineig=1;ineig<=neigSparse[ip*(maxNeig+1)];ineig++)
          if (neigSparse[ip*(maxNeig+1)+ineig] == jp)
            break;
        ineig -= 1;

        vecj[0] = normal0[6*iel+2*jloc];
        vecj[1] = normal0[6*iel+2*jloc+1];

        if (elast){
          if(ijacobi==1) {
            veck[0] = (avgMu[0]+avgLambda)*veci[0];
            veck[1] = avgMu[1]/2*veci[0] + avgMu[0]/2*veci[1];
            K[(4*maxNeig)*ip+4*ineig] += (veck[0]*vecj[0] + veck[1]*vecj[1])/(4.*area);
            
            veck[0] = avgMu[1]/2*veci[0] + avgMu[0]/2*veci[1];;
            veck[1] = avgLambda*veci[0] + avgMu[1]*veci[1];
            K[(4*maxNeig)*ip+4*ineig+1] += (veck[0]*vecj[0] + veck[1]*vecj[1])/(4.*area);
            
            veck[0] = avgMu[2]*veci[0] + avgLambda*veci[1];
            veck[1] = avgMu[3]/2*veci[0] + avgMu[2]/2*veci[1];
            K[(4*maxNeig)*ip+4*ineig+2] += (veck[0]*vecj[0] + veck[1]*vecj[1])/(4.*area);
            
            veck[0] = avgMu[3]/2*veci[0] + avgMu[2]/2*veci[1];
            veck[1] = (avgMu[3]+avgLambda)*veci[1];
            K[(4*maxNeig)*ip+4*ineig+3] += (veck[0]*vecj[0] + veck[1]*vecj[1])/(4.*area);
           
    //          K[(4*maxNeig)*ip+4*ineig+1] = (K[(4*maxNeig)*ip+4*ineig+1] + K[(4*maxNeig)*ip+4*ineig+2] )/2.;
    //          K[(4*maxNeig)*ip+4*ineig+2] = K[(4*maxNeig)*ip+4*ineig+1];
    
          }
        }
        else
          K[maxNeig*ip + ineig] += avgOmega/(4.*area)*(veci[0]*vecj[0] + veci[1]*vecj[1]);
        
      }
//      if (elast) {
//         b[ip] += ((avgMu[0]+2*avgLambda)*veci[0] +  avgMu[1]*veci[1])/2.;
//         b[ip+mesh->np] += ((avgMu[2])*veci[0] +  (avgMu[3]+2*avgLambda)*veci[1])/2.;
//
//      }
    }
  }


  ///// Calcul BC
  //if (elast)
  //  FMG_computeBC2d(K, mesh, normal0, lambda, neigSparse, maxNeig);

  ///// Matrice symmetrique
  if (elast) {
    if( ijacobi==1) {
      for (ip=1; ip<=mesh->np; ip++) {
        for (ineig=1;ineig<=neigSparse[ip*(maxNeig+1)];ineig++) {
          jp = neigSparse[ip*(maxNeig+1)+ineig];
          K[(4*maxNeig)*ip+4*(ineig-1)+1] = (K[(4*maxNeig)*ip+4*(ineig-1)+1] + K[(4*maxNeig)*ip+4*(ineig-1)+2] )/2.;
          K[(4*maxNeig)*ip+4*(ineig-1)+2] = K[(4*maxNeig)*ip+4*(ineig-1)+1];
        }
      }
    }
  }
  ////// CALCUL ALTERNATIF DE b
  //  if (elast)
  //    for (ip=1;ip<=mesh->np;ip++) {  
  //      b[ip] = g3[ip]*totalArea[ip]/lonBoulep[ip];
  //      b[ip+mesh->np] = g3[ip+mesh->np]*totalArea[ip]/lonBoulep[ip];
  //    }

  ////// CALCUL ALTERNATIF 2 DE b
  if (elast) {
    for (ip=0;ip<2*mesh->np+2;ip++)
      b[ip] = 0.;

//    for (iel=1; iel<=mesh->nt; iel++)
//      for (iloc=0; iloc<=2; iloc++) {
//        ip = mesh->tria[iel].v[iloc];
//        for (jloc=0; jloc<=2; jloc++) {
//          jp = mesh->tria[iel].v[jloc];
//          b[jp] += mesh->tria[iel].qual/9.*g3[ip];
//          b[jp+mesh->np] += mesh->tria[iel].qual/9.*g3[ip+mesh->np];
//        }
//      }

    for (iel=1; iel<=mesh->nt; iel++) {
      area = area0[iel];
      tmpFrc[0] = 0.0;
      tmpFrc[1] = 0.0;
      for (iloc=0; iloc<=2; iloc++) {
        ip = mesh->tria[iel].v[iloc];
        tmpFrc[0] += g3[ip];
        tmpFrc[1] += g3[ip+mesh->np];

      }
      for (iloc=0; iloc<=2; iloc++) {
        ip = mesh->tria[iel].v[iloc];
//        b[ip] += mesh->tria[iel].qual/9.*tmpFrc[0];
//        b[ip+mesh->np] += mesh->tria[iel].qual/9.*tmpFrc[1];
        b[ip] += area/9.*tmpFrc[0];
        b[ip+mesh->np] += area/9.*tmpFrc[1];
      }
    }

    //MMG5_SAFE_FREE(totalArea);
    //MMG5_SAFE_FREE(lonBoulep);
    MMG5_SAFE_FREE(sol0);
    MMG5_SAFE_FREE(g1);
    MMG5_SAFE_FREE(g2);
    MMG5_SAFE_FREE(g3);

  }
}


/**
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param dx x deplacement for each point
 * \param dy y deplacement for each point
 * \param K FEM matrix of coefficients
 * \param b FEM's right-hand side
 * \param neigSparse list of neighbor points of each point
 * \param maxNeig maximum number of neighbors for a point in the mesh
 *
 * Computes one Jacobi iteration (considering the structure for stocking the sparse matrix K)
 */


void FMG_iterJacobiSparse2d(MMG5_pMesh mesh, double* dx, double* dy, double* Kx, double* Ky,
                          double* b, int* neigSparse, int maxNeig, int elast, int aniso) {

  MMG5_pPoint ppt;
  int ip, jp, iloc, jloc, iel, ilist, k,ineig, ipip, max, imax;
  double rhsx, rhsy, eps=1e-6,vx,vy,tmpx,tmpy,alpha;
  double dxk[mesh->np+1], dyk[mesh->np+1];
  double prc = 20.0;//120.0;

  for (ip=1; ip<=mesh->np;ip++) {
    dxk[ip] = dx[ip];
    dyk[ip] = dy[ip];
  }

  if (elast) {
    for (ip=1;ip<=mesh->np;ip++) {
      ppt = &mesh->point[ip];
      
      ipip = (4*maxNeig)*ip+4*(neigSparse[ip*(maxNeig+1)] -1);
      //printf("\n%d %d %d %lf %lf %lf",ipip,ip,neigSparse[ip*(maxNeig+1) + neigSparse[ip*(maxNeig+1)] ], K[ipip-1],K[ipip],K[ipip+1]);

      if (Kx[ipip] < eps && !(ppt->tag & MG_CRN)) {
        printf("\n\n!!!!!! ERROR JACOBI null diag : %d !!!!!!!",ip);
        abort();
      }

      rhsx = b[ip]+prc*dxk[ip];
      rhsy = b[ip+mesh->np]+prc*dyk[ip];

      for (ineig=0;ineig<neigSparse[ip*(maxNeig+1)];ineig++) {

        jp = neigSparse[ip*(maxNeig+1)+ineig+1];
        
        if (jp != ip) {
          rhsx -= Kx[(4*maxNeig)*ip+4*ineig]*dxk[jp];
          rhsy -= Kx[(4*maxNeig)*ip+4*ineig+3]*dyk[jp];
        }
        rhsx -= Kx[(4*maxNeig)*ip+4*ineig+1]*dyk[jp];
        rhsy -= Kx[(4*maxNeig)*ip+4*ineig+2]*dxk[jp];
      }

      /*bdry condition*/
      if (mesh->point[ip].tag & MG_CRN){ /*nomove*/
        Kx[ipip]  = tgv;
        Kx[ipip+3]  = tgv;
        rhsx = 0;
        rhsy = 0;
      } else if (mesh->point[ip].tag & M_DIRICHLET) {
        if(mesh->point[ip].n[0]*mesh->point[ip].n[0]+mesh->point[ip].n[1]*mesh->point[ip].n[1] > 1e-6)
          printf("dirichlet non homogeneous (norm %e ) not implemented ==> no move\n"
            ,sqrt(mesh->point[ip].n[0]*mesh->point[ip].n[0]+mesh->point[ip].n[1]*mesh->point[ip].n[1]));
        Kx[ipip] = tgv;
        if (aniso)
          Ky[ipip] = tgv;
        rhsx = 0;
        rhsy = 0;
      }

//      dx[ip] = rhsx/Kx[ipip];
//      dy[ip] = rhsy/Kx[ipip+3];

      /*apply slip bc : projection on the tangent : n ^ (v^n)*/
      if(ppt->tag & MG_BDY && !(ppt->tag & MG_CRN) && !(ppt->tag & M_DIRICHLET)) {
        alpha = 1;//0.001;
        tmpx = ppt->n[1]*ppt->n[1]*(alpha*rhsx/(Kx[ipip]+prc))
             - ppt->n[0]*ppt->n[1]*(alpha*rhsy/(Kx[ipip+3]+prc));
        tmpy = ppt->n[0]*ppt->n[0]*(alpha*rhsy/(Kx[ipip+3]+prc))
             - ppt->n[0]*ppt->n[1]*(alpha*rhsx/(Kx[ipip]+prc));
        dx[ip] =  tmpx;
        dy[ip] =  tmpy;
      } else {
        dx[ip] =  rhsx/(Kx[ipip]+prc);
        dy[ip] =  rhsy/(Kx[ipip+3]+prc);
      }   
    }
  }


 
  else {
    for (ip=1; ip<=mesh->np; ip++) {
      ppt = &mesh->point[ip];
      
      ipip = maxNeig*ip+neigSparse[ip*(maxNeig+1)]-1;

      if (Kx[ipip] < eps) {
        printf("\n\n!!!!!! ERROR JACOBI null diag X : %d !!!!!!!",ip);
        abort();
      }
      if (aniso)
        if (Ky[ipip] < eps) {
          printf("\n\n!!!!!! ERROR JACOBI null diag Y : %d !!!!!!!",ip);
          abort();
        }

      rhsx = 0;
      rhsy = 0;

      for (ineig=0;ineig<neigSparse[ip*(maxNeig+1)];ineig++) {
        jp = neigSparse[ip*(maxNeig+1)+ineig+1];
        rhsx += Kx[maxNeig*ip + ineig] * mesh->point[jp].c[0];
        if (aniso)
          rhsy += Ky[maxNeig*ip + ineig] * mesh->point[jp].c[1];
        else
          rhsy += Kx[maxNeig*ip + ineig] * mesh->point[jp].c[1];
      }

      /*bdry condition*/
      if ( mesh->point[ip].tag & MG_CRN ) { /*nomove*/
        Kx[ipip] = 1;
        if (aniso)
          Ky[ipip] = 1;
        rhsx = 0;
        rhsy = 0;
      } else if ( mesh->point[ip].tag & M_DIRICHLET ) { /*dirichlet non homogeneous*/
        if(mesh->point[ip].n[0]*mesh->point[ip].n[0]+mesh->point[ip].n[1]*mesh->point[ip].n[1] > 1e-6)
          printf("dirichlet non homogeneous (norm %e ) not implemented ==> no move\n"
            ,sqrt(mesh->point[ip].n[0]*mesh->point[ip].n[0]+mesh->point[ip].n[1]*mesh->point[ip].n[1]));
        Kx[ipip] = 1;
        if (aniso)
          Ky[ipip] = 1;
        rhsx = 0;
        rhsy = 0;
      }
      /* if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_LEFT ||  mesh->point[ip].ref == M_RIGHT))) */
      /*   rhsx = 0; */
      /* else if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_BOTTOM ||  mesh->point[ip].ref == M_TOP))) */
      /*   rhsy = 0; */
      /* else if (mesh->point[ip].tag & MG_CRN){ */
      /*   Kx[ipip] = tgv; */
      /*   if (aniso) */
      /*     Ky[ipip] = tgv; */
      /*   rhsx = 0; */
      /*   rhsy = 0; */
      /* } else if ( mesh->point[ip].tag & MG_BDY ){ */
      /*  printf("boum %d %d\n",ip,mesh->point[ip].ref); */
      /*  exit(0); */
      /* } */
     
      /*apply slip bc : projection on the tangent : n ^ (v^n)*/
      if(mesh->point[ip].tag & MG_BDY && !(mesh->point[ip].tag & MG_CRN) && !(mesh->point[ip].tag & M_DIRICHLET)) {
        alpha = 1;//0.001;
        tmpx = ppt->n[1]*ppt->n[1]*(alpha*rhsx/Kx[ipip])
             - ppt->n[0]*ppt->n[1]*(alpha*rhsy/Kx[ipip]);
        tmpy = ppt->n[0]*ppt->n[0]*(alpha*rhsy/Kx[ipip])
             - ppt->n[0]*ppt->n[1]*(alpha*rhsx/Kx[ipip]);
        dx[ip] = dx[ip] - tmpx;
        dy[ip] = dy[ip] - tmpy;
      } else {
        dx[ip] = dx[ip] - rhsx/Kx[ipip];
        if (aniso)
          dy[ip] = dy[ip] - rhsy/Ky[ipip];
        else
          dy[ip] = dy[ip] - rhsy/Kx[ipip];
      }      
    }
    
  }/*end else of if(elas)*/
}


/**
 * \param K FEM matrix of coefficients
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param normal0 element's normal vectors in the initial mesh
 * \param lamdba lamdba values in each point of mesh
 * \param neigSparse list of neighbor points of each point
 * \param maxNeig maximum number of neighbors for a point in the mesh
 *
 * Computes boundary condition term for matrix K (necessary for elasticity model in non rectangular domaines or rectangular domaines not parallels to the axis)
 */


void FMG_computeBC2d(double* K, MMG5_pMesh mesh,  double* normal0, double* lambda, int* neigSparse, int maxNeig) {

  int iloc,jloc,ineig, k, ia, ib, ip, jp, iel, count, list[2];
  double xa,ya,xb,yb,normal[2],Nnormal[2], length, add;
  MMG5_pEdge pedge;

  fprintf(stdout,"elasticity model doesn't work : mesh->na = 0 here so you need to change the way to do that\n");
  //exit(0);

  for(k=1; k<=mesh->na; k++) {
    iel = 0;//trEdges[k];

    pedge = &mesh->edge[k];
    ia = pedge->a;
    ib = pedge->b;

    xa = mesh->point[ia].c[0];
    ya = mesh->point[ia].c[1];
    xb = mesh->point[ib].c[0];
    yb = mesh->point[ib].c[1];

    length = sqrt(pow(xa-xb,2) + pow(ya-yb,2));

    for (iloc=0; iloc<3; iloc++)
      if (mesh->tria[iel].v[iloc] != ia && mesh->tria[iel].v[iloc] != ib) {
        normal[0] = normal0[6*iel + 2*iloc];
        normal[1] = normal0[6*iel + 2*iloc+1];
        break;
      }

    Nnormal[0] = normal[0]/length;
    Nnormal[1] = normal[1]/length;

    for (count=1; count<=2; count++) {
      if (count==1)
        ip = ia;
      else
        ip = ib;

      for (ineig=1;ineig<=neigSparse[ip*(maxNeig+1)];ineig++) {
        jp = neigSparse[ip*(maxNeig+1)+ineig];

        for (jloc=0; jloc<3; jloc++)
          if (mesh->tria[iel].v[jloc] == jp)
            break;

        if (mesh->point[jp].tag & MG_BDY || mesh->point[jp].tag & MG_CRN) {
          add = length/4./mesh->tria[iel].qual*
            Nnormal[0]*pow(Nnormal[1],2)*(lambda[ia]+lambda[ib])*
            (normal0[6*iel+2*jloc]*Nnormal[1] - normal0[6*iel+2*jloc+1]*Nnormal[0]);

    //          printf ("add %lf\n",add);

          K[(4*maxNeig)*ip+4*(ineig-1)] += add;
          K[(4*maxNeig)*ip+4*(ineig-1)+1] -= add;
          K[(4*maxNeig)*ip+4*(ineig-1)+2] -= add;
          K[(4*maxNeig)*ip+4*(ineig-1)+3] += add;
 
        }

      }         

    }

  }


}



void FMG_computeJacobiResidual2d(MMG5_pMesh mesh, double *Kx, double *Ky, double *res, int *neigSparse, int maxNeig, int aniso) {

  int ip, jp, ineig;
  double ax, ay;

  res[0] = 0.;
  res[1] = 0.;
  for (ip=1; ip<=mesh->np; ip++) {
    ax = 0.;
    ay = 0.;
    for (ineig=0;ineig<neigSparse[ip*(maxNeig+1)];ineig++) {
      jp = neigSparse[ip*(maxNeig+1)+ineig+1];
      if (!( (ip == jp) && (mesh->point[ip].tag & MG_CRN))) {
        ax += Kx[maxNeig*ip + ineig] * mesh->point[jp].c[0];
        if (aniso)
          ay += Ky[maxNeig*ip + ineig] * mesh->point[jp].c[1];
        else
          ay += Kx[maxNeig*ip + ineig] * mesh->point[jp].c[1];
        //  printf("\n %d %d %lf %lf",ip,jp,Kx[maxNeig*ip + ineig],mesh->point[jp].c[0]);
      }
    }
    res[0] += ax*ax;
    res[1] += ay*ay;
  }
  
  res[0] = sqrt(res[0]);
  res[1] = sqrt(res[1]);
}



