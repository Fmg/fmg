/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"

/**
 * \param mesh pointer toward the mesh structure.
 *
 * Creates the correct reference for the surface points of a spherical domain
 *
 */


void FMG_correctRefCirc3d (MMG5_pMesh mesh) {

  int it, iloc;

  for (it=1; it<=mesh->nt; it++)
    for (iloc=0; iloc<3; iloc++)
      mesh->point[mesh->tria[it].v[iloc]].tag = MG_CRN; 
}


/**
 * \param mesh pointer toward the mesh structure.
 *
 * Corrects the edges' references regarding to M_LEFT, M_RIGHT, M_TOP, M_BOTTOM, M_FRONT and M_BACK .
 *
 */

int FMG_correctRef3d(MMG5_pMesh mesh) {
  int i, j, ia, tr, flag1, flag2, flag3;
  int vert[24], map[6], *imap, max,iref;
  double eps = 1e-12;

  for (i=0;i<24;i++)
    vert[i] = -1;

  // identify one example of each surface triangle and save its ref and points
  tr = 0;
  max = -1;
  for (ia=1;ia<=mesh->nt;ia++) {
    for(iref=0 ; iref<tr ; iref++) {
      if (mesh->tria[ia].ref == vert[4*iref]) break;
    }
    if(iref==tr) {
      for (i=tr;i<6;i++)
        vert[4*i] = mesh->tria[ia].ref;
      vert[4*tr+1] = mesh->tria[ia].v[0];
      vert[4*tr+2] = mesh->tria[ia].v[1];
      vert[4*tr+3] = mesh->tria[ia].v[2];
      tr++;
      if (mesh->tria[ia].ref > max)
        max = mesh->tria[ia].ref;
    }
     if (tr>5) break;
  }
  if (tr<=5) {
    printf("\n !! Warning3d : incorrect edges references. Fmg recompute them.\n");
    return(0);
  }

//for (i=0;i<4;i++)
//  printf(" \n * %d %d %d",vert[3*i],vert[3*i+1],vert[3*i+2]);

  // identify: sides or top/bottom
  flag1 = 0;
  flag2 = 0;
  flag3 = 0;
  for (i=0;i<4;i++) {
    if (fabs(mesh->point[vert[4*i+1]].c[0] - mesh->point[vert[4*i+2]].c[0] ) < eps &&
        fabs(mesh->point[vert[4*i+1]].c[0] - mesh->point[vert[4*i+3]].c[0] ) < eps ) {
      if (flag1 == 0) {
        map[M_FRONT-1] = i;
        flag1++;
      }
      else
        map[M_BACK-1] = i;
    }
    else if (fabs(mesh->point[vert[4*i+1]].c[1] - mesh->point[vert[4*i+2]].c[1] ) < eps &&
             fabs(mesh->point[vert[4*i+1]].c[1] - mesh->point[vert[4*i+3]].c[1] ) < eps){
      if (flag2 == 0) {
        map[M_LEFT-1] = i;
        flag2 ++;
      }
      else
        map[M_RIGHT-1] = i;
    }
    else if (fabs(mesh->point[vert[4*i+1]].c[2] - mesh->point[vert[4*i+2]].c[2] ) < eps &&
             fabs(mesh->point[vert[4*i+1]].c[2] - mesh->point[vert[4*i+3]].c[2] ) < eps){
      if (flag3 == 0) {
        map[M_TOP-1] = i;
        flag2 ++;
      }
      else
        map[M_BOTTOM-1] = i;
    }
  }

  // front x back
  i = 4*map[M_FRONT-1];
  j = 4*map[M_BACK-1];
  if (mesh->point[vert[i+1]].c[0] < mesh->point[vert[j+1]].c[0] ) {
    map[M_BACK-1] = vert[i];
    map[M_FRONT-1] = vert[j];
  }
  else {
    map[M_BACK-1] = vert[j];
    map[M_FRONT-1] = vert[i];
  }

  // left x right
  i = 4*map[M_LEFT-1];
  j = 4*map[M_RIGHT-1];
  if (mesh->point[vert[i+1]].c[1] < mesh->point[vert[j+1]].c[1] ) {
    map[M_LEFT-1] = vert[i];
    map[M_RIGHT-1] = vert[j];
  }
  else {
    map[M_LEFT-1] = vert[j];
    map[M_RIGHT-1] = vert[i];
  }

  // top x bottom
  i = 4*map[M_TOP-1];
  j = 4*map[M_BOTTOM-1];
  if (mesh->point[vert[i+1]].c[2] < mesh->point[vert[j+1]].c[2] ) {
    map[M_BOTTOM-1] = vert[i];
    map[M_TOP-1] = vert[j];
  }
  else {
    map[M_BOTTOM-1] = vert[j];
    map[M_TOP-1] = vert[i];
  }

//printf("\n ****** %d %d %d %d %d",max, map[M_LEFT-1], map[M_RIGHT-1],map[M_TOP-1],map[M_BOTTOM-1]);

  // inverse map
  MMG5_SAFE_CALLOC(imap,max+1,int,return 0);
  imap[map[M_FRONT-1]] = M_FRONT;
  imap[map[M_BACK-1]] = M_BACK;
  imap[map[M_LEFT-1]] = M_LEFT;
  imap[map[M_RIGHT-1]] = M_RIGHT; 
  imap[map[M_TOP-1]] = M_TOP;
  imap[map[M_BOTTOM-1]] = M_BOTTOM;

  // correct all ref
  for (ia=1;ia<=mesh->nt;ia++)
    mesh->edge[ia].ref = imap[mesh->edge[ia].ref];

  MMG5_SAFE_FREE(imap);
  return(1);
}




/**
 * \param mesh pointer toward the mesh structure.
 *
 * Corrects the edges' references regarding to M_LEFT, M_RIGHT, M_TOP and M_BOTTOM .
 *
 */

int FMG_correctRef2d(MMG5_pMesh mesh) {
  MMG5_pPoint pp1,pp2;
  int i, j, ia, edge, flag1, flag2;
  int vert[12], map[4], *imap, max,iref;
  double eps = 1e-12;

  for (i=0;i<12;i++)
    vert[i] = -1;

  // identify one example of each edge and save its ref and extrema
  edge = 0;
  max = -1;
  for (ia=1;ia<=mesh->na;ia++) {
    for(iref=0 ; iref<edge ; iref++) {
      if (mesh->edge[ia].ref == vert[3*iref]) break;
    }
    if(iref==edge) {
      for (i=edge;i<4;i++)
        vert[3*i] = mesh->edge[ia].ref;
      vert[3*edge+1] = mesh->edge[ia].a;
      vert[3*edge+2] = mesh->edge[ia].b;
      edge++;
      if (mesh->edge[ia].ref > max)
        max = mesh->edge[ia].ref;
    }
    if (edge>3) break;
  }

  if (edge<=3) {
    printf("\n !! Warning2d : incorrect edges references. Fmg recompute them.\n");
    return(0);
  }

  /* for (i=0;i<4;i++) { */
  /*   printf(" \n * i=%d -- %d %d %d\n",i,vert[3*i],vert[3*i+1],vert[3*i+2]); */
  /* } */
  // identify: sides or top/bottom
  flag1 = 0;
  flag2 = 0;
  for (i=0;i<4;i++) {
    pp1 = &mesh->point[vert[3*i+1]];
    pp2 = &mesh->point[vert[3*i+2]];
    if ((fabs(pp1->c[0] - pp2->c[0] ) < eps)) {
      if (flag1 == 0) {
        map[M_LEFT-1] = i;
        flag1++;
      }
      else
        map[M_RIGHT-1] = i;
    }
    else {
      if (flag2 == 0) {
        map[M_TOP-1] = i;
        flag2++;
      }
      else
        map[M_BOTTOM-1] = i;
    }
  }

  // left x right
  i = 3*map[M_LEFT-1];
  j = 3*map[M_RIGHT-1];
  if (mesh->point[vert[i+1]].c[0] < mesh->point[vert[j+1]].c[0] ) {
    map[M_LEFT-1] = vert[i];
    map[M_RIGHT-1] = vert[j];
  }
  else {
    map[M_LEFT-1] = vert[j];
    map[M_RIGHT-1] = vert[i];
  }

  // top x bottom
  i = 3*map[M_TOP-1];
  j = 3*map[M_BOTTOM-1];
  if (mesh->point[vert[i+1]].c[1] < mesh->point[vert[j+1]].c[1] ) {
    map[M_BOTTOM-1] = vert[i];
    map[M_TOP-1] = vert[j];
  }
  else {
    map[M_BOTTOM-1] = vert[j];
    map[M_TOP-1] = vert[i];
  }

  //printf("\n ****** %d %d %d %d %d",max, map[M_LEFT-1], map[M_RIGHT-1],map[M_TOP-1],map[M_BOTTOM-1]);
  // inverse map
  MMG5_SAFE_CALLOC(imap,max+1,int,return 0);
  imap[map[M_LEFT-1]] = M_LEFT;
  imap[map[M_RIGHT-1]] = M_RIGHT; 
  imap[map[M_TOP-1]] = M_TOP;
  imap[map[M_BOTTOM-1]] = M_BOTTOM;

  // correct all ref
  for (ia=1;ia<=mesh->na;ia++)
    mesh->edge[ia].ref = imap[mesh->edge[ia].ref];

  MMG5_SAFE_FREE(imap);
  return(1);
}


/**
 * \param mesh pointer toward the mesh structure.
 *
 * Verifies if the mesh points already have border references ( M_LEFT, M_RIGHT, M_TOP, M_BOTTOM, M_FRONT and M_BACK), and, if not, creates them, supposing the domaine is a parallepiped which sides are parallel to the axis.
 *
 */

void FMG_createRef3d(MMG5_pMesh mesh) {

  int ip, iref, flag, cntRef, refs[7];
  double xmin, xmax, ymin, ymax, zmin, zmax;

  // Verify if the (7) references already exist
  cntRef = 1;
  refs[0] = mesh->point[1].ref;
  for (ip=2;ip<=mesh->np;ip++) {
    flag = 0;
    for (iref=0; iref<cntRef; iref++) {
      if (mesh->point[ip].ref == refs[iref]) {
        flag = 1;
        break;
      } 
    }
    if (!flag) {
      cntRef++;
      refs[cntRef-1] = mesh->point[ip].ref;
    }
    if (cntRef == 7)
      break;
  }

  // Creer les references si necessaire
  if (cntRef != 7) {
    // identify coordonnes extrem.
    xmin = mesh->point[1].c[0];
    xmax = mesh->point[1].c[0];
    ymin = mesh->point[1].c[1];
    ymax = mesh->point[1].c[1];
    zmin = mesh->point[1].c[2];
    zmax = mesh->point[1].c[2];
 
    for (ip=2; ip<=mesh->np; ip++) {
      if (mesh->point[ip].c[0] < xmin)
        xmin = mesh->point[ip].c[0];
      if (mesh->point[ip].c[0] > xmax)
        xmax = mesh->point[ip].c[0];
      if (mesh->point[ip].c[1] < ymin)
        ymin = mesh->point[ip].c[1];
      if (mesh->point[ip].c[1] > ymax)
        ymax = mesh->point[ip].c[1]; 
      if (mesh->point[ip].c[2] < zmin)
        zmin = mesh->point[ip].c[2];
      if (mesh->point[ip].c[2] > zmax)
        zmax = mesh->point[ip].c[2];

    }

    // give a ref for each point
    for (ip=1; ip<=mesh->np; ip++) {
      mesh->point[ip].ref = M_INTERN;
      if (fabs(mesh->point[ip].c[0] - xmin) < FMG_eps) {
        if (fabs(mesh->point[ip].c[1] - ymin) < FMG_eps || fabs(mesh->point[ip].c[1] - ymax) < FMG_eps)
          mesh->point[ip].ref = M_LINEZ;
        else if (fabs(mesh->point[ip].c[2] - zmin) < FMG_eps || fabs(mesh->point[ip].c[2] - zmax) < FMG_eps)
          mesh->point[ip].ref = M_LINEY;
        else
          mesh->point[ip].ref = M_BACK;
        mesh->point[ip].tag = MG_BDY; 
      }
      else if (fabs(mesh->point[ip].c[0] - xmax) < FMG_eps) {
        if (fabs(mesh->point[ip].c[1] - ymin) < FMG_eps || fabs(mesh->point[ip].c[1] - ymax) < FMG_eps)
          mesh->point[ip].ref = M_LINEZ;
        else if (fabs(mesh->point[ip].c[2] - zmin) < FMG_eps || fabs(mesh->point[ip].c[2] - zmax) < FMG_eps)
          mesh->point[ip].ref = M_LINEY;
        else
          mesh->point[ip].ref = M_FRONT;
        mesh->point[ip].tag = MG_BDY; 
      }
      else if(fabs(mesh->point[ip].c[1] - ymin) < FMG_eps) {
        if (fabs(mesh->point[ip].c[0] - xmin) < FMG_eps || fabs(mesh->point[ip].c[0] - xmax) < FMG_eps)
          mesh->point[ip].ref = M_LINEZ;
        else if (fabs(mesh->point[ip].c[2] - zmin) < FMG_eps || fabs(mesh->point[ip].c[2] - zmax) < FMG_eps)
          mesh->point[ip].ref = M_LINEX;
        else
          mesh->point[ip].ref = M_LEFT;
        mesh->point[ip].tag = MG_BDY; 
      }
      else if (fabs(mesh->point[ip].c[1] - ymax) < FMG_eps) {
        if (fabs(mesh->point[ip].c[0] - xmin) < FMG_eps || fabs(mesh->point[ip].c[0] - xmax) < FMG_eps)
          mesh->point[ip].ref = M_LINEZ;
        else if (fabs(mesh->point[ip].c[2] - zmin) < FMG_eps || fabs(mesh->point[ip].c[2] - zmax) < FMG_eps)
          mesh->point[ip].ref = M_LINEX;
        else
          mesh->point[ip].ref = M_RIGHT;
        mesh->point[ip].tag = MG_BDY; 
      }
      else if(fabs(mesh->point[ip].c[2] - zmin) < FMG_eps) {
        if (fabs(mesh->point[ip].c[0] - xmin) < FMG_eps || fabs(mesh->point[ip].c[0] - xmax) < FMG_eps)
          mesh->point[ip].ref = M_LINEY;
        else if (fabs(mesh->point[ip].c[1] - ymin) < FMG_eps || fabs(mesh->point[ip].c[1] - ymax) < FMG_eps)
          mesh->point[ip].ref = M_LINEX;
        else
          mesh->point[ip].ref = M_BOTTOM;
        mesh->point[ip].tag = MG_BDY; 
      }
      else if (fabs(mesh->point[ip].c[2] - zmax) < FMG_eps) {
        if (fabs(mesh->point[ip].c[0] - xmin) < FMG_eps || fabs(mesh->point[ip].c[0] - xmax) < FMG_eps)
          mesh->point[ip].ref = M_LINEY;
        else if (fabs(mesh->point[ip].c[1] - ymin) < FMG_eps || fabs(mesh->point[ip].c[1] - ymax) < FMG_eps)
          mesh->point[ip].ref = M_LINEX;
        else
          mesh->point[ip].ref = M_TOP;
        mesh->point[ip].tag = MG_BDY; 
      }

      if ((mesh->point[ip].tag & MG_BDY) &&
          (fabs(mesh->point[ip].c[0] - xmin) < FMG_eps || fabs(mesh->point[ip].c[0] - xmax) < FMG_eps ) &&
          (fabs(mesh->point[ip].c[1] - ymin) < FMG_eps || fabs(mesh->point[ip].c[1] - ymax) < FMG_eps ) &&
          (fabs(mesh->point[ip].c[2] - zmin) < FMG_eps|| fabs(mesh->point[ip].c[2] - zmax) < FMG_eps ) )
        mesh->point[ip].tag = MG_CRN;
    }
  }
  
}


/**
 * \param mesh pointer toward the mesh structure.
 *
 * Verifies if the mesh points already have border references ( M_LEFT, M_RIGHT, M_TOP and M_BOTTOM), and, if not, creates them, supposing a rectangular domaine which sides are parallel to the axis.
 *
 */

void FMG_createRef2d(MMG5_pMesh mesh) {
  MMG5_pPoint pp1,pp2;
  int ip, iref, flag, cntRef, refs[5];
  int ia;
  double xmin, xmax, ymin, ymax;
  if(mesh->na) {
    for (ia=1;ia<=mesh->na;ia++) {
      pp1 = &mesh->point[mesh->edge[ia].a];
      pp2 = &mesh->point[mesh->edge[ia].b];
      if(fabs(pp1->c[0]-pp2->c[0]) < FMG_eps) {
        if(!pp1->ref) {
          pp1->ref = M_LEFT;
          pp1->tag = MG_BDY;
        } else if(pp1->ref!= M_LEFT) {
          pp1->tag = MG_CRN;
        }
        if(!pp2->ref) {
          pp2->ref = M_LEFT;
          pp2->tag = MG_BDY;
        } else if(pp2->ref!= M_LEFT) {
          pp2->tag = MG_CRN;
        }
      } else if(fabs(pp1->c[1]-pp2->c[1]) < FMG_eps) {
        if(!pp1->ref) {
          pp1->ref = M_TOP;
          pp1->tag = MG_BDY;
        } else if(pp1->ref!= M_TOP) {
          pp1->tag = MG_CRN;
        }
        if(!pp2->ref) {
          pp2->ref = M_TOP;
          pp2->tag = MG_BDY;
        } else if(pp2->ref!= M_TOP){
          pp2->tag = MG_CRN;
        }
      } else {
        printf("bdry edge number %d (%d %d) ignored \n",ia,mesh->edge[ia].a,mesh->edge[ia].b);
        pp1->tag = MG_CRN; 
        pp2->tag = MG_CRN;
      }
    }
  } else {
    // Verify if the (5) references already exist
    cntRef = 1;
    refs[0] = mesh->point[1].ref;
    for (ip=2;ip<=mesh->np;ip++) {
      flag = 0;
      for (iref=0; iref<cntRef; iref++) {
        if (mesh->point[ip].ref == refs[iref]) {
          flag = 1;
          break;
        } 
      }
      if (!flag) {
        cntRef++;
        refs[cntRef-1] = mesh->point[ip].ref;
      }
      if (cntRef == 5)
        break;
    }

    cntRef = 1;
    // Creer les references si necessaire
    if (cntRef != 5) {
      // identify coordonnes extrem.
      xmin = mesh->point[1].c[0];
      xmax = mesh->point[1].c[0];
      ymin = mesh->point[1].c[1];
      ymax = mesh->point[1].c[1];
  
      for (ip=2; ip<=mesh->np; ip++) {
        if (mesh->point[ip].c[0] < xmin)
          xmin = mesh->point[ip].c[0];
        if (mesh->point[ip].c[0] > xmax)
          xmax = mesh->point[ip].c[0];
        if (mesh->point[ip].c[1] < ymin)
          ymin = mesh->point[ip].c[1];
        if (mesh->point[ip].c[1] > ymax)
          ymax = mesh->point[ip].c[1];
      }

      // give a ref for each point
      for (ip=1; ip<=mesh->np; ip++) {
        mesh->point[ip].ref = 6;
        if (fabs(mesh->point[ip].c[0] - xmin) < FMG_eps) {
          mesh->point[ip].ref = M_LEFT;
          mesh->point[ip].tag = MG_BDY;
        }
        else if (fabs(mesh->point[ip].c[0] - xmax) < FMG_eps) {
          mesh->point[ip].ref = M_RIGHT;
          mesh->point[ip].tag = MG_BDY;
        }
        if (fabs(mesh->point[ip].c[1] - ymin) < FMG_eps) {
          mesh->point[ip].ref = M_BOTTOM;
          mesh->point[ip].tag = MG_BDY;
        }
        else if (fabs(mesh->point[ip].c[1] - ymax) < FMG_eps) {
          mesh->point[ip].ref = M_TOP;
          mesh->point[ip].tag = MG_BDY;
        }

        if ((mesh->point[ip].tag & MG_BDY) &&
            (fabs(mesh->point[ip].c[0] - xmin) < FMG_eps || fabs(mesh->point[ip].c[0] - xmax) < FMG_eps ) &&
            (fabs(mesh->point[ip].c[1] - ymin) < FMG_eps || fabs(mesh->point[ip].c[1] - ymax) < FMG_eps ) )
          mesh->point[ip].tag = MG_CRN;

      }
    }
  }

}


void FMG_definitiveRef3d(MMG5_pMesh mesh, FMG_pInfo param) {
  MMG5_pTetra  pt;
  MMG5_pxTetra pxt;
  MMG5_pPoint pp1,pp2,pp3,ppt;
  int         i,k,ie, ip, pa, pb,ib,ref,*adja;


  if (param->circDom) {
    /*all the bdry nodes will be untouched so put MG_CRN tag on them*/
    for (ip=1; ip<=mesh->np; ip++) {
      ppt = &mesh->point[ip];
      if( !(ppt->tag & MG_BDY) ) continue;
      ppt->tag = MG_CRN;
    }
  } else {
    /*if there are some defined reference*/
    if(param->nbc) {
      for (k=1; k<=mesh->ne; k++) {
        pt = &mesh->tetra[k];
        if ( !MG_EOK(pt) )  continue;
        if ( !pt->xt ) continue;

        pxt = &mesh->xtetra[pt->xt];
        adja = &mesh->adja[4*(k-1)+1];
        
        for (i=0; i<4; i++) {
          if(adja[i]) continue;
          ref = pxt->ref[i];
          /*find ref on the list*/
          for (ib = 0 ; ib < param->nbc ; ib++) {
            if(ref==param->bc[ib].ref) break;
          }
          pp1 = &mesh->point[pt->v[MMG5_idir[i][0]]];
          pp2 = &mesh->point[pt->v[MMG5_idir[i][1]]];
          pp3 = &mesh->point[pt->v[MMG5_idir[i][2]]];
          if(ib==param->nbc || param->bc[ib].isslip) {
            pp1->tag = MG_BDY;
            pp2->tag = MG_BDY;
            pp3->tag = MG_BDY;
          } else {
            if(pp1->ref && ref!=pp1->ref) {
              pp1->tag = MG_CRN;
            } else {
              pp1->tag = M_DIRICHLET;
              pp1->ref = ref;
              pp1->n[0] = param->bc[ib].dep[0];
              pp1->n[1] = param->bc[ib].dep[1];
              pp1->n[2] = param->bc[ib].dep[2];
            }
            if(pp2->ref && ref!=pp2->ref) {
              pp2->tag = MG_CRN;
            } else {
              pp2->tag = M_DIRICHLET;
              pp2->ref = ref;
              pp2->n[0] = param->bc[ib].dep[0];
              pp2->n[1] = param->bc[ib].dep[1];
              pp2->n[2] = param->bc[ib].dep[2];
            }
            if(pp3->ref && ref!=pp3->ref) {
              pp3->tag = MG_CRN;
            } else {
              pp3->tag = M_DIRICHLET;
              pp3->ref = ref;
              pp3->n[0] = param->bc[ib].dep[0];
              pp3->n[1] = param->bc[ib].dep[1];
              pp3->n[2] = param->bc[ib].dep[2];
            }    
          }
        }
      }
    } else { /*!param->nbc*/
      /*put slip bc everywhere : tag already ok thanks to analysis (MG_CRN and MG_BDY)*/
    }
  }

}
void FMG_definitiveRef2d(MMG5_pMesh mesh, FMG_pInfo param) {
  MMG5_pTria  pt;
  MMG5_pPoint pp1,pp2,ppt;
  int         i,k,ie, ip, pa, pb,ib,ref,*adja;


  if (param->circDom) {
    /*all the bdry nodes will be untouched so put MG_CRN tag on them*/
    for (ip=1; ip<=mesh->np; ip++) {
      ppt = &mesh->point[ip];
      if( !(ppt->tag & MG_BDY) ) continue;
      ppt->tag = MG_CRN;
    }
   } else {
    /*if there are some defined reference*/
    if(param->nbc) {
      for (k=1; k<=mesh->nt; k++) {
        pt = &mesh->tria[k];
        if ( !MG_EOK(pt) )  continue;

        adja = &mesh->adja[3*(k-1)+1];
        for (i=0; i<3; i++) {
          if(adja[i]) continue;
          ref = pt->edg[i];
          /*find ref on the list*/
          for (ib = 0 ; ib < param->nbc ; ib++) {
            if(ref==param->bc[ib].ref) break;
          }
          pp1 = &mesh->point[pt->v[MMG2D_iare[i][0]]];
          pp2 = &mesh->point[pt->v[MMG2D_iare[i][1]]];
          if(ib==param->nbc || param->bc[ib].isslip) {
            pp1->tag = MG_BDY;
            pp2->tag = MG_BDY;
          } else {
            if(pp1->ref && ref!=pp1->ref) {
              pp1->tag = MG_CRN;
            } else {
              pp1->tag = M_DIRICHLET;
              pp1->ref = ref;
              pp1->n[0] = param->bc[ib].dep[0];
              pp1->n[1] = param->bc[ib].dep[1];
            }
            if(pp2->ref && ref!=pp2->ref) {
              pp2->tag = MG_CRN;
            } else {
              pp2->tag = M_DIRICHLET;
              pp2->ref = ref;
              pp2->n[0] = param->bc[ib].dep[0];
              pp2->n[1] = param->bc[ib].dep[1];
            }
            
          }
        }
      }
    } else { /*!param->nbc*/
      /*put slip bc everywhere : tag already ok thanks to analysis (MG_CRN and MG_BDY)*/
    }
  }

}
