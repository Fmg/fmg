/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"
#include "string.h"

/**
 * \file API_functionsf_3d.c
 * \brief Fortran API functions 3d for FMG library.
 * \author Cecile Dobrzynski (Inria / IMB, Université de Bordeaux)
 * \version 5
 * \date 07 2015
 * \copyright GNU Lesser General Public License.
 * \note Please, refer to the \ref fmg/libfmg.h file for functions
 * documentation.
 *
 * Define the Fortran API functions 3d for FMG library: adds function
 * definitions with upcase, underscore and double underscore to match
 * any fortran compiler.
 *
 */
/**
 * See \ref FMG_Init_fmg3d function in libfmg.h file.
 */
FORTRAN_NAME(FMG_INIT_FMG3D, fmg_init_fmg3d,(MMG5_pMesh *mesh, FMG_pData *fmgdata,int *retval
    ),(mesh,fmgdata,retval)) {

  *retval = FMG_Init_fmg3d(mesh,fmgdata);
  return;
}
/**
 * See \ref FMG_Start_fmg3d function in libfmg.h file.
 */
FORTRAN_NAME(FMG_START_FMG3D, fmg_start_fmg3d,
    (MMG5_pMesh *mesh, MMG5_pSol *sol, FMG_pData *fmgdata,int *retval
    ),(mesh,sol,fmgdata,retval )) {
  /* data allocation */
  //if ( *param )  free(*param);
  *retval = FMG_Start_fmg3d(*mesh,*sol,*fmgdata);
  return;
}
/**
 * See \ref FMG_End_fmg3d function in libfmg.h file.
 */
FORTRAN_NAME(FMG_END_FMG3D, fmg_end_fmg3d,(MMG5_pMesh *mesh, FMG_pData *fmgdata
    ),(mesh,fmgdata
    )) { 
  FMG_End_fmg3d(*mesh,*fmgdata);
  return;
}
