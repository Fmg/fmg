/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"
/**
 * \param mesh pointer toward the mesh structure.
 * \param sol pointer toward the sol structure
 * \param x0 x-coordinate at time n
 * \param y0 y-coordinate at time n
 * \param sol0 sol at time n
 * \return 
 *
 * Compute the variable projection
 */
int FMG_ALE_projection(MMG5_pMesh mesh,MMG5_pSol sol,double* x0,double* y0,double* sol0) {
  MMG5_pTria  pt;
  MMG5_pPoint ppt,ppa,ppb;
  double      dbarx,dbary,dx[3],dy[3],midx,midy,n[2],huab,vab,ua,ub;
  double      ca[2],cb[2],cc[2],area,dmidx,dmidy,bary,barx,dd;
  int         k,i,j,ia,ib,isol;

  /*put 0 in mesh->point/n[0] to store sum phi^k*/
  /*put 0 in mesh->point.n[1] to store the area of FV cells*/
  for(k=1 ; k<=mesh->np ; k++) {
    ppt = &mesh->point[k];
    ppt->n[0] = 0.;
    ppt->n[1] = 0.;
    /*initialisation of sol at time n+1*/
    for(isol=0 ; isol<sol->size ; isol++) {
      sol->m[isol*(mesh->np+1) + k] = sol0[isol*(mesh->np+1) + k];
    }
  }

  /*computation of the C_i^(n+1)*/
  for(k=1 ; k<=mesh->nt ; k++) {
    pt = &mesh->tria[k];
    /*for each vertices*/
    for(i=0 ; i<3 ; i++) {
      ppt = &mesh->point[pt->v[i]];
      /*dx*/
      dx[i] = ppt->c[0] - x0[pt->v[i]];
      dy[i] = ppt->c[1] - y0[pt->v[i]];
    }
    /*area of the FV cells at tn+1*/
    ca[0] = x0[pt->v[0]] + dx[0];
    ca[1] = y0[pt->v[0]] + dy[0];
    cb[0] = x0[pt->v[1]] + dx[1];
    cb[1] = y0[pt->v[1]] + dy[1];
    cc[0] = x0[pt->v[2]] + dx[2];
    cc[1] = y0[pt->v[2]] + dy[2];
    area =  MMG2D_quickarea(&ca[0],&cb[0],&cc[0])/3.;
    mesh->point[pt->v[0]].n[1]+= area;
    mesh->point[pt->v[1]].n[1]+= area;
    mesh->point[pt->v[2]].n[1]+= area;
  }

  /*for each point i : sum of phi_i^K */
  for(k=1 ; k<=mesh->nt ; k++) {
    pt = &mesh->tria[k];
    /*barycenter at time tn+1/2*/
    dbarx = dbary = barx = bary = 0;
    /*for each vertices*/
    for(i=0 ; i<3 ; i++) {
      ppt = &mesh->point[pt->v[i]];
      /*dx*/
      dx[i] = ppt->c[0] - x0[pt->v[i]];
      dy[i] = ppt->c[1] - y0[pt->v[i]];

      /*barycenter at time n+1/2*/
      barx += x0[pt->v[i]] + 0.5*dx[i];
      bary += y0[pt->v[i]] + 0.5*dy[i];

      /*displacement in barycenter (xn+1-xn) at time n*/
      dbarx += dx[i];
      dbary += dy[i];

    }
    dbarx /= 3.;
    dbary /= 3.;
    barx  /= 3.;
    bary  /= 3.;

   
    for(i=0 ; i<3 ; i++) {
      ppt = &mesh->point[pt->v[i]];
      ia = pt->v[MMG2D_inxt[i]];
      ib = pt->v[MMG2D_inxt[i+1]];
      ppa = &mesh->point[ia];
      ppb = &mesh->point[ib];
      /*normal at t^(n+1/2)*/
      n[0] =  (ppa->c[1]-ppb->c[1])-0.5*(dy[MMG2D_inxt[i]]-dy[MMG2D_inxt[i+1]]);
      n[1] = -(ppa->c[0]-ppb->c[0])+0.5*(dx[MMG2D_inxt[i]]-dx[MMG2D_inxt[i+1]]);
      for(isol=0 ; isol<sol->size ; isol++) {
        /*1/3 * 1/|C_i^(n+1)|*/
        for(j=0 ; j<3 ; j++) {
          dd = 1./mesh->point[pt->v[j]].n[1];
          dd /= 3.;
          /*u_j^(n+1) = u_j^n + sum_i (1/3 * 1/C_j^(n+1) * 0.5* v_bary * n_i * u_i^n )*/
          sol->m[isol*(mesh->np+1) + pt->v[j]] += dd * 0.5*(n[0]*dbarx + n[1]*dbary)*sol0[isol*(mesh->np+1) + pt->v[i]];
        }
      }
      
    }

  }

  return(1);

}
/* int FMG_ALE_projection(MMG5_pMesh mesh,MMG5_pSol sol,double* x0,double* y0,double* sol0) { */
/*   MMG5_pTria  pt; */
/*   MMG5_pPoint ppt; */
/*   double      dbarx,dbary,dx[3],dy[3],midx,midy,n[2],huab,vab,ua,ub; */
/*   double      ca[2],cb[2],cc[2],area,dmidx,dmidy,bary,barx,dd; */
/*   int         k,i,ia,ib,isol; */

/*   /\*put 0 in mesh->point/n[0] to store sum phi^k*\/ */
/*   /\*put 0 in mesh->point.n[1] to store the area of FV cells*\/ */
/*   for(k=1 ; k<=mesh->np ; k++) { */
/*     ppt = &mesh->point[k]; */
/*     ppt->n[0] = 0.; */
/*     ppt->n[1] = 0.; */
/*   } */

/*   /\*for each point i : sum of phi_i^K *\/ */
/*   for(k=1 ; k<=mesh->nt ; k++) { */
/*     pt = &mesh->tria[k]; */
/*     /\*barycenter at time tn+1/2*\/ */
/*     dbarx = dbary = barx = bary = 0; */
/*     /\*for each vertices*\/ */
/*     for(i=0 ; i<3 ; i++) { */
/*       ppt = &mesh->point[pt->v[i]]; */
/*       /\*dx*\/ */
/*       dx[i] = ppt->c[0] - x0[pt->v[i]]; */
/*       dy[i] = ppt->c[1] - y0[pt->v[i]]; */

/*       /\*barycenter at time n+1/2*\/ */
/*       barx += x0[pt->v[i]] + 0.5*dx[i]; */
/*       bary += y0[pt->v[i]] + 0.5*dy[i]; */

/*       /\*displacement in barycenter (xn+1-xn) at time n*\/ */
/*       dbarx += dx[i]; */
/*       dbary += dy[i]; */

/*     } */
/*     dbarx /= 3.; */
/*     dbary /= 3.; */
/*     barx  /= 3.; */
/*     bary  /= 3.; */

/*     /\*area of the FV cells at tn+1*\/ */
/*     ca[0] = x0[pt->v[0]] + dx[0]; */
/*     ca[1] = y0[pt->v[0]] + dy[0]; */
/*     cb[0] = x0[pt->v[1]] + dx[1]; */
/*     cb[1] = y0[pt->v[1]] + dy[1]; */
/*     cc[0] = x0[pt->v[2]] + dx[2]; */
/*     cc[1] = y0[pt->v[2]] + dy[2]; */
/*     area =  MMG2D_quickarea(&ca[0],&cb[0],&cc[0])/3.; */
/*     mesh->point[pt->v[0]].n[1]+= area; */
/*     mesh->point[pt->v[1]].n[1]+= area; */
/*     mesh->point[pt->v[2]].n[1]+= area; */
  


/*     /\*for each edge*\/ */
/*     for(i=0 ; i<3 ; i++) { */
/*       ia = pt->v[MMG2D_iare[i][0]]; */
/*       ib = pt->v[MMG2D_iare[i][1]]; */

/*       /\********* mesh velocity : 0.5*(dM + dbar)*niaib^(n+1/2)*\/ */
/*       /\**        middle of the edge at tn+1/2*\/ */
/*       midx = 0.25*(mesh->point[ia].c[0] + x0[ia] */
/*                  + mesh->point[ib].c[0] + x0[ib]); */
/*       midy = 0.25*(mesh->point[ia].c[1] + y0[ia] */
/*                  + mesh->point[ib].c[1] + y0[ib]); */
      
/*       /\**        displacement of the midle **\/ */
/*       dmidx = 0.5*(dx[MMG2D_iare[i][0]]+dx[MMG2D_iare[i][1]]); */
/*       dmidy = 0.5*(dy[MMG2D_iare[i][0]]+dy[MMG2D_iare[i][1]]); */

/*       /\**        niaib^(n+1/2) = normal at barmid-Mmid*\/ */
/*       n[0] = (bary - midy); */
/*       n[1] = -(barx - midx); */
/*       /\*dd   = sqrt(n[0]*n[0]+n[1]*n[1]); */
/*       n[0] /= dd; */
/*       n[1] /= dd;*\/ */
/*       /\**        velocity *\/ */
/*       vab = 0.5*(dmidx + dbarx)*n[0]  */
/*           + 0.5*(dmidy + dbary)*n[1]; */

/*       /\********* H(ua,ub) = -0.5*(ub-ua)*vab - 0.5*abs(vab)*(ub-ua)*\/ */
/*       /\*H(ua,ub) different de H(ub,ua)*\/ */
/*       /\*H(ub,ua) = -0.5*(ub-ua)*vab + 0.5*abs(vab)*(ub-ua)*\/ */
/*       for(isol=0 ; isol<sol->size ; isol++) { */
/*        ua = sol0[isol*(mesh->np+1) + ia]; */
/*        ub = sol0[isol*(mesh->np+1) + ib]; */
/*        huab = -0.5*(ub-ua)*vab - 0.5*fabs(vab)*(ub-ua); */
/*        mesh->point[ia].n[0] += huab; */
/*        huab = -0.5*(ub-ua)*vab + 0.5*fabs(vab)*(ub-ua); */
/*        mesh->point[ib].n[0] += huab; */
/*       } */
/*     } */
/*   } */
/*   /\*update solution : u_i^n+1 = u_i^n - 1/C_i^n+1 sum(phi_i^K)*\/ */
/*   for(k=1 ; k<=mesh->np ; k++) { */
/*     ppt = &mesh->point[k]; */
/*     /\*if(!(ppt->tag & MG_BDY)) { */
/*       // printf("point bdry %d\n",k); */
/*       continue; */
/*       }*\/ */
/*     for(isol=0 ; isol<sol->size ; isol++) { */
/*       sol->m[isol*(mesh->np+1) + k] = sol0[isol*(mesh->np+1) + k] - (1./ppt->n[1]) * ppt->n[0]; */
/*     } */
/*   } */
/*   return(1); */

/* } */
