/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"



/**
 * \param Norm L2 norm of the solution on the mesh (array of size sol->size)
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * 
 * Computes the L2 norm of the solution over the mesh
 */

void FMG_computeL2Norm3d(double *Norm, MMG5_pMesh mesh, MMG5_pSol sol){

  int iel, iloc, ip, nsol;
  double area;
  MMG5_pTetra ptr;

  // Initialize norm
  for (nsol=1; nsol<=sol->size; nsol++)
    Norm[nsol-1] = 0.0;

  // Loop on elements
  for (iel=1; iel<=mesh->ne;iel++) {
    ptr = &mesh->tetra[iel];
    area = FMG_computeVolume(mesh,iel);

    for (iloc=0; iloc<4; iloc++) {
      ip = ptr->v[iloc];
      for (nsol=1; nsol<=sol->size; nsol++)
        Norm[nsol-1] += pow(sol->m[(mesh->np+1)*(nsol-1) + ip],2.0)*area/60.0;
    }
  }

}


/**
 * \param Norm L2 seminorm of the solution on the mesh (scalar)
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * 
 * Computes the L2 norm of the solution over the mesh
 */

void FMG_computeL2Norm2d(double *Norm, MMG5_pMesh mesh, MMG5_pSol sol){

  int iel, iloc, ip, nsol;
  double area;
  MMG5_pTria ptr;

  // Initialize norm
  for (nsol=1; nsol<=sol->size; nsol++)
    Norm[nsol-1] = 0.0;

  // Loop on elements
  for (iel=1; iel<=mesh->nt;iel++) {
    ptr = &mesh->tria[iel];
    area = FMG_computeArea(mesh,iel);

    for (iloc=0; iloc<3; iloc++) {
      ip = ptr->v[iloc];
      for (nsol=1; nsol<=sol->size; nsol++)
        Norm[nsol-1] += pow(sol->m[(mesh->np+1)*(nsol-1) + ip],2)*area/12.0;
    }
  }

}


/**
 * \param Norm H1 seminorm of the solution on the mesh (array of size sol->size)
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * 
 * Computes the H1 seminorm of the solution over the mesh
 */

void FMG_computeH1Seminorm3d(double *Norm, MMG5_pMesh mesh, MMG5_pSol sol){

  int iel, iloc, ip, nsol;
  double area, normalVec[3], graduh[3*sol->size];
  MMG5_pTetra ptr;

  // Initialize norm
  for (nsol=1; nsol<=sol->size; nsol++)
    Norm[nsol-1] = 0.0;

  // Loop on elements
  for (iel=1; iel<=mesh->ne;iel++) {
    ptr = &mesh->tetra[iel];
    area = FMG_computeVolume(mesh,iel);

    for (nsol=1; nsol<=sol->size; nsol++) {
      graduh[(nsol-1)*3] = 0.;
      graduh[(nsol-1)*3 + 1] = 0.;
      graduh[(nsol-1)*3 + 2] = 0.;
    }

    // Compute gradient for each nodal function, and sum it
    for (iloc=0; iloc<4; iloc++) {
      ip = ptr->v[iloc];
      FMG_computeNormalVector3d(normalVec, mesh, iel, iloc);
      for (nsol=1; nsol<=sol->size; nsol++) {
        graduh[(nsol-1)*3]   += sol->m[(mesh->np+1)*(nsol-1) + ip]*normalVec[0]/(6.0*area);
        graduh[(nsol-1)*3+1] += sol->m[(mesh->np+1)*(nsol-1) + ip]*normalVec[1]/(6.0*area);
        graduh[(nsol-1)*3+2] += sol->m[(mesh->np+1)*(nsol-1) + ip]*normalVec[2]/(6.0*area);
      }
    }

    // L2 norm of the gradient
    for (nsol=1; nsol<=sol->size; nsol++) {
      Norm[nsol-1] += area*( pow(graduh[(nsol-1)*3],  2.0)+
                             pow(graduh[(nsol-1)*3+1],2.0)+
                             pow(graduh[(nsol-1)*3+2],2.0) );
    }
  }

}


/**
 * \param Norm H1 seminorm of the solution on the mesh (scalar)
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * 
 * Computes the H1 seminorm of the solution over the mesh
 */

void FMG_computeH1Seminorm2d(double *Norm, MMG5_pMesh mesh, MMG5_pSol sol){

  int iel, iloc, ip, nsol;
  double area, normalVec[2], graduh[2*sol->size];
  MMG5_pTria ptr;

  // Initialize norm
  for (nsol=1; nsol<=sol->size; nsol++)
    Norm[nsol-1] = 0.0;

  // Loop on elements
  for (iel=1; iel<=mesh->nt;iel++) {
    ptr = &mesh->tria[iel];
    area = FMG_computeArea(mesh,iel);

    // Initialize gradient
    for (nsol=1; nsol<=sol->size; nsol++) {
      graduh[(nsol-1)*2] = 0.;
      graduh[(nsol-1)*2 + 1] = 0.;
    }

    // Compute gradient for each nodal function, and sum it
    for (iloc=0; iloc<3; iloc++) {
      ip = ptr->v[iloc];
      FMG_computeNormalVector2d(normalVec, mesh, iel, iloc);
      for (nsol=1; nsol<=sol->size; nsol++) {
        graduh[(nsol-1)*2]   += .5*sol->m[(mesh->np+1)*(nsol-1) + ip]*normalVec[0]/area;
        graduh[(nsol-1)*2+1] += .5*sol->m[(mesh->np+1)*(nsol-1) + ip]*normalVec[1]/area;
      }
    }

    // L2 norm of the gradient
    for (nsol=1; nsol<=sol->size; nsol++) {
      Norm[nsol-1] += area*(pow(graduh[(nsol-1)*2],2.0)+pow(graduh[(nsol-1)*2+1],2.0));
    }
  }

}


/**
 * \param Grd gradient of the solution on the mesh points (array of size 3*mesh->np)
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * 
 * Computes the gradient of the solution over the mesh
 */

void FMG_computeGrd3d(double *Grd, MMG5_pMesh mesh, MMG5_pSol sol,int *listv,int maxNeig){

  int iel, iloc, ip, nsol;
  int *list, ilist, lon;
  double totalArea, normalVec[3], graduh[3*sol->size], eps=1e-6;
  MMG5_pTetra ptr;

  for (ip=1;ip<=mesh->np;ip++) {
     for (nsol=1; nsol<=sol->size; nsol++) {
       Grd[3*mesh->np*(nsol-1) + 3*ip]   = 0.;
       Grd[3*mesh->np*(nsol-1) + 3*ip+1] = 0.;
       Grd[3*mesh->np*(nsol-1) + 3*ip+2] = 0.;
     }
   }

   for (iel=1; iel<=mesh->ne;iel++) {
     ptr = &mesh->tetra[iel];

     for (nsol=1; nsol<=sol->size; nsol++) {
       graduh[(nsol-1)*3]     = 0.;
       graduh[(nsol-1)*3 + 1] = 0.;
       graduh[(nsol-1)*3 + 2] = 0.;
     }

     for (iloc=0; iloc<4; iloc++) {
       ip = ptr->v[iloc];
       FMG_computeNormalVector3d(normalVec, mesh, iel, iloc);
       for (nsol=1; nsol<=sol->size; nsol++) {
         graduh[(nsol-1)*3]   += sol->m[(mesh->np+1)*(nsol-1) + ip]*normalVec[0]/6.0;
         graduh[(nsol-1)*3+1] += sol->m[(mesh->np+1)*(nsol-1) + ip]*normalVec[1]/6.0;
         graduh[(nsol-1)*3+2] += sol->m[(mesh->np+1)*(nsol-1) + ip]*normalVec[2]/6.0;
       }
     }
 
     for (iloc=0;iloc<4;iloc++) {
       ip = ptr->v[iloc];
       for (nsol=1; nsol<=sol->size; nsol++) {
         Grd[3*mesh->np*(nsol-1) + 3*ip]   += graduh[(nsol-1)*3];
         Grd[3*mesh->np*(nsol-1) + 3*ip+1] += graduh[(nsol-1)*3+1];
         Grd[3*mesh->np*(nsol-1) + 3*ip+2] += graduh[(nsol-1)*3+2];
       }
     }
   }

  for (ip=1; ip<=mesh->np; ip++) {
    totalArea = 0.0;
    list = &listv[(maxNeig+1)*ip];
    lon = list[0];
    for (ilist=1; ilist<=lon; ilist++) {
      iel = list[ilist]/4;
      ptr = &mesh->tetra[iel];
      totalArea += ptr->qual;
    }
    for (nsol=1; nsol<=sol->size; nsol++) {
      Grd[3*mesh->np*(nsol-1) + 3*ip]   = Grd[3*mesh->np*(nsol-1) + 3*ip]/totalArea;
      Grd[3*mesh->np*(nsol-1) + 3*ip+1] = Grd[3*mesh->np*(nsol-1) + 3*ip+1]/totalArea;
      Grd[3*mesh->np*(nsol-1) + 3*ip+2] = Grd[3*mesh->np*(nsol-1) + 3*ip+2]/totalArea;
    }
  }
}




/**
 * \param Grd gradient of the solution on the mesh points (array of size 2*mesh->np)
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * 
 * Computes the gradient of the solution over the mesh
 */

void FMG_computeGrd2d(double *Grd, MMG5_pMesh mesh, MMG5_pSol sol){

  int iel, iloc, ip, nsol;
  double totalArea[mesh->np+1], normalVec[2], graduh[2*sol->size], eps=1e-6;
  MMG5_pTria ptr;

  /*Grd initialisation*/
  for (ip=1;ip<=mesh->np;ip++) {
     totalArea[ip] = 0.;
     for (nsol=1; nsol<=sol->size; nsol++) {
       Grd[2*(mesh->np+1)*(nsol-1) + 2*ip] = 0.;
       Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+1] = 0.;
     }
   }

   for (iel=1; iel<=mesh->nt;iel++) {
     ptr = &mesh->tria[iel];

     for (nsol=1; nsol<=sol->size; nsol++) {
       graduh[(nsol-1)*2] = 0.;
       graduh[(nsol-1)*2 + 1] = 0.;
     }

     for (iloc=0; iloc<3; iloc++) {
       ip = ptr->v[iloc];
       FMG_computeNormalVector2d(normalVec, mesh, iel, iloc);
       for (nsol=1; nsol<=sol->size; nsol++) {
         graduh[(nsol-1)*2] += .5*sol->m[(mesh->np+1)*(nsol-1) + ip]*normalVec[0];
         graduh[(nsol-1)*2+1] += .5*sol->m[(mesh->np+1)*(nsol-1) + ip]*normalVec[1];
       }
     }
 
     for (iloc=0;iloc<3;iloc++) {
       ip = ptr->v[iloc];
       totalArea[ip] += ptr->qual;
       for (nsol=1; nsol<=sol->size; nsol++) {
         Grd[2*(mesh->np+1)*(nsol-1) + 2*ip] += graduh[(nsol-1)*2];
         Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+1] += graduh[(nsol-1)*2+1];
       }
     }
   }

  for (ip=1; ip<=mesh->np; ip++) {
    for (nsol=1; nsol<=sol->size; nsol++) {
      Grd[2*(mesh->np+1)*(nsol-1) + 2*ip] = Grd[2*(mesh->np+1)*(nsol-1) + 2*ip]/totalArea[ip];
      Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+1] = Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+1]/totalArea[ip];
    }
  }
}






/**
 * \param Hes hessian of the solution on the mesh points (array of size 4*mesh->np)
 * \param mesh pointer on the mesh structure
 * \param Grd gradient of the solution in the noeuds
 * 
 * Computes the hessian of the solution over the mesh
 */

void FMG_computeHes3d(double *Hes, MMG5_pMesh mesh, MMG5_pSol sol, double* Grd){

  int iel, iloc, ip, nsol;
  double totalArea[mesh->np+1], normalVec[3], hesui[6*sol->size];
  MMG5_pTetra ptr;


  for (ip=1;ip<=mesh->np;ip++) {
    for (nsol=1; nsol<=sol->size; nsol++) {
      totalArea[ip] = 0.;
      for (iloc=0; iloc<6; iloc++)        
        Hes[6*(mesh->np+1)*(nsol-1) + 6*ip + iloc] = 0;
    }
//   printf("\n%d %d %d",ip,4*mesh->np*(1-1)+4*ip,4*mesh->np*(2-1)+4*ip);
  }


  for (iel=1; iel<=mesh->ne;iel++) {
    ptr = &mesh->tetra[iel];

    for (nsol=1; nsol<=sol->size; nsol++) {
      for (iloc=0; iloc<6; iloc++)
        hesui[(nsol-1)*6 + iloc] = 0.;
    }

    for (iloc=0; iloc<4; iloc++) {
      ip = ptr->v[iloc];
      FMG_computeNormalVector3d(normalVec, mesh, iel, iloc);
      for (nsol=1; nsol<=sol->size; nsol++) {
        hesui[(nsol-1)*6] += Grd[2*mesh->np*(nsol-1) + 2*ip]*normalVec[0]/3.0;
        hesui[(nsol-1)*6 + 1] += Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+1]*normalVec[0]/3.0;
        hesui[(nsol-1)*6 + 2] += Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+2]*normalVec[0]/3.0;
        hesui[(nsol-1)*6 + 3] += Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+1]*normalVec[1]/3.0;
        hesui[(nsol-1)*6 + 4] += Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+2]*normalVec[1]/3.0;
        hesui[(nsol-1)*6 + 5] += Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+2]*normalVec[2]/3.0;
      }
    }

    for (iloc=0; iloc<4; iloc++) {
      ip = ptr->v[iloc];
      for (nsol=1; nsol<=sol->size; nsol++) {
        Hes[6*(mesh->np+1)*(nsol-1) + 6*ip] += hesui[(nsol-1)*6];
        Hes[6*(mesh->np+1)*(nsol-1) + 6*ip+1] += hesui[(nsol-1)*6+ 1];
        Hes[6*(mesh->np+1)*(nsol-1) + 6*ip+2] += hesui[(nsol-1)*6 + 2];    
        Hes[6*(mesh->np+1)*(nsol-1) + 6*ip+3] += hesui[(nsol-1)*6 + 3];    
        Hes[6*(mesh->np+1)*(nsol-1) + 6*ip+4] += hesui[(nsol-1)*6 + 4];    
        Hes[6*(mesh->np+1)*(nsol-1) + 6*ip+5] += hesui[(nsol-1)*6 + 5];    
//        Hes[9*mesh->np*(nsol-1) + 9*ip+3] += hesui[(nsol-1)*6 + 1];    
//        Hes[9*mesh->np*(nsol-1) + 9*ip+4] += hesui[(nsol-1)*6 + 3];    
//        Hes[9*mesh->np*(nsol-1) + 9*ip+5] += hesui[(nsol-1)*6 + 4];    
//        Hes[9*mesh->np*(nsol-1) + 9*ip+6] += hesui[(nsol-1)*6 + 2];    
//        Hes[9*mesh->np*(nsol-1) + 9*ip+7] += hesui[(nsol-1)*6 + 4];    
//        Hes[9*mesh->np*(nsol-1) + 9*ip+8] += hesui[(nsol-1)*6 + 5];    
      } 
      totalArea[ip] += ptr->qual;     
    }
  }

  for (ip=1; ip<=mesh->np; ip++) {
    for (nsol=1; nsol<=sol->size; nsol++) {
      for (iloc=0; iloc<6; iloc++)
        Hes[6*(mesh->np+1)*(nsol-1) + 6*ip + iloc] = Hes[6*(mesh->np+1)*(nsol-1) + 6*ip + iloc]/totalArea[ip];
    }
  }


}



/**
 * \param Hes hessian of the solution on the mesh points (array of size 4*mesh->np)
 * \param mesh pointer on the mesh structure
 * \param Grd gradient of the solution in the noeuds
 * 
 * Computes the hessian of the solution over the mesh
 */

void FMG_computeHes2d(double *Hes, MMG5_pMesh mesh, MMG5_pSol sol, double* Grd){

  int iel, iloc, ip, nsol;
  double totalArea[mesh->np+1], normalVec[2], hesui[3*sol->size];
  MMG5_pTria ptr;


  for (ip=1;ip<=mesh->np;ip++) {
    for (nsol=1; nsol<=sol->size; nsol++) {
      totalArea[ip] = 0.;
      Hes[3*(mesh->np+1)*(nsol-1) + 3*ip] = 0;
      Hes[3*(mesh->np+1)*(nsol-1) + 3*ip+1] = 0;
      Hes[3*(mesh->np+1)*(nsol-1) + 3*ip+2] = 0;
//      Hes[4*mesh->np*(nsol-1) + 4*ip+3] = 0;
    }
//   printf("\n%d %d %d",ip,4*mesh->np*(1-1)+4*ip,4*mesh->np*(2-1)+4*ip);
  }


  for (iel=1; iel<=mesh->nt;iel++) {
    ptr = &mesh->tria[iel];
 
    for (nsol=1; nsol<=sol->size; nsol++) {
      hesui[(nsol-1)*3] = 0.;
      hesui[(nsol-1)*3 + 1] = 0.;
      hesui[(nsol-1)*3 + 2] = 0.;
    }

    for (iloc=0; iloc<3; iloc++) {
      ip = ptr->v[iloc];
      FMG_computeNormalVector2d(normalVec, mesh, iel, iloc);
      for (nsol=1; nsol<=sol->size; nsol++) {
        hesui[(nsol-1)*3] += .5*Grd[2*(mesh->np+1)*(nsol-1) + 2*ip]*normalVec[0];
        hesui[(nsol-1)*3 + 1] += .5*Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+1]*normalVec[0];
        hesui[(nsol-1)*3 + 2] += .5*Grd[2*(mesh->np+1)*(nsol-1) + 2*ip+1]*normalVec[1];
      }
    }

    for (iloc=0; iloc<3; iloc++) {
      ip = ptr->v[iloc];
      for (nsol=1; nsol<=sol->size; nsol++) {
        Hes[3*(mesh->np+1)*(nsol-1) + 3*ip] += hesui[(nsol-1)*3];
        Hes[3*(mesh->np+1)*(nsol-1) + 3*ip+1] += hesui[(nsol-1)*3+ 1];
//        Hes[4*mesh->np*(nsol-1) + 4*ip+3] += hesui[(nsol-1)*3 + 2];    
        Hes[3*(mesh->np+1)*(nsol-1) + 3*ip+2] += hesui[(nsol-1)*3 + 2];    
      } 
      totalArea[ip] += ptr->qual;     
    }
  }

  for (ip=1; ip<=mesh->np; ip++) {
    for (nsol=1; nsol<=sol->size; nsol++) {
      Hes[3*(mesh->np+1)*(nsol-1) + 3*ip] = Hes[3*(mesh->np+1)*(nsol-1) + 3*ip]/totalArea[ip];
      Hes[3*(mesh->np+1)*(nsol-1) + 3*ip+1] = Hes[3*(mesh->np+1)*(nsol-1) + 3*ip+1]/totalArea[ip];
//      Hes[4*mesh->np*(nsol-1) + 4*ip+2] = Hes[4*mesh->np*(nsol-1) + 4*ip+1];
//      Hes[4*mesh->np*(nsol-1) + 4*ip+3] = Hes[4*mesh->np*(nsol-1) + 4*ip+3]/totalArea[ip]; 
      Hes[3*(mesh->np+1)*(nsol-1) + 3*ip+2] = Hes[3*(mesh->np+1)*(nsol-1) + 3*ip+2]/totalArea[ip]; 
    }
  }


}


