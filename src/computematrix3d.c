/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/



#include "fmg.h"

/**
 * \param mesh pointer toward the mesh structure.
 * \param coor array of point coordinates..
 * \param listv pointer toward the volumic ball of the point.
 * \param ilistv size of the volumic ball.
 * \param lists pointer toward the surfacic ball of the point.
 * \param ilists size of the surfacic ball.
 * \return 0 if we can not move, 1 if success, -1 if fail.
 *
 * update normal at regular point, whose volumic and surfacic balls are passed.
 *
 * \remark from mmg3d MMG5_movdyregpt_iso
 */
int FMG_updatebdyregpt(MMG5_pMesh mesh,double *coor, int *listv,
       int ilistv,int *lists,int ilists) {
  MMG5_pTetra       pt,pt0;
  MMG5_pxTetra      pxt;
  MMG5_pPoint       p0,p1,p2,ppt0;
  MMG5_Tria         tt;
  MMG5_pxPoint      pxp;
  MMG5_Bezier      b;
  double            *n,r[3][3],lispoi[3*MMG3D_LMAX+1],ux,uy,uz,det2d;
  double            detloc,oppt[2],step,lambda[3];
  double            ll,m[2],uv[2],o[3],no[3],to[3],normal[2],nn,hsize,proj;
  int               k,kel,iel,l,n0,na,nb,ntempa,ntempb,ntempc,nut,nxp,flag;
  unsigned char     i0,iface,i;

  step = 0.1;
  nut    = 0;
  oppt[0] = 0.0;
  oppt[1] = 0.0;
  if ( ilists < 2 )      return(0);

  k      = listv[0] / 4;
  i0 = listv[0] % 4;
  pt = &mesh->tetra[k];
  n0 = pt->v[i0];
  p0 = &mesh->point[n0];
  assert( p0->xp && !MG_EDG(p0->tag) );

  n = &(mesh->xpoint[p0->xp].n1[0]);

  /** Step 1 : rotation matrix that sends normal n to the third coordinate vector of R^3 */
  if ( !MMG5_rotmatrix(n,r) ) return(0);

  /** Step 2 : rotation of the oriented surfacic ball with r : lispoi[k] is the common edge
      between faces lists[k-1] and lists[k] */
  k     = lists[0] / 4;
  iface = lists[0] % 4;
  pt    = &mesh->tetra[k];
  na = nb = 0;
  for (i=0; i<3; i++) {
    if ( pt->v[MMG5_idir[iface][i]] != n0 ) {
      if ( !na )
        na = pt->v[MMG5_idir[iface][i]];
      else
        nb = pt->v[MMG5_idir[iface][i]];
    }
  }

  for (l=1; l<ilists; l++) {
    k     = lists[l] / 4;
    iface = lists[l] % 4;
    pt    = &mesh->tetra[k];
    ntempa = ntempb = 0;
    for (i=0; i<3; i++) {
      if ( pt->v[MMG5_idir[iface][i]] != n0 ) {
        if ( !ntempa )
          ntempa = pt->v[MMG5_idir[iface][i]];
        else
          ntempb = pt->v[MMG5_idir[iface][i]];
      }
    }
    if ( ntempa == na )
      p1 = &mesh->point[na];
    else if ( ntempa == nb )
      p1 = &mesh->point[nb];
    else if ( ntempb == na )
      p1 = &mesh->point[na];
    else {
      assert(ntempb == nb);
      p1 = &mesh->point[nb];
    }
    ux = p1->c[0] - p0->c[0];
    uy = p1->c[1] - p0->c[1];
    uz = p1->c[2] - p0->c[2];

    lispoi[3*l+1] =      r[0][0]*ux + r[0][1]*uy + r[0][2]*uz;
    lispoi[3*l+2] =      r[1][0]*ux + r[1][1]*uy + r[1][2]*uz;
    lispoi[3*l+3] =      r[2][0]*ux + r[2][1]*uy + r[2][2]*uz;

    na = ntempa;
    nb = ntempb;
  }

  /* Finish with point 0 */;
  k      = lists[0] / 4;
  iface  = lists[0] % 4;
  pt     = &mesh->tetra[k];
  ntempa = ntempb = 0;
  for (i=0; i<3; i++) {
    if ( pt->v[MMG5_idir[iface][i]] != n0 ) {
      if ( !ntempa )
        ntempa = pt->v[MMG5_idir[iface][i]];
      else
        ntempb = pt->v[MMG5_idir[iface][i]];
    }
  }
  if ( ntempa == na )
    p1 = &mesh->point[na];
  else if ( ntempa == nb )
    p1 = &mesh->point[nb];
  else if ( ntempb == na )
    p1 = &mesh->point[na];
  else {
    assert(ntempb == nb);
    p1 = &mesh->point[nb];
  }

  ux = p1->c[0] - p0->c[0];
  uy = p1->c[1] - p0->c[1];
  uz = p1->c[2] - p0->c[2];

  lispoi[1] =    r[0][0]*ux + r[0][1]*uy + r[0][2]*uz;
  lispoi[2] =    r[1][0]*ux + r[1][1]*uy + r[1][2]*uz;
  lispoi[3] =    r[2][0]*ux + r[2][1]*uy + r[2][2]*uz;

  /* list goes modulo ilist */
  lispoi[3*ilists+1] = lispoi[1];
  lispoi[3*ilists+2] = lispoi[2];
  lispoi[3*ilists+3] = lispoi[3];

  /* At this point, lispoi contains the oriented surface ball of point p0, that has been rotated
     through r, with the convention that triangle l has edges lispoi[l]; lispoi[l+1] */

  /** Step 3 : Project the new position over the tangent plane */
  oppt[0] = r[0][0]*coor[0] + r[0][1]*coor[1] + r[0][2]*coor[2];
  oppt[1] = r[1][0]*coor[0] + r[1][1]*coor[1] + r[1][2]*coor[2];
  //printf("align ??? %e\n", r[2][0]*coor[0] + r[2][1]*coor[1] + r[2][2]*coor[2]);
  step = 1.0;

  /* Check all projections over tangent plane. */
  for (k=0; k<ilists-1; k++) {
    det2d = lispoi[3*k+1]*lispoi[3*(k+1)+2] - lispoi[3*k+2]*lispoi[3*(k+1)+1];
    if ( det2d < 0.0 )  return(0);
  }
  det2d = lispoi[3*(ilists-1)+1]*lispoi[3*0+2] - lispoi[3*(ilists-1)+2]*lispoi[3*0+1];
  if ( det2d < 0.0 )    return(0);
 
  /** Step 4 : locate new point in the ball, and compute its barycentric coordinates */
  det2d = lispoi[1]*oppt[1] - lispoi[2]*oppt[0];
  kel = 0;
  if ( det2d >= 0.0 ) {
    for (k=0; k<ilists; k++) {
      detloc = oppt[0]*lispoi[3*(k+1)+2] - oppt[1]*lispoi[3*(k+1)+1];
      if ( detloc >= 0.0 ) {
        kel = k;
        break;
      }
    }
    if ( k == ilists ) return(0);
  }
  else {
    for (k=ilists-1; k>=0; k--) {
      detloc = lispoi[3*k+1]*oppt[1] - lispoi[3*k+2]*oppt[0];
      if ( detloc >= 0.0 ) {
        kel = k;
        break;
      }
    }
    if ( k == -1 ) return(0);
  }

  /* Sizing of time step : make sure point does not go out corresponding triangle. */
  det2d = -oppt[1]*(lispoi[3*(kel+1)+1] - lispoi[3*(kel)+1]) + \
    oppt[0]*(lispoi[3*(kel+1)+2] - lispoi[3*(kel)+2]);
  if ( fabs(det2d) < MMG5_EPSD2 ) return(0);

  step = 1.0;
  det2d = 1.0 / det2d;
  step *= det2d;

  det2d = lispoi[3*(kel)+1]*(lispoi[3*(kel+1)+2] - lispoi[3*(kel)+2]) - \
    lispoi[3*(kel)+2 ]*(lispoi[3*(kel+1)+1] - lispoi[3*(kel)+1]);
  step *= det2d;
  step  = fabs(step);
  if( step < 1.0 / (1.0 - MMG5_EPS) ) {
    step *= 1.0 - MMG5_EPS;
    oppt[0] *= step;
    oppt[1] *= step;
  }

  /* Computation of the barycentric coordinates of the new point in the corresponding triangle. */
  det2d = lispoi[3*kel+1]*lispoi[3*(kel+1)+2] - lispoi[3*kel+2]*lispoi[3*(kel+1)+1];
  if ( det2d < MMG5_EPSD2 )    return(0);
  det2d = 1.0 / det2d;
  lambda[1] = lispoi[3*(kel+1)+2]*oppt[0] - lispoi[3*(kel+1)+1]*oppt[1];
  lambda[2] = -lispoi[3*(kel)+2]*oppt[0] + lispoi[3*(kel)+1]*oppt[1];
  lambda[1]*= (det2d);
  lambda[2]*= (det2d);
  lambda[0] = 1.0 - lambda[1] - lambda[2];

  /** Step 5 : come back to original problem, and compute patch in triangle iel */
  iel    = lists[kel] / 4;
  iface  = lists[kel] % 4;
  pt     = &mesh->tetra[iel];
  pxt    = &mesh->xtetra[pt->xt];

  MMG5_tet2tri(mesh,iel,iface,&tt);

  if(!MMG5_bezierCP(mesh,&tt,&b,MG_GET(pxt->ori,iface))){
    fprintf(stderr,"%s:%d: Error: function MMG5_bezierCP return 0\n",
            __FILE__,__LINE__);
    return -1;
  }

  /* Now, for Bezier interpolation, one should identify which of i,i1,i2 is 0,1,2
     recall uv[0] = barycentric coord associated to pt->v[1], uv[1] associated to pt->v[2] and
     1 - uv[0] - uv[1] is associated to pt->v[0]. For this, use the fact that kel, kel + 1 is
     positively oriented with respect to n */
  na = nb = 0;
  for( i=0 ; i<4 ; i++ ){
    if ( (pt->v[i] != n0) && (pt->v[i] != pt->v[iface]) ) {
      if ( !na )
        na = pt->v[i];
      else
        nb = pt->v[i];
    }
  }
  p1 = &mesh->point[na];
  p2 = &mesh->point[nb];
  detloc = MMG5_det3pt1vec(p0->c,p1->c,p2->c,n);

  /* ntempa = point to which is associated 1 -uv[0] - uv[1], ntempb = uv[0], ntempc = uv[1] */
  ntempb = pt->v[MMG5_idir[iface][1]];
  ntempc = pt->v[MMG5_idir[iface][2]];

  /* na = lispoi[kel] -> lambda[1], nb = lispoi[kel+1] -> lambda[2] */
  if ( detloc > 0.0 ) {
    if ( ntempb == na )
      uv[0] = lambda[1];
    else if (ntempb == nb )
      uv[0] = lambda[2];
    else {
      assert(ntempb == n0);
      uv[0] = lambda[0];
    }
    if ( ntempc == na )
      uv[1] = lambda[1];
    else if (ntempc == nb )
      uv[1] = lambda[2];
    else {
      assert(ntempc == n0);
      uv[1] = lambda[0];
    }
  }
  /* nb = lispoi[kel] -> lambda[1], na = lispoi[kel+1] -> lambda[2] */
  else {
    if ( ntempb == na )
      uv[0] = lambda[2];
    else if (ntempb == nb )
      uv[0] = lambda[1];
    else {
      assert(ntempb == n0);
      uv[0] = lambda[0];
    }
    if ( ntempc == na )
      uv[1] = lambda[2];
    else if (ntempc == nb )
      uv[1] = lambda[1];
    else {
      assert(ntempc == n0);
      uv[1] = lambda[0];
    }
  }
  if(!MMG3D_bezierInt(&b,uv,o,no,to)){
    fprintf(stderr,"%s:%d: Error: function MMG3D_bezierInt return 0\n",
            __FILE__,__LINE__);
    return -1;
  }

//  /* Test : make sure that geometric approximation has not been degraded too much */
//  ppt0 = &mesh->point[0];
//  ppt0->c[0] = o[0];
//  ppt0->c[1] = o[1];
//  ppt0->c[2] = o[2];
//
//  ppt0->tag      = p0->tag;
//  ppt0->ref      = p0->ref;
//
//
//  nxp = mesh->xp + 1;
//  if ( nxp > mesh->xpmax ) {
//    MMG5_TAB_RECALLOC(mesh,mesh->xpoint,mesh->xpmax,0.2,MMG5_xPoint,
//                       "larger xpoint table",
//                       return(0),0);
//    n = &(mesh->xpoint[p0->xp].n1[0]);
//  }
//  ppt0->xp = nxp;
//  pxp = &mesh->xpoint[nxp];
//  memcpy(pxp,&(mesh->xpoint[p0->xp]),sizeof(MMG5_xPoint));
//  pxp->n1[0] = no[0];
//  pxp->n1[1] = no[1];
//  pxp->n1[2] = no[2];
//
//
//  memset(pxp,0,sizeof(MMG5_xPoint));

 
  /* When all tests have been carried out, update coordinates and normals */
  p0->c[0] = o[0];
  p0->c[1] = o[1];
  p0->c[2] = o[2];

 
  n[0] = no[0];
  n[1] = no[1];
  n[2] = no[2];

  for(l=0; l<ilistv; l++){
    (&mesh->tetra[listv[l]/4])->mark=mesh->mark;
  }
  return(1);
}


/**
 * \param mesh pointer toward the mesh structure.
 * \param listv pointer toward the volumic ball of the point.
 * \param ilistv size of the volumic ball.
 * \param lists pointer toward the surfacic ball of the point.
 * \param ilists size of the surfacic ball.
 * \return 0 if fail, 1 if success.
 *
 * Updqte normal at boundary ridge point, whose volumic and surfacic balls are
 * passed.
 *
 * \remark From _MMG5_movbdyridpt_iso
 *
 */
int FMG_updatebdyridpt(MMG5_pMesh mesh, double *coor, int *listv,
                          int ilistv,int *lists,int ilists) {
  MMG5_pTetra          pt,pt0;
  MMG5_pxTetra         pxt;
  MMG5_pPoint          p0,p1,p2,ppt0;
  MMG5_Tria            tt;
  MMG5_pxPoint         pxp;
  MMG5_pPar            par;
  double               step,ll1old,ll2old,o[3],no1[3],no2[3],to[3];
  double               calold,calnew,caltmp,*callist,hmax,hausd,proj;
  int                  l,iel,ip0,ipa,ipb,iptmpa,iptmpb,ip1,ip2,ip,nxp;
  int                  j,isloc;
  int16_t              tag;
  unsigned char        i,i0,ie,iface,iea,ieb;

  step = 0.0;
  ip1 = ip2 = 0;
  pt    = &mesh->tetra[listv[0]/4];
  ip0 = pt->v[listv[0]%4];
  p0    = &mesh->point[ip0];

  assert ( MG_GEO & p0->tag );

  /* Travel surfacic ball an recover the two ending points of ridge : two senses must be used
     POSSIBLE OPTIMIZATION HERE : One travel only is needed */
  iel           = lists[0] / 4;
  iface         = lists[0] % 4;
  pt            = &mesh->tetra[iel];
  ipa           = ipb = 0;
  for (i=0; i<3; i++) {
    if ( pt->v[MMG5_idir[iface][i]] != ip0 ) {
      if ( !ipa )
        ipa = pt->v[MMG5_idir[iface][i]];
      else
        ipb = pt->v[MMG5_idir[iface][i]];
    }
  }
  assert(ipa && ipb);

  for (l=1; l<ilists; l++) {
    iel         = lists[l] / 4;
    iface = lists[l] % 4;
    pt  = &mesh->tetra[iel];
    iea = ieb = 0;
    for (i=0; i<3; i++) {
      ie = MMG5_iarf[iface][i]; //edge i on face iface
      if ( (pt->v[MMG5_iare[ie][0]] == ip0) || (pt->v[MMG5_iare[ie][1]] == ip0) ) {
        if ( !iea )
          iea = ie;
        else
          ieb = ie;
      }
    }
    if ( pt->v[MMG5_iare[iea][0]] != ip0 )
      iptmpa = pt->v[MMG5_iare[iea][0]];
    else {
      assert(pt->v[MMG5_iare[iea][1]] != ip0);
      iptmpa = pt->v[MMG5_iare[iea][1]];
    }
    if ( pt->v[MMG5_iare[ieb][0]] != ip0 )
      iptmpb = pt->v[MMG5_iare[ieb][0]];
    else {
      assert(pt->v[MMG5_iare[ieb][1]] != ip0);
      iptmpb = pt->v[MMG5_iare[ieb][1]];
    }
    if ( (iptmpa == ipa) || (iptmpa == ipb) ) {
      if ( pt->xt )  tag = mesh->xtetra[pt->xt].tag[iea];
      else  tag = 0;
      if ( MG_GEO & tag ) {
        ip1 = iptmpa;
        break;
      }
    }
    if ( (iptmpb == ipa) || (iptmpb == ipb) ) {
      if ( pt->xt )  tag = mesh->xtetra[pt->xt].tag[ieb];
      else  tag = 0;
      if ( MG_GEO & tag ) {
        ip1 = iptmpb;
        break;
      }
    }
    ipa = iptmpa;
    ipb = iptmpb;
  }

  /* Now travel surfacic list in the reverse sense so as to get the second ridge */
  iel           = lists[0] / 4;
  iface = lists[0] % 4;
  pt    = &mesh->tetra[iel];
  ipa = ipb = 0;
  for (i=0; i<3; i++) {
    if ( pt->v[MMG5_idir[iface][i]] != ip0 ) {
      if ( !ipa )
        ipa = pt->v[MMG5_idir[iface][i]];
      else
        ipb = pt->v[MMG5_idir[iface][i]];
    }
  }
  assert(ipa && ipb);

  for (l=ilists-1; l>0; l--) {
    iel         = lists[l]/4;
    iface = lists[l]%4;
    pt  = &mesh->tetra[iel];
    iea = ieb = 0;
    for (i=0; i<3; i++) {
      ie = MMG5_iarf[iface][i]; //edge i on face iface
      if ( (pt->v[MMG5_iare[ie][0]] == ip0) || (pt->v[MMG5_iare[ie][1]] == ip0) ) {
        if ( !iea )
          iea = ie;
        else
          ieb = ie;
      }
    }
    if ( pt->v[MMG5_iare[iea][0]] != ip0 )
      iptmpa = pt->v[MMG5_iare[iea][0]];
    else {
      assert(pt->v[MMG5_iare[iea][1]] != ip0);
      iptmpa = pt->v[MMG5_iare[iea][1]];
    }
    if ( pt->v[MMG5_iare[ieb][0]] != ip0 )
      iptmpb = pt->v[MMG5_iare[ieb][0]];
    else {
      assert(pt->v[MMG5_iare[ieb][1]] != ip0);
      iptmpb = pt->v[MMG5_iare[ieb][1]];
    }
    if ( (iptmpa == ipa) || (iptmpa == ipb) ) {
      if ( pt->xt )  tag = mesh->xtetra[pt->xt].tag[iea];
      else  tag = 0;
      if ( MG_GEO & tag ) {
        ip2 = iptmpa;
        break;
      }
    }
    if ( (iptmpb == ipa) || (iptmpb == ipb) ) {
      if ( pt->xt )  tag = mesh->xtetra[pt->xt].tag[ieb];
      else  tag = 0;
      if ( MG_GEO & tag ) {
        ip2 = iptmpb;
        break;
      }
    }
    ipa = iptmpa;
    ipb = iptmpb;
  }
  if ( !(ip1 && ip2 && (ip1 != ip2)) ) return 0;

  /* At this point, we get the point extremities of the ridge curve passing through ip0 :
     ip1, ip2, along with support tets it1,it2, the surface faces iface1,iface2, and the
     associated edges ie1,ie2.*/

  step = coor[0]*p0->n[0] + coor[1]*p0->n[1] + coor[2]*p0->n[2];

  /* Changes needed for choice of time step : see manuscript notes */
  p1 = &mesh->point[ip1];
  p2 = &mesh->point[ip2];

  ll1old = (p1->c[0] -p0->c[0])* p0->n[0] \
         + (p1->c[1] -p0->c[1])* p0->n[1] \
         + (p1->c[2] -p0->c[2])* p0->n[2];
  ll2old = (p2->c[0] -p0->c[0])* p0->n[0] \
         + (p2->c[1] -p0->c[1])* p0->n[1] \
         + (p2->c[2] -p0->c[2])* p0->n[2];

  if ( step/ll1old < step/ll2old ) { //move towards p2
    ip = ip2;
    step /= ll2old;
  }
  else {
    ip = ip1;
    step /= ll1old;
  }
  if( step > 1.0 - MMG5_EPS ) step = 1.0 - MMG5_EPS;

  /* Compute support of the associated edge, and features of the new position */
  if ( !(MMG5_BezierRidge(mesh,ip0,ip,step,o,no1,no2,to)) )  return 0;

//  /* Test : make sure that geometric approximation has not been degraded too much */
//  ppt0 = &mesh->point[0];
//  ppt0->c[0] = o[0];
//  ppt0->c[1] = o[1];
//  ppt0->c[2] = o[2];
//  ppt0->tag      = p0->tag;
//  ppt0->ref      = p0->ref;
//
//  nxp = mesh->xp+1;
//  if ( nxp > mesh->xpmax ) {
//    MMG5_TAB_RECALLOC(mesh,mesh->xpoint,mesh->xpmax,0.2,MMG5_xPoint,
//                       "larger xpoint table",
//                       return 0,0);
//  }
//  ppt0->xp = nxp;
//  pxp = &mesh->xpoint[nxp];
//  memcpy(pxp,&(mesh->xpoint[p0->xp]),sizeof(MMG5_xPoint));
//
//  ppt0->n[0] = to[0];
//  ppt0->n[1] = to[1];
//  ppt0->n[2] = to[2];
//
//  pxp->n1[0] = no1[0];
//  pxp->n1[1] = no1[1];
//  pxp->n1[2] = no1[2];
//
//  pxp->n2[0] = no2[0];
//  pxp->n2[1] = no2[1];
//  pxp->n2[2] = no2[2];
//
//  /* For each surfacic triangle, build a virtual displaced triangle for check purposes */
//  calold = calnew = DBL_MAX;
//  for (l=0; l<ilists; l++) {
//    iel         = lists[l] / 4;
//    iface       = lists[l] % 4;
//    pt          = &mesh->tetra[iel];
//    pxt         = &mesh->xtetra[pt->xt];
//
//    MMG5_tet2tri(mesh,iel,iface,&tt);
//    calold = MG_MIN(calold,MMG5_caltri(mesh,met,&tt));
//
//    for (i=0; i<3; i++) {
//      if ( tt.v[i] == ip0 )      break;
//    }
//    assert(i<3);
//    return 0;
//
//    tt.v[i] = 0;
//
//    caltmp = MMG5_caltri(mesh,met,&tt);
//    if ( caltmp < MMG5_EPSD2 )        return 0;
//    calnew = MG_MIN(calnew,caltmp);
//
//    /* Local parameters for tt and iel */
//    hmax  = mesh->info.hmax;
//    hausd = mesh->info.hausd;
//
//    isloc = 0;
//    if ( mesh->info.parTyp & MG_Tetra ) {
//      for ( j=0; j<mesh->info.npar; ++j ) {
//        par = &mesh->info.par[j];
//
//        if ( par->elt != MMG5_Tetrahedron )  continue;
//        if ( par->ref != pt->ref ) continue;
//
//        hmax = par->hmax;
//        hausd = par->hausd;
//        isloc = 1;
//        break;
//      }
//    }
//    if ( mesh->info.parTyp & MG_Tria ) {
//      if ( isloc ) {
//        for ( j=0; j<mesh->info.npar; ++j ) {
//          par = &mesh->info.par[j];
//
//          if ( par->elt != MMG5_Triangle )  continue;
//          if ( par->ref != tt.ref ) continue;
//
//          hmax = MG_MIN(hmax,par->hmax);
//          hausd = MG_MIN(hausd,par->hausd);
//          break;
//        }
//      }
//      else {
//        for ( j=0; j<mesh->info.npar; ++j ) {
//          par = &mesh->info.par[j];
//
//          if ( par->elt != MMG5_Triangle )  continue;
//          if ( par->ref != tt.ref ) continue;
//
//          hmax  = par->hmax;
//          hausd = par->hausd;
//          isloc = 1;
//          break;
//        }
//      }
//    }
//
//    if ( MMG5_chkedg(mesh,&tt,MG_GET(pxt->ori,iface),hmax,hausd,isloc) ) {
//      memset(pxp,0,sizeof(MMG5_xPoint));
//      return 0;
//    }
//  }
//  if ( calold < MMG5_EPSOK && calnew <= calold )    return 0;
//  else if ( calnew <= calold )  return 0;
//  memset(pxp,0,sizeof(MMG5_xPoint));
//
//  /* Test : check whether all volumes remain positive with new position of the point */
//  // Dynamic allocations for windows compatibility
//  MMG5_SAFE_MALLOC(callist, ilistv, double,0);
//
//  calold = calnew = DBL_MAX;
//  for (l=0; l<ilistv; l++) {
//    iel = listv[l] / 4;
//    i0  = listv[l] % 4;
//    pt  = &mesh->tetra[iel];
//    pt0 = &mesh->tetra[0];
//    memcpy(pt0,pt,sizeof(MMG5_Tetra));
//    pt0->v[i0] = 0;
//    calold = MG_MIN(calold, pt->qual);
//    callist[l]=MMG5_orcal(mesh,met,0);
//    if (callist[l] < MMG5_NULKAL) {
//      MMG5_SAFE_FREE(callist);
//      return 0;
//    }
//    calnew = MG_MIN(calnew,callist[l]);
//  }
//  if ((calold < MMG5_EPSOK && calnew <= calold) ||
//      (calnew < MMG5_EPSOK) || (calnew <= 0.3*calold)) {
//    MMG5_SAFE_FREE(callist);
//    return 0;
//  } else if (improve && calnew < calold) {
//    MMG5_SAFE_FREE(callist);
//    return 0;
//  }

  /* Update coordinates, normals, for new point */
  p0->c[0] = o[0];
  p0->c[1] = o[1];
  p0->c[2] = o[2];

  pxp = &mesh->xpoint[p0->xp];
  pxp->n1[0] = no1[0];
  pxp->n1[1] = no1[1];
  pxp->n1[2] = no1[2];

  pxp->n2[0] = no2[0];
  pxp->n2[1] = no2[1];
  pxp->n2[2] = no2[2];

  p0->n[0] = to[0];
  p0->n[1] = to[1];
  p0->n[2] = to[2];

  for(l=0; l<ilistv; l++){
//    (&mesh->tetra[listv[l]/4])->qual = callist[l];
    (&mesh->tetra[listv[l]/4])->mark = mesh->mark;
  }
//  MMG5_SAFE_FREE(callist);
  return 1;
}



/**
 * \param mesh pointer on the mesh
 * \param k tetra index
 * \param i0 local point index on tetras
 * \param ifac tetra face index
 * \param disp point 3D displacement
 * \param listv pointer to (empty) volumic ball
 * \param lists pointer to (empty) surfacic ball
 * \return -1 if fail, 0 if point is not treated, 1 if success.
 *
 * For the point on slip boundary : update the normal and project the new position on the boundary
 */
int FMG_normalUpdate_point3d(MMG5_pMesh mesh,int k,int i0,int ifac,double *disp,int *listv,int *lists) {
  MMG5_pTetra        pt;
  MMG5_pxTetra       pxt;
  MMG5_pPoint        ppt;
  int                ier;
  int                ilists,ilistv;
  double             *n;

  pt = &mesh->tetra[k];
  ppt = &mesh->point[pt->v[i0]];
  pxt = &mesh->xtetra[pt->xt];

  ier = 0;
  if( ppt->tag & MG_NOM ) {
    printf("NOM not treated, point %d\n",pt->v[i0]);
  }
  else if ( ppt->tag & MG_GEO ) {
    ier=MMG5_boulesurfvolp(mesh,k,i0,ifac,listv,&ilistv,lists,&ilists,0);
    if ( ier<0 ) return(-1);
    // Compute compatible new position
    ier = FMG_updatebdyridpt(mesh,disp,listv,ilistv,lists,ilists);
    if (ier < 0 ) return -1;
  }
  else if ( ppt->tag & MG_REF ) {
    printf("REF not treated, point %d\n",pt->v[i0]);
  }
  else {
    ier=MMG5_boulesurfvolp(mesh,k,i0,ifac,listv,&ilistv,lists,&ilists,0);
    if ( ier<0 ) return(-1);
    n = &(mesh->xpoint[ppt->xp].n1[0]);
    if ( !MG_GET(pxt->ori,ifac) ) {
      if ( !MMG5_directsurfball(mesh,pt->v[i0],lists,ilists,n) )
        return -1;
    }
    // Compute compatible new position
    ier = FMG_updatebdyregpt(mesh,disp,listv,ilistv,lists,ilists);
    if (ier < 0 ) return -1;
  }

  return ier;
}


/**
 * \param mesh pointer on the mesh 
 * \param dxk x displacement
 * \param dyk y displacement
 * \param dzk z displacement
 * \param xref ref x coordinates
 * \param yref ref y coordinates
 * \param zref ref z coordinates
 * 
 * For the points on slip boundary : update the normal and project the new position on the boundary
 */
int FMG_normalUpdate3d(MMG5_pMesh mesh,double *dxk,double *dyk,double *dzk,
       double *xref,double *yref,double *zref) {
  MMG5_pTetra  pt;
  MMG5_pxTetra pxt;
  MMG5_pPoint  ppt;
  int          *adja,i,j,i0,k,ier;
  int          lists[MMG3D_LMAX+2],listv[MMG3D_LMAX+2];
  double       disp[3],dd;

  /* Reset points flag */
  for (i=1; i<=mesh->xp; i++) {
    mesh->point[i].flag = 0;
  }

  /* Loop on boundary faces */
  for(k=1 ; k<=mesh->ne ; k++) {
    pt = &mesh->tetra[k];
    if ( !MG_EOK(pt) )  continue;
    
    adja = &mesh->adja[4*(k-1)+1];
    for (i=0; i<4; i++) {
      if(adja[i]) continue;
      for (j=0; j<3; j++) {
        if ( pt->xt ) {
          pxt = &mesh->xtetra[pt->xt];
        } else
          pxt = 0;
        
        i0  = MMG5_idir[i][j];
        ppt = &mesh->point[pt->v[i0]];

        /* Skip singular points */
        if ( MG_SIN(ppt->tag) )  continue;

        /* Skip points already updated */
        if ( ppt->flag ) continue;

        assert(ppt->tag & MG_BDY);
        if ( !pt->xt || !(MG_BDY & pxt->ftag[i]) )  continue;

        // Guess new displqcement
        disp[0] = xref[pt->v[i0]]+dxk[pt->v[i0]]-ppt->c[0];
        disp[1] = yref[pt->v[i0]]+dyk[pt->v[i0]]-ppt->c[1];
        disp[2] = zref[pt->v[i0]]+dzk[pt->v[i0]]-ppt->c[2];

        /*no interpolation if the displacement is almost null*/
        dd = sqrt( disp[0]*disp[0]+
                   disp[1]*disp[1]+
                   disp[2]*disp[2]);
        if (dd<1e-12) continue;
 
        /* Project point on the surface and interpolate normal vectors */
        ier = FMG_normalUpdate_point3d(mesh,k,i0,i,disp,listv,lists);
        if (ier < 0 )
          return -1;
        else if( ier ) {
          // Translate the new position in a compatible displacement
          dxk[pt->v[i0]] = ppt->c[0]-xref[pt->v[i0]];
          dyk[pt->v[i0]] = ppt->c[1]-yref[pt->v[i0]];
          dzk[pt->v[i0]] = ppt->c[2]-zref[pt->v[i0]];
        }
        /* Flag point as treated */
        ppt->flag++;
      } /*end for j*/
    } /*end for i*/
  } /*end for ip*/
 
  return 1;
}

///**
// * \param K FEM matrix of coefficients
// * \param mesh pointer on the mesh in the before the last Jacobi iteration
// * \param area0 elements' areas in the initial mesh
// * \param normal0 element's normal vectors in the initial mesh
// * \omega omega values in each point of mesh
// * 
// * Computes matrix K of the depalcement linear system
// */
//
//void FMG_computeMatrix(double** K, MMG5_pMesh mesh, double* area0, double* normal0, double* omega){
//  
//  int ip, jp, iel, iloc, jloc;
//  double avgOmega, area;
//  double veci[2], vecj[2];
//  MMG5_pTria ptr;
//
//
//  for (ip=1;ip<=mesh->np;ip++)
//   for (jp=1;jp<=mesh->np;jp++)  
//     K[ip][jp] = 0.;
// 
//  for (iel=1; iel<=mesh->nt; iel++) {
//    ptr = &mesh->tria[iel];
//    area = area0[iel];
//
//    avgOmega = 0;
//    for (iloc=0; iloc<=2; iloc++)
//      avgOmega += omega[ptr->v[iloc]]/3.;
//
//    for (iloc=0; iloc<=2; iloc++) {
//      for (jloc=0; jloc<= 2; jloc++) {
//        ip = ptr->v[iloc];
//        jp = ptr->v[jloc];
//
//        veci[0] = normal0[6*iel+2*iloc];
//        veci[1] = normal0[6*iel+2*iloc+1];
//        vecj[0] = normal0[6*iel+2*jloc];
//        vecj[1] = normal0[6*iel+2*jloc+1];
//            
//        K[ip][jp] += avgOmega/(4.*area)*(veci[0]*vecj[0] + veci[1]*vecj[1]);
//      }
//    }
//
//  }
//}


/**
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param listv pointer to the volumic ball of all points
 * \param neigSparse list of neighbor points of each point
 * \param maxNeig maximum number of neighbors for a point in the mesh
 *
 * Constructs a structure for stocking a sparse matrix.
 * The volumic ball of all points is stored in slots of size (maxNeig+1).
 * Position 0 stores the number of tetras, positions starting from 1 store the
 * tetra ID as: 4*(tetra ID)+the local point ID on tetra.
 */

void FMG_constructSparseStruct3d(MMG5_pMesh mesh, int* listv, int* neigSparse, int maxNeig) {

  int ip, jp, iloc, jloc, iel, ilist, lon=mesh->np, flag = 0, k;
  int *visitedNeig, *list;

  MMG5_SAFE_CALLOC(visitedNeig,mesh->np+1,int,return);
  for (ip=0; ip<=mesh->np;ip++)
    visitedNeig[ip] = 0;

  for (ip=1;ip<=mesh->np;ip++) {

    /* Pointer to the volumic ball of the current point */
    list = &listv[(maxNeig+1)*ip+1];

    iel = mesh->point[ip].s;
    for (iloc=0;iloc<4;iloc++)
      if (mesh->tetra[iel].v[iloc] == ip)
        break;
    for (ilist=0;ilist<lon;ilist++)
      list[ilist] = 0;
    lon = MMG5_boulevolp(mesh,iel,iloc,list);
    list[-1]=lon;

    for (jp=1;jp<=visitedNeig[0];jp++)
      visitedNeig[jp] = 0;
    visitedNeig[0] = 0;

    for (ilist=0; ilist<lon; ilist++) {
      iel = list[ilist]/4; 
      for (jloc=0; jloc<4; jloc++) {
        flag = 0;
        jp = mesh->tetra[iel].v[jloc];
        for (k=1; k<=visitedNeig[0]; k++)
          if (visitedNeig[k]==jp) {
            flag = 1;
            break;
          }
        if (jp != ip && flag == 0) {
          neigSparse[(maxNeig+1)*ip+visitedNeig[0]+1] = jp;
          visitedNeig[0] += 1;
          visitedNeig[visitedNeig[0]] = jp;
        }
      }
    }
    neigSparse[(maxNeig+1)*ip+visitedNeig[0]+1] = ip;
    neigSparse[(maxNeig+1)*ip] = visitedNeig[0]+1;
  }

  MMG5_SAFE_FREE(visitedNeig);
}


/**
 * \param K FEM matrix of coefficients
 * \param b FEM's right-hand side
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param area0 elements' areas in the initial mesh
 * \param normal0 element's normal vectors in the initial mesh
 * \param omega omega values in each point of mesh
 * \param mu mu values in each point of mesh
 * \param lamdba lamdba values in each point of mesh
 * \param neigSparse list of neighbor points of each point
 * \param maxNeig maximum number of neighbors for a point in the mesh
 * \param elast indicates if lapalcian (0) ou linear elasticity (1) model
 *
 * Computes matrix K and the RHS of the deplacement linear system (considering the structure for stocking the sparse matrix K)
 */

int FMG_computeLinSystSparse3d(double* K, double* b, MMG5_pMesh mesh, double* area0, double* normal0, double* omega, double* mu, double* lambda, int* neigSparse, int* listv, int maxNeig, int elast,MMG5_pSol sol,int ijacobi){

  int ip, jp, iel, iloc, jloc, ineig;
  // int lonBoulep[mesh->np+1];
  double avgOmega, area, avgLambda;
  double veci[3], vecj[3], veck[3], avgMu[9], g1[3*mesh->np+3]/*, g2[3*mesh->np+3]*/
    ,g3[3*mesh->np+3],sol0[mesh->np+1],tmpFrc[3];/*,totalArea[mesh->np+1]*/;
  MMG5_pTetra ptr;

  ////// CALCUL ALTERNATIF DE b
  if (elast) {
//#warning change here if mu is not diagonal
//  for (ip=1; ip<=mesh->np;ip++) {
//    //totalArea[ip] = 0;
//    //lonBoulep[ip] = 0;
//    sol0[ip] = sol->m[ip];
//    sol->m[ip] = mu[9*ip] + 2*lambda[ip];
//  }
//  FMG_computeGrd3d(g1,mesh,sol);
//  //mu diago so it is not necessary to compute this gradient (same below)
//  //for (ip=1; ip<=mesh->np;ip++)
//  //  sol->m[ip] = mu[9*ip+1];
//  //FMG_computeGrd3d(g2,mesh,sol);
//  for (ip=1; ip<=mesh->np;ip++)
//    g3[ip] = g1[2*ip];// + g2[2*ip+1];
//    
//    
//  //for (ip=1; ip<=mesh->np;ip++)
//  //  sol->m[ip] = mu[9*ip+2];
//  //FMG_computeGrd3d(g1,mesh,sol);
//  for (ip=1; ip<=mesh->np;ip++)
//    sol->m[ip] = mu[9*ip+4] + 2*lambda[ip];
//  FMG_computeGrd3d(g1,mesh,sol);
//  for (ip=1; ip<=mesh->np;ip++)
//    g3[ip+mesh->np] = g1[2*ip+1] ;//+ g2[2*ip+1];
//
//  //for (ip=1; ip<=mesh->np;ip++)
//  //  sol->m[ip] = mu[9*ip+4];
//  //FMG_computeGrd3d(g1,mesh,sol);
//  for (ip=1; ip<=mesh->np;ip++)
//    sol->m[ip] = mu[9*ip+8] + 2*lambda[ip];
//  FMG_computeGrd3d(g1,mesh,sol);
//  for (ip=1; ip<=mesh->np;ip++)
//    g3[ip+2*mesh->np] = g1[2*ip+2] ;//+ g2[2*ip+1];
//    
//  for (ip=1; ip<=mesh->np;ip++)
//    sol->m[ip] = sol0[ip];

    for (ip=1; ip<=mesh->np; ip++){
      sol0[ip] = sol->m[ip];
      sol->m[ip] = omega[ip];
    }
    FMG_computeGrd3d(g1,mesh,sol,listv,maxNeig);
    for (ip=1; ip<=mesh->np;ip++){
      g3[ip] = g1[3*ip];
      g3[ip+mesh->np] = g1[3*ip+1];
      g3[ip+2*mesh->np] = g1[3*ip+2];
      sol->m[ip] = sol0[ip];
    }
  }

  ////////


  if (elast) {
    for (ip=0;ip<3*mesh->np+3;ip++)
      b[ip] = 0.;
    if(ijacobi==1)
      for (ip=0;ip<9*maxNeig*(mesh->np+1);ip++)
        K[ip] = 0.;
  }
  else
    for (ip=0;ip<maxNeig*(mesh->np+1);ip++)
      K[ip] = 0.;

 
  for (iel=1; iel<=mesh->ne; iel++) {
    ptr = &mesh->tetra[iel];
    area = area0[iel];
  
    if (!elast)
      avgOmega = 0;
    else {
      avgLambda = 0;
      for (ip=0; ip <9; ip++)
        avgMu[ip] = 0;
    }
  
    for (iloc=0; iloc<4; iloc++) {
      if (!elast)
        avgOmega += omega[ptr->v[iloc]]/4.;
      else {
        avgLambda += lambda[ptr->v[iloc]]/4.;
        for (ip=0; ip<9; ip++)
          avgMu[ip] += mu[9*ptr->v[iloc]+ip]/4.;
      }
    }

    for (iloc=0; iloc<=3; iloc++) {
      ip = ptr->v[iloc];
      //totalArea[ip] += area;
      //lonBoulep[ip] += 1;
      veci[0] = normal0[12*iel+3*iloc];
      veci[1] = normal0[12*iel+3*iloc+1];
      veci[2] = normal0[12*iel+3*iloc+2];
  
      for (jloc=0; jloc<= 3; jloc++) {
        jp = ptr->v[jloc];
  
        for (ineig=1;ineig<=neigSparse[ip*(maxNeig+1)];ineig++)
          if (neigSparse[ip*(maxNeig+1)+ineig] == jp)
            break;
        ineig -= 1;
  
        vecj[0] = normal0[12*iel+3*jloc];
        vecj[1] = normal0[12*iel+3*jloc+1];
        vecj[2] = normal0[12*iel+3*jloc+2];
  
        if (elast) {
          if(ijacobi==1){
            
            // K11 - Pointer+0
            veck[0] = (avgMu[0]+avgLambda)*veci[0];
            veck[1] = avgMu[1]/2*veci[0] + avgMu[0]/2*veci[1];
            veck[2] = avgMu[2]/2*veci[0] + avgMu[0]/2*veci[2];
            K[(9*maxNeig)*ip+9*ineig] += (veck[0]*vecj[0] + veck[1]*vecj[1] + veck[2]*vecj[2])/(36.*area);
            
            //K12 - Pointer+1
            veck[0] = avgMu[1]/2*veci[0] + avgMu[0]/2*veci[1];
            veck[1] = avgLambda*veci[0] + avgMu[1]*veci[1];
            veck[2] = avgMu[2]/2*veci[1] + avgMu[1]/2*veci[2];
            K[(9*maxNeig)*ip+9*ineig+1] += (veck[0]*vecj[0] + veck[1]*vecj[1] + veck[2]*vecj[2])/(36.*area);
  
            //K13 - Pointer+2
            veck[0] = avgMu[2]/2*veci[0] + avgMu[0]/2*veci[1];
            veck[1] = avgMu[2]/2*veci[1] + avgMu[1]/2*veci[2];
            veck[2] = avgLambda*veci[0] + avgMu[2]*veci[2];
            K[(9*maxNeig)*ip+9*ineig+2] += (veck[0]*vecj[0] + veck[1]*vecj[1] + veck[2]*vecj[2])/(36.*area);
  
            //K21 - Pointer+3
            veck[0] = avgMu[3]*veci[0] + avgLambda*veci[1];
            veck[1] = avgMu[4]/2*veci[0] + avgMu[3]/2*veci[1];
            veck[2] = avgMu[5]/2*veci[0] + avgMu[3]/2*veci[2];
            K[(9*maxNeig)*ip+9*ineig+3] += (veck[0]*vecj[0] + veck[1]*vecj[1] + veck[2]*vecj[2])/(36.*area);
  
            //K22 - Pointer+4
            veck[0] = avgMu[4]/2*veci[0] + avgMu[3]/2*veci[1];
            veck[1] = (avgMu[4]+avgLambda)*veci[1];
            veck[2] = avgMu[5]/2*veci[1] + avgMu[4]/2*veci[2];
            K[(9*maxNeig)*ip+9*ineig+4] += (veck[0]*vecj[0] + veck[1]*vecj[1] + veck[2]*vecj[2])/(36.*area);
  
            //K23 - Pointer+5
            veck[0] = avgMu[5]/2*veci[0] + avgMu[3]/2*veci[2];
            veck[1] = avgMu[5]/2*veci[1] + avgMu[4]/2*veci[2];
            veck[2] = avgLambda*veci[1] + avgMu[5]*veci[2];
            K[(9*maxNeig)*ip+9*ineig+5] += (veck[0]*vecj[0] + veck[1]*vecj[1] + veck[2]*vecj[2])/(36.*area);
          
            //K31 - Pointer+6
            veck[0] = avgMu[6]*veci[0] + avgLambda*veci[2];
            veck[1] = avgMu[7]/2*veci[0] + avgMu[6]/2*veci[1];
            veck[2] = avgMu[8]/2*veci[0] + avgMu[6]/2*veci[2];
            K[(9*maxNeig)*ip+9*ineig+6] += (veck[0]*vecj[0] + veck[1]*vecj[1] + veck[2]*vecj[2])/(36.*area);
  
            //K32 - Pointer+7
            veck[0] = avgMu[7]/2*veci[0] + avgMu[6]/2*veci[1];
            veck[1] = avgMu[7]*veci[1] + avgLambda*veci[2];
            veck[2] = avgMu[8]/2*veci[1] + avgMu[7]/2*veci[2];
            K[(9*maxNeig)*ip+9*ineig+7] += (veck[0]*vecj[0] + veck[1]*vecj[1] + veck[2]*vecj[2])/(36.*area);
  
            //K33 - Pointer+8
            veck[0] = avgMu[8]/2*veci[0] + avgMu[6]/2*veci[2];
            veck[1] = avgMu[8]/2*veci[1] + avgMu[7]/2*veci[2];
            veck[2] = (avgMu[8]+avgLambda)*veci[2];
            K[(9*maxNeig)*ip+9*ineig+8] += (veck[0]*vecj[0] + veck[1]*vecj[1] + veck[2]*vecj[2])/(36.*area);
          }
        }

        else
          K[maxNeig*ip + ineig] += avgOmega/(36.*area)*(veci[0]*vecj[0] + veci[1]*vecj[1] + veci[2]*vecj[2]);
    
      }
//    if (elast) {
//      b[ip]            += 0.5*((avgMu[0]+2*avgLambda)*veci[0] +  avgMu[1]*veci[1] +  avgMu[2]*veci[2]);
//      b[ip+mesh->np]   += 0.5*((avgMu[3])*veci[0] +  (avgMu[4]+2*avgLambda)*veci[1] +  avgMu[5]*veci[2]);
//      b[ip+2*mesh->np] += 0.5*((avgMu[6])*veci[0] +   avgMu[7]*veci[1] + (avgMu[8]+2*avgLambda)*veci[2]);
//
//    }
    }
  }


///// Calcul BC
//  if (elast)
//    FMG_computeBC2d(K, mesh, normal0, lambda, neigSparse, maxNeig);

  ///// Matrice symmetrique
  if (elast) {
    if(ijacobi==1){
      for (ip=1; ip<=mesh->np; ip++) {
        for (ineig=1;ineig<=neigSparse[ip*(maxNeig+1)];ineig++) {
          jp = neigSparse[ip*(maxNeig+1)+ineig];
          K[(9*maxNeig)*ip+9*(ineig-1)+1] = (K[(9*maxNeig)*ip+9*(ineig-1)+1] + K[(9*maxNeig)*ip+9*(ineig-1)+3] )/2.;
          K[(9*maxNeig)*ip+9*(ineig-1)+3] = K[(9*maxNeig)*ip+9*(ineig-1)+1];
          K[(9*maxNeig)*ip+9*(ineig-1)+2] = (K[(9*maxNeig)*ip+9*(ineig-1)+2] + K[(9*maxNeig)*ip+9*(ineig-1)+6] )/2.;
          K[(9*maxNeig)*ip+9*(ineig-1)+6] = K[(9*maxNeig)*ip+9*(ineig-1)+2];
          K[(9*maxNeig)*ip+9*(ineig-1)+5] = (K[(9*maxNeig)*ip+9*(ineig-1)+5] + K[(9*maxNeig)*ip+9*(ineig-1)+7] )/2.;
          K[(9*maxNeig)*ip+9*(ineig-1)+7] = K[(9*maxNeig)*ip+9*(ineig-1)+5];
        }
      }
    }
  }
////// CALCUL ALTERNATIF DE b
//  if (elast)
//    for (ip=1;ip<=mesh->np;ip++) {  
//      b[ip] = g3[ip]*totalArea[ip]/lonBoulep[ip];
//      b[ip+mesh->np] = g3[ip+mesh->np]*totalArea[ip]/lonBoulep[ip];
//    }

  ////// CALCUL ALTERNATIF 2 DE b
  if (elast) {
    for (ip=0;ip<3*mesh->np+3;ip++)
      b[ip] = 0.;

    for (iel=1; iel<=mesh->ne; iel++) {
      area = area0[iel];
      for (jloc=0; jloc<3; jloc++)
        tmpFrc[jloc] = 0.0;
      for (iloc=0; iloc<4; iloc++) {
        ip = mesh->tetra[iel].v[iloc];
        for (jloc=0; jloc<3; jloc++)
          tmpFrc[jloc] += g3[ip+jloc*mesh->np];
      }
      for (iloc=0; iloc<4; iloc++) {
        ip = mesh->tetra[iel].v[iloc];
        for (jloc=0; jloc<3; jloc++)
          b[ip+jloc*mesh->np] += area/16.*tmpFrc[jloc];
      }
    }
  }
}


/**
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param dx x deplacement for each point
 * \param dy y deplacement for each point
 * \param dz z deplacement for each point
 * \param Kx FEM matrix of coefficients (for the linear system resolving x coordinates)
 * \param Ky FEM matrix of coefficients (for the linear system resolving y coordinates)
 * \param Kz FEM matrix of coefficients (for the linear system resolving z coordinates)
 * \param b FEM's right-hand side
 * \param neigSparse list of neighbor points of each point
 * \param maxNeig maximum number of neighbors for a point in the mesh
 *
 * Computes one Jacobi iteration (considering the structure for stocking the sparse matrix K)
 */


void FMG_iterJacobiSparse3d(MMG5_pMesh mesh, double* dx, double* dy, double *dz,
                            double* dxk, double* dyk, double* dzk,
                            double* Kx, double* Ky, double* Kz,
                            double* b, int* neigSparse, int maxNeig, int elast, int aniso) {
  MMG5_pPoint  ppt;
  MMG5_pxPoint pxp;
  int          ip, jp, iloc, jloc, iel, ilist, k,ineig, ipip, max, imax;
  double       rhsx, rhsy, rhsz, eps=1e-6,dd,vx,vy,vz,tmpx,tmpy,tmpz,alpha,unx,uny,unz;
  double       prc = 20.0;//120.0;

  if (elast) {
    for (ip=1;ip<=mesh->np;ip++) {
      ppt = &mesh->point[ip];
      
      ipip = (9*maxNeig)*ip+9*(neigSparse[ip*(maxNeig+1)] -1);
      //printf("\n%d %d %d %lf %lf %lf",ipip,ip,neigSparse[ip*(maxNeig+1) + neigSparse[ip*(maxNeig+1)] ], K[ipip-1],K[ipip],K[ipip+1]);

      if (Kx[ipip] < eps && !(ppt->tag & MG_CRN)) {
        printf("\n\n!!!!!! ERROR JACOBI null diag : %d !!!!!!!",ip);
        abort();
      }

      rhsx = b[ip]+prc*dxk[ip];
      rhsy = b[ip+mesh->np]+prc*dyk[ip];
      rhsz = b[ip+2*mesh->np]+prc*dzk[ip];

      for (ineig=0;ineig<neigSparse[ip*(maxNeig+1)];ineig++) {

        jp = neigSparse[ip*(maxNeig+1)+ineig+1];
        
        if (jp != ip) {
          rhsx -= Kx[(9*maxNeig)*ip+9*ineig]*dxk[jp];
          rhsy -= Kx[(9*maxNeig)*ip+9*ineig+4]*dyk[jp];
          rhsz -= Kx[(9*maxNeig)*ip+9*ineig+8]*dzk[jp];
        }
        rhsx -= Kx[(9*maxNeig)*ip+9*ineig+1]*dyk[jp];
        rhsx -= Kx[(9*maxNeig)*ip+9*ineig+2]*dzk[jp];
        rhsy -= Kx[(9*maxNeig)*ip+9*ineig+3]*dxk[jp];
        rhsy -= Kx[(9*maxNeig)*ip+9*ineig+5]*dzk[jp];
        rhsz -= Kx[(9*maxNeig)*ip+9*ineig+6]*dxk[jp];
        rhsz -= Kx[(9*maxNeig)*ip+9*ineig+7]*dyk[jp];
      }

//      if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_LEFT ||  mesh->point[ip].ref == M_RIGHT)))
//        rhsx = 0;
//      else if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_BOTTOM ||  mesh->point[ip].ref == M_TOP)))
//        rhsy = 0;
//      else if (mesh->point[ip].tag & MG_CRN){
//        Kx[ipip]  = tgv;
//        Kx[ipip+4]  = tgv;
//        Kx[ipip+8]  = tgv;
//        rhsx = 0;
//        rhsy = 0;
//        rhsz = 0;
//      }

      /*bdry condition*/
      if ( (ppt->tag & MG_CRN ||/* ppt->tag & MG_GEO
        || ppt->tag & MG_REF ||*/ ppt->tag & MG_NOM) &&
          !(ppt->tag & M_DIRICHLET )) { /*nomove*/
        Kx[ipip] = tgv;
        Kx[ipip+4] = tgv;
        Kx[ipip+8] = tgv;
        rhsx = 0;
        rhsy = 0;
        rhsz = 0;
      }

      /*apply slip bc : projection on the tangent: n^(v^n)*/
#warning check the tags to move the edges/ridges
      if(ppt->tag & MG_BDY &&
          !(ppt->tag & MG_CRN || ppt->tag & MG_GEO ||
            ppt->tag & MG_REF || ppt->tag & MG_NOM) &&
          !(ppt->tag & M_DIRICHLET)) {
        assert(ppt->xp);
        pxp = &mesh->xpoint[ppt->xp];
        alpha = 1;//0.001;
        vx = alpha*rhsx/(Kx[ipip]+prc);
        vy = alpha*rhsy/(Kx[ipip+4]+prc);
        vz = alpha*rhsz/(Kx[ipip+8]+prc);
        unx = vy*pxp->n1[2] - vz * pxp->n1[1];
        uny = vz*pxp->n1[0] - vx * pxp->n1[2];
        unz = vx*pxp->n1[1] - vy * pxp->n1[0];
        tmpx = pxp->n1[1]*unz - pxp->n1[2]*uny;
        tmpy = pxp->n1[2]*unx - pxp->n1[0]*unz;
        tmpz = pxp->n1[0]*uny - pxp->n1[1]*unx;
        dx[ip] = tmpx;
        dy[ip] = tmpy;
        dz[ip] = tmpz;
      } // Slip BCs on ridges/edges
#warning Luca: check that it makes sense to do that on edges
      else if(ppt->tag & MG_BDY &&
              !(ppt->tag & MG_CRN || ppt->tag & MG_NOM) &&
              !(ppt->tag & M_DIRICHLET)) {
        assert(ppt->xp);
        pxp = &mesh->xpoint[ppt->xp];
        alpha = 1;//0.001;
        vx = alpha*rhsx/(Kx[ipip]+prc);
        vy = alpha*rhsy/(Kx[ipip+4]+prc);
        vz = alpha*rhsz/(Kx[ipip+8]+prc);
//        // Normalize w.r.t. n1
//        unx = vy*pxp->n1[2] - vz * pxp->n1[1];
//        uny = vz*pxp->n1[0] - vx * pxp->n1[2];
//        unz = vx*pxp->n1[1] - vy * pxp->n1[0];
//        tmpx = pxp->n1[1]*unz - pxp->n1[2]*uny;
//        tmpy = pxp->n1[2]*unx - pxp->n1[0]*unz;
//        tmpz = pxp->n1[0]*uny - pxp->n1[1]*unx;
//        // Normalize w.r.t. n2
//        unx = tmpy*pxp->n2[2] - tmpz * pxp->n2[1];
//        uny = tmpz*pxp->n2[0] - tmpx * pxp->n2[2];
//        unz = tmpx*pxp->n2[1] - tmpy * pxp->n2[0];
//        tmpx = pxp->n2[1]*unz - pxp->n2[2]*uny;
//        tmpy = pxp->n2[2]*unx - pxp->n2[0]*unz;
//        tmpz = pxp->n2[0]*uny - pxp->n2[1]*unx;
//        dx[ip] = tmpx;
//        dy[ip] = tmpy;
//        dz[ip] = tmpz;
        tmpx = ppt->n[0];
        tmpy = ppt->n[1];
        tmpz = ppt->n[2];
//        tmpx = pxp->n1[1]*pxp->n2[2] - pxp->n1[2]*pxp->n2[1];
//        tmpy = pxp->n1[2]*pxp->n2[0] - pxp->n1[0]*pxp->n2[2];
//        tmpz = pxp->n1[0]*pxp->n2[1] - pxp->n1[1]*pxp->n2[0];
        alpha = vx*tmpx + vy*tmpy + vz*tmpz;
        dx[ip] = alpha*tmpx;
        dy[ip] = alpha*tmpy;
        dz[ip] = alpha*tmpz;
      } else if( !(ppt->tag & M_DIRICHLET) ) {
        dx[ip] = rhsx/(Kx[ipip]+prc);
        dy[ip] = rhsy/(Kx[ipip+4]+prc);
        dz[ip] = rhsz/(Kx[ipip+8]+prc);
        
      }
    }
  }


 
  else {
    for (ip=1; ip<=mesh->np; ip++) {
      ppt = &mesh->point[ip];

      ipip = maxNeig*ip+neigSparse[ip*(maxNeig+1)]-1;

      if (Kx[ipip] < eps) {
        printf("\n\n!!!!!! ERROR JACOBI null diag X : %d : %lf!!!!!!!",ip,Kx[ipip]);
        abort();
      }
      if (aniso) {
        if (Ky[ipip] < eps) {
          printf("\n\n!!!!!! ERROR JACOBI null diag Y : %d !!!!!!!",ip);
          abort();
        }
        if (Kz[ipip] < eps) {
          printf("\n\n!!!!!! ERROR JACOBI null diag Z : %d !!!!!!!",ip);
          abort();
        }
      }


      rhsx = 0;
      rhsy = 0;
      rhsz = 0;

      for (ineig=0;ineig<neigSparse[ip*(maxNeig+1)];ineig++) {
        jp = neigSparse[ip*(maxNeig+1)+ineig+1];
        rhsx += Kx[maxNeig*ip + ineig] * mesh->point[jp].c[0];
        if (aniso) {
          rhsy += Ky[maxNeig*ip + ineig] * mesh->point[jp].c[1];
          rhsz += Kz[maxNeig*ip + ineig] * mesh->point[jp].c[2];
        }
        else {
          rhsy += Kx[maxNeig*ip + ineig] * mesh->point[jp].c[1];
          rhsz += Kx[maxNeig*ip + ineig] * mesh->point[jp].c[2];
        }
      }


      /*bdry condition*/
      if ( ppt->tag & MG_CRN || /*ppt->tag & MG_GEO
        || ppt->tag & MG_REF ||*/ ppt->tag & MG_NOM ) { /*nomove*/
        Kx[ipip] = 1;
        if (aniso) {
          Ky[ipip] = 1;
          Kz[ipip] = 1;
        }
        rhsx = 0;
        rhsy = 0;
        rhsz = 0;
      } else if ( ppt->tag & M_DIRICHLET ) { /*dirichlet non homogeneous*/
        dd = ppt->n[0]*ppt->n[0]  + ppt->n[1]*ppt->n[1] + ppt->n[2]*ppt->n[2]; 
        if( dd > 1e-6)
          printf("dirichlet non homogeneous (norm %e ) not implemented ==> no move\n"
            ,sqrt(dd));
        Kx[ipip] = 1;
        if (aniso) {
          Ky[ipip] = 1;
          Kz[ipip] = 1;
        }
        rhsx = 0;
        rhsy = 0;
        rhsz = 0;
      }
      /* //      printf("\n^^ %d %d %d",ip,mesh->point[ip].tag,mesh->point[ip].ref); */
      /*       if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_BACK ||  mesh->point[ip].ref == M_FRONT))) */
      /*         rhsx = 0.; */
      /*       else if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_LEFT ||  mesh->point[ip].ref == M_RIGHT))) */
      /*         rhsy = 0.; */
      /*       else if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_BOTTOM ||  mesh->point[ip].ref == M_TOP))) */
      /*         rhsz = 0.;   */
      /*       if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_LINEX))){ */
      /*         rhsy = 0.; */
      /*         rhsz = 0.; */
      /*       } */
      /*       if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_LINEY))){ */
      /*         rhsx = 0.; */
      /*         rhsz = 0.; */
      /*       } */
      /*       if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_LINEZ))){ */
      /*         rhsx = 0.; */
      /*         rhsy = 0.; */
      /*       } */

      /*       else if (mesh->point[ip].tag & MG_CRN){ */
      /* //    if (fabs(mesh->point[ip].c[0] - 1.) < eps || fabs(mesh->point[ip].c[1] - 1.) < eps || fabs(mesh->point[ip].c[2] - 1.) < eps) { */
      /*         Kx[ipip] = tgv; */
      /*         if (aniso) { */
      /*           Ky[ipip] = tgv; */
      /*           Kz[ipip] = tgv; */
      /*         }           */
      /*         rhsx = 0.; */
      /*         rhsy = 0.; */
      /*         rhsz = 0.; */
      /*       } */

      /*apply slip bc : projection on the tangent*/
#warning check the tags to move edges/ridges
      // Normal slip surface points (not corners, ridges, edges, non-manifold)
      if(ppt->tag & MG_BDY &&
         !(ppt->tag & MG_CRN || ppt->tag & MG_GEO ||
           ppt->tag & MG_REF || ppt->tag & MG_NOM) &&
         !(ppt->tag & M_DIRICHLET)) {
        assert(ppt->xp);
        pxp = &mesh->xpoint[ppt->xp];
        alpha = 1;//0.001;
        vx = alpha*rhsx/Kx[ipip];
        vy = alpha*rhsy/Kx[ipip];
        vz = alpha*rhsz/Kx[ipip];
        if(aniso) {
          printf("boum aniso\n");
          vy = alpha*rhsy/Ky[ipip];
          vz = alpha*rhsz/Kz[ipip];
        }
        unx = vy*pxp->n1[2] - vz * pxp->n1[1];
        uny = vz*pxp->n1[0] - vx * pxp->n1[2];
        unz = vx*pxp->n1[1] - vy * pxp->n1[0];
        tmpx = pxp->n1[1]*unz - pxp->n1[2]*uny;
        tmpy = pxp->n1[2]*unx - pxp->n1[0]*unz;
        tmpz = pxp->n1[0]*uny - pxp->n1[1]*unx;
        dx[ip] = dx[ip] - tmpx;
        dy[ip] = dy[ip] - tmpy;
        dz[ip] = dz[ip] - tmpz;
      } // Slip BCs on ridges and edges
      else if(ppt->tag & MG_BDY &&
              !(ppt->tag & MG_CRN || ppt->tag & MG_NOM) &&
              !(ppt->tag & M_DIRICHLET)) {
        assert(ppt->xp);
        pxp = &mesh->xpoint[ppt->xp];
        alpha = 1;//0.001;
        vx = alpha*rhsx/Kx[ipip];
        vy = alpha*rhsy/Kx[ipip];
        vz = alpha*rhsz/Kx[ipip];
        if(aniso) {
          printf("boum aniso\n");
          vy = alpha*rhsy/Ky[ipip];
          vz = alpha*rhsz/Kz[ipip];
        }
//        // Normalize w.r.t n1
//        unx = vy*pxp->n1[2] - vz * pxp->n1[1];
//        uny = vz*pxp->n1[0] - vx * pxp->n1[2];
//        unz = vx*pxp->n1[1] - vy * pxp->n1[0];
//        tmpx = pxp->n1[1]*unz - pxp->n1[2]*uny;
//        tmpy = pxp->n1[2]*unx - pxp->n1[0]*unz;
//        tmpz = pxp->n1[0]*uny - pxp->n1[1]*unx;
//        // Normalize w.r.t. n2
//        unx = tmpy*pxp->n2[2] - tmpz * pxp->n2[1];
//        uny = tmpz*pxp->n2[0] - tmpx * pxp->n2[2];
//        unz = tmpx*pxp->n2[1] - tmpy * pxp->n2[0];
//        tmpx = pxp->n2[1]*unz - pxp->n2[2]*uny;
//        tmpy = pxp->n2[2]*unx - pxp->n2[0]*unz;
//        tmpz = pxp->n2[0]*uny - pxp->n2[1]*unx;
        tmpx = ppt->n[0];
        tmpy = ppt->n[1];
        tmpz = ppt->n[2];
//        tmpx = pxp->n1[1]*pxp->n2[2] - pxp->n1[2]*pxp->n2[1];
//        tmpy = pxp->n1[2]*pxp->n2[0] - pxp->n1[0]*pxp->n2[2];
//        tmpz = pxp->n1[0]*pxp->n2[1] - pxp->n1[1]*pxp->n2[0];
        alpha = vx*tmpx + vy*tmpy + vz*tmpz;
        tmpx = alpha*tmpx;
        tmpy = alpha*tmpy;
        tmpz = alpha*tmpz;
        dx[ip] = dx[ip] - tmpx;
        dy[ip] = dy[ip] - tmpy;
        dz[ip] = dz[ip] - tmpz; 
      } else {
        dx[ip] = dx[ip] - rhsx/Kx[ipip];
        if (aniso) {
          dy[ip] = dy[ip] - rhsy/Ky[ipip];
          dz[ip] = dz[ip] - rhsz/Kz[ipip];
        }
        else {
          dy[ip] = dy[ip] - rhsy/Kx[ipip];
          dz[ip] = dz[ip] - rhsz/Kx[ipip];
        }
      }
    }   

  }
}


/**
 * \param K FEM matrix of coefficients
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param normal0 element's normal vectors in the initial mesh
 * \param lamdba lamdba values in each point of mesh
 * \param neigSparse list of neighbor points of each point
 * \param maxNeig maximum number of neighbors for a point in the mesh
 *
 * Computes boundary condition term for matrix K (necessary for elasticity model in non rectangular domaines or rectangular domaines not parallels to the axis)
 */


//void FMG_computeBC(double* K, MMG5_pMesh mesh,  double* normal0, double* lambda, int* neigSparse, int maxNeig) {
//
//  int iloc,jloc,ineig, k, ia, ib, ip, jp, iel, count, list[2];
//  double xa,ya,xb,yb,normal[2],Nnormal[2], length, add;
//  MMG5_pEdge pedge;
//  
//  for(k=1; k<=mesh->na; k++) {
//    iel = trEdges[k];
//
//    pedge = &mesh->edge[k];
//    ia = pedge->a;
//    ib = pedge->b;
//
//    xa = mesh->point[ia].c[0];
//    ya = mesh->point[ia].c[1];
//    xb = mesh->point[ib].c[0];
//    yb = mesh->point[ib].c[1];
//
//    length = sqrt(pow(xa-xb,2) + pow(ya-yb,2));
//
//    for (iloc=0; iloc<3; iloc++)
//      if (mesh->tria[iel].v[iloc] != ia && mesh->tria[iel].v[iloc] != ib) {
//        normal[0] = normal0[6*iel + 2*iloc];
//        normal[1] = normal0[6*iel + 2*iloc+1];
//        break;
//      }
//
//    Nnormal[0] = normal[0]/length;
//    Nnormal[1] = normal[1]/length;
//
//    for (count=1; count<=2; count++) {
//      if (count==1)
//        ip = ia;
//      else
//        ip = ib;
//
//      for (ineig=1;ineig<=neigSparse[ip*(maxNeig+1)];ineig++) {
//        jp = neigSparse[ip*(maxNeig+1)+ineig];
//
//        for (jloc=0; jloc<3; jloc++)
//          if (mesh->tria[iel].v[jloc] == jp)
//            break;
//
//        if (mesh->point[jp].tag & MG_BDY || mesh->point[jp].tag & MG_CRN) {
//          add = length/4./mesh->tria[iel].qual*
//                Nnormal[0]*pow(Nnormal[1],2)*(lambda[ia]+lambda[ib])*
//                (normal0[6*iel+2*jloc]*Nnormal[1] - normal0[6*iel+2*jloc+1]*Nnormal[0]);
//
////          printf ("add %lf\n",add);
//
//          K[(4*maxNeig)*ip+4*(ineig-1)] += add;
//          K[(4*maxNeig)*ip+4*(ineig-1)+1] -= add;
//          K[(4*maxNeig)*ip+4*(ineig-1)+2] -= add;
//          K[(4*maxNeig)*ip+4*(ineig-1)+3] += add;
// 
//        }
//
//      }         
//
//    }
//
//  }
//
//
//}


/**
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param Kx FEM matrix of coefficients (for the linear system resolving x coordinates)
 * \param Ky FEM matrix of coefficients (for the linear system resolving y coordinates)
 * \param Kz FEM matrix of coefficients (for the linear system resolving z coordinates)
 * \param res Jacobi residual for each system (1x3 array)
 * \param neigSparse list of neighbor points of each point
 * \param maxNeig maximum number of neighbors for a point in the mesh
 * \param aniso parameter indicating if the adaptation is isotropic or anisotropic
 *
 * Computes residuals of the resolution of the linear systems
 */

void FMG_computeJacobiResidual3d(MMG5_pMesh mesh, double *Kx, double *Ky, double* Kz, double *res, int *neigSparse, int maxNeig, int aniso) {

  int ip, jp, ineig;
  double ax, ay, az;

  res[0] = 0.;
  res[1] = 0.;
  res[2] = 0.;
  for (ip=1; ip<=mesh->np; ip++) {
    ax = 0.;
    ay = 0.;
    az = 0.;
    for (ineig=0;ineig<neigSparse[ip*(maxNeig+1)];ineig++) {
      jp = neigSparse[ip*(maxNeig+1)+ineig+1];
      if (!( (ip == jp) && (mesh->point[ip].tag & MG_CRN))) {
        ax += Kx[maxNeig*ip + ineig] * mesh->point[jp].c[0];
        if (aniso){
          ay += Ky[maxNeig*ip + ineig] * mesh->point[jp].c[1];
          az += Kz[maxNeig*ip + ineig] * mesh->point[jp].c[2];
        }
        else {
          ay += Kx[maxNeig*ip + ineig] * mesh->point[jp].c[1];
          az += Kx[maxNeig*ip + ineig] * mesh->point[jp].c[2];
        }
        //  printf("\n %d %d %lf %lf",ip,jp,Kx[maxNeig*ip + ineig],mesh->point[jp].c[0]);
      }
    }
    res[0] += ax*ax;
    res[1] += ay*ay;
    res[2] += az*az;
  }
  
  res[0] = sqrt(res[0]);
  res[1] = sqrt(res[1]);
  res[2] = sqrt(res[2]);
}



