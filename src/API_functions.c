/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"
#include "string.h"


/**
 * \param mesh pointer toward the mesh structure.
 * \param sol pointer toward the sol structure.
 * \param fmgdata  pointer toward the FMG_data structure
 * \param np number of solutions.
 * \param nsol number of solution.
 * \return 0 if failed, 1 otherwise.
 *
 * Set the solution number, dimension and type.
 *
 */
int FMG_Set_solSize(MMG5_pMesh mesh, MMG5_pSol sol,FMG_pData fmgdata,
    int np, int nsol) {
  FMG_pInfo info;

#warning check the free all
  if ( ( (mesh->info.imprim > 5) || mesh->info.ddebug ) && sol->m )
    fprintf(stdout,"  ## Warning: new solution\n");

  sol->size = nsol;

  sol->type = MMG5_Scalar;
  if ( np ) {
    sol->np  = np;
    sol->npi = np;
    if ( sol->m )
      MMG5_DEL_MEM(mesh,sol->m);

    sol->npmax = mesh->npmax;
    MMG5_ADD_MEM(mesh,(sol->size*(sol->npmax+1))*sizeof(double),"initial solution",
                  printf("  Exit program.\n");
                  exit(EXIT_FAILURE));
    MMG5_SAFE_CALLOC(sol->m,(sol->size*(sol->npmax+1)),double,return 0);
  }

  /*allocate*/
  info = fmgdata->info;
  info->nsols = nsol;
  MMG5_ADD_MEM(mesh,nsol*sizeof(int),"soladapt",
                printf("  Exit program.\n");
                 exit(EXIT_FAILURE));
  MMG5_SAFE_CALLOC(info->soladapt,nsol*sizeof(int),int,return 0);

  return(1);
}
/**
 * \param met pointer toward the sol structure.
 * \param fmgdata  pointer toward the FMG_data structure
 * \param s solution scalar value.
 * \param pos position of the solution in the mesh.
 * \param isol number of this solution (begin the numbering at 0)
 * \param adapt = 1/0 if adapt with this solution or not
 * \return 0 if failed, 1 otherwise.
 *
 * Set scalar value \a s at position \a pos in solution structure
 *
 */
int FMG_Set_scalarSol(MMG5_pSol met,FMG_pData fmgdata, double s, int pos,int isol, int adapt) {

  if ( !met->np ) {
    fprintf(stdout,"  ## Error: You must set the number of solution with the");
    fprintf(stdout," MMG2D_Set_solSize function before setting values");
    fprintf(stdout," in solution structure \n");
    return(0);
  }

  if ( pos >= met->npmax ) {
    fprintf(stdout,"  ## Error: unable to set a new solution.\n");
    fprintf(stdout,"    max number of solutions: %d\n",met->npmax);
    return(0);
  }

  if ( pos > met->np ) {
    fprintf(stdout,"  ## Error: attempt to set new solution at position %d.",pos);
    fprintf(stdout," Overflow of the given number of solutions: %d\n",met->np);
    fprintf(stdout,"  ## Check the solution size, its compactness or the position");
    fprintf(stdout," of the solution.\n");
    return(0);
  }

  met->m[isol*(met->np+1)+pos] = s;
  
  fmgdata->info->soladapt[isol]=adapt;
  return(1);
}

/**
 * \param met pointer toward the sol structure.
 * \param fmgdata  pointer toward the FMG_data structure
 * \param s pointer toward the scalar solution value.
 * \param pos position of the solution in the mesh.
 * \param isol number of this solution (begin the numbering at 0)
 * \return 0 if failed, 1 otherwise.
 *
 * Set scalar value \a s at position \a pos in solution structure
 *
 */
int FMG_Get_scalarSol(MMG5_pSol met, double* s, int pos,int isol) {

  if ( !met->np ) {
    fprintf(stdout,"  ## Error: You must set the number of solution with the");
    fprintf(stdout," MMG2D_Set_solSize function before setting values");
    fprintf(stdout," in solution structure \n");
    return(0);
  }

  if ( pos > met->np ) {
    fprintf(stdout,"  ## Error: unable to get the solution.\n");
    fprintf(stdout," Overflow of the given number of solutions: %d\n",met->np);
    fprintf(stdout,"  ## Check the solution size, its compactness or the position");
    fprintf(stdout," of the solution.\n");    return(0);
  }

  if ( isol > met->size ) {
    fprintf(stdout,"  ## Error: the solution number %d(/%d) does not exist.",isol,met->size);
    return(0);
  }

  *s = met->m[isol*(met->np+1)+pos];
  
  return(1);
}
/**
 * \param mesh pointer toward the mesh structure.
 * \param fmgdata  pointer toward the FMG_data structure
 *
 * initialize all the structures for fmglib2d (mesh analysis) 
 * and put default values for parameters
 *
 *
 */
int FMG_Init_fmg2d(MMG5_pMesh *mesh,FMG_pData *fmgdata) {
  int       k;

  if ( *fmgdata )  MMG5_SAFE_FREE(*fmgdata);
  MMG5_SAFE_CALLOC(*fmgdata,1,FMG_Data,return 0);

  if ( (*fmgdata)->info ) MMG5_SAFE_FREE((*fmgdata)->info);
  MMG5_SAFE_CALLOC(((*fmgdata)->info),1,FMG_Info,return 0);
  (*fmgdata)->data = (FMG_Datastr*) calloc(1,sizeof(FMG_Datastr));

  _FMG_Init_parameters((*fmgdata)->info);

  /* analysis */
  if ( !MMG2D_analys(*mesh))  return(FMG_STRONGFAILURE);

  // Test FMG_computeArea
  for( k=1; k<=(*mesh)->nt;k++) {
    (*mesh)->tria[k].qual = FMG_computeArea(*mesh,k);
    //   printf ("Area of triangle %d : %lf \n",k, mmgMesh->tria[k].qual);
  } 

  return(FMG_SUCCESS);
}

/**
 * \param mesh pointer toward the mesh structure.
 * \param sol pointer toward the sol structure
 * \param fmgdata  pointer toward the FMG_data structure
 *
 * allocate all the data structures needed for fmg computation
 *
 *
 */
int FMG_Start_fmg2d(MMG5_pMesh mesh,MMG5_pSol sol,FMG_pData fmgdata) {
  FMG_pDatastr  data;
  FMG_pInfo     param;
  int    k,ip,iel,iloc,list[1000],lon,jp;
  int    usefulMem;

  data = fmgdata->data;
  param = fmgdata->info;

  // For adaptation on analytical solution (not LS), switch on param->soladapt
  // (as it would be done in FMG_Set_scalarSol).
  if( param->physAdapt )
    if( !param->levelSet )
      param->soladapt[0]=1;
    else
      param->soladapt[1]=1;

  /*Gradient, Hessian and sol for physical domain (computed at each iteration)*/
  MMG5_SAFE_CALLOC(data->Grd,sol->size*(2*mesh->np+2),double,return 0);
  MMG5_SAFE_CALLOC(data->Hes,sol->size*(3*mesh->np+4),double,return 0);
  MMG5_SAFE_CALLOC(data->NSol,sol->size*(mesh->np+1),double,return 0);
  /*Norm of the gradient, hessian (computed at each iteration)*/
  MMG5_SAFE_CALLOC(data->NormGrd,sol->size*(mesh->np+1),double,return 0);
  MMG5_SAFE_CALLOC(data->NormHes,sol->size*(mesh->np+1),double,return 0);
  /*Omega*/

  MMG5_SAFE_CALLOC(data->OmegaX,mesh->np+1,double,return 0);
  if (param->aniso || param->model==2)
    MMG5_SAFE_CALLOC(data->OmegaY,mesh->np+1,double,return 0);
  /*dep computed by jacobi iteration*/
  MMG5_SAFE_CALLOC(data->dxk,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->dyk,mesh->np+1,double,return 0);
  if(param->model==2){
    MMG5_SAFE_CALLOC(data->dxk_aux,mesh->np+1,double,return 0);
    MMG5_SAFE_CALLOC(data->dyk_aux,mesh->np+1,double,return 0);
    MMG5_SAFE_CALLOC(data->weightModel,mesh->np+1,double,return 0);
  }

  /*LS solution and Grad of LS*/
  MMG5_SAFE_CALLOC(data->LS,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->GrdLS,2*mesh->np+2,double,return 0);
  /*init solution and position of the physical domain*/
#warning improve here : we need only to keep the solution on which we adapt
  MMG5_SAFE_CALLOC(data->sol0,sol->size*(mesh->np+1),double,return 0);
  MMG5_SAFE_CALLOC(data->x0,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->y0,mesh->np+1,double,return 0);
  /*position used in case of mesh optimization by point relocation*/
  MMG5_SAFE_CALLOC(data->xprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->yprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->dxkprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->dykprev,mesh->np+1,double,return 0);
  /*areas and normal of reference mesh : computed once*/
  MMG5_SAFE_CALLOC(data->area0,mesh->nt+1,double,return 0);
  MMG5_SAFE_CALLOC(data->normal0,6*mesh->nt+6,double,return 0);

  /*allocation for elasticity model*/
#warning do we need to have mu and lambda on each point ?
  if (param->model) {
    MMG5_SAFE_CALLOC(data->belast,2*mesh->np+2,double,return 0);
    MMG5_SAFE_CALLOC(data->mu,4*mesh->np+4,double,return 0);
    MMG5_SAFE_CALLOC(data->lambda,mesh->np+1,double,return 0);
  }
  //*************//
  //** LOCATES **//
  //*************//

  // *** Points
  data->maxNeig = 0;
  usefulMem = 0;
  for (ip=1; ip<=mesh->np; ip++) {
    iel = FMG_locatePointTria2d(mesh, mesh->point[ip].c[0], mesh->point[ip].c[1], mesh->point[ip].s);
    mesh->point[ip].s = iel;
    for (iloc=0;iloc<3;iloc++)
      if (mesh->tria[iel].v[iloc] == ip)
        break;
    lon = MMG2D_boulep(mesh,iel,iloc,list);
    if(lon > 999) {
      fprintf(stdout,"a lot of neighbour, check your mesh\n");
    }
    usefulMem += lon;
    if (lon>data->maxNeig)
      data->maxNeig = lon;
  }
  data->maxNeig += 1;


  //*******************//
  // Allouer matrice **//
  //*******************//
  if (param->sparse) {
    if(param->model==FMG_LAP) { 
      MMG5_SAFE_CALLOC(data->KsparseX,data->maxNeig*(mesh->np+1),double,return 0);
      if (param->aniso)
        MMG5_SAFE_CALLOC(data->KsparseY,data->maxNeig*(mesh->np+1),double,return 0);
    } else {
      MMG5_SAFE_CALLOC(data->KsparseX,4*data->maxNeig*(mesh->np+1),double,return 0);
      if (param->aniso)
        MMG5_SAFE_CALLOC(data->KsparseY,4*data->maxNeig*(mesh->np+1),double,return 0);
      if(param->model==FMG_MIX){
        MMG5_SAFE_CALLOC(data->KsparseX_aux,data->maxNeig*(mesh->np+1),double,return 0);
        if (param->aniso)
          MMG5_SAFE_CALLOC(data->KsparseY_aux,data->maxNeig*(mesh->np+1),double,return 0);

      }
    }
  }

  if (!param->model && !param->sparse) {
    MMG5_SAFE_CALLOC(data->K,mesh->np+1,double*,return 0);
    for (k=0; k<=mesh->np;k++)
      MMG5_SAFE_CALLOC(data->K[k],mesh->np+1,double,return 0);
  }

  if (param->sparse) {
    MMG5_SAFE_CALLOC(data->neigSparse,(data->maxNeig+1)*(mesh->np+1),int,return 0);
    FMG_constructSparseStruct2d(mesh, data->neigSparse, data->maxNeig);
  }

  FMG_Init_listquad( data );
  return(FMG_SUCCESS);
}

/**
 * \param mesh pointer toward the MMG5_Mesh structure
 * \param fmgdata  pointer toward the FMG_data structure
 *
 * deallocate all the data structures used by fmg 
 *
 *
 */
void FMG_End_fmg2d(MMG5_pMesh mesh,FMG_pData fmgdata) {
  FMG_pInfo param;
  FMG_pDatastr data;
  int       k;

  param = fmgdata->info;
  data  = fmgdata->data;

  MMG5_SAFE_FREE(data->Grd);
  MMG5_SAFE_FREE(data->Hes);
  MMG5_SAFE_FREE(data->NSol);
  MMG5_SAFE_FREE(data->NormGrd);
  MMG5_SAFE_FREE(data->NormHes);
  MMG5_SAFE_FREE(data->OmegaX);
  if (param->aniso || param->model==2)
    MMG5_SAFE_FREE(data->OmegaY);
  MMG5_SAFE_FREE(data->dxk);
  MMG5_SAFE_FREE(data->dyk);
  if(param->model==2){
    MMG5_SAFE_FREE(data->dxk_aux);
    MMG5_SAFE_FREE(data->dyk_aux);
    MMG5_SAFE_FREE(data->weightModel);
  }
  MMG5_SAFE_FREE(data->x0);
  MMG5_SAFE_FREE(data->y0);
  MMG5_SAFE_FREE(data->xprev);
  MMG5_SAFE_FREE(data->yprev);
  MMG5_SAFE_FREE(data->dxkprev);
  MMG5_SAFE_FREE(data->dykprev);
  MMG5_SAFE_FREE(data->area0);
  MMG5_SAFE_FREE(data->normal0);
  MMG5_SAFE_FREE(data->LS);
  MMG5_SAFE_FREE(data->GrdLS);
  MMG5_SAFE_FREE(data->sol0);

  if (param->model) {
    MMG5_SAFE_FREE(data->belast);
    MMG5_SAFE_FREE(data->mu);
    MMG5_SAFE_FREE(data->lambda);
  }

  MMG5_SAFE_FREE(data->neigSparse);

  if (param->sparse) {
    MMG5_SAFE_FREE(data->KsparseX);
    if (param->aniso)
      MMG5_SAFE_FREE(data->KsparseY);
  }

  if(param->model==FMG_MIX){
    MMG5_SAFE_FREE(data->KsparseX_aux);
    if(param->aniso)
      MMG5_SAFE_FREE(data->KsparseY_aux);
  }

  if (param->verbose>=2) printf("\n *** Fin desalouer vecteurs");

  if (!param->model && !param->sparse) {
    for (k=0; k<=mesh->np;k++)
      MMG5_SAFE_FREE(data->K[k]);
    MMG5_SAFE_FREE(data->K);
  }
  if (param->verbose >= 2) printf("\n *** Fin desallouer matrice");

  FMG_Free_listquad( data );
}

/**
 * \param param pointer toward the fmg_info structure.
 *
 * Initialization of the input parameters (stored in the Info structure).
 *
 */
int _FMG_Init_parameters(FMG_pInfo param) {

  int isol;

  param->verbose   =  1;  /* [-10..10],Tune level of imprim */
  param->imprim    =  0;  /* save the mesh at iteration modulo this parameter*/
  param->mem      = -1;  /* [n/-1]   ,Set memory size to n Mbytes/keep the default value */
  param->debug   = 0;

  param->circDom = 0; /*< [0/1], Rectangular/circular domain. For circular, no move on bdry */
  param->nbc     = 0; /*!< [val], number of boundary references (to impose bdry conditions)*/
  param->ijacobi_max = 10; /*< [n], Maximum number of Jacobi iterations*/

  param->iinner_max = 100; /*< [n], Maximum number of inner iterations in the mixed model*/

  /*metric adaptation parameters*/
  param->desSize = 1; /*metric adaptation (=1) / omega = sqrt(1+...) (=0)*/
  param->hmin = 0.0001;
  param->epsMet = param->hmin;
  param->hmax = 2.;

  /*omega parameters (param->desSize = 0)*/

  /*LS parameters*/
  param->levelSet = 0; /*activate levelSet Adaptation*/
  param->delta = 0.01; /*relative width for LS (0<delta<1)*/
  param->interpolate = 1; /*interpolate the sol (=1) or recompute the ls (=0)*/

  /*Physical parameters*/
  param->physAdapt = 0;


  param->sumAdapt = 0; 
  param->reference = 0;

#warning check and modify those parameters
  param->maxOptim = 0; /*quality mesh improvement (number of iter)*/
  param->revert = 0;

  /*only for library version*/
  param->fileID = 0; /*in order to begin the mesh at each iter with a number greater than 0*/

  param->normalize = 1; /**/
  param->sparse = 1;
  param->model = 0;
  param->aniso = 0;

  param->solCase  = 0;   /* 0 if solution read from .sol; solCase>0 if analytic */

  param->nsols = 2; /*number of solution to adapt (max 2 ; default 1)*/

  MMG5_SAFE_CALLOC(param->alpha,param->nsols,double,return 0);
  MMG5_SAFE_CALLOC(param->beta,param->nsols,double,return 0);
  MMG5_SAFE_CALLOC(param->tau,param->nsols,double,return 0);
  MMG5_SAFE_CALLOC(param->gammaG,param->nsols,double,return 0);
  MMG5_SAFE_CALLOC(param->gammaH,param->nsols,double,return 0);
  MMG5_SAFE_CALLOC(param->gammaS,param->nsols,double,return 0);

  for (isol=0; isol<param->nsols; isol++) {
    param->alpha[isol] = 1000.;
    param->beta[isol] = 0.;
    param->tau[isol] = 0.;
    param->gammaG[isol] = 1.;
    param->gammaH[isol] = 1.; 
    param->gammaS[isol] = 1.;
  }
  param->nsols = 1;

  MMG5_SAFE_CALLOC(param->refName,42,char,return 0);
  MMG5_SAFE_CALLOC(param->physName,42,char,return 0);
  MMG5_SAFE_CALLOC(param->lsName,42,char,return 0);

  strcpy(param->refName,"reference.mesh");
  strcpy(param->physName,"u");
  strcpy(param->lsName,"ls");
}

/**
 * \param data  pointer toward the FMG_data structure
 * \param iparam parameter to be set (FMG_IPARAM_*)
 * \param val value to attribute to the parameter
 * \return FMG_SUCCESS if success; FMG_LOWFAILURE else
 *
 * Attributes a value to an integer parameter
 *
 */
int FMG_Set_iparameter(MMG5_pMesh mesh,FMG_pData data, int iparam, int val) {
  FMG_pInfo  param;
  int        k;
  
  param = data->info;

  switch (iparam) {
  
  case (FMG_IPARAM_verbose) :
    param->verbose = val;
    break;
  case (FMG_IPARAM_numberOfBdry) :
    
    if ( param->bc ) {
      MMG5_DEL_MEM(mesh,param->bc);
      if ( (param->verbose > 5) )
        fprintf(stdout,"  ## Warning: new boundary parameter values\n");
    }
    param->nbc = val;  
    MMG5_ADD_MEM(mesh,param->nbc*sizeof(FMG_BC),"bdrycondition",
                  printf("  Exit program.\n");
                  exit(EXIT_FAILURE));
    MMG5_SAFE_CALLOC(param->bc,param->nbc,FMG_BC,return 0);
    for (k=0; k<param->nbc; k++) {
      param->bc[k].ref   = INT_MAX;
      param->bc[k].dep[0]   = 0.;
      param->bc[k].dep[1]   = 0.;
      param->bc[k].dep[2]   = 0.;
      param->bc[k].isslip   = 0.;
     }
    break;
  case (FMG_IPARAM_debug) :
    param->debug = val;
    break;
  case (FMG_IPARAM_imprim) :
    param->imprim = val;
    break;
  case (FMG_IPARAM_levelSet) :
    param->levelSet = val;
    break;
  case (FMG_IPARAM_maxOptim) : 
    if (val >= 0)
      param->maxOptim = val;
    else {
      printf("  ## Warning : maximum number of elements to optimize must be a non-negative integer\n");
      printf("  ## Reset to default value.\n");
      return (FMG_LOWFAILURE);
    }
    break;
  case (FMG_IPARAM_circDom) : 
    param->circDom = val;
    break;
  case (FMG_IPARAM_interp) : 
    param->interpolate = val;
    break;
  case (FMG_IPARAM_model) :
    param->model = val;
    break;
  case (FMG_IPARAM_nsols) : 
    if (val >= 0)
      param->nsols = val;
    else {
      printf("  ## Warning : number of solutions must be a strictly positive integer\n");
      printf("  ## Reset to default value.\n");
      return (FMG_LOWFAILURE);
    }
    
    if (val==1)
      param->sumAdapt=1;
    break;
  case (FMG_IPARAM_solCase) : 
    param->solCase = val;  
    break;
  case (FMG_IPARAM_fileID) :
    param->fileID = val;
    break;
    case (FMG_IPARAM_ijacobi_max) : 
      if (val >= 1)
        param->ijacobi_max = val;
      else {
        printf("  ## Warning : number of Jacobi iterations must be a strictly positive integer\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;
    case (FMG_IPARAM_iinner_max) : 
      if (val >= 1)
        param->iinner_max = val;
      else {
        printf("  ## Warning : number of inner iterations must be a strictly positive integer\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;
    case (FMG_IPARAM_revert) :
      param->revert = val;
      break;
    case (FMG_IPARAM_desSize) :
      param->desSize = val;
      break;
    case (FMG_IPARAM_physAdapt) :
      param->physAdapt = val;
      break;
    default : 
      printf("ERROR : unknown parameter!!\nEXIT\n\n");
      exit(FMG_STRONGFAILURE);
      return FMG_STRONGFAILURE;      

  }
  return FMG_SUCCESS;
    
}

/**
 * \param data  pointer toward the FMG_data structure
 * \param dparam parameter to be set (FMG_DPARAM_*)
 * \param val value to attribute to the parameter
 * \return FMG_SUCCESS if success; FMG_LOWFAILURE else
 *
 * Attributes a value to a double parameter
 *
 */

int FMG_Set_dparameter(MMG5_pMesh mesh,FMG_pData data, int dparam, double val) {
  FMG_pInfo param;
  int i;

  param = data->info;
 
  switch (dparam) {
  
    case (FMG_DPARAM_delta) :
//    if (param->levelSet) { 
      if (val > -FMG_eps)
        param->delta = val;
      else {
        printf("  ## Warning : smoothing region must have a nonegative size\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
//      }
//      else {
//        printf("  ## Warning : FMG_IPARAM_levelSet is set to 0, so FMG_DPARAM_delta has no sense\n");
//        printf("  ## Reset to default value : FMG_DPARAM_delta = 0.\n");
//        param->delta = 0.;
//      }

      break;
    case (FMG_DPARAM_alpha0) :
      if (val >= 0)
        param->alpha[0] = val;
      else {
        printf("  ## Warning : alpha must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;     
    case (FMG_DPARAM_beta0) :
      if (val >= 0)
        param->beta[0] = val;
      else {
        printf("  ## Warning : beta must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;     
    case (FMG_DPARAM_tau0) :
      if (val >= 0)
        param->tau[0] = val;
      else {
        printf("  ## Warning : tau must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;     
    case (FMG_DPARAM_alpha1) :
      if (param->nsols<1) {
        printf("  ## Warning : the mesh will be adapted only to one solution\n");
        printf("  ## The given value will be ignored.\n");
        return (FMG_LOWFAILURE);
        break;
      }
      if (val >= 0)
        param->alpha[1] = val;
      else {
        printf("  ## Warning : alpha must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;     
    case (FMG_DPARAM_beta1) :
      if (param->nsols<1) {
        printf("  ## Warning : the mesh will be adapted only to one solution\n");
        printf("  ## The given value will be ignored.\n");
        return (FMG_LOWFAILURE);
        break;
      }
      if (val >= 0)
        param->beta[1] = val;
      else {
        printf("  ## Warning : beta must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;     
    case (FMG_DPARAM_tau1) :
      if (param->nsols<1) {
        printf("  ## Warning : the mesh will be adapted only to one solution\n");
        printf("  ## The given value will be ignored.\n");
        return (FMG_LOWFAILURE);
        break;
      }
      if (val >= 0)
        param->tau[1] = val;
      else {
        printf("  ## Warning : alpha must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;    
    case (FMG_DPARAM_gammaG0) :
      if (val > 0)
        param->gammaG[0] = val;
      else {
        printf("  ## Warning : gammaG must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;    
    case (FMG_DPARAM_gammaH0) :
      if (val > 0)
        param->gammaH[0] = val;
      else {
        printf("  ## Warning : gammaH must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;    
    case (FMG_DPARAM_gammaS0) :
      if (val > 0)
        param->gammaS[0] = val;
      else {
        printf("  ## Warning : gammaS must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;    
    case (FMG_DPARAM_gammaG1) :
      if (val > 0)
        param->gammaG[1] = val;
      else {
        printf("  ## Warning : gammaG must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;    
    case (FMG_DPARAM_gammaH1) :
      if (val > 0)
        param->gammaH[1] = val;
      else {
        printf("  ## Warning : gammaH must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;    
    case (FMG_DPARAM_gammaS1) :
      if (val > 0)
        param->gammaS[1] = val;
      else {
        printf("  ## Warning : gammaS must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;    
    case (FMG_DPARAM_hmin) :
      if (val >= 0)
        param->hmin = val;
      else {
        printf("  ## Warning : hmin must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;    
    case (FMG_DPARAM_hmax) :
      if (val >= 0)
        param->hmax = val;
      else {
        printf("  ## Warning : hmax must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;     
    case (FMG_DPARAM_eps) :
      if (val >= 0)
        param->epsMet = val;
      else {
        printf("  ## Warning : epsMet must be non-negative\n");
        printf("  ## Reset to default value.\n");
        return (FMG_LOWFAILURE);
      }
      break;     

    default : 
      printf("ERROR : unknown parameter!!\nEXIT\n\n");
      exit(FMG_STRONGFAILURE);
    return FMG_STRONGFAILURE;      

  }
  return FMG_SUCCESS;

}

/**
 * \param mesh pointer toward the mesh structure.
 * \param met pointer toward the sol structure.
 * \param fmgparam pointer toward a pointer toward the FMG_info structure
 *
 * Deallocations before return.
 *
 */
void FMG_Free_all(MMG5_pMesh mesh,MMG5_pSol met,FMG_pInfo param
  ){

  if(mesh->dim==3) 
    MMG3D_Free_all(MMG5_ARG_start,
                   MMG5_ARG_ppMesh,mesh,MMG5_ARG_ppMet,met,
                   MMG5_ARG_end);
  else
    MMG2D_Free_all(MMG5_ARG_start,
                   MMG5_ARG_ppMesh,mesh,MMG5_ARG_ppMet,met,
                   MMG5_ARG_end);
  

}


int FMG_Read_commandlineparam(MMG5_pMesh mesh, FMG_pInfo param, int argc, char *argv[]) {

  int i, phys, ls;
  char *c;
  FILE *f;

  for (i=1; i<argc; i++) {
    if (!strcmp(argv[i],"-h")) {
      FMG_helpMsg(argv[0]);
      exit(0);
    }
    if (!strcmp(argv[i],"-3d")) {
      mesh->dim = 3;
    }
    if (!strcmp(argv[i],"-debug")) {
      param->debug = 1;
    }
    if (!strcmp(argv[i],"-delta")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->delta = atof(argv[i]);
      }
      else {
        printf("\n!!! Missing argument for option -delta\n\n");
        FMG_helpMsg(argv[0]);
      }
    }
    if (!strcmp(argv[i],"-hmin")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->hmin = atof(argv[i]);
      }
      else {
        printf("\n!!! Missing argument for option -hmin\n\n");
        FMG_helpMsg(argv[0]);
      }
    }
    if (!strcmp(argv[i],"-hmax")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->hmax = atof(argv[i]);
      }
      else {
        printf("\n!!! Missing argument for option -hmax\n\n");
        FMG_helpMsg(argv[0]);
      }
    }
    if (!strcmp(argv[i],"-eps")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->epsMet = atof(argv[i]);
      }
      else {
        printf("\n!!! Missing argument for option -eps\n\n");
        FMG_helpMsg(argv[0]);
      }
    }



    if (!strcmp(argv[i],"-revert")) {
      param->revert = 1;
    }
    if (!strcmp(argv[i],"-circ")) {
      param->circDom = 1;
    }

    if (!strcmp(argv[i],"-it")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->ijacobi_max = atoi(argv[i]);
      }
      else {
        printf("\n!!! Missing argument for option -it\n\n");
        FMG_helpMsg(argv[0]);
      }    
    }
    if (!strcmp(argv[i],"-itmix")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->iinner_max = atoi(argv[i]);
      }
      else {
        printf("\n!!! Missing argument for option -itmix\n\n");
        FMG_helpMsg(argv[0]);
      }    
    }
    if (!strcmp(argv[i],"-save")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->imprim = atoi(argv[i]);
      }
      else {
        printf("\n!!! Missing argument for option -save\n\n");
        FMG_helpMsg(argv[0]);
      }    
    }
   

//  if (!strcmp(argv[i],"-ls")) {
//    param->levelSet = 1;
//  }
    if (!strcmp(argv[i],"-grad")) {
      param->desSize = 0;
    }

    if (!strcmp(argv[i],"-ls")) {
      param->levelSet = 1;
      MMG5_SAFE_REALLOC(param->lsName,42,strlen(argv[i+1])+1,char,"",return 0);
      strcpy(param->lsName,argv[i+1]);
 
      // check physical solution file
      MMG5_SAFE_CALLOC(c,strlen(param->lsName)+6,char,return 0);
      sprintf(c,"%s.sol",param->lsName);
      if (!(f = fopen(c,"r"))) {
        fprintf(stdout,"\n LEVEL SET SOLUTION FILE %s NOT FOUND\n",c);
        fprintf(stdout,"\n ABORTING FMG\n\n");
        exit(0);
      }
      MMG5_SAFE_FREE(c);
    }

    if (!strcmp(argv[i],"-phys")) {
      param->physAdapt = 1;
      MMG5_SAFE_REALLOC(param->physName,42,strlen(argv[i+1])+1,char,"",return 0);
      strcpy(param->physName,argv[i+1]);
 
      // check physical solution file
      MMG5_SAFE_CALLOC(c,strlen(param->physName)+6,char,return 0);
      sprintf(c,"%s.sol",param->physName);
      if (!(f = fopen(c,"r"))) {
        fprintf(stdout,"\n PHYSICAL SOLUTION FILE %s NOT FOUND\n",c);
        fprintf(stdout,"\n ABORTING FMG\n\n");
        exit(0);
      }
      MMG5_SAFE_FREE(c);
    }
    if (!strcmp(argv[i],"-ref")) {
      param->reference = 1;
      MMG5_SAFE_REALLOC(param->refName,42,strlen(argv[i+1])+1,char,"",return 0);
      strcpy(param->refName,argv[i+1]);
//      printf("\n$$$ REFERENCE MESH : %s",argv[i+1]);
    }
    if (!strcmp(argv[i],"-val")) {
      FMG_showDefaultValues(mesh,param);
    } else  if (!strcmp(argv[i],"-v")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->verbose = atoi(argv[i]);
      }
      else {
        printf("\n!!! Missing argument for option -v\n\n");
        FMG_helpMsg(argv[0]);
      }  
    }

    if (!strcmp(argv[i],"-model")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->model = atoi(argv[i]);
      }
      else {
        printf("\n!!! Missing arguments for option -model\n\n");
        FMG_helpMsg(argv[0]);
      }
    }

    if (!strcmp(argv[i],"-alpha")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->alpha[0] = atof(argv[i]);
      }
      else {
        printf("\n!!! Missing arguments for option -alpha\n\n");
        FMG_helpMsg(argv[0]);
      }
      if ((param->levelSet && param->physAdapt) || param->model==2) {
        if ( ++i < argc && isdigit(argv[i][0]) ) {
          param->alpha[1] = atof(argv[i]);
        }
        else {
          printf("\n!!! Missing second argument for option -alpha\n\n");
          FMG_helpMsg(argv[0]);
        }
      }
    }

    if (!strcmp(argv[i],"-beta")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->beta[0] = atof(argv[i]);
      }
      else {
        printf("\n!!! Missing arguments for option -beta\n\n");
        FMG_helpMsg(argv[0]);
      }
      if ((param->levelSet && param->physAdapt) || param->model==2) {
        if ( ++i < argc && isdigit(argv[i][0]) ) {
          param->beta[1] = atof(argv[i]);
        }
        else {
          printf("\n!!! Missing second argument for option -beta\n\n");
          FMG_helpMsg(argv[0]);
        }
      }
    }

    if (!strcmp(argv[i],"-tau")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->tau[0] = atof(argv[i]);
      }
      else {
        printf("\n!!! Missing arguments for option -tau\n\n");
        FMG_helpMsg(argv[0]);
      }
      if ((param->levelSet && param->physAdapt) || param->model==2) {
        if ( ++i < argc && isdigit(argv[i][0]) ) {
          param->tau[1] = atof(argv[i]);
        }
        else {
          printf("\n!!! Missing second argument for option -tau\n\n");
          FMG_helpMsg(argv[0]);
        }
      }
    }

    if (!strcmp(argv[i],"-gammaG")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->gammaG[0] = atof(argv[i]);
      }
      else {
        printf("\n!!! Missing arguments for option -gammaG\n\n");
        FMG_helpMsg(argv[0]);
      }
      if (param->model==2) {
        if ( ++i < argc && isdigit(argv[i][0]) ) {
          param->gammaG[1] = atof(argv[i]);
        }
        else {
          printf("\n!!! Missing second argument for option -gammaG\n\n");
          FMG_helpMsg(argv[0]);
        }
      }
    }
 
    if (!strcmp(argv[i],"-gammaH")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->gammaH[0] = atof(argv[i]);
      }
      else {
        printf("\n!!! Missing arguments for option -gammaH\n\n");
        FMG_helpMsg(argv[0]);
      }
      if (param->model==2) {
        if ( ++i < argc && isdigit(argv[i][0]) ) {
          param->gammaH[1] = atof(argv[i]);
        }
        else {
          printf("\n!!! Missing second argument for option -gammaH\n\n");
          FMG_helpMsg(argv[0]);
        }
      }
    }
   
    if (!strcmp(argv[i],"-gammaS")) {
      if ( ++i < argc && isdigit(argv[i][0]) ) {
        param->gammaS[0] = atof(argv[i]);
      }
      else {
        printf("\n!!! Missing arguments for option -gammaS\n\n");
        FMG_helpMsg(argv[0]);
      }
      if (param->model==2) {
        if ( ++i < argc && isdigit(argv[i][0]) ) {
          param->gammaS[1] = atof(argv[i]);
        }
        else {
          printf("\n!!! Missing second argument for option -gammaS\n\n");
          FMG_helpMsg(argv[0]);
        }
      }
    }

  }


  if (!param->levelSet && !param->physAdapt) {
    fprintf(stdout,"\n ERROR : Please select option -ls and/or -phys [filename]");
    fprintf(stdout,"\n ABORTING FMG\n\n");
    exit(0);
  }

}



void FMG_helpMsg(char *prog) {

  fprintf(stdout,"\nUsage : %s [options]",prog);

  fprintf(stdout,"\n\n*** Generic options ***");
  fprintf(stdout,"\n*    -h : show this message");
  fprintf(stdout,"\n*    -v [n]  Tune level of verbosity, [-10..10]");
  fprintf(stdout,"\n*    -val : show default values for the parameters");
  fprintf(stdout,"\n*    -debug : active le mode de débougage");

  fprintf(stdout,"\n\n*** File specifications ***");
  fprintf(stdout,"\n*    -ref file.mesh: use file.mesh as reference mesh");
  fprintf(stdout,"\n*    -phys [physSol]: turn on physical adaptation, with physSol.sol (if -phys AND -ls are used) or dom.sol (if ONLY -phys is used) containing solution values");
  fprintf(stdout,"\n*    -ls [lsSol]: turn on Level Set adaptation, with lsSol.sol (if -phys AND -ls are used) or dom.sol (if ONLY -ls is used) containing solution values ");

  fprintf(stdout,"\n\n*** Adaptation options and parameters ***");
  fprintf(stdout,"\n\n*** Parameters");
  fprintf(stdout,"\n*    -delta val : width for LS");
  fprintf(stdout,"\n*    -it val : number of iterations");
  fprintf(stdout,"\n*    -save val : save mesh each val iteration");
  fprintf(stdout,"\n*    -3d : turn on 3D adaptation");
  fprintf(stdout,"\n*    -revert : turn on unadaptation");
  fprintf(stdout,"\n*    -hmin : minimal desired size for the computation of the metric");
  fprintf(stdout,"\n*    -hmax : minimal desired size for the computation of the metric");
  fprintf(stdout,"\n*    -eps : error bound for the computation of the metric");
  fprintf(stdout,"\n*    -grad : turn on adaptation based on the gradient and hessian of the solution");
  fprintf(stdout,"\n*    -alpha val1 [val2] : multiplying coefficient for the gradient if -grad is selected; if -phys and -ls are both selected, val1 is for phys. and val2 for LS");
  fprintf(stdout,"\n*    -beta val1 [val2] : multiplying coefficient for the hessian if -grad is selected; if -phys and -ls are both selected, val1 is for phys. and val2 for LS");

  fprintf(stdout,"\n\n");
  exit(0);
}

void FMG_showDefaultValues(MMG5_pMesh mesh, FMG_pInfo param) {

  fprintf(stdout,"\n*** Default values :");

  fprintf(stdout,"\n*    delta : %lf",param->delta);
  fprintf(stdout,"\n*    it : %d",param->ijacobi_max);
  fprintf(stdout,"\n*    itmix : %d",param->iinner_max);
  fprintf(stdout,"\n*    reference file : %s",param->refName);
  fprintf(stdout,"\n*    physical file : %s.sol",param->physName);
  fprintf(stdout,"\n*    level set file : %s.sol", param->lsName);
  fprintf(stdout,"\n*    ls : %d", param->levelSet);
  fprintf(stdout,"\n*    phys : %d", param->physAdapt);
  fprintf(stdout,"\n*    dimension : %d", mesh->dim);
  fprintf(stdout,"\n*    revert : %d", param->revert);
  fprintf(stdout,"\n*    v : %d", param->verbose);
  fprintf(stdout,"\n*    hmin : %lf", param->hmin);
  fprintf(stdout,"\n*    hmax : %lf", param->hmax);
  fprintf(stdout,"\n*    eps : %lf", param->epsMet);
  fprintf(stdout,"\n*    grad : %d", (param->desSize-1)*(-1));
  fprintf(stdout,"\n*    alpha : %lf %lf", param->alpha[0], param->alpha[1]);
  fprintf(stdout,"\n*    beta : %lf %lf", param->beta[0], param->beta[1]);

  fprintf(stdout,"\n\n");
  exit(0);
}


