/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"
#include "string.h"

/**
 * \param mesh pointer toward the mesh structure.
 * \param fmgdata  pointer toward the FMG_data structure
 *
 * initialize all the structures for fmglib2d (mesh analysis) 
 * and put default values for parameters
 *
 *
 */
int FMG_Init_fmg3d(MMG5_pMesh *mesh,FMG_pData *fmgdata) {
  int       k;

  if ( *fmgdata )  MMG5_SAFE_FREE(*fmgdata);
  MMG5_SAFE_CALLOC(*fmgdata,1,FMG_Data,return 0);

  if ( (*fmgdata)->info ) MMG5_SAFE_FREE((*fmgdata)->info);
  MMG5_SAFE_CALLOC(((*fmgdata)->info),1,FMG_Info,return 0);
  (*fmgdata)->data = (FMG_Datastr*) calloc(1,sizeof(FMG_Datastr));

  _FMG_Init_parameters((*fmgdata)->info);

  /* analysis */
  if ( !MMG3D_analys(*mesh))  return(FMG_STRONGFAILURE);

  // Test FMG_computeVolume
  for( k=1; k<=(*mesh)->ne;k++) {
    (*mesh)->tetra[k].qual = FMG_computeVolume(*mesh,k);
    //   printf ("Volume of tetra %d : %lf \n",k, mmgMesh->tetra[k].qual);
  } 

  return(FMG_SUCCESS);
}



/**
 * \param mesh pointer toward the mesh structure.
 * \param sol pointer toward the sol structure
 * \param fmgdata  pointer toward the FMG_data structure
 *
 * allocate all the data structures needed for fmg computation
 *
 *
 */
int FMG_Start_fmg3d(MMG5_pMesh mesh,MMG5_pSol sol,FMG_pData fmgdata) {
  FMG_pDatastr  data;
  FMG_pInfo     param;
  int    k,ip,iel,iloc,list[1000],lon,jp;
  int    usefulMem;
  int ineig,jloc,nneig,flag,*visitedNeig,ilist;

  data = fmgdata->data;
  param = fmgdata->info;

  /*Gradient, Hessian and sol for physical domain (computed at each iteration)*/
  MMG5_SAFE_CALLOC(data->Grd,sol->size*(3*mesh->np+3),double,return 0);
  MMG5_SAFE_CALLOC(data->Hes,sol->size*(6*mesh->np+6),double,return 0);
  MMG5_SAFE_CALLOC(data->NSol,sol->size*(mesh->np+1),double,return 0);
  /*Norm of the gradient, hessian (computed at each iteration)*/
  MMG5_SAFE_CALLOC(data->NormGrd,sol->size*(mesh->np+1),double,return 0);
  MMG5_SAFE_CALLOC(data->NormHes,sol->size*(mesh->np+1),double,return 0);
  /*Omega*/
  MMG5_SAFE_CALLOC(data->OmegaX,mesh->np+1,double,return 0);
  if (param->aniso || param->model==2)
    MMG5_SAFE_CALLOC(data->OmegaY,mesh->np+1,double,return 0);
  if (param->aniso)
    MMG5_SAFE_CALLOC(data->OmegaZ,mesh->np+1,double,return 0);
  /*dep computed by jacobi iteration*/
  MMG5_SAFE_CALLOC(data->dxk,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->dyk,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->dzk,mesh->np+1,double,return 0);
  if(param->model==2){
    MMG5_SAFE_CALLOC(data->dxk_aux,mesh->np+1,double,return 0);
    MMG5_SAFE_CALLOC(data->dyk_aux,mesh->np+1,double,return 0);
    MMG5_SAFE_CALLOC(data->dzk_aux,mesh->np+1,double,return 0);
    MMG5_SAFE_CALLOC(data->weightModel,mesh->np+1,double,return 0);
  }

  /*LS solution and Grad of LS*/
  MMG5_SAFE_CALLOC(data->LS,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->GrdLS,3*mesh->np+3,double,return 0);
  /*init solution and position of the physical domain*/
#warning improve here : we need only to keep the solution on which we adapt
  MMG5_SAFE_CALLOC(data->sol0,sol->size*(mesh->np+1),double,return 0);
  MMG5_SAFE_CALLOC(data->x0,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->y0,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->z0,mesh->np+1,double,return 0);
  /*position used in case of mesh optimization by point relocation*/
  MMG5_SAFE_CALLOC(data->xprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->yprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->zprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->dxkprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->dykprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(data->dzkprev,mesh->np+1,double,return 0);
  /*areas and normal of reference mesh : computed once*/
  MMG5_SAFE_CALLOC(data->area0,mesh->ne+1,double,return 0);
  MMG5_SAFE_CALLOC(data->normal0,12*mesh->ne+12,double,return 0);

  MMG5_SAFE_CALLOC(data->rlxpt,param->maxOptim+1,int,return 0);

  /*allocation for elasticity model*/
#warning do we need to have mu and lambda on each point ?
  if (param->model) {
    MMG5_SAFE_CALLOC(data->belast,3*mesh->np+3,double,return 0);
    MMG5_SAFE_CALLOC(data->mu,9*mesh->np+9,double,return 0);
    MMG5_SAFE_CALLOC(data->lambda,mesh->np+1,double,return 0);
  }
  //*************//
  //** LOCATES **//
  //*************//
#warning to use less memory, use this
  //MMG5_SAFE_CALLOC(visitedNeig,mesh->np,int,return 0);
  // *** Points
  data->maxNeig = 0;
  usefulMem = 0;
  for(ip=1; ip<=mesh->np; ip++) mesh->point[ip].s = 0;
  for(iel=1; iel<=mesh->ne; iel++){
    for(iloc=0; iloc<4; iloc++){
      ip = mesh->tetra[iel].v[iloc];
      if(!(mesh->point[ip].s)) mesh->point[ip].s = iel;
    }
  }
  for (ip=1; ip<=mesh->np; ip++) {
    iel = mesh->point[ip].s;
    for (iloc=0;iloc<4;iloc++)
      if (mesh->tetra[iel].v[iloc] == ip)
        break;
    if(iloc==4) printf("pbs here %d : %d -- %d %d %d %d\n",ip,iel,mesh->tetra[iel].v[0]
        ,mesh->tetra[iel].v[1],mesh->tetra[iel].v[2],mesh->tetra[iel].v[3]);
    assert(iloc<4);
    lon = MMG5_boulevolp(mesh,iel,iloc,list);
    if(lon > 999) {
      fprintf(stdout,"a lot of neighbour, check your mesh\n");
    }
    /* visitedNeig[0] = ip; */
    /* nneig = 1; */
    /* for (ilist=0; ilist<lon; ilist++) { */
    /*   iel = list[ilist]/4; */
    /*   for (jloc=0; jloc<4; jloc++) { */
    /*     flag = 1; */
    /*     jp = mesh->tetra[iel].v[jloc]; */
    /*     if (jp != ip) { */
    /*       flag = 0; */
    /*       for(ineig=0; ineig<nneig; ineig++) */
    /*         if (visitedNeig[ineig] == jp) { */
    /*           flag = 1; */
    /*           break; */
    /*         } */
    /*     } */
    /*     if (!flag) { */
    /*       visitedNeig[nneig] = jp; */
    /*       nneig++; */
    /*     } */
    /*   } */
    /* } */
    usefulMem += lon; //+=nneig ??
    if (lon>data->maxNeig)
      data->maxNeig = lon;
      /* if (nneig>data->maxNeig) */
      /*  data->maxNeig = nneig; */
  }
  data->maxNeig += 1;


  //*******************//
  // Allouer matrice **//
  //*******************//
  if (param->sparse) {
    if(param->model==FMG_LAP) {
      MMG5_SAFE_CALLOC(data->KsparseX,data->maxNeig*(mesh->np+1),double,return 0);
      if (param->aniso) {
        MMG5_SAFE_CALLOC(data->KsparseY,data->maxNeig*(mesh->np+1),double,return 0);
        MMG5_SAFE_CALLOC(data->KsparseZ,data->maxNeig*(mesh->np+1),double,return 0);
      }
    } else {
      MMG5_SAFE_CALLOC(data->KsparseX,9*data->maxNeig*(mesh->np+1),double,return 0);
      if (param->aniso) {
        MMG5_SAFE_CALLOC(data->KsparseY,9*data->maxNeig*(mesh->np+1),double,return 0);
        MMG5_SAFE_CALLOC(data->KsparseZ,9*data->maxNeig*(mesh->np+1),double,return 0);
      }
      if(param->model==FMG_MIX){
        MMG5_SAFE_CALLOC(data->KsparseX_aux,data->maxNeig*(mesh->np+1),double,return 0);
        if (param->aniso) {
          MMG5_SAFE_CALLOC(data->KsparseY_aux,data->maxNeig*(mesh->np+1),double,return 0);
          MMG5_SAFE_CALLOC(data->KsparseZ_aux,data->maxNeig*(mesh->np+1),double,return 0);

        }
      }
    }
  }

  if (!param->model && !param->sparse) {
    MMG5_SAFE_CALLOC(data->K,mesh->np+1,double*,return 0);
    for (k=0; k<=mesh->np;k++)
      MMG5_SAFE_CALLOC(data->K[k],mesh->np+1,double,return 0);
  }

  if (param->sparse) {
    MMG5_SAFE_CALLOC(data->vballp,(data->maxNeig+1)*(mesh->np+1),int,return 0);
    MMG5_SAFE_CALLOC(data->neigSparse,(data->maxNeig+1)*(mesh->np+1),int,return 0);
    FMG_constructSparseStruct3d(mesh, data->vballp, data->neigSparse, data->maxNeig);
  }
  FMG_Init_listquad( data );
  return(FMG_SUCCESS);
}

/**
 * \param mesh pointer toward the MMG5_Mesh structure
 * \param fmgdata  pointer toward the FMG_data structure
 *
 * deallocate all the data structures used by fmg 
 *
 *
 */
void FMG_End_fmg3d(MMG5_pMesh mesh,FMG_pData fmgdata) {
  FMG_pInfo param;
  FMG_pDatastr data;
  int       k;

  param = fmgdata->info;
  data  = fmgdata->data;

  MMG5_SAFE_FREE(data->Grd);
  MMG5_SAFE_FREE(data->Hes);
  MMG5_SAFE_FREE(data->NSol);
  MMG5_SAFE_FREE(data->NormGrd);
  MMG5_SAFE_FREE(data->NormHes);
  MMG5_SAFE_FREE(data->OmegaX);
  if (param->aniso || param->model==2)
    MMG5_SAFE_FREE(data->OmegaY);
  if (param->aniso)
    MMG5_SAFE_FREE(data->OmegaZ);
  MMG5_SAFE_FREE(data->dxk);
  MMG5_SAFE_FREE(data->dyk);
  MMG5_SAFE_FREE(data->dzk);
  if(param->model==2){
    MMG5_SAFE_FREE(data->dxk_aux);
    MMG5_SAFE_FREE(data->dyk_aux);
    MMG5_SAFE_FREE(data->dzk_aux);
    MMG5_SAFE_FREE(data->weightModel);
  }
  MMG5_SAFE_FREE(data->x0);
  MMG5_SAFE_FREE(data->y0);
  MMG5_SAFE_FREE(data->z0);
  MMG5_SAFE_FREE(data->xprev);
  MMG5_SAFE_FREE(data->yprev);
  MMG5_SAFE_FREE(data->zprev);
  MMG5_SAFE_FREE(data->dxkprev);
  MMG5_SAFE_FREE(data->dykprev);
  MMG5_SAFE_FREE(data->dzkprev);
  MMG5_SAFE_FREE(data->area0);
  MMG5_SAFE_FREE(data->normal0);
  MMG5_SAFE_FREE(data->LS);
  MMG5_SAFE_FREE(data->GrdLS);
  MMG5_SAFE_FREE(data->sol0);

  if (param->model) {
    MMG5_SAFE_FREE(data->belast);
    MMG5_SAFE_FREE(data->mu);
    MMG5_SAFE_FREE(data->lambda);
  }

  MMG5_SAFE_FREE(data->vballp);
  MMG5_SAFE_FREE(data->neigSparse);

  if (param->sparse) {
    MMG5_SAFE_FREE(data->KsparseX);
    if (param->aniso) {
      MMG5_SAFE_FREE(data->KsparseY);
      MMG5_SAFE_FREE(data->KsparseZ);
    }
  }

  if (param->model==FMG_MIX) {
    MMG5_SAFE_FREE(data->KsparseX_aux);
    if (param->aniso) {
      MMG5_SAFE_FREE(data->KsparseY_aux);
      MMG5_SAFE_FREE(data->KsparseZ_aux);
    }
  }

  if (param->verbose>=2) printf("\n *** Fin desalouer vecteurs");

  if (!param->model && !param->sparse) {
    for (k=0; k<=mesh->np;k++)
      MMG5_SAFE_FREE(data->K[k]);
    MMG5_SAFE_FREE(data->K);
  }
  if (param->verbose >= 2) printf("\n *** Fin desallouer matrice");

  FMG_Free_listquad( data );
}

