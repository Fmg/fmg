/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/



#include "fmg.h"
#include "math.h"

/**
 * \param mesh : pointer on the mesh structure
 * \param sol : pointer on the sol structure
 * \param param : parameters for the algorithm
 *
 * Adapts the mesh to a given solution
 *
 */

void FMG_movemsh3d(MMG5_pMesh mesh, MMG5_pSol sol, FMG_pDatastr data,FMG_pInfo param, double *xref, double *yref,double *zref) {

  int                ijacobi, finaljacobi, iinner, iinner_max, ismooth=0, ismooth_max=5, smoothed=0, k, ip, jp, iloc, jloc, iel, ilist, ineig, nneig,
                     stop, lon=0, verbose = 1, ixmax, iymax, izmax, ipip, cnt1, cnt2, cnt, isol, optimized, origOUT;
  int                *list, *visitedNeig, *rlxpt;
  double             *NormSolH1, *Grd,*Hes, *NSol, *NormGrd, *NormHes, *OmegaX, *OmegaY, *OmegaZ, 
                     *dxk, *dyk, *dzk, *dxk_aux, *dyk_aux, *dzk_aux, *LS, *GrdLS, *sol0, *x0, *y0, *z0, *xprev, *yprev, *zprev, 
                     *dxkprev, *dykprev, *dzkprev, *area0, *normal0, *normalk, **K;
  double             rhsx,rhsy,relax_param=1., *relax_paramArr, *rmin, eps= 1e-12, dxmax, dymax, dzmax, normal[3],resid[3],dxavg,dyavg,dzavg,a;
  double             minvol,maxvol;
  char               fname[15], fname2[20], *fileNames[] = {"phys","ls"};
  FILE               *resFile;
  MMG5_pTetra        ptr;

  // Elasticite lineaire
  int *neigSparse;
  double *KsparseX, *KsparseY, *KsparseZ, *KsparseX_aux, *KsparseY_aux, *KsparseZ_aux, *belast, *mu, *lambda, *weightModel;
  double E, nu;
  double ax, ay, az, edge_size, edge_size_min, edge_size_max;

  // Linear elasticity parameters
  E = 1.0;//0.5*pow(param->alpha[0]+param->beta[0]+param->tau[0],0.5);
  nu = 0.45;//0.1;


  setbuf(stdout, NULL);
  if (param->verbose >= 1){fprintf(stdout,"\n*********************************************\n"); 
                          fprintf    (stdout,"********* FMG : Mesh Adaptation *************\n"); 
                          fprintf    (stdout,"*********************************************\n\n");
                          fprintf(stdout,"git branch: %s\n",FMG_GIT_BRANCH);
                          fprintf(stdout,"git commit: %s\n",FMG_GIT_COMMIT);
                          fprintf(stdout,"git date:   %s\n\n",FMG_GIT_DATE);
                          if (param->fileID == 0) {
                          fprintf    (stdout,"**** Output files : dom.*.mesh \n");
                          fprintf    (stdout,"                    dom.*.sol  \n");
                          }
                          else {
                          fprintf    (stdout,"**** Output files : dom.%d*.mesh \n",param->fileID);
                          fprintf    (stdout,"                    dom.%d*.sol  \n",param->fileID);
                          }
                          }


  if (param->verbose >= 3){
    printf("\n ****** Paramètres choisis par l'utilisateur : ");
    printf("\n ** levelSet : %d",param->levelSet);
    printf("\n ** circDom : %d",param->circDom);
    printf("\n ** nsols : %d %d",param->nsols,sol->size);
    printf("\n ** delta : %lf",param->delta);
    printf("\n ** alpha,beta : %lf %lf",param->alpha[0], param->beta[0]);
    printf("\n ******");
  }

  rlxpt   = data->rlxpt ;
  Grd     = data->Grd;
  Hes     = data->Hes;
  NSol    = data->NSol;
  NormGrd = data->NormGrd;
  NormHes = data->NormHes;
  OmegaX  = data->OmegaX;
  OmegaY  = data->OmegaY;
  OmegaZ  = data->OmegaZ;

  dxk     = data->dxk;
  dyk     = data->dyk;
  dzk     = data->dzk;
  dxk_aux = data->dxk_aux;
  dyk_aux = data->dyk_aux;
  dzk_aux = data->dzk_aux;

  LS      = data->LS;
  GrdLS   = data->GrdLS;
  sol0    = data->sol0;
  x0      = data->x0;
  y0      = data->y0;
  z0      = data->z0;
  xprev   = data->xprev;
  yprev   = data->yprev;
  zprev   = data->zprev;
  dxkprev = data->dxkprev;
  dykprev = data->dykprev;
  dzkprev = data->dzkprev;
  area0   = data->area0;
  normal0 = data->normal0;

  neigSparse = data->neigSparse; 
  KsparseX   = data->KsparseX;  
  KsparseY   = data->KsparseY;
  KsparseZ   = data->KsparseZ; 
  KsparseX_aux = data->KsparseX_aux;
  KsparseY_aux = data->KsparseY_aux;
  KsparseZ_aux = data->KsparseZ_aux;
  weightModel  = data->weightModel;

  K          = data->K; 
  belast     = data->belast;
  mu         = data->mu;
  lambda     = data->lambda;

  if (param->levelSet)
    for (ip=1;ip<=mesh->np;ip++) {
      LS[ip] = sol->m[ip+(mesh->np+1)*(sol->size-1)];
    }

  if (param->levelSet && !param->desSize) {
    FMG_defineObject(mesh,sol,LS);
    if (param->delta > 1e-6)
      FMG_smoothObject(mesh, sol, LS, param->delta);
  }

  // Solution analytique sur le maillage initiale
  if (param->solCase) {
    FMG_analytic(mesh,sol,param);
    if (param->verbose >= 3) printf("\n *** Fin calcul analytique sur le maillage initiale");
  }

  /* Store pt->s into pt->tmp (it is need for LS gradient and hessian) */
  for (ip=1;ip<=mesh->np;ip++)
    mesh->point[ip].tmp = mesh->point[ip].s;

  for (ip=1;ip<=mesh->np;ip++) {
    for (isol=1; isol<= sol->size; isol++)
      sol0[(isol-1)*(mesh->np+1) + ip] = sol->m[(isol-1)*(mesh->np+1) + ip];
    x0[ip] = mesh->point[ip].c[0];
    y0[ip] = mesh->point[ip].c[1];
    z0[ip] = mesh->point[ip].c[2];
  }
 
  for (ip=1; ip<=mesh->np; ip++){
    dxk[ip] = x0[ip]-xref[ip];
    dyk[ip] = y0[ip]-yref[ip];
    dzk[ip] = z0[ip]-zref[ip];
    dxkprev[ip] = dxk[ip];
    dykprev[ip] = dyk[ip];
    dzkprev[ip] = dzk[ip];
    if(param->model==2){
      dxk_aux[ip] = 0;
      dyk_aux[ip] = 0;
      dzk_aux[ip] = 0;
    }
  }

#warning : stock normal0 or recompute at each jacobi iteration ?
  /*computation of area and normal on ref mesh*/
  minvol = 1.0/MMG5_EPS;
  for (iel=1; iel<=mesh->ne; iel++) {
    mesh->tetra[iel].qual = FMG_computeVolumeRef(mesh,iel,xref,yref,zref);
    area0[iel] = mesh->tetra[iel].qual;
    if( area0[iel] < minvol ) minvol = area0[iel];
    for (iloc=0;iloc<4;iloc++){
      FMG_computeNormalVector3dRef(normal, mesh, iel, iloc,xref,yref,zref);
      normal0[12*iel+3*iloc] = normal[0]; 
      normal0[12*iel+3*iloc+1] = normal[1];   
      normal0[12*iel+3*iloc+2] = normal[2];   
    }
  }

  /* Correct hmin to make it compatible with smaller than the minimum
   * volume in the reference mesh */
  if( minvol < pow(param->hmin,3.0)*sqrt(2.0)/12.0 ) {
    param->hmin = pow(12.0*minvol/sqrt(2.0),1./3);
    printf("\nWARNING: Updated minimum characteristic length: %e\n", param->hmin);
  }


  if (param->imprim) {
    sol->size = 1;
    sprintf(fname, "dom.%d.mesh", param->fileID + 0);
    if ( !MMG3D_Set_outputMeshName(mesh,fname) )  exit(EXIT_FAILURE);
    if ( !MMG3D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);
    sprintf(fname, "dom.%d.sol", param->fileID + 0);
    if ( !MMG3D_Set_outputSolName(mesh,sol,fname) ) exit(EXIT_FAILURE);
    if ( !MMG3D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
    sol->size = param->nsols;
  }

 
  MMG5_SAFE_CALLOC(normalk,12*mesh->ne+12,double,return); 
  MMG5_SAFE_CALLOC(relax_paramArr,mesh->np+1,double,return);
  MMG5_SAFE_CALLOC(rmin,mesh->np+1,double,return);
  for(ip=1; ip<=mesh->np; ip++)
    relax_paramArr[ip] = 1.0;


  // Calcul Omega cas mixte
  if (param->model == 2) {
    a = .500000;
    for (ip=1; ip<=mesh->np; ip++) {
//      mu[9*ip] =  sqrt(1. + param->tau*pow(NSol[ip],2) + param->alpha*pow(NormGrd[ip],2) + param->beta*pow(NormHes[ip],2));
      mu[9*ip] = 2.0*E/(2.*(1+nu));//a*OmegaX[ip];
      mu[9*ip+1] = 0.;
      mu[9*ip+2] = 0.;
      mu[9*ip+3] = 0.;
      mu[9*ip+4] = mu[9*ip];
      mu[9*ip+5] = 0.;
      mu[9*ip+6] = 0.;
      mu[9*ip+7] = 0.;
      mu[9*ip+8] = mu[9*ip];

      lambda[ip] = (E*nu)/((1+nu)*(1-2*nu));//(OmegaX[ip] - mu[9*ip])/2.;
//      lambda[ip] = a*mu[4*ip];
    }
    if (param->verbose >= 3) printf("\n *** Fin calcul mu et lambda");

    param->alpha[1] = 0.0;
    param->beta[1] = 0.0;
    param->tau[1] = 0.0;
    FMG_computeLinSystSparse3d(KsparseX, belast, mesh, area0, normal0, OmegaX, mu, lambda, neigSparse, data->vballp, data->maxNeig, param->model,sol,1);
    if (param->aniso) {
      FMG_computeLinSystSparse3d(KsparseY, belast, mesh, area0, normal0, OmegaY, mu, lambda, neigSparse, data->vballp, data->maxNeig, param->model,sol,1);
      FMG_computeLinSystSparse3d(KsparseZ, belast, mesh, area0, normal0, OmegaZ, mu, lambda, neigSparse, data->vballp, data->maxNeig, param->model,sol,1);
     if (param->verbose >= 3) printf("\n *** Fin calcul matrice");
    }
  }


//  //******************//
//  //** Info memoire **//
//  //******************//
//
//  if (param->verbose >= 4) {
//    if (verbose) printf("\n* MAXNEIG = %d",maxNeig);
//    if (verbose) printf("\n* Useful Mem with sparse structure = %d/%d = %lf in K; %d/%d = %lf in neigSparse",
//                         usefulMem,(3*param->model+1)*maxNeig*(mesh->np+1),
//                         (double)usefulMem/(3*param->model+1)/maxNeig/(mesh->np+1),
//                         usefulMem+mesh->np,((3*param->model+1)*maxNeig+1)*(mesh->np+1),
//                         (double)(usefulMem+mesh->np)/((3*param->model+1)*maxNeig+1)/(mesh->np+1));
//    if (verbose) printf("\n* Useful Mem with original structure = %d/%lf = %lf in K",
//                         usefulMem,pow((param->model+1)*(mesh->np+1),2),
//                         (double)usefulMem/pow((param->model+1)*(mesh->np+1),2));
//  } 





/////////// Block de test : DIFFUSION HYPERBOLIQUE
//  param->ijacobi_max = 0;
//  FMG_diffusionHyperbolique(mesh, sol, param, OmegaX, normal0,
//                               Grd, Hes, NormGrd, NormHes, NSol, x0, y0, sol0);
///////////

////// Block de test : OPTIMISATION sur un element (voir resultat sur dom.9999)
//  param->ijacobi_max=0;
//  ptr=&mesh->tetra[6846];
//  printf("\n AVANT : %e",MMG5_caltet_iso(mesh,sol,ptr->v[0],ptr->v[1],ptr->v[2],ptr->v[3]));
//  FMG_optlen3d(mesh,sol,6846);
//  printf("\n APRES : %e",MMG5_caltet_iso(mesh,sol,ptr->v[0],ptr->v[1],ptr->v[2],ptr->v[3]));
//////

//  if (param->desSize)
//    FMG_computeDesiredSize(mesh, OmegaX, LS, Grd, param, 0);


//  FMG_readRefMesh3d(mesh, "reference.mesh", x0, y0, z0, normal0, area0);
  /* for (ip=1;ip<=mesh->np;ip++) { */
  /*   for (isol=1; isol<= sol->size; isol++) */
  /*     sol0[(isol-1)*(mesh->np+1) + ip] = sol->m[(isol-1)*(mesh->np+1) + ip]; */
  /*   x0[ip] = mesh->point[ip].c[0]; */
  /*   y0[ip] = mesh->point[ip].c[1]; */
  /*   z0[ip] = mesh->point[ip].c[2]; */
  /* } */


  if (param->debug)
    resFile = fopen("residu","w");
  optimized = 0;
  stop = 0;
  verbose = param->verbose;
  //****************//
  //** ITERATIONS **//
  //****************//
  for (ijacobi = 1; ijacobi <= param->ijacobi_max; ijacobi++){
  
    for (iel=1; iel<=mesh->ne; iel++) {
      for (iloc=0; iloc<4; iloc++) {
        FMG_computeNormalVector3d(normal,mesh,iel,iloc);
        normalk[12*iel+3*iloc]   = normal[0];
        normalk[12*iel+3*iloc+1] = normal[1];
        normalk[12*iel+3*iloc+2] = normal[2];
      }
    }

#warning LUCA: check this loop
    /*compute the minimum and maximum edge length*/
    if (ijacobi == 1) {
      edge_size_min = 1000.;
      edge_size_max = 0.;
      // Loop on elements
      for(iel=1;iel<=mesh->nt;iel++){
        ptr = &mesh->tetra[iel];
        // Assembly total area and vertex contribution
        for(iloc=0;iloc<3;iloc++){
          for(jloc=iloc+1;jloc<4;jloc++){
    
            /* compute edge size */
            ax = xref[ptr->v[iloc]] - xref[ptr->v[jloc]];
            ay = yref[ptr->v[iloc]] - yref[ptr->v[jloc]];
            az = zref[ptr->v[iloc]] - zref[ptr->v[jloc]];
      
            edge_size = sqrt(ax*ax+ay*ay+az*az);
            if (edge_size < edge_size_min) 
              edge_size_min = edge_size;
            if (edge_size > edge_size_max) 
              edge_size_max = edge_size;
          }
        }
      }
    }

  while(1) { // pour refaire l'iteration si optimized
 
    if (param->verbose >= 2) {
      if (!optimized) printf("\n\n******** ITERATION %d *********",ijacobi);
      else            printf("\n\n******** ITERATION %d BIS *********",ijacobi);
    }


    dxmax = 0.;
    dymax = 0.;
    dzmax = 0.;
    ixmax = 0;
    iymax = 0;
    izmax = 0;


    FMG_computeGrd3d(Grd,mesh,sol,data->vballp,data->maxNeig);
    //FMG_computeGrdLS_3d(Grd,mesh,sol);
    //FMG_computeHes3d(Hes,mesh,sol,Grd);
    FMG_computeHessLS_3d(Hes,mesh,sol,Grd);
     
    if (param->verbose >= 3) printf("\n *** Fin compute Grd et Hes");

    FMG_computeNorm(Grd,NormGrd,mesh->np,3,sol->size);  // OK
    FMG_computeTensorNorm(Hes,NormHes,mesh->np,3,sol->size); // OK

    for (isol=0; isol<sol->size; isol++)
      for (ip=1; ip<=mesh->np; ip++)
        NSol[isol*mesh->np + ip] = sol->m[isol*mesh->np + ip];

    if(param->normalize) {
      FMG_normalizeVec(NormGrd,mesh->np,param->gammaG[0],sol->size);
      FMG_normalizeVec(NormHes,mesh->np,param->gammaH[0],sol->size);
      FMG_normalizeVec(NSol,mesh->np,param->gammaS[0],sol->size);
    }
    if (param->verbose >= 3) printf("\n *** Fin normalisation");


    if (param->desSize) {
      if (param->levelSet) 
        if (!param->interpolate)
          FMG_readLS(mesh, LS, ijacobi-1);
        else 
          if (param->nsols == 1)
            for (ip=1; ip<=mesh->np;ip++)
              LS[ip] = sol->m[ip];
          else
            for (ip=1; ip<=mesh->np;ip++)
              LS[ip] = sol->m[ip+mesh->np+1];
      
      
      FMG_computeDesiredSize3d(mesh, sol, param, OmegaX, Hes, LS, edge_size_min, edge_size_max);
    }


    // Calcul Omega 
    cnt1 = 0;
    cnt2 = 0;

    if (!param->revert) {  
      if (!param->desSize && param->model==2){ // Adaptation on physical solution, mixed (2) model
        // Use OmegaY for the Laplacian (0) part.
        for (isol=0; isol<sol->size; isol++) {
          for (ip=1; ip<=mesh->np; ip++) {
            OmegaY[ip] = 1.;
            OmegaY[ip] += param->tau[0]*pow(NSol[isol*(mesh->np+1) + ip],2) +
                          param->alpha[0]*pow(NormGrd[isol*(mesh->np+1) + ip],2) +
                          param->beta[0]*pow(NormHes[isol*(mesh->np+1) + ip],2);
            OmegaY[ip] = sqrt(OmegaY[ip]); // OK
          }
        }
        // Compute new normalization for the elastic (1) part.
        FMG_computeNorm(Grd,NormGrd,mesh->np,3,sol->size);  // OK
        FMG_computeTensorNorm(Hes,NormHes,mesh->np,3,sol->size); // OK
        for (isol=0; isol<sol->size; isol++)
          for (ip=1; ip<=mesh->np; ip++)
            NSol[isol*(mesh->np+1) + ip] = sol->m[isol*(mesh->np+1) + ip];
        if(param->normalize) {
          FMG_normalizeVec(NormGrd,mesh->np,param->gammaG[1],sol->size);
          FMG_normalizeVec(NormHes,mesh->np,param->gammaH[1],sol->size);
          FMG_normalizeVec(NSol,mesh->np,param->gammaS[1],sol->size);
        }
        // Use OmegaX for the elastic (1) part.
        for (isol=0; isol<sol->size; isol++) {
          for (ip=1; ip<=mesh->np; ip++) {
            OmegaX[ip] = 1.;
            OmegaX[ip] += param->tau[1]*pow(NSol[isol*(mesh->np+1) + ip],2) +
                          param->alpha[1]*pow(NormGrd[isol*(mesh->np+1) + ip],2) +
                          param->beta[1]*pow(NormHes[isol*(mesh->np+1) + ip],2);
            OmegaX[ip] = sqrt(OmegaX[ip]); // OK
          }
        }
        if (param->aniso)
          printf("ERROR: You can't use anisotropy with the mixed model\n");
      }
      else if (!param->desSize && param->model!=2) 
        for (ip=1; ip<=mesh->np; ip++) {
          OmegaX[ip] = 1.;

          if(ismooth) continue; // Constant Laplacian for smoothing
    
          if (!param->sumAdapt) {
            if (param->alpha[0]*NormGrd[ip] > param->alpha[1]*NormGrd[mesh->np+1 + ip]) {
               NormGrd[mesh->np + 1 +ip] = 0.;
               cnt1++;
            }
            else
               NormGrd[ip] = 0;
            if (param->beta[0]*NormHes[ip] > param->beta[1]*NormHes[mesh->np+1 + ip]) {
               NormHes[mesh->np + 1 +ip] = 0.;
               cnt2 ++;
            }
            else
               NormHes[ip] = 0;
          }
    
          for (isol=0; isol<sol->size; isol++)
            OmegaX[ip] += param->tau[isol]*pow(NSol[isol*(mesh->np+1) + ip],2) +
                           param->alpha[isol]*pow(NormGrd[isol*mesh->np + ip],2) +
                           param->beta[isol]*pow(NormHes[isol*mesh->np + ip],2);
            OmegaX[ip] = sqrt(OmegaX[ip]); // OK
            if (param->aniso) {
              OmegaY[ip] = OmegaX[ip];
              OmegaZ[ip] = OmegaX[ip];
            }
        }
  
      else {
          for (ip=1; ip<=mesh->np; ip++) {
            OmegaX[ip] = 1./OmegaX[ip];
            if (param->aniso) {
#warning if aniso, we could compute 3 eigenvalues on desisze!
              OmegaY[ip] = OmegaX[ip];;
              OmegaZ[ip] = OmegaX[ip];;
            }
          }
      }
    }
    if (param->revert)
      for (ip=1; ip<=mesh->np; ip++) {
        OmegaX[ip] = 1.;
        if (param->aniso) {
          OmegaY[ip] = 1.;
          OmegaZ[ip] = 1.;
        }
      }
        
    if (param->verbose >= 3) printf("\n *** Fin calcul Omega : %lf %lf",(double)cnt1/mesh->np,(double)cnt2/mesh->np);

    // Calcul Omega cas elasticite
    if (param->model == 1) {
      a = .500000;
      for (ip=1; ip<=mesh->np; ip++) {
//      mu[9*ip] =  sqrt(1. + param->tau*pow(NSol[ip],2) + param->alpha*pow(NormGrd[ip],2) + param->beta*pow(NormHes[ip],2));
        mu[9*ip] = 2.0*E/(2.*(1+nu));//a*OmegaX[ip];
        mu[9*ip+1] = 0.;
        mu[9*ip+2] = 0.;
        mu[9*ip+3] = 0.;
        mu[9*ip+4] = mu[9*ip];
        mu[9*ip+5] = 0.;
        mu[9*ip+6] = 0.;
        mu[9*ip+7] = 0.;
        mu[9*ip+8] = mu[9*ip];

        lambda[ip] = (E*nu)/((1+nu)*(1-2*nu));//(OmegaX[ip] - mu[9*ip])/2.;
//      lambda[ip] = a*mu[4*ip];
      }
      if (param->verbose >= 3) printf("\n *** Fin calcul mu et lambda");
    }

//    for(ip=1; ip<=mesh->np; ip++)
//      OmegaX[ip] = 1.0+pow(relax_paramArr[ip],1.0)*(OmegaX[ip]-1.0);
//    for(ip=1; ip<=10; ip++)
//      FMG_smoothFunction3d(OmegaX,OmegaX,mesh,area0);


    // With the mixed model, solve laplacian first
    if(param->model==2){

      // Construction et resolution du systeme
      FMG_computeLinSystSparse3d(KsparseX_aux, belast, mesh, area0, normal0, OmegaY, mu, lambda, neigSparse, data->vballp, data->maxNeig, param->model-2,sol,ijacobi);
      if (param->aniso){
        FMG_computeLinSystSparse3d(KsparseY_aux, belast, mesh, area0, normal0, OmegaY, mu, lambda, neigSparse, data->vballp, data->maxNeig, param->model-2,sol,ijacobi);
        FMG_computeLinSystSparse3d(KsparseZ_aux, belast, mesh, area0, normal0, OmegaZ, mu, lambda, neigSparse, data->vballp, data->maxNeig, param->model-2,sol,ijacobi);
      }
      if (param->verbose >= 3) printf("\n *** Fin calcul matrice");
      FMG_iterJacobiSparse3d(mesh, dxk, dyk, dzk, dxkprev, dykprev, dzkprev, KsparseX_aux, KsparseY_aux, KsparseZ_aux, belast, neigSparse, data->maxNeig, param->model-2, param->aniso);

    } else {

      // Construction et resolution du systeme
      FMG_computeLinSystSparse3d(KsparseX, belast, mesh, area0, normal0, OmegaX, mu, lambda, neigSparse, data->vballp, data->maxNeig, param->model,sol,ijacobi);
      if (param->aniso) {
        FMG_computeLinSystSparse3d(KsparseY, belast, mesh, area0, normal0, OmegaY, mu, lambda, neigSparse, data->vballp, data->maxNeig, param->model,sol,ijacobi);
        FMG_computeLinSystSparse3d(KsparseZ, belast, mesh, area0, normal0, OmegaZ, mu, lambda, neigSparse, data->vballp, data->maxNeig, param->model,sol,ijacobi);
      }
      if (param->verbose >= 3) printf("\n *** Fin calcul matrice");
      FMG_iterJacobiSparse3d(mesh, dxk, dyk, dzk, dxkprev, dykprev, dzkprev, KsparseX, KsparseY, KsparseZ, belast, neigSparse, data->maxNeig, param->model, param->aniso);

    }
    if (param->verbose >= 3) printf("\n *** Fin calcul deplacements");
 
//    // Blend laplacian and elastic model
//    if(param->model==2){
//      FMG_blendModelsMetric3d(mesh, sol, param, weightModel, Hes, LS, edge_size_min,edge_size_max);
//      for(ip=1; ip<=mesh->np; ip++){
//        dxk[ip] = (1.0-weightModel[ip])*dxk_aux[ip]+weightModel[ip]*dxk[ip];
//        dyk[ip] = (1.0-weightModel[ip])*dyk_aux[ip]+weightModel[ip]*dyk[ip];
//        dzk[ip] = (1.0-weightModel[ip])*dzk_aux[ip]+weightModel[ip]*dzk[ip];
//      }
//      /*FMG_blendModels3d(OmegaX,mesh,area0,weightModel);*/
//      /* for(ip=1; ip<=mesh->np; ip++){ */
//      /*   dxk[ip] = (1.0-weightModel[ip])*dxk[ip]+weightModel[ip]*dxk_aux[ip]; */
//      /*   dyk[ip] = (1.0-weightModel[ip])*dyk[ip]+weightModel[ip]*dyk_aux[ip]; */
//      /*   dzk[ip] = (1.0-weightModel[ip])*dzk[ip]+weightModel[ip]*dzk_aux[ip]; */
//      /* } */
//    }
    
    dxmax = 0.;
    dymax = 0.;
    dzmax = 0.;
    for (ip=1; ip<=mesh->np; ip++) {
      if (fabs(dxk[ip])>dxmax){
        dxmax = fabs(dxk[ip]);
        ixmax = ip;
      }
      if (fabs(dyk[ip])>dymax){
        dymax = fabs(dyk[ip]);
        iymax = ip;
      }
      if (fabs(dzk[ip])>dzmax){
        dzmax = fabs(dzk[ip]);
        izmax = ip;
      }

    }


    for (ip=1; ip<=mesh->np; ip++) {
       xprev[ip] = mesh->point[ip].c[0];
       yprev[ip] = mesh->point[ip].c[1];
       zprev[ip] = mesh->point[ip].c[2];
    }

    // si on n'a pas encore optimise dans cette iteration
    if (!optimized) {
//      cnt = FMG_prechkArea3d(mesh,sol,x0,y0,z0,dxk,dyk,dzk,xprev,yprev,zprev,param->maxOptim,rlxpt);
//      if (param->verbose >= 3) printf("\n *** Nb d'éléments qui demandent de la relaxation : %d", cnt ); 
 
      // s'il faut optimiser
      if (param->maxOptim && rlxpt[0] > 0 && ijacobi < 3){ // && rlxpt[0]<=maxrlxpt) {
        if (param->verbose >= 3) printf("\n ******* DEBUT OPTIMISATION (%d elements)",rlxpt[0]);
        for (iel=1; iel<=rlxpt[0]; iel++)
          FMG_optlen3d(mesh,sol,rlxpt[iel]);
        if (param->verbose >= 3) printf("\n ******* FIN OPTIMISATION");
        optimized = 1;
        ijacobi --;
        for (ip=1; ip<=mesh->np; ip++) {
          dxk[ip] = dxkprev[ip];
          dyk[ip] = dykprev[ip];
          dzk[ip] = dzkprev[ip];
        }
        FMG_interp3d(mesh, sol, x0, y0, z0, sol0);

        // ijacobi = 0 --> optimiser = modifier mesh initial
        if (ijacobi == 0) {
          for (ip=1; ip<=mesh->np; ip++) {
            sol0[ip] = sol->m[ip];
            x0[ip] = mesh->point[ip].c[0];
            y0[ip] = mesh->point[ip].c[1];
            z0[ip] = mesh->point[ip].c[2];
          } 
        }

        break; //sortir du while(1)
      }    
    }
    

    FMG_relax_nodewise3d(mesh,data->vballp,neigSparse,data->maxNeig,
                         dxk,dyk,dzk,xprev,yprev,zprev,xref,yref,zref,
                         normalk,param->hmin);
    relax_param=1.0;

    /* Use elasticity as smoother */
    if(param->model==2){

      /* Homogeneous Dirichlet BCs on all the boundary */
      for( ip = 1; ip <= mesh->np; ip++ )
        if( mesh->point[ip].tag & MG_BDY ) mesh->point[ip].tag |= M_DIRICHLET;

      // Construction et resolution du systeme
      for(iinner=1;iinner<=param->iinner_max;iinner++){
        if (param->verbose >= 3) printf("\n *** Smoothing");
        FMG_iterJacobiSparse3d(mesh, dxk, dyk, dzk, dxkprev, dykprev, dzkprev, KsparseX, KsparseY, KsparseZ, belast, neigSparse, data->maxNeig, param->model, param->aniso);
      }

      /* Reset boundary */
      for( ip = 1; ip <= mesh->np; ip++ )
        if( mesh->point[ip].tag & MG_BDY ) mesh->point[ip].tag &= ~M_DIRICHLET;

    }


//    /* for slip bdry condition, normal update*/
//    FMG_normalUpdate3d(mesh,dxk,dyk,dzk,xref,yref,zref);


    optimized = 0;

    if(!ismooth)
      relax_param =  1.0;//FMG_chkArea3d(mesh, xref, yref, zref, dxk, dyk, dzk, xprev, yprev, zprev);
    else {
      relax_param = 1.0;
      ismooth += 1;
      ijacobi -= 1;
      if(ismooth > ismooth_max) {
        ismooth = 0;
        smoothed = 1;
      }
    }

//      // Uncomment here for local relaxation
//      MMG5_SAFE_CALLOC(relax_paramArr,mesh->np+1,double,return 0);
//      FMG_chkArea3d_arr(mesh, neigSparse, data->maxNeig, x0, y0, z0, dxk, dyk, dzk, xprev, yprev, zprev, relax_paramArr);

//    if (param->verbose >= 3) printf("\n *** Fin calcul relax: relax_param = %lf", relax_param);
//    if (relax_param > eps) {
//      for (ip=1; ip<=mesh->np; ip++) {
//         mesh->point[ip].c[0] = xprev[ip] + relax_param*(xref[ip] + dxk[ip] - xprev[ip]);
//         mesh->point[ip].c[1] = yprev[ip] + relax_param*(yref[ip] + dyk[ip] - yprev[ip]);
//         mesh->point[ip].c[2] = zprev[ip] + relax_param*(zref[ip] + dzk[ip] - zprev[ip]);
//      }
//    }
//    else {
//
//      for (ip=1; ip<=mesh->np; ip++) {
//         mesh->point[ip].c[0] = xprev[ip];
//         mesh->point[ip].c[1] = yprev[ip];
//         mesh->point[ip].c[2] = zprev[ip];
//      }
//      if(!smoothed){
//        ismooth = 1;
//        ijacobi -= 1;
//      } else 
//        stop = 1;
//    }

//// Comment the previous "IF" and uncomment here for local relaxation
//      for (ip=1; ip<=mesh->np; ip++) {
//        if (relax_param > eps){
//          mesh->point[ip].c[0] = xprev[ip] + relax_paramArr[ip]*(xref[ip] + dxk[ip] - xprev[ip]);
//          mesh->point[ip].c[1] = yprev[ip] + relax_paramArr[ip]*(yref[ip] + dyk[ip] - yprev[ip]);
//          mesh->point[ip].c[2] = zprev[ip] + relax_paramArr[ip]*(zref[ip] + dzk[ip] - zprev[ip]);
//        }
//        else {
//          mesh->point[ip].c[0] = xprev[ip];
//          mesh->point[ip].c[1] = yprev[ip];
//          mesh->point[ip].c[2] = zprev[ip];
//          stop = 1;
//        }
//      }

    dxavg = 0.;
    dyavg = 0.;
    dzavg = 0.;
    for (ip=1;ip<=mesh->np;ip++) {
      dxavg += fabs(dxk[ip]);
      dyavg += fabs(dyk[ip]);
      dzavg += fabs(dzk[ip]);
    }

    for (ip=1; ip<=mesh->np; ip++) {
      dxkprev[ip] = dxk[ip];
      dykprev[ip] = dyk[ip];
      dzkprev[ip] = dzk[ip];
    }


    FMG_computeJacobiResidual3d(mesh, KsparseX, KsparseY, KsparseZ, resid, neigSparse, data->maxNeig, param->aniso);

    /* Store current volume as element quality */
    minvol = 1.0/MMG5_EPS;
    maxvol = MMG5_EPSD;
    for (iel=1; iel<=mesh->ne; iel++) {
      mesh->tetra[iel].qual = FMG_computeVolume(mesh,iel);
      if( mesh->tetra[iel].qual < MMG5_EPSD ) {
        printf("\nERROR: Invalid tetra %d, volume %e\n",iel,mesh->tetra[iel].qual);
        return;
      }
      if( mesh->tetra[iel].qual < minvol ) minvol = mesh->tetra[iel].qual;
      if( mesh->tetra[iel].qual > maxvol ) maxvol = mesh->tetra[iel].qual;
    }


    if( param->verbose >= 6 )
      FMG_compute_minMaxEdgeLength(mesh,data->neigSparse,data->maxNeig,&edge_size_min,&edge_size_max);

    if (param->verbose >= 2) {
      printf("\n *** Fin actualisation des points iteration %d",ijacobi);
      printf("\n              * relax = %lf",relax_param);
      printf("\n              * MAX dx= %lf , point %d",relax_param*dxmax,ixmax);
      printf("\n              * MAX dx= %lf , point %d",relax_param*dymax,iymax);
      printf("\n              * MAX dx= %lf , point %d",relax_param*dzmax,izmax);
      printf("\n              * AVG dx = (%lf, %lf, %lf)",dxavg/mesh->np, dyavg/mesh->np, dzavg/mesh->np);
      printf("\n              * Res  x = %lf",resid[0]);
      printf("\n              * Res  y = %lf",resid[1]);
      printf("\n              * Res  z = %lf",resid[2]);
      printf("\n              * MIN volume = %e",minvol);
      printf("\n              * MAX volume = %e",maxvol);
      if( param->verbose >= 6 ) {
        printf("\n              * MIN edge size = %e",edge_size_min);
        printf("\n              * MAX edge size = %e",edge_size_max);
      }
    }
    else if (param->verbose==0) // minimal
      fprintf(stdout,"\n *** Iteration %d/%d : relax = %lf, max deplc = (%lf,%lf,%lf)\n",ijacobi,param->ijacobi_max,relax_param,relax_param*dxmax,relax_param*dymax,relax_param*dzmax);
    if (param->debug)
      fprintf(resFile,"%d %lf %lf\n",ijacobi,resid[0],resid[1]);

    // Solution sur le nouveau maillage
    if (!param->solCase) {
      FMG_interp3d(mesh, sol, x0, y0, z0, sol0);   // interpoler solution 
      if (param->verbose >= 3) printf("\n *** Fin interpolation\n\n");
    }
    else {
      FMG_analytic(mesh,sol,param);
      if (param->verbose >= 3) printf("\n *** Fin calcul analytique sur nouveau maillage");
    }


    // Save new mesh
    if (param->imprim && !(ijacobi%param->imprim)) {
      sol->size = 1;
      sprintf(fname, "dom.%d.mesh", ijacobi/param->imprim);
      if ( !MMG3D_Set_outputMeshName(mesh,fname) ) exit(EXIT_FAILURE);
      if ( !MMG3D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);
      sprintf(fname, "dom.%d.sol", ijacobi/param->imprim);
      if ( !MMG3D_Set_outputSolName(mesh,sol,fname) ) exit(EXIT_FAILURE);
      if ( !MMG3D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
      sol->size = param->nsols;
    }

    finaljacobi = ijacobi-1;
    if (stop && ijacobi<param->ijacobi_max) {
      ijacobi = param->ijacobi_max;
      if (param->verbose >= 2) printf("\n\n *** !!! Exécution arrêtée par relaxation complète!!");
    }
 

    break; // !optimized -> sortir du while
  }
  }

  if (ijacobi == param->ijacobi_max + 1 && !stop) finaljacobi = param->ijacobi_max;
  if (param->verbose >= 1) printf("\n***** Fin Jacobi après %d itérations\n",finaljacobi);
 
  // Compute solution H1 seminorm
  MMG5_SAFE_CALLOC(NormSolH1,sol->size,double,return);
//  FMG_computeH1Seminorm3d(NormSolH1,mesh,sol);
  NormSolH1[0] = sqrt(FMG_num_quad_3D(mesh,param,&data->listquad[FMG_QUAD_3D_P2],sol->m,FMG_QUAD_GRAD,FMG_QUAD_ERR));
  printf("H1 seminorm: %e\n",NormSolH1[0]);
//  FMG_computeL2Norm3d(NormSolH1,mesh,sol);
  NormSolH1[0] = sqrt(FMG_num_quad_3D(mesh,param,&data->listquad[FMG_QUAD_3D_P2],sol->m,FMG_QUAD_FUNC,FMG_QUAD_ERR));
  printf("L2 norm: %e\n",NormSolH1[0]);
  MMG5_SAFE_FREE(NormSolH1);


  if (param->debug) 
    fclose(resFile);

  // Save final mesh
  if (param->imprim) {
    sol->size = 1;
    if (param->nsols == 1) {
      sprintf(fname, "dom.%d.mesh", 9999);
      if ( !MMG3D_Set_outputMeshName(mesh,fname) ) exit(EXIT_FAILURE);
      if ( !MMG3D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);
      sprintf(fname, "dom.%d.sol", 9999);
      if ( !MMG3D_Set_outputSolName(mesh,sol,fname) ) exit(EXIT_FAILURE);
      if ( !MMG3D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
    }
    else
      for (isol=1;isol<=param->nsols;isol++) {
        sprintf(fname, ".%d.mesh", 9999);
        strcpy(fname2,fileNames[isol-1]);
        strcat(fname2,fname);
        if ( !MMG3D_Set_outputMeshName(mesh,fname2) ) exit(EXIT_FAILURE);
        if ( !MMG3D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);

        sprintf(fname, ".%d.sol", 9999);
        strcpy(fname2,fileNames[isol-1]);
        strcat(fname2,fname);
        if ( !MMG3D_Set_outputSolName(mesh,sol,fname2) ) exit(EXIT_FAILURE);
        if ( !MMG3D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
        for (ip=1; ip<=mesh->np; ip++)
          sol->m[ip] = sol->m[isol*(mesh->np+1) + ip];
      }
      sol->size = param->nsols;
  }

  if (param->verbose>=2) printf("\n *** Fin desalouer vecteurs");


  if (param->verbose >= 2) printf("\n *** Fin desallouer matrice");

  if (param->verbose >= 1) {fprintf(stdout,"\n ***************************"); 
                           fprintf(stdout,"\n ********* End FMG *********");
                           fprintf(stdout,"\n ***************************\n\n");}

  if (param->verbose==-10)
    fprintf(stdout,"\n******* End FMG :\n******* fileID: %d; nb iterations: %d\n\n", param->fileID, finaljacobi);

//  // Uncomment here for local relaxation
  MMG5_SAFE_FREE(relax_paramArr);
  MMG5_SAFE_FREE(rmin);
  MMG5_SAFE_FREE(normalk);

}

void FMG_blendModelsMetric3d(MMG5_pMesh mesh, MMG5_pSol sol, FMG_pInfo param, double *sizes, double *Hes, double *LS, double edge_size_min, double edge_size_max) {

  double sizeMax, sizeMin, xa = M_PI_4*0.8;
  int ip;

  FMG_computeDesiredSize3d(mesh, sol, param, sizes, Hes, LS, edge_size_min, edge_size_max);

  // Minimum and maximum
  sizeMax=sizes[1];
  sizeMin=sizes[1];
  for(ip=2; ip<=mesh->np; ip++){
    if(sizes[ip]>sizeMax)
      sizeMax=sizes[ip];
    else if(sizes[ip]<sizeMin)
      sizeMin=sizes[ip];
  }

//  // Take min(sizeMin,edge_size_min) and max(sizeMax,edge_size_max)
//  if(sizeMin<edge_size_min) edge_size_min = sizeMin;
//  if(sizeMax>edge_size_max) edge_size_max = sizeMax;
 
  // Rescale
  for(ip=1; ip<=mesh->np; ip++){
    sizes[ip] = (sizes[ip]-sizeMin)/(sizeMax-sizeMin);
    //sizes[ip] = (sizes[ip]-edge_size_min)/(edge_size_max-edge_size_min);
    //sizes[ip] = 0.5*(tan(2*xa*((sizes[ip]-edge_size_min)/(edge_size_max-edge_size_min)-0.5))+tan(xa));
    //sizes[ip] = 0.5*(tan(2*xa*((sizes[ip]-edge_size_min)/(edge_size_max-edge_size_min)-0.5))/tan(xa)+1); // LAST TESTED ON SPHERE
 
  }


}
void FMG_blendModels3d(double *Omega,MMG5_pMesh mesh,double *area0,double *weight){
  double funcMax,funcMin;
  int ip;
  double func[mesh->np+1];

  // Initialize
  for(ip=1; ip<=mesh->np; ip++)
    func[ip]=Omega[ip];

  // Smooth
  for(ip=1; ip<=5; ip++)
    FMG_smoothFunction3d(func,func,mesh,area0);

  // Minimum and maximum
  funcMax=func[1];
  funcMin=func[1];
  for(ip=2; ip<=mesh->np; ip++){
    if(func[ip]>funcMax)
      funcMax=func[ip];
    else if(func[ip]<funcMin)
      funcMin=func[ip];
  }

  // Rescale
  for(ip=1; ip<=mesh->np; ip++)
    weight[ip] = (func[ip]-funcMin)/(funcMax-funcMin);
}

void FMG_relax_nodewise3d(MMG5_pMesh mesh,int *vballp, int *neigSparse,
  int maxNeig,double *dxk,double *dyk,double *dzk,
  double *xprev,double *yprev,double *zprev,
  double *xref,double *yref,double *zref,
  double *normalk,double hmin){
  MMG5_pTetra pt;
  MMG5_pPoint ppt0,ppt1;
  MMG5_pxPoint pxp;
  double normal[3],nn,h[3],disp[3],cm[3],hsize,proj,qualp,qualn,qualtmp,minqual;
  double c0p[3],relax,dd;
  double n[3],n1[3],n2[3];
  int ia, ib, ic, ier,ifac,k,i0, *adja, bdylistv[MMG3D_LMAX+2],lists[MMG3D_LMAX+2];
  int iel,ip,jp,iloc,i,ilist,*listv,lon,nel,base,found,step,maxstep;

#warning: LUCA: fix numerical stability of this min quality check
  minqual = pow(hmin,3)*sqrt(2.0)/12.0;

  /* Reset base flags */
  base = mesh->np+1;
  for( ip = 1; ip <= mesh->np; ip++ ) {
    mesh->point[ip].flag = 0;
  }

  maxstep = 4;

  /* Loop on mesh points */
  for( ip = 1; ip <= mesh->np; ip++ ) {

    /* Get point and initialize data */
    ppt0 = &mesh->point[ip];
 
    /* Get volumic ball */
    listv = &vballp[(maxNeig+1)*ip];
    lon = listv[0];

    /* If boundary point, find the first boundary tetra in the volumic ball */
    if( (ppt0->tag & MG_BDY) && !MG_SIN(ppt0->tag) ) {
      pxp = &mesh->xpoint[ppt0->xp];
 
      /* Stock old normals */
      for( i = 0; i < 3; i++ ) {
        n1[i] = pxp->n1[i];
        n2[i] = pxp->n2[i];
        n[i]  = ppt0->n[i];
      }

      found = 0;
      ilist = 1;
      while( !found && ilist <= lon ) {
        iel  = listv[ilist] / 4;
        iloc = listv[ilist] % 4;
        pt = &mesh->tetra[iel];
        /* Scan if boundary tetra */
        if( pt->xt ) {
          adja = &mesh->adja[4*(iel-1)+1];
          i = 0;
          while( !found && i < 3 ) {
            ifac = MMG5_idir[iloc][i];
            if( !adja[ifac] && (mesh->xtetra[pt->xt].ftag[ifac] & MG_BDY) ) {
              found++;
              k = iel;
              i0 = iloc;
            }
            i++;
          }
        }
        ilist++;
      }
    }

    /** Compute minimum quality in the volumic ball */
    qualp = 1.0/MMG5_EPS;
    for( ilist = 1; ilist <= lon; ilist++ ) {
      iel  = listv[ilist] / 4;
      iloc = listv[ilist] % 4;

      /* Get tetrahedron and points on the opposite face */
      pt = &mesh->tetra[iel];
      ia = pt->v[MMG5_idir[iloc][0]];
      ib = pt->v[MMG5_idir[iloc][1]];
      ic = pt->v[MMG5_idir[iloc][2]];

      /* Compute virtual tetra quality */
      qualtmp = MMG5_det4pt( ppt0->c,
                             mesh->point[ia].c,
                             mesh->point[ib].c,
                             mesh->point[ic].c );

      if( qualtmp < qualp ) qualp = qualtmp;
    }


    /* Guess displacement */
    disp[0] = dxk[ip]+xref[ip]-xprev[ip];
    disp[1] = dyk[ip]+yref[ip]-yprev[ip];
    disp[2] = dzk[ip]+zref[ip]-zprev[ip];
 
    /* Temporarily update coordinates to build virtual tetrahedron */
    if( (ppt0->tag & MG_BDY) && !MG_SIN(ppt0->tag) ) {

      /*no interpolation if the displacement is almost null*/
      dd = sqrt( disp[0]*disp[0]+
                 disp[1]*disp[1]+
                 disp[2]*disp[2]);
      if( dd > MMG5_EPSD ) {
         /* Project point on the surface and interpolate normal vectors */
        ier = FMG_normalUpdate_point3d(mesh,k,i0,ifac,disp,bdylistv,lists);
        disp[0] = ppt0->c[0]-xprev[ip];
        disp[1] = ppt0->c[1]-yprev[ip];
        disp[2] = ppt0->c[2]-zprev[ip];
      }
    } else {
      for( i = 0; i < 3; i++ )
        ppt0->c[i] += disp[i];
    }

    /** Compute minimum quality in the volumic ball */
    qualn = 1.0/MMG5_EPS;
    for( ilist = 1; ilist <= lon; ilist++ ) {
      iel  = listv[ilist] / 4;
      iloc = listv[ilist] % 4;

      /* Get tetrahedron and points on the opposite face */
      pt = &mesh->tetra[iel];
      ia = pt->v[MMG5_idir[iloc][0]];
      ib = pt->v[MMG5_idir[iloc][1]];
      ic = pt->v[MMG5_idir[iloc][2]];

      /* Compute virtual tetra quality */
      qualtmp = MMG5_det4pt( ppt0->c,
                             mesh->point[ia].c,
                             mesh->point[ib].c,
                             mesh->point[ic].c );

      if( qualtmp < qualn ) qualn = qualtmp;
    }

    step = 0;
    while( (qualn/minqual < 1.0 + MMG5_EPS) && (step < maxstep) ) {

      if( fabs(qualp-qualn)/minqual < MMG5_EPS ) break;
 
      /** Guess new displacement */
      relax = 0.5;
//      relax = ( qualp - (1.0 + MMG5_EPS)*minqual ) /( qualp - qualn );
      if( relax < 0.0 || relax > 1.0 ) printf("WARNING: relax %e, qualp %e, qualn %e, minqual %e point %d\n",relax,qualp,qualn,minqual,ip);
      if( relax < MMG5_EPS ) {
        relax = MMG5_EPS;
        ppt0->flag += base;
      }
      for( i = 0; i < 3; i++ )
        disp[i] *= relax;

      /** Reset coordinates */
      ppt0->c[0] = xprev[ip];
      ppt0->c[1] = yprev[ip];
      ppt0->c[2] = zprev[ip];
      if( (ppt0->tag & MG_BDY) && !MG_SIN(ppt0->tag) ) {
        /* Reset old normals */
        for( i = 0; i < 3; i++ ) {
          pxp->n1[i] = n1[i];
          pxp->n2[i] = n2[i];
          ppt0->n[i] = n[i];
        }
      }

      /** Temporarily update coordinates to build virtual tetrahedron */
      if( (ppt0->tag & MG_BDY) && !MG_SIN(ppt0->tag) ) {

        /*no interpolation if the displacement is almost null*/
        dd = sqrt( disp[0]*disp[0]+
                   disp[1]*disp[1]+
                   disp[2]*disp[2]);
        if( dd > MMG5_EPSD ) {
           /* Project point on the surface and interpolate normal vectors */
          ier = FMG_normalUpdate_point3d(mesh,k,i0,ifac,disp,bdylistv,lists);
          disp[0] = ppt0->c[0]-xprev[ip];
          disp[1] = ppt0->c[1]-yprev[ip];
          disp[2] = ppt0->c[2]-zprev[ip];
        }
      } else {
        for( i = 0; i < 3; i++ )
          ppt0->c[i] += disp[i];
      }

      /** Compute minimum quality in the volumic ball */
      qualn = 1.0/MMG5_EPS;
      for( ilist = 1; ilist <= lon; ilist++ ) {
        iel  = listv[ilist] / 4;
        iloc = listv[ilist] % 4;
  
        /* Get tetrahedron and points on the opposite face */
        pt = &mesh->tetra[iel];
        ia = pt->v[MMG5_idir[iloc][0]];
        ib = pt->v[MMG5_idir[iloc][1]];
        ic = pt->v[MMG5_idir[iloc][2]];
  
        /* Compute virtual tetra quality */
        qualtmp = MMG5_det4pt( ppt0->c,
                               mesh->point[ia].c,
                               mesh->point[ib].c,
                               mesh->point[ic].c );
  
        if( qualtmp < qualn ) qualn = qualtmp;
      }

      step++;
    }

    /** If cannot improve quality */
    if( (qualn/minqual < 1.0 + MMG5_EPS) && (step == maxstep) ) {
      /* Reset coordinates */
      ppt0->c[0] = xprev[ip];
      ppt0->c[1] = yprev[ip];
      ppt0->c[2] = zprev[ip];
      disp[0] = ppt0->c[0]-xprev[ip];
      disp[1] = ppt0->c[1]-yprev[ip];
      disp[2] = ppt0->c[2]-zprev[ip];
      if( (ppt0->tag & MG_BDY) && !MG_SIN(ppt0->tag) ) {
        /* Reset old normals */
        for( i = 0; i < 3; i++ ) {
          pxp->n1[i] = n1[i];
          pxp->n2[i] = n2[i];
          ppt0->n[i] = n[i];
        }
      }
    }

    /* Update point displacement and position */
    dxk[ip] = ppt0->c[0]-xref[ip];
    dyk[ip] = ppt0->c[1]-yref[ip];
    dzk[ip] = ppt0->c[2]-zref[ip];

  }

}
