/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"
#include "math.h"


/**
 * \param Nvec vector norm
 * \param vec 1D vector
 * \param np number of points
 * 
 * Computes the norm of a 1D vector
 */

void FMG_computeNorm(double* vec, double *norm, int np, int dim, int nsols){

  int ip, c, ns;

  for (ns=1; ns<=nsols; ns++) {
    for (ip=1; ip<=np; ip++){
      norm[(ns-1)*(np+1) + ip] = 0;
      for (c=0;c<dim;c++)
        norm[(ns-1)*(np+1) + ip] += vec[dim*(np+1)*(ns-1) + dim*ip+c]*vec[dim*(np+1)*(ns-1) + dim*ip+c];
      norm[(ns-1)*(np+1) + ip] = sqrt(norm[(ns-1)*(np+1) + ip]);
    }
  }
}

/**
 * \param vec 1D vector of components of a symmetric tensor field
 * \param norm output norm
 * \param np number of points
 * \param ndim number of space dimensions
 * \param nsols number of solutions
 * 
 * Computes the norm of a symmetric tensor field, stored as a 1D vector
 */

void FMG_computeTensorNorm(double *vec, double *norm, int np, int ndim, int nsols){

  int ip, c, r, k, ns, dim;
  
  // Nb. of components
  dim = ndim*(ndim+1)/2;

  for (ns=1; ns<=nsols; ns++) {
    for (ip=1; ip<=np; ip++){
      norm[(ns-1)*(np+1) + ip] = 0;
      k=0;
      for (c=0;c<ndim;c++){  // Loop on columns
        for (r=0; r<c; r++){ // Loop on rows < columns
          norm[(ns-1)*(np+1) + ip] += 2*vec[dim*(np+1)*(ns-1) + dim*ip+k]*vec[dim*(np+1)*(ns-1) + dim*ip+k]; // Extra-diag. component
          ++k;
        }
        norm[(ns-1)*(np+1) + ip] += vec[dim*(np+1)*(ns-1) + dim*ip+k]*vec[dim*(np+1)*(ns-1) + dim*ip+k]; // Diag. component
        ++k;
      }
      norm[(ns-1)*(np+1) + ip] = sqrt(norm[(ns-1)*(np+1) + ip]);
    }
  }

}


/**
 * \param vec 1D vector
 * \param np number of points
 * \double gamma normalization coefficient
 * 
 * Normalizes a 1D vector in the interval [0, 1.] using a coefficient gamma : vec[i] = min(1., vec[1]/(gamma*max{vec}))
 */

void FMG_normalizeVec(double *vec, int np, double gamma, int nsols){
  
  double vecmax;
  int ip, ns;

  for (ns=1; ns<=nsols; ns++) {
    vecmax = fabs(vec[(np+1)*(ns-1)]);
  
    for (ip=1; ip<=np; ip++)
      if (fabs(vec[(np+1)*(ns-1) + ip]) > vecmax)
        vecmax = fabs(vec[(np+1)*(ns-1) + ip]);
   
    if (vecmax > 0)
      for (ip=1; ip<=np; ip++) {
        vec[(np+1)*(ns-1) + ip] = vec[(np+1)*(ns-1) + ip]/vecmax/gamma;
        if (vec[(np+1)*(ns-1) + ip] > 1.)
          vec[(np+1)*(ns-1) + ip] = 1.;
      }
  }
}
