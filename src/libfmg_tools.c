/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/



#include "fmg.h"

int FMG_parsop(MMG5_pMesh mesh,FMG_pData fmgdata) {
  FMG_pInfo   info;
  float       fp1,fp2,hausd;
  int         ref,i,j,ret,npar;
  char       *ptr,buf[256],buf2[256],data[256];
  double      depx,depy,depz;
  FILE       *in;

  info = fmgdata->info;
  
  /* check for parameter file */
  in = fopen("DEFAULT.fmg","rb");
  if(!in) {
    info->nbc = 0;
    return(1);
  }
  fprintf(stdout,"  %%%% %s OPENED\n","DEFAULT.fmg");

  /* read parameters */
  while ( !feof(in) ) {
    /* scan line */
    ret = fscanf(in,"%s",data);
    if ( !ret || feof(in) )  break;
    for (i=0; i<strlen(data); i++) data[i] = tolower(data[i]);

    /* check for condition type */
    if ( !strcmp(data,"parameters") ) {
      fscanf(in,"%d",&npar);
      if ( FMG_Set_iparameter(mesh,fmgdata,FMG_IPARAM_numberOfBdry,npar) )
        exit(EXIT_FAILURE);

      for (i=0; i<info->nbc; i++) {
        ret = fscanf(in,"%d %s %s",&ref,buf,buf2);
        info->bc[i].ref = ref;
        for (j=0; j<strlen(buf); j++)  buf[j] = tolower(buf[j]);

        if ( !strcmp(buf,"edge") || !strcmp(buf,"Edge") ) {
          printf("bdry condition on the edge of ref %d\n",ref);
        } else if ( !strcmp(buf,"vertex") || !strcmp(buf,"Vertex") ) {
          printf("bdry condition on the vertices of ref %d\n",ref);
        } else if ( !strcmp(buf,"triangle") || !strcmp(buf,"Triangle") ) {
          printf("bdry condition on the triangles of ref %d\n",ref);
        } else {
          fprintf(stderr,"  %%%% Wrong format: %s\n",buf);
                exit(0);	  
        }
        
        
        for (j=0; j<strlen(buf2); j++)  buf2[j] = tolower(buf2[j]);
        if ( !strcmp(buf2,"dirichlet") || !strcmp(buf2,"Dirichlet") ) {
          if(mesh->dim==2) {
            ret = fscanf(in,"%lf %lf",&depx,&depy);
            depz = 0;
            printf("dirichet %e %e\n",depx,depy);
          } else {
            ret = fscanf(in,"%lf %lf %lf",&depx,&depy,&depz);
            printf("dirichet %e %e %e\n",depx,depy,depz);
          }
          info->bc[i].dep[0] = depx;
          info->bc[i].dep[1] = depy;
          info->bc[i].dep[2] = depz;
          
        } else if ( !strcmp(buf2,"Slip") || !strcmp(buf2,"slip") ) {
          printf("slip\n");
          info->bc[i].isslip = 1;
        } else {
          fprintf(stderr,"  %%%% Wrong type of bdry condition: %s\n",buf2);
          exit(0);
        }
      }
    }
  }
  fclose(in);
  return(1);
}

void FMG_smoothFunction2d(double *output, double *input, MMG5_pMesh mesh, double *area0){
  int ip, iloc, iel;
  double totalArea[mesh->np+1], vec[mesh->np+1], sum;
  MMG5_pTria ptr;

  // Initialization
  for(ip=1;ip<=mesh->np;ip++){
    totalArea[ip]=0.0;
    vec[ip]=0.0;
  }
  // Loop on elements
  for(iel=1;iel<=mesh->nt;iel++){
    ptr = &mesh->tria[iel];
    sum = 0.0;
    // Compute sum on element
    for(iloc=0;iloc<3;iloc++){
      ip = ptr->v[iloc];
      sum+=input[ip];
    }
    // Assembly total area and vertex contribution
    for(iloc=0;iloc<3;iloc++){
      ip = ptr->v[iloc];
      totalArea[ip]+=area0[iel];
      vec[ip]+=sum*area0[iel]/3.0;
    }
  }
  // Normalization
  for(ip=1;ip<=mesh->np;ip++)
    output[ip]=vec[ip]/totalArea[ip];
}

void FMG_smoothFunction3d(double *output, double *input, MMG5_pMesh mesh, double *vol0){
  int ip, iloc, iel;
  double totalVol[mesh->np+1], vec[mesh->np+1], sum;
  MMG5_pTetra ptr;

  // Initialization
  for(ip=1;ip<=mesh->np;ip++){
    totalVol[ip]=0.0;
    vec[ip]=0.0;
  }
  // Loop on elements
  for(iel=1;iel<=mesh->ne;iel++){
    ptr = &mesh->tetra[iel];
    sum = 0.0;
    // Compute sum on element
    for(iloc=0;iloc<4;iloc++){
      ip = ptr->v[iloc];
      sum+=input[ip];
    }
    // Assembly total area and vertex contribution
    for(iloc=0;iloc<4;iloc++){
      ip = ptr->v[iloc];
      totalVol[ip]+=vol0[iel];
      vec[ip]+=sum*vol0[iel]/4.0;
    }
  }
  // Normalization
  for(ip=1;ip<=mesh->np;ip++)
    output[ip]=vec[ip]/totalVol[ip];
}

/**
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param neigSparse list of neighbor points of each point
 * \param maxNeig maximum number of neighbors for a point in the mesh
 * \param hmin pointer to minimum edge length
 * \param hmax pointer to maximum edge length
 *
 * \return 1 if success
 *
 * Compute minimum and maximum edge length (Euclidean metric).
 */

int FMG_compute_minMaxEdgeLength(MMG5_pMesh mesh,int *neigSparse,int maxNeig,double *hmin,double *hmax) {
  MMG5_pPoint ppti,pptj;
  double      h;
  int         ineig,ip,jp;

  *hmin = 1.0/MMG5_EPS;
  *hmax = MMG5_EPSD;

  /* Loop on the adjacent of each point. Each edge is analized twice, but it
   * uses already available data structures.
   */
  for( ip = 1; ip <= mesh->np; ip++ ) {
    ppti = &mesh->point[ip];
    /* Loop on adjacent nodes (different from ip) */
    for( ineig = 0;ineig < neigSparse[ip*(maxNeig+1)]-1; ineig++ ) {
      jp = neigSparse[ip*(maxNeig+1)+ineig+1];
      pptj = &mesh->point[jp];
      h = sqrt( pow(ppti->c[0]-pptj->c[0],2.0)+
                pow(ppti->c[1]-pptj->c[1],2.0)+
                pow(ppti->c[2]-pptj->c[2],2.0) );
      if( h < *hmin ) *hmin = h;
      if( h > *hmax ) *hmax = h;
    }
  }

  return 1;
}


void FMG_save_vballp(MMG5_pMesh mesh,int *vballp,int maxNeig,int ip,char *filename){
  MMG5_pPoint ppt0,ppt1;
  MMG5_pTetra pt;
  FILE *fid;
  int iel,jp,iloc,i,ilist,*listv,lon,np,base,flag,count;

  /* Update base to flag visited neighbours */
  base = mesh->np+1;

  /* Get point and initialize data */
  ppt0 = &mesh->point[ip];
 
  /* Get volumic ball */
  listv = &vballp[(maxNeig+1)*ip];
  lon = listv[0];

  ppt0->flag = base;
  np = 1;

  /* Loop on tetras in the volumic ball */
  for( ilist = 1; ilist <= lon; ilist++ ) {
    iel  = listv[ilist] / 4;
    iloc = listv[ilist] % 4;

    /* Loop on points on the opposite face */
    for( i = 0; i < 3; i++ ) {
        iloc = MMG5_inxt3[iloc];
        jp = mesh->tetra[iel].v[iloc];
        ppt1 = &mesh->point[jp];
        if( ppt1->flag == base ) continue;

        ppt1->flag = base;
        np++;
    }
  }

  fid = fopen(filename,"w");
  fprintf(fid,"MeshVersionFormatted 2\n\n\n");
  fprintf(fid,"Dimension 3\n\n\n");


  fprintf(fid,"Vertices\n%d\n",np);

  count = 1;
  fprintf(fid,"%f %f %f %d\n",ppt0->c[0],ppt0->c[1],ppt0->c[2],ppt0->ref);
  ppt0->flag=count++;
  
  /* Loop on tetras in the volumic ball */
  for( ilist = 1; ilist <= lon; ilist++ ) {
    iel  = listv[ilist] / 4;
    iloc = listv[ilist] % 4;
   
    /* Loop on points on the opposite face */
    for( i = 0; i < 3; i++ ) {
        iloc = MMG5_inxt3[iloc];
        jp = mesh->tetra[iel].v[iloc];
        ppt1 = &mesh->point[jp];
        if( ppt1->flag != base ) continue;

        fprintf(fid,"%f %f %f %d\n",ppt1->c[0],ppt1->c[1],ppt1->c[2],ppt1->ref);
        ppt1->flag=count++;
    }
  }

  fprintf(fid,"\n");
  fprintf(fid,"Tetrahedra\n%d\n",lon);
  /* Loop on tetras in the volumic ball */
  for( ilist = 1; ilist <= lon; ilist++ ) {
    iel  = listv[ilist] / 4;
    pt = &mesh->tetra[iel];
    fprintf(fid,"%d %d %d %d %d\n",mesh->point[pt->v[0]].flag,
                                   mesh->point[pt->v[1]].flag,
                                   mesh->point[pt->v[2]].flag,
                                   mesh->point[pt->v[3]].flag,pt->ref);
  }

  fprintf(fid,"\n\nEnd");
  fclose(fid);
  ppt0->flag = mesh->base;
  /* Loop on tetras in the volumic ball */
  for( ilist = 1; ilist <= lon; ilist++ ) {
    iel  = listv[ilist] / 4;
    iloc = listv[ilist] % 4;
   
    /* Loop on points on the opposite face */
    for( i = 0; i < 3; i++ ) {
        iloc = MMG5_inxt3[iloc];
        jp = mesh->tetra[iel].v[iloc];
        ppt1 = &mesh->point[jp];
        if( ppt1->flag != mesh->base ) ppt1->flag = mesh->base;
    }
  }
}
