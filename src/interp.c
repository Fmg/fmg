/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"

int FMG_exactinterp(MMG5_pMesh mesh2,MMG5_pSol sol) {
  int ip,isol;
  double xn,yn,zn;
  for (ip=1; ip<=mesh2->np; ip++) {

    for (isol=0; isol<sol->size;isol++) {
      sol->m[isol*(mesh2->np+1) + ip] = 0.;
      xn = mesh2->point[ip].c[0];///5.;
      yn = mesh2->point[ip].c[1];///5.;
      zn = mesh2->point[ip].c[2];///5.;
   
      sol->m[isol*(mesh2->np+1) + ip] = exp(-100*(yn-xn*xn-zn*zn)*(yn-xn*xn-zn*zn));
    }
  }
  return(1);
}
/**
 * \param mesh2 pointer on the new mesh after the last JAcobi iteration
 * \param sol pointer on the solution (defined on mesh)
 * \param x0 x coordinates of the points in the initial mesh
 * \param y0 y coordinates of the points in the initial mesh
 * \param sol0 solution values in the initial mesh 
 * 
 * Interpolate the solution over mesh2
 */

int FMG_interp3d( MMG5_pMesh mesh2, MMG5_pSol sol, double* x0, double *y0, double *z0, double*  sol0) {

  int ip, idxTr, iloc, isol;
  double denom, eps=1e-6, *xprev, *yprev, *zprev, phi[4];
  MMG5_pTetra ptr;

  //  return(FMG_exactinterp(mesh2,sol));
  MMG5_SAFE_CALLOC(xprev,mesh2->np+1,double,return 0);
  MMG5_SAFE_CALLOC(yprev,mesh2->np+1,double,return 0);
  MMG5_SAFE_CALLOC(zprev,mesh2->np+1,double,return 0);

  for (ip=1; ip<=mesh2->np; ip++){
    xprev[ip] = mesh2->point[ip].c[0];
    yprev[ip] = mesh2->point[ip].c[1];
    zprev[ip] = mesh2->point[ip].c[2];
    mesh2->point[ip].c[0] = x0[ip];
    mesh2->point[ip].c[1] = y0[ip];
    mesh2->point[ip].c[2] = z0[ip];
  }

  for (ip=1; ip<=mesh2->np; ip++) {
    #warning CECILE remove this and find a solution!!!!!!!!
    if(mesh2->point[ip].tag & MG_BDY) continue;
    idxTr = FMG_locatePointTria3d(mesh2, xprev[ip], yprev[ip], zprev[ip], mesh2->point[ip].s);
    if(!idxTr) continue;

    ptr = &mesh2->tetra[idxTr];
  
    FMG_baryCoord3d(mesh2, ptr, xprev[ip], yprev[ip], zprev[ip], phi);
//    for (iloc=0; iloc<4; iloc++)
//      assert(phi[iloc]>-FMG_eps);

    for (isol=0; isol<sol->size;isol++) {
      sol->m[isol*(mesh2->np+1) + ip] = 0.;
      for (iloc = 0; iloc<4; iloc++) {
        sol->m[isol*(mesh2->np+1) + ip] += phi[iloc]*sol0[isol*(mesh2->np+1) + ptr->v[iloc]];
      }
    }
  }

  for (ip=1; ip<=mesh2->np; ip++){
    mesh2->point[ip].c[0] = xprev[ip];
    mesh2->point[ip].c[1] = yprev[ip];
    mesh2->point[ip].c[2] = zprev[ip];
  }

  MMG5_SAFE_FREE(xprev);
  MMG5_SAFE_FREE(yprev);
  MMG5_SAFE_FREE(zprev);

}




/**
 * \param mesh2 pointer on the new mesh after the last JAcobi iteration
 * \param sol pointer on the solution (defined on mesh)
 * \param x0 x coordinates of the points in the initial mesh
 * \param y0 y coordinates of the points in the initial mesh
 * \param sol0 solution values in the initial mesh 
 * 
 * Interpolate the solution over mesh2
 */

int FMG_interp2d( MMG5_pMesh mesh2, MMG5_pSol sol, double* x0, double *y0, double*  sol0) {

  int ip, idxTr, iloc, isol;
  double denom, x[4],y[4], phi[4], eps=1e-6, *xprev, *yprev, barycoord[2];
  MMG5_pTria ptr;

  MMG5_SAFE_CALLOC(xprev,mesh2->np+1,double,return 0);
  MMG5_SAFE_CALLOC(yprev,mesh2->np+1,double,return 0);

  for (ip=1; ip<=mesh2->np; ip++){
    xprev[ip] = mesh2->point[ip].c[0];
    yprev[ip] = mesh2->point[ip].c[1];
    mesh2->point[ip].c[0] = x0[ip];
    mesh2->point[ip].c[1] = y0[ip];
  }

  for (ip=1; ip<=mesh2->np; ip++) {

    
//    x[0] = mesh2->point[ip].c[0];
//    y[0] = mesh2->point[ip].c[1];
    x[0] = xprev[ip];
    y[0] = yprev[ip];
    idxTr = FMG_locatePointTria2d(mesh2, x[0], y[0], mesh2->point[ip].s);
    if(!idxTr) continue;
    ptr = &mesh2->tria[idxTr];

    for (iloc=0;iloc<3;iloc++) {
      x[iloc+1] = x0[ptr->v[iloc]];
      y[iloc+1] = y0[ptr->v[iloc]];
    }

    denom = (x[2]-x[1])*(y[3]-y[1]) + (y[1]-y[2])*(x[3]-x[1]);
    phi[1] = (x[2]*y[3] - x[3]*y[2] + x[0]*(y[2]-y[3]) + y[0]*(x[3]-x[2]) )/denom;
    phi[2] = (x[3]*y[1] - x[1]*y[3] + x[0]*(y[3]-y[1]) + y[0]*(x[1]-x[3]) )/denom;   
    phi[3] = (x[1]*y[2] - x[2]*y[1] + x[0]*(y[1]-y[2]) + y[0]*(x[2]-x[1]) )/denom;   
//    assert(fabs(phi[1]+phi[2]+phi[3]-1.) < FMG_eps);
    for (iloc=0; iloc<4; iloc++)
//      assert(phi[iloc]>-FMG_eps);

//FMG_baryCoord(mesh2, *ptr, xprev[ip], yprev[ip], barycoord);
//printf("\n %d %lf %lf %lf",ip,barycoord[0],barycoord[1],1.-barycoord[0]-barycoord[1]);
//printf("\n %d %lf %lf %lf",ip,phi[1],phi[2],phi[3]);

    for (isol=0; isol<sol->size;isol++) {
      sol->m[isol*(mesh2->np+1) + ip] = 0.;
      for (iloc = 0; iloc<3; iloc++) {
        sol->m[isol*(mesh2->np+1) + ip] += phi[iloc+1]*sol0[isol*(mesh2->np+1) + ptr->v[iloc]];
      }
    }
  }


  for (ip=1; ip<=mesh2->np; ip++){
    mesh2->point[ip].c[0] = xprev[ip];
    mesh2->point[ip].c[1] = yprev[ip];
  }

  MMG5_SAFE_FREE(xprev);
  MMG5_SAFE_FREE(yprev);

}



