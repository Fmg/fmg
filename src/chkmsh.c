/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"
#include "inlined_functions_3d.h"

/**
 * \param mesh pointer on the mesh structure
 * \param x0 x coordinates of the points in the initial mesh
 * \param y0 y coordinates of the points in the initial mesh
 * \param z0 z coordinates of the points in the initial mesh
 * \param dx x deplacement of the points
 * \param dy y deplacement of the points
 * \param dz z deplacement of the points
 * \param xprev x coordinates of the points in the previous iteration
 * \param yprev y coordinates of the points in the previous iteration
 * \param zprev z coordinates of the points in the previous iteration
 * \return the relaxation parameter
 *
 * Check the quality of the computed mesh and calculates the relaxation parameter
 */

double FMG_chkArea3d(MMG5_pMesh mesh, double* x0, double* y0, double *z0, double *dx, double *dy, double *dz,
                     double *xprev, double *yprev, double *zprev){

  int iel,ip, cross;
  double relax_param = 1.0, relax_step = 0.01, vol_min = 1e-9, eps = 1e-6;

  cross = 1;
  while (relax_param>eps && cross == 1) {
    cross = 0;
    for (ip=1; ip<=mesh->np; ip++) {
      mesh->point[ip].c[0] = xprev[ip] + relax_param*(x0[ip] + dx[ip] - xprev[ip]);
      mesh->point[ip].c[1] = yprev[ip] + relax_param*(y0[ip] + dy[ip] - yprev[ip]);
      mesh->point[ip].c[2] = zprev[ip] + relax_param*(z0[ip] + dz[ip] - zprev[ip]);
    }

    for (iel=1; iel<=mesh->ne; iel++) {
      if (FMG_computeVolume(mesh, iel) < vol_min) {
        relax_param -= relax_step;
        if (relax_param < eps)
          printf("\n           $$$!!! ELEMENT RELAX : %d --> relax = %lf",iel,relax_param);
        cross = 1;
        break;
      }    
    }

  }  
 
  return relax_param;
}


/**
 * \param mesh pointer on the mesh structure
 * \param x0 x coordinates of the points in the initial mesh
 * \param y0 y coordinates of the points in the initial mesh
 * \param z0 z coordinates of the points in the initial mesh
 * \param dx x deplacement of the points
 * \param dy y deplacement of the points
 * \param dz z deplacement of the points
 * \param xprev x coordinates of the points in the previous iteration
 * \param yprev y coordinates of the points in the previous iteration
 * \param zprev z coordinates of the points in the previous iteration
 * \return the relaxation parameter
 *
 * Check the quality of the computed mesh and calculates the relaxation parameter
 */

double FMG_chkArea3d_arr(MMG5_pMesh mesh, int* neigSparse, int maxNeig, double* x0, double* y0, double *z0, double *dx, double *dy, double *dz,
                       double *xprev, double *yprev, double *zprev, double *relax_param){

  int iel, ip, ineig, jp, l, it, cross;
  double relax_min, relax_step = 0.01, vol_min = 1e-9, eps = 1e-6, tau;
  double dist, phi, phi_min = 0.314, a[3],b[3],c[3];
  double hsize[mesh->np+1],weightNeig[(maxNeig+1)*(mesh->np+1)];
  int flag[mesh->np+1], mark[mesh->np+1], updated[mesh->np+1];
  int marked[mesh->ne+1];
  MMG5_pTetra pt;
  int check_phi,dim;

  // Build point graph weighted by distance
  for (ip=1; ip<=mesh->np; ip++) {
    hsize[ip] = 0.0;
    for (ineig=1; ineig<=neigSparse[ip*(maxNeig+1)]; ineig++) {
      jp = neigSparse[ip*(maxNeig+1)+ineig];
      dist = sqrt(pow(mesh->point[ip].c[0]-mesh->point[jp].c[0],2)+
                  pow(mesh->point[ip].c[1]-mesh->point[jp].c[1],2)+
                  pow(mesh->point[ip].c[2]-mesh->point[jp].c[2],2));
      hsize[ip] += dist;
//      weightNeig[ip*(maxNeig+1)+ineig] = dist;
    }
    hsize[ip] = hsize[ip]/(neigSparse[ip*(maxNeig+1)]-1);
  }

  // Initialize next pool
  for(ip=1; ip<=mesh->np; ip++){
    flag[ip] = 0;
    relax_param[ip] = 1.0;
    mark[ip] = 0;
  }
  cross = 1;
  relax_min = 1;
  tau = 1.0;

  while (cross) {
    
    cross = 0;

    // Guess new positions
    for(ip=1; ip<=mesh->np; ip++){
      mesh->point[ip].c[0] = xprev[ip] + relax_param[ip]*(x0[ip] + dx[ip] - xprev[ip]);
      mesh->point[ip].c[1] = yprev[ip] + relax_param[ip]*(y0[ip] + dy[ip] - yprev[ip]);
      mesh->point[ip].c[2] = zprev[ip] + relax_param[ip]*(z0[ip] + dz[ip] - zprev[ip]);      
    }

    for(iel=1;iel<=mesh->ne;iel++){
       
      if (FMG_computeVolume(mesh, iel) < vol_min) {
//        printf("\nNull element %d\n",iel);
        cross += 1;
        for(l=0; l<4; l++){
          ip=mesh->tetra[iel].v[l];
          flag[ip] = 1;
        }// loop on local vertices
      }// volume check
    } // loop on elements

    if(cross>100) {
      for(ip=1; ip<=mesh->np; ip++)
        relax_param[ip] *= 0.5;
      relax_min *= 0.5;
    } else {
      for(ip=1; ip<=mesh->np; ip++) {
        for(jp=1; jp<=mesh->np; jp++) {
          if(!flag[jp]) continue;
          dist = sqrt(pow(mesh->point[ip].c[0]-mesh->point[jp].c[0],2)+
                      pow(mesh->point[ip].c[1]-mesh->point[jp].c[1],2)+
                      pow(mesh->point[ip].c[2]-mesh->point[jp].c[2],2));
          dist = 1.0-exp(-dist/(tau*hsize[ip]));
          if(dist < relax_param[ip]) relax_param[ip] = dist;
        }
      }
      tau *= 5.0;
    }

    // Exit condition
    if((cross > 100 && relax_min<1e-3) || (cross >= 100 && tau > 100)){
      cross=0; // Exit while
      relax_min = 0.0; // Relaxation complete
    }


//    for(ip=1; ip<=mesh->np; ip++) {
//      if(!flag[ip]) continue;
//      recursive(ip, neigSparse, maxNeig, weightNeig, 0.0, mark);
//      flag[ip] = 0.0;
//    }

  } // while

  FILE *fn;
  fn=fopen("marked.sol","w");
  fprintf(fn,"MeshVersionFormatted 2\n Dimension 3\n SolAtVertices \n %d\n 1 1 \n",mesh->np);
  for(ip=1; ip<=mesh->np;ip++)
    fprintf(fn,"%d\n",flag[ip]);
  fclose(fn);

  fn=fopen("relax.sol","w");
  fprintf(fn,"MeshVersionFormatted 2\n Dimension 3\n SolAtVertices \n %d\n 1 1 \n",mesh->np);
  for(ip=1; ip<=mesh->np;ip++)
    fprintf(fn,"%f\n",relax_param[ip]);
  fclose(fn);


  return relax_min;
}


/**
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * \param x0 x coordinates of the points in the initial mesh
 * \param y0 y coordinates of the points in the initial mesh
 * \param z0 z coordinates of the points in the initial mesh
 * \param dx x deplacement of the points
 * \param dy y deplacement of the points
 * \param dz z deplacement of the points
 * \param xprev x coordinates before current Jacobi iteration
 * \param yprev y coordinates before current Jacobi iteration
 * \param zprev z coordinates before current Jacobi iteration
 * \param maxcnt maximum number of elements to optimize
 * \param rlxpt list of index of the elements to optimize
 * \return number of elements that require optimization
 *
 * Check which elements will require relaxation and return their indices in descending order of importance
 */

int FMG_prechkArea3d(MMG5_pMesh mesh, MMG5_pSol sol, double* x0, double* y0, double *z0,
                     double *dx, double *dy, double *dz, double *xprev, double *yprev, double *zprev,
                   int maxcnt, int *rlxpt){

  int iel,ip,cnt;
  double vol_min = 0.0/*1e-6*/, eps = 1e-6;
  double *val;
  MMG5_pTetra ptr;

  MMG5_SAFE_CALLOC(val,maxcnt+1,double,return 0);

  for (ip=0;ip<=maxcnt;ip++)
    rlxpt[ip] = 0;

  for (ip=1; ip<=mesh->np; ip++) {
    mesh->point[ip].c[0] = x0[ip] + dx[ip];
    mesh->point[ip].c[1] = y0[ip] + dy[ip];
    mesh->point[ip].c[2] = z0[ip] + dz[ip];
  }

  cnt = 0;
  rlxpt[0] = 0;
  for (iel=1; iel<=mesh->ne && cnt <= mesh->ne; iel++) 
    if (FMG_computeVolume(mesh, iel) < vol_min) {
      ptr = &mesh->tetra[iel];
//      FMG_sortIntVect(rlxpt,val,iel,-MMG5_caltet_iso(mesh,sol,ptr->v[0],ptr->v[1],ptr->v[2],ptr->v[3]),
      #warning put the right function here
      FMG_sortIntVect(rlxpt,val,iel,-0/*MMG5_caltet_iso(mesh,sol,ptr)*/,maxcnt);
      cnt += 1;
//    rlxpt[cnt] = iel;
    }

  for (ip=1; ip<=mesh->np; ip++) {
    mesh->point[ip].c[0] = xprev[ip];
    mesh->point[ip].c[1] = yprev[ip];
    mesh->point[ip].c[2] = zprev[ip];
  }

  MMG5_SAFE_FREE(val);

  return cnt;
}




/**
 * \param mesh pointer on the mesh structure
 * \param x0 x coordinates of the points in the initial mesh
 * \param y0 y coordinates of the points in the initial mesh
 * \param dx x deplacement of the points
 * \param dy y deplacement of the points
 * \param xprev x coordinates of the points in the previous iteration
 * \param yprev y coordinates of the points in the previous iteration
 * \return the relaxation parameter
 *
 * Check the quality of the computed mesh and calculates the relaxation parameter
 */


double FMG_chkArea2d(MMG5_pMesh mesh, double* x0, double* y0, double *dx, double *dy, double *xprev, double *yprev){

  int iel,ip, cross;
  double relax_param = 1.0, relax_step = 0.1, area_min = 1e-6, eps = 1e-6;

  cross = 1;
  while (relax_param>eps && cross == 1) {
    cross = 0;
    for (ip=1; ip<=mesh->np; ip++) {
//       mesh->point[ip].c[0] = x0[ip] + relax_param*dx[ip];
//       mesh->point[ip].c[1] = y0[ip] + relax_param*dy[ip];
      mesh->point[ip].c[0] = xprev[ip] + relax_param*(x0[ip] + dx[ip] - xprev[ip]);
      mesh->point[ip].c[1] = yprev[ip] + relax_param*(y0[ip] + dy[ip] - yprev[ip]);
    }

    for (iel=1; iel<=mesh->nt; iel++) {
      if (FMG_computeArea(mesh, iel) < area_min) {
        relax_param -= relax_step;
        if (relax_param < eps)
          printf("\n           $$$!!! ELEMENT RELAX : %d --> relax = %lf",iel,relax_param);
        cross = 1;
        break;
      }    
    }

  }  
 
  return relax_param;
}


/**
 * \param mesh pointer on the mesh structure
 * \param x0 x coordinates of the points in the initial mesh
 * \param y0 y coordinates of the points in the initial mesh
 * \param dx x deplacement of the points
 * \param dy y deplacement of the points
 * \param xprev x coordinates of the points in the previous iteration
 * \param yprev y coordinates of the points in the previous iteration
 * \return the relaxation parameter
 *
 * Check the quality of the computed mesh and calculates the relaxation parameter
 */


void FMG_chkArea2d_arr(MMG5_pMesh mesh, double* x0, double* y0, double *dx, double *dy, double *xprev, double *yprev, double *relax_param){

  int iel,ip,k,iloc, cross;
  double relax_min = 1.0, relax_step = 0.1, area_min = 0/*1e-6*/, eps = 1e-6;

  for (ip=1; ip<=mesh->np; ip++){
    relax_param[ip] = 1.0;
  }

  cross = 1;
    while (cross == 1) {
      cross = 0;
      for (ip=1; ip<=mesh->np; ip++) {
        mesh->point[ip].c[0] = xprev[ip] + relax_param[ip]*(x0[ip] + dx[ip] - xprev[ip]);
        mesh->point[ip].c[1] = yprev[ip] + relax_param[ip]*(y0[ip] + dy[ip] - yprev[ip]);
      }

      for (iel=1; iel<=mesh->nt; iel++) {
        if (FMG_computeArea(mesh, iel) < area_min) {
          for (k=0;k<3;k++){
            iloc = mesh->tria[iel].v[k];
            if(relax_param[iloc]-relax_step>=eps){
              relax_param[iloc] -= relax_step;
              if(relax_param[iloc]<relax_min) relax_min = relax_param[iloc];
              cross = 1;
            }
          }
//         if (relax_min < eps)
//           printf("\n           $$$!!! ELEMENT RELAX : %d --> relax = %lf",iel,relax_min);
//         cross = 1;
//         break;
      }    
    }

  }  
 
  return;
}


/**
 * \param mesh pointer on the mesh structure
 * \param sol pointer on the solution
 * \param x0 x coordinates of the points in the initial mesh
 * \param y0 y coordinates of the points in the initial mesh
 * \param dx x deplacement of the points
 * \param dy y deplacement of the points
 * \param xprev x coordinates bfore current Jacobi iteration
 * \param yprev y coordinates bfore current Jacobi iteration
 * \param maxcnt maximum number of elements to optimize
 * \param rlxpt list of index of the elements to optimize
 * \return number of elements that require optimization
 *
 * Check which elements will require relaxation and return their indices in descending order of importance
 */

int FMG_prechkArea2d(MMG5_pMesh mesh, MMG5_pSol sol, double* x0, double* y0, double *dx, double *dy, double *xprev, double *yprev,
                   int maxcnt, int *rlxpt){

  int iel,ip,cnt;
  double area_min = 0/*1e-6*/, eps = 1e-6;
  double *val;
  MMG5_pTria ptr;

  MMG5_SAFE_CALLOC(val,maxcnt+1,double,return 0);

  for (ip=0;ip<=maxcnt;ip++)
    rlxpt[ip] = 0;

  for (ip=1; ip<=mesh->np; ip++) {
    mesh->point[ip].c[0] = x0[ip] + dx[ip];
    mesh->point[ip].c[1] = y0[ip] + dy[ip];
  }

  cnt = 0;
  rlxpt[0] = 0;
  for (iel=1; iel<=mesh->nt && cnt <= mesh->nt; iel++) 
    if (FMG_computeArea(mesh, iel) < area_min) {
      ptr = &mesh->tria[iel];
      FMG_sortIntVect(rlxpt,val,iel,caltri_iso_in(mesh,sol,ptr),maxcnt);
      cnt += 1;
//    rlxpt[cnt] = iel;
    }

  for (ip=1; ip<=mesh->np; ip++) {
    mesh->point[ip].c[0] = xprev[ip];
    mesh->point[ip].c[1] = yprev[ip];
  }

//  if (maxcnt) {
//    printf("\n ** %d premiers elements qui demandent de la relaxation : ", maxcnt);
//    for (ip=1;ip<=rlxpt[0];ip++)
//      printf("\n               #%d :  %d %lf",ip,rlxpt[ip],val[ip]);
//  }

  MMG5_SAFE_FREE(val);

  return cnt;
}


/**
 * \param idxVect array of elements' index
 * \param valVect array of elements' quality
 * \param idx index of the element to add to the list
 * \param val quality of the element to add the list
 * \param n maximum number of items in the list
 *
 * Add a new element to sorted list in descending order 
 */


void FMG_sortIntVect(int* idxVect, double* valVect, int idx, double val, int n) {

  int i, j;

  valVect[0] = fabs(val) + fabs(valVect[1]) + 1;

  if (idxVect[0] == 0) {
    idxVect[0] ++;
    idxVect[idxVect[0]] = idx;
    valVect[idxVect[0]] = val;
    return;
  }
  else
  {
//      for (i=idxVect[0]; i>=1; i--) {
    for (i=n; i>=1; i--) {
      if (val > valVect[i] && val < valVect[i-1]) {
        for (j=idxVect[0]-1; j>=i; j--) {
          idxVect[j+1] = idxVect[j];
          valVect[j+1] = valVect[j];
        }
        idxVect[i] = idx;
        valVect[i] = val;
        idxVect[0] ++;//
        break;
      }
    }
  }
  
  if (idxVect[0] >= n)
    idxVect[0] = n;
 
}
