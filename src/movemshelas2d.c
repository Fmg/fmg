/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/



#include "fmg.h"
#include "math.h"



static inline double FMG_norm(double* vec,int ip, int np, int dim, int nsol) {
  double dd;
  int    i;
#warning check the validity if there are several solution
  for(i=0 ; i<dim ; i++)
    dd += vec[dim*np*(nsol-1) + dim*ip+i]*vec[dim*np*(nsol-1) + dim*ip+i];

  dd = sqrt(dd);

  return(dd);
}

/**
 * \param mesh : pointer on the mesh structure
 * \param sol : pointer on the sol structure
 * \param param : parameters for the algorithm
 *
 * Adapts the mesh to a given solution with elasticity
 *
 */

int FMG_movemshelas2d(MMG5_pMesh mesh, MMG5_pSol sol, FMG_pInfo param, double *xref, double *yref) {

  int                ijacobi, finaljacobi, k, ip, jp, iloc, jloc, iel, ilist, ineig,
    flag, lon=0, verbose = 1, ixmax, iymax, ipip, cnt1, cnt2, cnt, isol, optimized, origOUT;
  int                list[1000],  *rlxpt;
  double             *Grd,*Hes, *NSol, *NormGrd, *NormHes,*OmegaX, *OmegaY,  
    *dxk, *dyk, *LS, *GrdLS, *sol0, *x0, *y0, *xprev, *yprev, *dxkprev, *dykprev, *area0, *normal0, **K;
  double             rhsx,rhsy,relax_param=1., eps= 1e-12, dxmax, dymax, normal[2],resid[2],dxavg,dyavg,a;
  char               fname[15], fname2[20], *fileNames[] = {"phys","ls"};
  FILE               *resFile;

  // Elasticite lineaire
  int maxNeig, usefulMem;
  int *neigSparse;
  double *KsparseX, *KsparseY, *belast, *mu, *lambda;

  double ax, ay, edge_size, edge_size_min, edge_size_max;
  MMG5_pTria ptr;
  MMG5_pPoint    p1,p2;

  setbuf(stdout, NULL);
  if (param->verbose >= 1){
    fprintf(stdout,"\n*********************************************\n"); 
    fprintf    (stdout,"********* FMG : Mesh Adaptation *************\n"); 
    fprintf    (stdout,"*********************************************\n\n");
    if (param->imprim && param->fileID == 0) {
      fprintf    (stdout,"**** Output files : dom.*.mesh \n");
      fprintf    (stdout,"                    dom.*.sol  \n");
    }
    else if(param->imprim) {
      fprintf    (stdout,"**** Output files : dom.%d*.mesh \n",param->fileID);
      fprintf    (stdout,"                    dom.%d*.sol  \n",param->fileID);
    }
  }

  /*Gradient, Hessian and sol for physical domain (computed at each iteration)*/
  MMG5_SAFE_CALLOC(Grd,sol->size*(2*mesh->np+2),double,return 0);
  MMG5_SAFE_CALLOC(Hes,sol->size*(3*mesh->np+4),double,return 0);
  MMG5_SAFE_CALLOC(NSol,sol->size*(mesh->np+1),double,return 0);
  /*Norm of the gradient, hessian (computed at each iteration)*/
  MMG5_SAFE_CALLOC(NormGrd,sol->size*(mesh->np+1),double,return 0);
  MMG5_SAFE_CALLOC(NormHes,sol->size*(mesh->np+1),double,return 0);
  /*Omega*/
  MMG5_SAFE_CALLOC(OmegaX,mesh->np+1,double,return 0);
  if (param->aniso)
    MMG5_SAFE_CALLOC(OmegaY,mesh->np+1,double,return 0);
  /*dep computed by jacobi iteration*/
  MMG5_SAFE_CALLOC(dxk,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(dyk,mesh->np+1,double,return 0);
  /*LS solution and Grad of LS*/
  MMG5_SAFE_CALLOC(LS,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(GrdLS,2*mesh->np+2,double,return 0);
  /*init solution and position of the physical domain*/
  MMG5_SAFE_CALLOC(sol0,sol->size*(mesh->np+1),double,return 0);
  MMG5_SAFE_CALLOC(x0,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(y0,mesh->np+1,double,return 0);
  /*position used in case of mesh optimization by point relocation*/
  MMG5_SAFE_CALLOC(xprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(yprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(dxkprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(dykprev,mesh->np+1,double,return 0);
  /*areas and normal of reference mesh : computed once*/
  MMG5_SAFE_CALLOC(area0,mesh->nt+1,double,return 0);
  MMG5_SAFE_CALLOC(normal0,6*mesh->nt+6,double,return 0);

  MMG5_SAFE_CALLOC(rlxpt,param->maxOptim+1,int,return 0);

  if (param->model) {
    MMG5_SAFE_CALLOC(belast,2*mesh->np+2,double,return 0);
    MMG5_SAFE_CALLOC(mu,4*mesh->np+4,double,return 0);
    MMG5_SAFE_CALLOC(lambda,mesh->np+1,double,return 0);
  }


  if (param->levelSet)
    for (ip=1;ip<=mesh->np;ip++) {
      LS[ip] = sol->m[ip+(mesh->np+1)*(sol->size-1)];
    }

  if (param->levelSet && !param->desSize) {
    FMG_defineObject(mesh,sol,LS);
    if (param->delta > 1e-6)
      FMG_smoothObject(mesh, sol, LS, param->delta);
  }

 
  for (ip=1;ip<=mesh->np;ip++) {
    for (isol=1; isol<= sol->size; isol++)
      sol0[(isol-1)*(mesh->np+1) + ip] = sol->m[(isol-1)*(mesh->np+1) + ip];
    x0[ip] = mesh->point[ip].c[0];
    y0[ip] = mesh->point[ip].c[1];
  }
 
  for (ip=1; ip<=mesh->np; ip++){
    mesh->point[ip].s = 1;
    dxk[ip] = 0;
    dyk[ip] = 0;
    dxkprev[ip] = 0.;
    dykprev[ip] = 0.;
  }

  for (ip=1; ip<=mesh->np; ip++) {
    xprev[ip] = mesh->point[ip].c[0];
    yprev[ip] = mesh->point[ip].c[1];
  }

  /*computation of area and normal on ref mesh*/
  for (iel=1; iel<=mesh->nt; iel++) {
    area0[iel] = FMG_computeAreaRef(mesh,iel,xref,yref);
    for (iloc=0;iloc<3;iloc++){
      FMG_computeNormalVector2dRef(normal, mesh, iel, iloc,xref,yref);
      normal0[6*iel+2*iloc] = normal[0]; 
      normal0[6*iel+2*iloc+1] = normal[1];   
    }
  }
 

  if (param->imprim) {
    sol->size = 1;
    sprintf(fname, "dom.%d.mesh", param->fileID + 0);
    if ( !MMG2D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);
    sprintf(fname, "dom.%d.sol", param->fileID + 0);
    if ( !MMG2D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
    sol->size = param->nsols;
  }

  //*************//
  //** LOCATES **//
  //*************//

  // *** Points
  maxNeig = 0;
  usefulMem = 0;
  for (ip=1; ip<=mesh->np; ip++) {
    iel = FMG_locatePointTria2d(mesh, mesh->point[ip].c[0], mesh->point[ip].c[1], mesh->point[ip].s);
    mesh->point[ip].s = iel;
    for (iloc=0;iloc<3;iloc++)
      if (mesh->tria[iel].v[iloc] == ip)
        break;
    lon = MMG2D_boulep(mesh,iel,iloc,list);
    if(lon > 999) {
      fprintf(stdout,"a lot of neighbour, check your mesh\n");
    }
    usefulMem += lon;
    if (lon>maxNeig)
      maxNeig = lon;
  }
  maxNeig += 1;

  // *** Edges
  for (k=1; k<=mesh->na; k++) {
    ip = mesh->edge[k].a;
    jp = mesh->edge[k].b,
    iel = FMG_locateEdge(mesh, ip, jp, mesh->point[ip].s, mesh->point[jp].s, maxNeig);
    //trEdges[k] = iel;
  }


//  //******************//
//  //** Info memoire **//
//  //******************//
//
//  if (param->verbose >= 4) {
//    if (verbose) printf("\n* MAXNEIG = %d",maxNeig);
//    if (verbose) printf("\n* Useful Mem with sparse structure = %d/%d = %lf in K; %d/%d = %lf in neigSparse",
//                         usefulMem,(3*param->model+1)*maxNeig*(mesh->np+1),
//                         (double)usefulMem/(3*param->model+1)/maxNeig/(mesh->np+1),
//                         usefulMem+mesh->np,((3*param->model+1)*maxNeig+1)*(mesh->np+1),
//                         (double)(usefulMem+mesh->np)/((3*param->model+1)*maxNeig+1)/(mesh->np+1));
//    if (verbose) printf("\n* Useful Mem with original structure = %d/%lf = %lf in K",
//                         usefulMem,pow((param->model+1)*(mesh->np+1),2),
//                         (double)usefulMem/pow((param->model+1)*(mesh->np+1),2));
//  } 


  //*******************//
  // Allouer matrice **//
  //*******************//
  if (param->model && param->sparse)
    MMG5_SAFE_CALLOC(KsparseX,4*maxNeig*(mesh->np+1),double,return 0);

  if (!param->model && param->sparse) {
    MMG5_SAFE_CALLOC(KsparseX,maxNeig*(mesh->np+1),double,return 0);
    if (param->aniso)
      MMG5_SAFE_CALLOC(KsparseY,maxNeig*(mesh->np+1),double,return 0);
  }

  if (!param->model && !param->sparse) {
    MMG5_SAFE_CALLOC(K,mesh->np+1,double*,return 0);
    for (k=0; k<=mesh->np;k++)
      MMG5_SAFE_CALLOC(K[k],mesh->np+1,double,return 0);
  }

  if (param->sparse) {
    MMG5_SAFE_CALLOC(neigSparse,(maxNeig+1)*(mesh->np+1),int,return 0);
    FMG_constructSparseStruct2d(mesh, neigSparse, maxNeig);
  }


/////////// Block de test : DIFFUSION HYPERBOLIQUE
//  param->ijacobi_max = 0;
//  FMG_diffusionHyperbolique(mesh, sol, param, OmegaX, normal0,
//                               Grd, Hes, NormGrd, NormHes, NSol, x0, y0, sol0);
///////////

////////// Block de test : OPTIMISATION sur un element (voir resultat sur dom.9999)
//  param->ijacobi_max=0;
//  FMG_optlen(mesh,sol,16232);
//////////

//  if (param->desSize)
//    FMG_computeDesiredSize(mesh, OmegaX, LS, Grd, param, 0);


/////  if (param->ref)
/////    FMG_readRefMesh2d(mesh, param->refName, xref, yref, normal0, area0);
/////  else
/////    for (ip=1; ip<=mesh->np; ip++) {
/////      xref[ip] = mesh->point[ip].c[0];
/////      yref[ip] = mesh->point[ip].c[1];
/////    }

  for (ip=1;ip<=mesh->np;ip++) {
    for (isol=1; isol<= sol->size; isol++)
      sol0[(isol-1)*(mesh->np+1) + ip] = sol->m[(isol-1)*(mesh->np+1) + ip];
    x0[ip] = mesh->point[ip].c[0];
    y0[ip] = mesh->point[ip].c[1];
  }

  if (param->debug)
    resFile = fopen("residu","w");
  optimized = 0;
  flag = 0;
  verbose = param->verbose;
  //****************//
  //** ITERATIONS **//
  //****************//
  for (ijacobi = 1; ijacobi <= param->ijacobi_max; ijacobi++){
   
    if (ijacobi == 1) {
      edge_size_min = 1000.;
      edge_size_max = 0.;
      // Loop on elements
      for(iel=1;iel<=mesh->nt;iel++){
        ptr = &mesh->tria[iel];
        // Assembly total area and vertex contribution
        for(iloc=0;iloc<3;iloc++){
          jloc = iloc + 1;
          if (jloc == 3) jloc = 0;
    
          ip = ptr->v[iloc];
          jp = ptr->v[jloc];
    
          p1 = &mesh->point[ip];
          p2 = &mesh->point[jp];
    
          /* compute edge size */
          ax = p2->c[0] - p1->c[0];
          ay = p2->c[1] - p1->c[1];
          edge_size = sqrt(ax*ax+ay*ay);
          if (edge_size < edge_size_min) 
            edge_size_min = edge_size;
          if (edge_size > edge_size_max) 
            edge_size_max = edge_size;
        }
      }
    }
    while(1) { // pour refaire l'iteration si optimized

 
      if (param->verbose > 1) {
        if (!optimized) printf("\n\n******** ITERATION %d *********\n",ijacobi);
        else            printf("\n\n******** ITERATION %d BIS *********\n",ijacobi);
      }


      dxmax = 0.;
      dymax = 0.;
      ixmax = 0;
      iymax = 0;
 
      for (iel=1; iel<=mesh->nt; iel++)
        mesh->tria[iel].qual = FMG_computeArea(mesh,iel);

      /*compute grad and hess on physical mesh*/
      FMG_computeGrd2d(Grd,mesh,sol);
      FMG_computeHes2d(Hes,mesh,sol,Grd);
      if (param->verbose >= 3) printf("\n *** Fin compute Grd et Hes");

      FMG_computeNorm(Grd,NormGrd,mesh->np,2,sol->size);  // OK
      FMG_computeNorm(Hes,NormHes,mesh->np,3,sol->size); // OK

      for (isol=0; isol<sol->size; isol++)
        for (ip=1; ip<=mesh->np; ip++)
          NSol[isol*mesh->np + ip] = sol->m[isol*mesh->np + ip];

      if (!param->desSize) {
        param->gammaG = 0;
        for (isol=0; isol<sol->size; isol++) {
          for (ip=1; ip<=mesh->np; ip++) {
#warning check the validity if there are several solution
            //dd = FMG_norm(Grd,ip,mesh->np,2,isol+1);
            if(NormGrd[ip] > param->gammaG[isol])
              param->gammaG[isol] = NormGrd[ip];
          }
          param->gammaG[isol] *= 0.05;
        }
        if(param->normalize) {
          FMG_normalizeVec(NormGrd,mesh->np+1,param->gammaG[0],sol->size);
          FMG_normalizeVec(NormHes,mesh->np+1,param->gammaH[0],sol->size);
          FMG_normalizeVec(NSol,mesh->np+1,param->gammaS[0],sol->size);
        }
        if (param->verbose >= 3) printf("\n *** Fin normalisation");
      }


      if (param->desSize) {
        if (param->levelSet)
          if (!param->interpolate)
            FMG_readLS(mesh, LS, ijacobi-1);
          else {
            if (param->nsols == 1)
              for (ip=1; ip<=mesh->np;ip++)
                LS[ip] = sol->m[ip];
            else
              for (ip=1; ip<=mesh->np;ip++)
                LS[ip] = sol->m[ip+mesh->np+1];
          }
        FMG_computeDesiredSize2d(mesh, sol, param, OmegaX, Hes, LS, edge_size_min, edge_size_max);
      }


      // Calcul Omega 
      cnt1 = 0;
      cnt2 = 0;
   
      if (!param->desSize) {
        for (ip=1; ip<=mesh->np; ip++) {
          OmegaX[ip] = 1.;
  
          if (!param->sumAdapt) {
            if (param->alpha[0]*NormGrd[ip] > param->alpha[1]*NormGrd[mesh->np+1 + ip]) {
              NormGrd[mesh->np + 1 +ip] = 0.;
              cnt1++;
            }
            else
              NormGrd[ip] = 0;
            if (param->beta[0]*NormHes[ip] > param->beta[1]*NormHes[mesh->np+1 + ip]) {
              NormHes[mesh->np + 1 +ip] = 0.;
              cnt2 ++;
            }
            else
              NormHes[ip] = 0;
          }
  
          for (isol=0; isol<sol->size; isol++)
            OmegaX[ip] += param->tau[isol]*pow(NSol[isol*(mesh->np+1) + ip],2) +
              param->alpha[isol]*pow(NormGrd[isol*mesh->np + ip],2) +
              param->beta[isol]*pow(NormHes[isol*mesh->np + ip],2);
          OmegaX[ip] = sqrt(OmegaX[ip]); // OK
          //printf("\n%d %lf",ip,OmegaX[ip]);
          if (param->aniso)
            OmegaY[ip] = OmegaX[ip]; 
        }
      }
      else { /*desSize==1*/
        if (ijacobi <= (int)(10*param->ijacobi_max)) // test adapt puis desadapt
          for (ip=1; ip<=mesh->np; ip++) {
            OmegaX[ip] = 1./OmegaX[ip];
            if (param->aniso)
              OmegaY[ip] = OmegaX[ip];
          }
        else
          for (ip=1; ip<=mesh->np; ip++) {
            OmegaX[ip] = 1.;
            if (param->aniso)
              OmegaY[ip] = 1.;
          }
      }
      if (param->revert)
        for (ip=1; ip<=mesh->np; ip++) {
          OmegaX[ip] = 1.;//OmegaX[ip];
          if (param->aniso)
            OmegaY[ip] = 1.;//OmegaY[ip];
        }
        
      if (param->verbose >= 3) printf("\n *** Fin calcul Omega : %lf %lf",(double)cnt1/mesh->np,(double)cnt2/mesh->np);

      // Calcul Omega cas elasticite
      if (param->model) {
        a = .500000;
        for (ip=1; ip<=mesh->np; ip++) {
          //      mu[4*ip] =  sqrt(1. + param->tau*pow(NSol[ip],2) + param->alpha*pow(NormGrd[ip],2) + param->beta*pow(NormHes[ip],2));
          mu[4*ip] = a*OmegaX[ip];
          mu[4*ip+1] = 0.;
          mu[4*ip+3] = mu[4*ip];
          mu[4*ip+2] = 0.;
          lambda[ip] = (OmegaX[ip] - mu[4*ip])/2.;
          //      lambda[ip] = a*mu[4*ip];
        }
        if (param->verbose >= 3) printf("\n *** Fin calcul mu et lambda");
      }

      // Construction et resolution du systeme
      FMG_computeLinSystSparse2d(KsparseX, belast, mesh, area0, normal0, OmegaX, mu, lambda, neigSparse, maxNeig, param->model,sol,ijacobi);
      if (param->aniso)
        FMG_computeLinSystSparse2d(KsparseY, belast, mesh, area0, normal0, OmegaY, mu, lambda, neigSparse, maxNeig, param->model,sol,ijacobi);
      if (param->verbose >= 3) printf("\n *** Fin calcul matrice");
      FMG_iterJacobiSparse2d(mesh, dxk, dyk, KsparseX, KsparseY, belast, neigSparse, maxNeig, param->model, param->aniso);
      if (param->verbose >= 3) printf("\n *** Fin calcul deplacements");

      dxmax = 0.;
      dymax = 0.;
      for (ip=1; ip<=mesh->np; ip++) {
        if (fabs(dxk[ip])>dxmax){
          dxmax = fabs(dxk[ip]);
          ixmax = ip;
        }
        if (fabs(dyk[ip])>dymax){
          dymax = fabs(dyk[ip]);
          iymax = ip;
        }
      }


      for (ip=1; ip<=mesh->np; ip++) {
        xprev[ip] = mesh->point[ip].c[0];
        yprev[ip] = mesh->point[ip].c[1];
      }

      // si on n'a pas encore optimise dans cette iteration
      if (!optimized) {
        cnt = FMG_prechkArea2d(mesh,sol,x0,y0,dxk,dyk,xprev,yprev,param->maxOptim,rlxpt);
        if (param->verbose >= 3) printf("\n *** Nb d'éléments qui demandent de la relaxation : %d", cnt ); 
        
        // s'il faut optimiser
        if (param->maxOptim && rlxpt[0] > 0 && ijacobi < 3){ // && rlxpt[0]<=maxrlxpt) {
          if (param->verbose >= 3) printf("\n ******* DEBUT OPTIMISATION (%d elements)",rlxpt[0]);
          for (iel=1; iel<=rlxpt[0]; iel++)
            FMG_optlen2d(mesh,sol,rlxpt[iel]);
          if (param->verbose >= 3) printf("\n ******* FIN OPTIMISATION");
          optimized = 1;
          ijacobi --;
          for (ip=1; ip<=mesh->np; ip++) {
            dxk[ip] = dxkprev[ip];
            dyk[ip] = dykprev[ip];
          }
          FMG_interp2d(mesh, sol, x0, y0, sol0);
        
          // ijacobi = 0 --> optimiser = modifier mesh initial
          if (ijacobi == 0) {
            for (ip=1; ip<=mesh->np; ip++) {
              sol0[ip] = sol->m[ip];
              x0[ip] = mesh->point[ip].c[0];
              y0[ip] = mesh->point[ip].c[1];
            } 
          }
        
          break; //sortir du while(1)
        }    
      }
    
      optimized = 0;

      relax_param =  FMG_chkArea2d(mesh, x0, y0, dxk, dyk, xprev, yprev);
      if (param->verbose >= 3) printf("\n *** Fin calcul relax: relax_param = %lf", relax_param);

      if (relax_param > eps) {
        for (ip=1; ip<=mesh->np; ip++) {
          mesh->point[ip].c[0] = xprev[ip] + relax_param*(x0[ip] + dxk[ip] - xprev[ip]);
          mesh->point[ip].c[1] = yprev[ip] + relax_param*(y0[ip] + dyk[ip] - yprev[ip]);
        }
      }
      else {
        for (ip=1; ip<=mesh->np; ip++) {
          mesh->point[ip].c[0] = xprev[ip];
          mesh->point[ip].c[1] = yprev[ip];
        }
        flag = 1;
      }

      dxavg = 0.;
      dyavg = 0.;
      resid[0] = 0;
      resid[1] = 0;
      for (ip=1;ip<=mesh->np;ip++) {
        dxavg += fabs(dxk[ip]);
        dyavg += fabs(dyk[ip]);
        if(resid[0] < fabs(dxk[ip]-dxkprev[ip]))
           resid[0] = fabs(dxk[ip]-dxkprev[ip]);
        if(resid[1] < fabs(dyk[ip]-dykprev[ip]))
           resid[1] = fabs(dyk[ip]-dykprev[ip]);
      }

      for (ip=1; ip<=mesh->np; ip++) {
        dxkprev[ip] = dxk[ip];
        dykprev[ip] = dyk[ip];
      }


      // FMG_computeJacobiResidual2d(mesh, KsparseX, KsparseY, resid, neigSparse, maxNeig, param->aniso);

      if (param->verbose >= 2) {
        printf("\n *** Fin actualisation des points iteration %d",ijacobi);
        printf("\n              * relax = %lf",relax_param);
        printf("\n              * MAX dx= %lf , point %d",relax_param*dxmax,ixmax);
        printf("\n              * MAX dy= %lf , point %d",relax_param*dymax,iymax);
        printf("\n              * AVG dx= (%lf,%lf)",dxavg/mesh->np,dyavg/mesh->np);
        printf("\n              * Res = (%lf,%lf)\n",resid[0],resid[1]);
      }
      else if (param->verbose>0) // minimal
        fprintf(stdout,"\n *** Iteration %d/%d : relax = %lf, max deplc = (%lf,%lf)\n",ijacobi,param->ijacobi_max,relax_param,relax_param*dxmax,relax_param*dymax);
      if (param->debug)
        fprintf(resFile,"%d %lf %lf\n",ijacobi,resid[0],resid[1]);


      // Solution sur le nouveau maillage
      if (!param->solCase) {
        FMG_interp2d(mesh, sol, x0, y0, sol0);   // interpoler solution   
        if (param->verbose >= 3) printf("\n *** Fin interpolation\n\n");
      }
      else {
        FMG_analytic(mesh,sol,param);
        if (param->verbose >= 3) printf("\n *** Fin calcul analytique sur nouveau maillage");
      }

      // Save new mesh
      if (param->imprim && !(ijacobi%param->imprim)) {
        sol->size = 1;
        sprintf(fname, "dom.%d.mesh", ijacobi/param->imprim);
        if ( !MMG2D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);
        sprintf(fname, "dom.%d.sol", ijacobi/param->imprim);
        if ( !MMG2D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
        sol->size = param->nsols;
      }

      finaljacobi = ijacobi-1;
      if (flag && ijacobi<param->ijacobi_max) {
        ijacobi = param->ijacobi_max;
        if (param->verbose >= 2) printf("\n\n *** !!! Exécution arrêtée par relaxation complète!!");
      }

      break; // !optimized -> sortir du while
    }
  }

  if (ijacobi == param->ijacobi_max + 1 && !flag) finaljacobi = param->ijacobi_max;
  if (param->verbose >= 1) printf("\n***** Fin Jacobi après %d itérations\n",finaljacobi);
 
  if (param->debug) 
    fclose(resFile);

  // Save final mesh
  if (param->imprim) {
    sol->size = 1;
    if (param->nsols == 1) {
      sprintf(fname, "dom.%d.mesh", 9999);
      if ( !MMG2D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);
      sprintf(fname, "dom.%d.sol", 9999);
      if ( !MMG2D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
    }
    else
      for (isol=1;isol<=param->nsols;isol++) {
        sprintf(fname, ".%d.mesh", 9999);
        strcpy(fname2,fileNames[isol-1]);
        strcat(fname2,fname);
        if ( !MMG2D_saveMesh(mesh,fname2) )  exit(EXIT_FAILURE);


        sprintf(fname, ".%d.sol", 9999);
        strcpy(fname2,fileNames[isol-1]);
        strcat(fname2,fname);
        if ( !MMG2D_saveSol(mesh,sol,fname2) )  exit(EXIT_FAILURE);
        for (ip=1; ip<=mesh->np; ip++)
          sol->m[ip] = sol->m[isol*(mesh->np+1) + ip];
      }
    sol->size = param->nsols;
  }

  MMG5_SAFE_FREE(Grd);
  MMG5_SAFE_FREE(Hes);
  MMG5_SAFE_FREE(NSol);
  MMG5_SAFE_FREE(NormGrd);
  MMG5_SAFE_FREE(NormHes);
  MMG5_SAFE_FREE(OmegaX);
  if (param->aniso)
    MMG5_SAFE_FREE(OmegaY);
  MMG5_SAFE_FREE(dxk);
  MMG5_SAFE_FREE(dyk);
  MMG5_SAFE_FREE(x0);
  MMG5_SAFE_FREE(y0);
  MMG5_SAFE_FREE(xprev);
  MMG5_SAFE_FREE(yprev);
  MMG5_SAFE_FREE(dxkprev);
  MMG5_SAFE_FREE(dykprev);
  MMG5_SAFE_FREE(area0);
  MMG5_SAFE_FREE(normal0);
  MMG5_SAFE_FREE(neigSparse);
  MMG5_SAFE_FREE(LS);
  MMG5_SAFE_FREE(GrdLS);
  MMG5_SAFE_FREE(sol0);
  MMG5_SAFE_FREE(x0);
  MMG5_SAFE_FREE(y0);

  if (param->model) {
    MMG5_SAFE_FREE(mu);
    MMG5_SAFE_FREE(lambda);
    MMG5_SAFE_FREE(belast);
  }
  if (param->sparse) {
    MMG5_SAFE_FREE(KsparseX);
    if (param->aniso)
      MMG5_SAFE_FREE(KsparseY);
  }

  if (param->verbose>=2) printf("\n *** Fin desalouer vecteurs");

  if (!param->model && !param->sparse) {
    for (k=0; k<=mesh->np;k++)
      MMG5_SAFE_FREE(K[k]);
    MMG5_SAFE_FREE(K);
  }
  if (param->verbose >= 2) printf("\n *** Fin desallouer matrice");


  if (param->verbose >= 1) {fprintf(stdout,"\n ***************************"); 
    fprintf(stdout,"\n ********* End FMG *********");
    fprintf(stdout,"\n ***************************\n\n");}

  if (param->verbose==-10)
    fprintf(stdout,"\n******* End FMG :\n******* fileID: %d; nb iterations: %d\n\n", param->fileID, finaljacobi);


}


