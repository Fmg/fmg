/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"

static inline double Perm21(int iloc,int iquad,double a) {
  if( iloc+iquad == 2 ) return 1.0-2.0*a;
  else                  return a;
}

static inline double Perm31(int iloc,int iquad,double a) {
  if( iloc+iquad == 3 ) return 1.0-3.0*a;
  else                  return a;
}

static inline void FMG_Set_quad_pts1(FMG_pQuad quad,double (*perm)(int,int,double),double a) {
  int iloc,iquad;

  MMG5_SAFE_MALLOC(quad->points,quad->npoints*(quad->dim+1),double,return);

  for( iquad = 0; iquad < quad->npoints; iquad++ )
    for( iloc = 0; iloc < quad->dim+1; iloc++ )
      quad->points[(quad->dim+1)*iquad+iloc] = (*perm)(iloc,iquad,a);

}

static void FMG_Init_quad_2D_P2(FMG_pQuad quad) {
  quad->dim     = 2;
  quad->order   = 2;
  quad->npoints = 3;
  FMG_Set_quad_pts1( quad, &Perm21, 1.0/6.0 );
  quad->weight = 1.0/3.0;
}

static void FMG_Init_quad_3D_P2(FMG_pQuad quad) {
  quad->dim     = 3;
  quad->order   = 2;
  quad->npoints = 4;
  FMG_Set_quad_pts1( quad, &Perm31, (5.0-sqrt(5.0))/20.0 );
  quad->weight = 0.25;
}

void FMG_Init_listquad(FMG_pDatastr data) {

  MMG5_SAFE_MALLOC(data->listquad,2,FMG_Quad,return);
  
  FMG_Init_quad_2D_P2( &(data->listquad[FMG_QUAD_2D_P2]) );
  FMG_Init_quad_3D_P2( &(data->listquad[FMG_QUAD_3D_P2]) );
}

static void FMG_eval_grad_quad_2D_ex(MMG5_pMesh mesh,MMG5_pTria pt,FMG_pInfo param,FMG_pQuad quad,int iquad,double *sol,double *vecval) {
  double x,y,phi;
  int    iloc,ip;

  x = y = 0.0;

  for( iloc = 0; iloc < quad->dim+1; iloc++ ) {
    ip = pt->v[iloc];
    phi = quad->points[(quad->dim+1)*iquad+iloc];

    x += phi*mesh->point[ip].c[0];
    y += phi*mesh->point[ip].c[1];

  }

  FMG_analytic_grad_point(param,x,y,0.0,vecval);

}

static void FMG_eval_grad_quad_3D_ex(MMG5_pMesh mesh,MMG5_pTetra pt,FMG_pInfo param,FMG_pQuad quad,int iquad,double *sol,double *vecval) {
  double x,y,z,phi;
  int    iloc,ip;

  x = y = z = 0.0;

  for( iloc = 0; iloc < quad->dim+1; iloc++ ) {
    ip = pt->v[iloc];
    phi = quad->points[(quad->dim+1)*iquad+iloc];

    x += phi*mesh->point[ip].c[0];
    y += phi*mesh->point[ip].c[1];
    z += phi*mesh->point[ip].c[2];
  }

  FMG_analytic_grad_point(param,x,y,z,vecval);

}

static void FMG_eval_quad_2D_ex(MMG5_pMesh mesh,MMG5_pTria pt,FMG_pInfo param,FMG_pQuad quad,int iquad,double *sol,double *fnval) {
  double x,y,phi;
  int    iloc,ip;

  x = y = 0.0;

  for( iloc = 0; iloc < quad->dim+1; iloc++ ) {
    ip = pt->v[iloc];
    phi = quad->points[(quad->dim+1)*iquad+iloc];

    x += phi*mesh->point[ip].c[0];
    y += phi*mesh->point[ip].c[1];

  }

  *fnval = FMG_analytic_point(param,x,y,0.0);

}

static void FMG_eval_quad_3D_ex(MMG5_pMesh mesh,MMG5_pTetra pt,FMG_pInfo param,FMG_pQuad quad,int iquad,double *sol,double *fnval) {
  double x,y,z,phi;
  int    iloc,ip;

  x = y = z = 0.0;

  for( iloc = 0; iloc < quad->dim+1; iloc++ ) {
    ip = pt->v[iloc];
    phi = quad->points[(quad->dim+1)*iquad+iloc];

    x += phi*mesh->point[ip].c[0];
    y += phi*mesh->point[ip].c[1];
    z += phi*mesh->point[ip].c[2];
  }

  *fnval = FMG_analytic_point(param,x,y,z);

}

static void FMG_eval_quad_2D_num(MMG5_pMesh mesh,MMG5_pTria pt,FMG_pInfo param,FMG_pQuad quad,int iquad,double *sol,double *fnval){
  int iloc,ip;

  *fnval = 0;
  /* Evaluate approximate function on quadrature point */
  for( iloc = 0; iloc < quad->dim+1; iloc++ ) {
    ip = pt->v[iloc];
    *fnval += quad->points[(quad->dim+1)*iquad+iloc]*sol[ip];
  }
}

static void FMG_eval_quad_3D_num(MMG5_pMesh mesh,MMG5_pTetra pt,FMG_pInfo param,FMG_pQuad quad,int iquad,double *sol,double *fnval){
  int iloc,ip;

  *fnval = 0;
  /* Evaluate approximate function on quadrature point */
  for( iloc = 0; iloc < quad->dim+1; iloc++ ) {
    ip = pt->v[iloc];
    *fnval += quad->points[(quad->dim+1)*iquad+iloc]*sol[ip];
  }
}

static void FMG_eval_grad_quad_2D_num(MMG5_pMesh mesh,MMG5_pTria pt,FMG_pInfo param,FMG_pQuad quad,int iquad,double *sol,double *vecval) {
  double normal[2];
  int iloc,idim,ip,v1,v2;

  /* Evaluate approximate function on quadrature point */
  for( iloc = 0; iloc < quad->dim+1; iloc++ ) {
    ip = pt->v[iloc];
    v1 = pt->v[MMG2D_inxt[iloc]];
    v2 = pt->v[MMG2D_inxt[iloc+1]];
    normal[0] = mesh->point[v2].c[1]-mesh->point[v1].c[1];
    normal[1] = mesh->point[v1].c[0]-mesh->point[v2].c[0];
    /* Get inward direction (vector norm is the length) */
    for( idim = 0; idim <2; idim++ ) {
      vecval[idim] += -normal[idim]*sol[ip]/(2.0*pt->qual);
    }
  }
}

static void FMG_eval_grad_quad_3D_num(MMG5_pMesh mesh,MMG5_pTetra pt,FMG_pInfo param,FMG_pQuad quad,int iquad,double *sol,double *vecval) {
  double normal[3];
  int iloc,idim,ip;

  /* Evaluate approximate function on quadrature point */
  for( iloc = 0; iloc < quad->dim+1; iloc++ ) {
    ip = pt->v[iloc];
    if( !MMG5_nonUnitNorPts(mesh,
                            pt->v[MMG5_idir[iloc][0]],
                            pt->v[MMG5_idir[iloc][1]],
                            pt->v[MMG5_idir[iloc][2]],
                            normal) ) printf("\n!!!! Error norface");
    /* Get inward direction (vector norm is twice the area) */
    for( idim = 0; idim <3; idim++ ) {
      vecval[idim] += -normal[idim]*sol[ip]/(6.0*pt->qual);
    }
  }
}

double FMG_num_quad_2D_elem(MMG5_pMesh mesh,MMG5_pTria pt,FMG_pInfo param,FMG_pQuad quad,double *sol,int solSize,int evalType) {
  void   (*func)(MMG5_pMesh,MMG5_pTria,FMG_pInfo param,FMG_pQuad,int,double*,double*);
  void   (*func_ref)(MMG5_pMesh,MMG5_pTria,FMG_pInfo param,FMG_pQuad,int,double*,double*);
  double fnval[3],fnval_ref[3],res;
  int    iquad,idim,ndim;

  /* Initialization */
  res = 0;

  /* Select function */
  if ( solSize == FMG_QUAD_FUNC ) {
    ndim = 1;
    if ( evalType == FMG_QUAD_EX )
      func = &FMG_eval_quad_2D_ex;
    else if( evalType == FMG_QUAD_NUM )
      func = &FMG_eval_quad_2D_num;
    else if( evalType == FMG_QUAD_ERR ) {
      func = &FMG_eval_quad_2D_num;
      func_ref = &FMG_eval_quad_2D_ex;
    }
  }
  else if( solSize == FMG_QUAD_GRAD ) {
    ndim = 2;
    if( evalType == FMG_QUAD_EX )
      func = &FMG_eval_grad_quad_2D_ex;
    else if ( evalType == FMG_QUAD_NUM )
      func = &FMG_eval_grad_quad_2D_num;
    else if ( evalType == FMG_QUAD_ERR ) {
      func = &FMG_eval_grad_quad_2D_num;
      func_ref = &FMG_eval_grad_quad_2D_ex;
    }
  }

  for( idim = 0; idim < 3; idim ++ )
    fnval[idim] = fnval_ref[idim] = 0.0;

  for( iquad = 0; iquad < quad->npoints; iquad++ ) {

    /* Evaluate approximate function on each quadrature point */
    (*func)( mesh, pt, param, quad, iquad, sol, fnval);

    if( evalType == FMG_QUAD_ERR ) {
      (*func_ref)( mesh, pt, param, quad, iquad, sol, fnval_ref);
      for( idim = 0; idim < ndim; idim ++ )
        fnval[idim] += -fnval_ref[idim];
    }

    /* Evaluate (squared) Euclidean norm */
    for( idim = 0; idim < ndim; idim ++ )
      res += fnval[idim]*fnval[idim];

  }
  /* Scale by element volume and quadrature weight */
  res *= pt->qual*quad->weight;

  return res;
}


double FMG_num_quad_3D_elem(MMG5_pMesh mesh,MMG5_pTetra pt,FMG_pInfo param,FMG_pQuad quad,double *sol,int solSize,int evalType) {
  void   (*func)(MMG5_pMesh,MMG5_pTetra,FMG_pInfo param,FMG_pQuad,int,double*,double*);
  void   (*func_ref)(MMG5_pMesh,MMG5_pTetra,FMG_pInfo param,FMG_pQuad,int,double*,double*);
  double fnval[3],fnval_ref[3],res;
  int    iquad,idim,ndim;

  /* Initialization */
  res = 0;

  /* Select function */
  if ( solSize == FMG_QUAD_FUNC ) {
    ndim = 1;
    if ( evalType == FMG_QUAD_EX )
      func = &FMG_eval_quad_3D_ex;
    else if( evalType == FMG_QUAD_NUM )
      func = &FMG_eval_quad_3D_num;
    else if( evalType == FMG_QUAD_ERR ) {
      func = &FMG_eval_quad_3D_num;
      func_ref = &FMG_eval_quad_3D_ex;
    }
  }
  else if( solSize == FMG_QUAD_GRAD ) {
    ndim = 3;
    if( evalType == FMG_QUAD_EX )
      func = &FMG_eval_grad_quad_3D_ex;
    else if ( evalType == FMG_QUAD_NUM )
      func = &FMG_eval_grad_quad_3D_num;
    else if ( evalType == FMG_QUAD_ERR ) {
      func = &FMG_eval_grad_quad_3D_num;
      func_ref = &FMG_eval_grad_quad_3D_ex;
    }
  }

  for( idim = 0; idim < 3; idim ++ )
    fnval[idim] = fnval_ref[idim] = 0.0;

  for( iquad = 0; iquad < quad->npoints; iquad++ ) {

    /* Evaluate approximate function on each quadrature point */
    (*func)( mesh, pt, param, quad, iquad, sol, fnval);

    if( evalType == FMG_QUAD_ERR ) {
      (*func_ref)( mesh, pt, param, quad, iquad, sol, fnval_ref);
      for( idim = 0; idim < ndim; idim ++ )
        fnval[idim] += -fnval_ref[idim];
    }

    /* Evaluate (squared) Euclidean norm */
    for( idim = 0; idim < ndim; idim ++ )
      res += fnval[idim]*fnval[idim];

  }
  /* Scale by element volume and quadrature weight */
  res *= pt->qual*quad->weight;

  return res;
}

double FMG_num_quad_2D(MMG5_pMesh mesh,FMG_pInfo param,FMG_pQuad quad,double *sol,int solSize,int evalType) {
  MMG5_pTria pt;
  double     res;
  int        ie;

  res = 0;
  for( ie = 1; ie <= mesh->nt; ie++ ) {
    pt = &mesh->tria[ie];
    res += FMG_num_quad_2D_elem( mesh, pt, param, quad, sol, solSize, evalType ); 
  }

  return res;
}

double FMG_num_quad_3D(MMG5_pMesh mesh,FMG_pInfo param,FMG_pQuad quad,double *sol,int solSize,int evalType) {
  MMG5_pTetra pt;
  double      res;
  int         ie;

  res = 0;
  for( ie = 1; ie <= mesh->ne; ie++ ) {
    pt = &mesh->tetra[ie];
    res += FMG_num_quad_3D_elem( mesh, pt, param, quad, sol, solSize, evalType ); 
  }

  return res;
}

static void FMG_Free_quad(FMG_pQuad quad) {
  MMG5_SAFE_FREE(quad->points);
}

void FMG_Free_listquad(FMG_pDatastr data) {
  FMG_Free_quad( &(data->listquad[FMG_QUAD_2D_P2]) );
  FMG_Free_quad( &(data->listquad[FMG_QUAD_3D_P2]) );
  MMG5_SAFE_FREE(data->listquad);
}
