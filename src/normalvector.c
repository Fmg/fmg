/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"


/**
 * \param vec normal vector
 * \param mesh pointer on the mesh
 * \param iel element's index
 * \param iloc point's local index in the element iel
 * 
 * Computes normal vector in the face of the element iel opposed to the point iloc
 */

void FMG_computeNormalVector3d(double vec[3], MMG5_pMesh mesh, int iel, int iloc) {

  MMG5_pTetra ptr;
  int dim;

  ptr = &mesh->tetra[iel];

  // Compute non-normalized face normal given three points on the surface
  if( !MMG5_nonUnitNorPts(mesh,
                           ptr->v[MMG5_idir[iloc][0]],
                           ptr->v[MMG5_idir[iloc][1]],
                           ptr->v[MMG5_idir[iloc][2]],
                           vec) ) printf("\n!!!! Error norface");

  // Get inward direction (vector norm is twice the area)
  for (dim=0; dim<3; dim++)
    vec[dim] = -vec[dim];
}




/**
 * \param vec normal vector
 * \param mesh pointer on the mesh
 * \param iel element's index
 * \param iloc point's local index in the element iel
 * 
 * Computes normal vector in the face of the element iel opposed to the point iloc 
 * for reference coordinates. Closely modeled after MMG5_norface,
 * MMG5_nonUnitNorPts and MMG5_norPts.
 */

void FMG_computeNormalVector3dRef(double vec[3], MMG5_pMesh mesh, int iel, int iloc
                                  ,double *xref, double *yref,double* zref) {

  MMG5_pTetra ptr;
  int dim, k[3];
  double u[3], v[3], area;

  ptr = &mesh->tetra[iel];

  for (dim=0; dim<3; dim++)
    k[dim] = MMG5_idir[iloc][dim];
 
  u[0] = xref[ptr->v[k[1]]] - xref[ptr->v[k[0]]];
  u[1] = yref[ptr->v[k[1]]] - yref[ptr->v[k[0]]];
  u[2] = zref[ptr->v[k[1]]] - zref[ptr->v[k[0]]];
 
  v[0] = xref[ptr->v[k[2]]] - xref[ptr->v[k[0]]];
  v[1] = yref[ptr->v[k[2]]] - yref[ptr->v[k[0]]];
  v[2] = zref[ptr->v[k[2]]] - zref[ptr->v[k[0]]];

  vec[0] = u[1]*v[2] - u[2]*v[1];
  vec[1] = u[2]*v[0] - u[0]*v[2];
  vec[2] = u[0]*v[1] - u[1]*v[0];

//  area = 0.;
//  for (dim=0; dim<3; dim++)
//    area += vec[dim]*vec[dim];
//
//  if(area <MMG5_EPSD2) printf("\n!!!! Error norface");

  // Get inward direction (vector norm is twice the area)
  for (dim=0; dim<3; dim++)
    vec[dim] = -vec[dim];

}


/**
 * \param vec normal vector
 * \param mesh pointer on the mesh
 * \param iel element's index
 * \param iloc point's local index in the element iel
 * 
 * Computes normal vector in the face of the element iel opposed to the point iloc
 */

void FMG_computeNormalVector2d(double vec[2], MMG5_pMesh mesh, int iel, int iloc) {

  MMG5_pTria ptr;
  int ip, jp;
  double vec2[2];

  ptr = &mesh->tria[iel];
  
  vec[0] = 0.;
  vec[1] = 0.;  
  if (iloc == 0) {
    ip = 1;
    jp = 2;
  } 
  else if (iloc == 1) {
    ip = 2;
    jp = 0;
  }
  else {
    ip = 0;
    jp = 1;
  }
 
  vec[0] = mesh->point[ptr->v[ip]].c[1] - mesh->point[ptr->v[jp]].c[1];
  vec[1] = mesh->point[ptr->v[jp]].c[0] - mesh->point[ptr->v[ip]].c[0];
 

}
/**
 * \param vec normal vector
 * \param mesh pointer on the mesh
 * \param iel element's index
 * \param iloc point's local index in the element iel
 * \param xref xcoordinates of the vertex
 * \param yref ycoordinates of the vertex
 * 
 * Computes normal vector in the face of the element iel opposed to the point iloc
 */

void FMG_computeNormalVector2dRef(double vec[2], MMG5_pMesh mesh, int iel, int iloc,double *xref, double *yref) {

  MMG5_pTria ptr;
  int ip, jp;
  double vec2[2];

  ptr = &mesh->tria[iel];
  
  vec[0] = 0.;
  vec[1] = 0.;  
  if (iloc == 0) {
    ip = 1;
    jp = 2;
  } 
  else if (iloc == 1) {
    ip = 2;
    jp = 0;
  }
  else {
    ip = 0;
    jp = 1;
  }
 
  vec[0] = yref[ptr->v[ip]] - yref[ptr->v[jp]];
  vec[1] = xref[ptr->v[jp]] - xref[ptr->v[ip]];
 

}

