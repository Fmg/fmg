/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/



#include "fmg.h"
#include "math.h"

/**
 * \param mesh : pointer on the mesh structure
 * \param sol : pointer on the sol structure
 * \param param : parameters for the algorithm
 *
 * Adapts the mesh to a given solution
 *
 */

void FMG_movemsh2d(MMG5_pMesh mesh, MMG5_pSol sol, FMG_pDatastr data,FMG_pInfo param, double *xref, double *yref) {
  MMG5_pTria           pt,ptr;
  MMG5_pPoint          pp1,pp2,ppt;
  int                ijacobi, finaljacobi, iinner, iinner_max, k, ip, jp, iloc, jloc, iel, ilist, ineig,
    flag, lon=0, verbose = 1, ixmax, iymax, ipip, cnt1, cnt2, cnt, isol, optimized, origOUT;
  int                *rlxpt,*neigSparse,tmp,ip2;
  double             rhsx,rhsy,relax_param=1.,*relax_paramArr, eps= 1e-12, dxmax, dymax, normal[2],resid[2],dxavg,dyavg,a;
  double             *NormSolH1, *Grd,*Hes, *NSol, *NormGrd, *NormHes,*OmegaX, *OmegaY,  
    *dxk, *dyk, *dxk_aux, *dyk_aux, *LS, *GrdLS, *sol0, *x0, *y0, *xprev, *yprev, *dxkprev, *dykprev, *area0, *normal0, **K;
  char               fname[15], fname2[20], *fileNames[] = {"phys","ls"};
  FILE               *resFile;
  double       *KsparseX, *KsparseY, *belast, *mu, *lambda, *KsparseX_aux, *KsparseY_aux, *weightModel;
  double       E, nu, hmin, max_Omega;
  double ax, ay, edge_size, edge_size_min, edge_size_max;

  // Linear elasticity parameters
  E = 1.0;//0.5*pow(param->alpha[0]+param->beta[0]+param->tau[0],0.5);
  nu = 0.45;//0.1;


  setbuf(stdout, NULL);
  if (param->verbose >= 1){
    fprintf(stdout,"\n*********************************************\n"); 
    fprintf    (stdout,"********* FMG : Mesh Adaptation *************\n"); 
    fprintf    (stdout,"*********************************************\n\n");
    fprintf(stdout,"git branch: %s\n",FMG_GIT_BRANCH);
    fprintf(stdout,"git commit: %s\n",FMG_GIT_COMMIT);
    fprintf(stdout,"git date:   %s\n\n",FMG_GIT_DATE);
    if (param->imprim && param->fileID == 0) {
      fprintf    (stdout,"**** Output files : dom.*.mesh \n");
      fprintf    (stdout,"                    dom.*.sol  \n");
    }
    else if(param->imprim) {
      fprintf    (stdout,"**** Output files : dom.%d*.mesh \n",param->fileID);
      fprintf    (stdout,"                    dom.%d*.sol  \n",param->fileID);
    }
    fprintf(stdout,"\n Modèle ");
    switch(param->model){
      case 0:
        fprintf(stdout,"laplacien.\n");
        break;
      case 1:
        fprintf(stdout,"élastique.\n");
        break;
      case 2:
        fprintf(stdout,"mixte.\n");
        break;
    }
  }

 
  Grd     = data->Grd;
  Hes     = data->Hes;
  NSol    = data->NSol;
  NormGrd = data->NormGrd;
  NormHes = data->NormHes;
  OmegaX  = data->OmegaX;
  OmegaY  = data->OmegaY;
  dxk     = data->dxk;
  dyk     = data->dyk;
  dxk_aux = data->dxk_aux;
  dyk_aux = data->dyk_aux;
  LS      = data->LS;
  GrdLS   = data->GrdLS;
  sol0    = data->sol0;
  x0      = data->x0;
  y0      = data->y0;
  xprev   = data->xprev;
  yprev   = data->yprev;
  dxkprev = data->dxkprev;
  dykprev = data->dykprev;
  area0   = data->area0;
  normal0 = data->normal0;

  neigSparse   = data->neigSparse; 
  KsparseX     = data->KsparseX;  
  KsparseY     = data->KsparseY;
  KsparseX_aux = data->KsparseX_aux;
  KsparseY_aux = data->KsparseY_aux;
  weightModel  = data->weightModel;
  K            = data->K; 
  belast       = data->belast;
  mu           = data->mu;
  lambda       = data->lambda;

#warning change here : consider LS as the first solution
  if (param->levelSet)
    for (ip=1;ip<=mesh->np;ip++) {
      LS[ip] = sol->m[ip+(mesh->np+1)];
    }

  if (param->levelSet && !param->desSize) {
    FMG_defineObject(mesh,sol,LS);
    if (param->delta > 1e-6)
      FMG_smoothObject(mesh, sol, LS, param->delta);
  }

  // Solution analytique sur le maillage initiale
  if (param->solCase) {
    FMG_analytic(mesh,sol,param);
    if (param->verbose >= 3) printf("\n *** Fin calcul analytique sur le maillage initiale");
  }

  // Compute solution H1 seminorm
//  MMG5_SAFE_CALLOC(NormSolH1,sol->size,double,return);
////  FMG_computeH1Seminorm2d(NormSolH1,mesh,sol);
//  NormSolH1[0] = sqrt(FMG_num_quad_2D(mesh,param,&data->listquad[FMG_QUAD_2D_P2],sol->m,FMG_QUAD_GRAD,FMG_QUAD_ERR));
//  printf("H1 seminorm: %e\n",NormSolH1[0]);
////  FMG_computeL2Norm2d(NormSolH1,mesh,sol);
//  NormSolH1[0] = sqrt(FMG_num_quad_2D(mesh,param,&data->listquad[FMG_QUAD_2D_P2],sol->m,FMG_QUAD_FUNC,FMG_QUAD_ERR));
//  printf("L2 norm: %e\n",NormSolH1[0]);
////  MMG5_SAFE_FREE(NormSolH1);


  /* Store pt->s into pt->tmp (it is need for LS gradient and hessian) */
  for (ip=1;ip<=mesh->np;ip++)
    mesh->point[ip].tmp = mesh->point[ip].s;

  for (ip=1;ip<=mesh->np;ip++) {
    for (isol=1; isol<= sol->size; isol++)
      sol0[(isol-1)*(mesh->np+1) + ip] = sol->m[(isol-1)*(mesh->np+1) + ip];
    x0[ip] = mesh->point[ip].c[0];
    y0[ip] = mesh->point[ip].c[1];
  }
 
  for (ip=1; ip<=mesh->np; ip++){
    dxk[ip] = x0[ip]-xref[ip];
    dyk[ip] = y0[ip]-yref[ip];
    dxkprev[ip] = dxk[ip];
    dykprev[ip] = dyk[ip];
    if(param->model==2){
      dxk_aux[ip] = 0;
      dyk_aux[ip] = 0;
    }
  }

  for (ip=1; ip<=mesh->np; ip++) {
    xprev[ip] = mesh->point[ip].c[0];
    yprev[ip] = mesh->point[ip].c[1];
  }

  /*computation of area and normal on ref mesh*/
  for (iel=1; iel<=mesh->nt; iel++) {
    area0[iel] = FMG_computeAreaRef(mesh,iel,xref,yref);
    for (iloc=0;iloc<3;iloc++){
      FMG_computeNormalVector2dRef(normal, mesh, iel, iloc,xref,yref);
      normal0[6*iel+2*iloc] = normal[0]; 
      normal0[6*iel+2*iloc+1] = normal[1];   
    }
  }

  if (param->imprim) {
    tmp = sol->size;
    sol->size = 1;
    sprintf(fname, "dom.%d.mesh", param->fileID + 0);
    if ( !MMG2D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);
    sprintf(fname, "dom.%d.sol", param->fileID + 0);
    if ( !MMG2D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
    sol->size = tmp;
  }

 


//  //******************//
//  //** Info memoire **//
//  //******************//
//
//  if (param->verbose >= 4) {
//    if (verbose) printf("\n* MAXNEIG = %d",maxNeig);
//    if (verbose) printf("\n* Useful Mem with sparse structure = %d/%d = %lf in K; %d/%d = %lf in neigSparse",
//                         usefulMem,(3*param->model+1)*maxNeig*(mesh->np+1),
//                         (double)usefulMem/(3*param->model+1)/maxNeig/(mesh->np+1),
//                         usefulMem+mesh->np,((3*param->model+1)*maxNeig+1)*(mesh->np+1),
//                         (double)(usefulMem+mesh->np)/((3*param->model+1)*maxNeig+1)/(mesh->np+1));
//    if (verbose) printf("\n* Useful Mem with original structure = %d/%lf = %lf in K",
//                         usefulMem,pow((param->model+1)*(mesh->np+1),2),
//                         (double)usefulMem/pow((param->model+1)*(mesh->np+1),2));
//  } 





/////////// Block de test : DIFFUSION HYPERBOLIQUE
//  param->ijacobi_max = 0;
//  FMG_diffusionHyperbolique(mesh, sol, param, OmegaX, normal0,
//                               Grd, Hes, NormGrd, NormHes, NSol, x0, y0, sol0);
///////////

////////// Block de test : OPTIMISATION sur un element (voir resultat sur dom.9999)
//  param->ijacobi_max=0;
//  FMG_optlen(mesh,sol,16232);
//////////

//  if (param->desSize)
//    FMG_computeDesiredSize(mesh, OmegaX, LS, Grd, param, 0);


/////  if (param->ref)
/////    FMG_readRefMesh2d(mesh, param->refName, xref, yref, normal0, area0);
/////  else
/////    for (ip=1; ip<=mesh->np; ip++) {
/////      xref[ip] = mesh->point[ip].c[0];
/////      yref[ip] = mesh->point[ip].c[1];
/////    }

/* L'initialisation a déja été faite plus haut
  for (ip=1;ip<=mesh->np;ip++) {
    for (isol=1; isol<= sol->size; isol++)
      sol0[(isol-1)*(mesh->np+1) + ip] = sol->m[(isol-1)*(mesh->np+1) + ip];
    x0[ip] = mesh->point[ip].c[0];
    y0[ip] = mesh->point[ip].c[1];
  }
*/

  if (param->debug)
    resFile = fopen("residu","w");
  optimized = 0;
  flag = 0;
  verbose = param->verbose;
  //****************//
  //** ITERATIONS **//
  //****************//
  for (ijacobi = 1; ijacobi <= param->ijacobi_max; ijacobi++){

/*compute the minimum and maximum edge length*/
if (ijacobi == 1) {
  edge_size_min = 1000.;
  edge_size_max = 0.;
  // Loop on elements
  for(iel=1;iel<=mesh->nt;iel++){
    ptr = &mesh->tria[iel];
    // Assembly total area and vertex contribution
    for(iloc=0;iloc<3;iloc++){
      jloc = iloc + 1;
      if (jloc == 3) jloc = 0;

      /* compute edge size */
      ax = yref[ptr->v[iloc]] - yref[ptr->v[jloc]];
      ay = xref[ptr->v[iloc]] - xref[ptr->v[jloc]];

      edge_size = sqrt(ax*ax+ay*ay);
      if (edge_size < edge_size_min) 
        edge_size_min = edge_size;
      if (edge_size > edge_size_max) 
        edge_size_max = edge_size;
    }
  }
}
   
    while(1) { // pour refaire l'iteration si optimized

 
      if (param->verbose > 1) {
        if (!optimized) printf("\n\n******** ITERATION %d *********\n",ijacobi);
        else            printf("\n\n******** ITERATION %d BIS *********\n",ijacobi);
      }


      dxmax = 0.;
      dymax = 0.;
      ixmax = 0;
      iymax = 0;
 
      for (iel=1; iel<=mesh->nt; iel++)
        mesh->tria[iel].qual = FMG_computeArea(mesh,iel);

      /*compute grad and hess on physical mesh (finite elements)*/
      //FMG_computeGrd2d(Grd,mesh,sol);
      //FMG_computeHes2d(Hes,mesh,sol,Grd);

      /*compute grad and hess on physical mesh (least square approximation)*/
      for (isol=1; isol<=sol->size; isol++)
        for (ip=1; ip<=mesh->np; ip++) {
          FMG_gradLS_2d(mesh, sol, ip, isol, Grd);
        }
      for (isol=1; isol<=sol->size; isol++)
        for (ip=1; ip<=mesh->np; ip++){
          FMG_hessLS_2d(mesh, sol, ip, isol, Grd, Hes);
        }   

      if (param->verbose >= 3) printf("\n *** Fin compute Grd et Hes");

      FMG_computeNorm(Grd,NormGrd,mesh->np,2,sol->size);  // OK
      FMG_computeTensorNorm(Hes,NormHes,mesh->np,2,sol->size); // OK

      for (isol=0; isol<sol->size; isol++)
        for (ip=1; ip<=mesh->np; ip++)
          NSol[isol*(mesh->np+1) + ip] = sol->m[isol*(mesh->np+1) + ip];
          
      if (!param->desSize) {
        if(param->normalize) {
          FMG_normalizeVec(NormGrd,mesh->np,param->gammaG[0],sol->size);
          FMG_normalizeVec(NormHes,mesh->np,param->gammaH[0],sol->size);
          FMG_normalizeVec(NSol,mesh->np,param->gammaS[0],sol->size);
        }
        if (param->verbose >= 3) printf("\n *** Fin normalisation");
      }


      if (param->desSize) {
        if (param->levelSet) {
          if (!param->interpolate) {
            if(!param->imprim) {
              fprintf(stdout,"TURN ON THE IMPRIM PARAMETER\n");
              exit(0);
            }
            FMG_readLS(mesh, LS, ijacobi-1);
          }
          else {
            /*the level function is always the first solution*/
            for (ip=1; ip<=mesh->np;ip++)
              LS[ip] = sol->m[ip];
          }
        }
        FMG_computeDesiredSize2d(mesh, sol, param, OmegaX, Hes, LS, edge_size_min, edge_size_max);
      }


      // Calcul Omega 
      cnt1 = 0;
      cnt2 = 0;
  
      if (!param->desSize && param->model==2){ // Adaptation on physical solution, mixed (2) model
        // Use OmegaY for the Laplacian (0) part.
        for (isol=0; isol<sol->size; isol++) {
          for (ip=1; ip<=mesh->np; ip++) {
            OmegaY[ip] = 1.;
            OmegaY[ip] += param->tau[0]*pow(NSol[isol*(mesh->np+1) + ip],2) +
                          param->alpha[0]*pow(NormGrd[isol*(mesh->np+1) + ip],2) +
                          param->beta[0]*pow(NormHes[isol*(mesh->np+1) + ip],2);
            OmegaY[ip] = sqrt(OmegaY[ip]); // OK
          }
        }
        // Compute new normalization for the elastic (1) part.
        FMG_computeNorm(Grd,NormGrd,mesh->np,2,sol->size);  // OK
        FMG_computeTensorNorm(Hes,NormHes,mesh->np,2,sol->size); // OK
        for (isol=0; isol<sol->size; isol++)
          for (ip=1; ip<=mesh->np; ip++)
            NSol[isol*(mesh->np+1) + ip] = sol->m[isol*(mesh->np+1) + ip];
        if(param->normalize) {
          FMG_normalizeVec(NormGrd,mesh->np,param->gammaG[1],sol->size);
          FMG_normalizeVec(NormHes,mesh->np,param->gammaH[1],sol->size);
          FMG_normalizeVec(NSol,mesh->np,param->gammaS[1],sol->size);
        }
        // Use OmegaX for the elastic (1) part.
        for (isol=0; isol<sol->size; isol++) {
          for (ip=1; ip<=mesh->np; ip++) {
            OmegaX[ip] = 1.;
            OmegaX[ip] += param->tau[1]*pow(NSol[isol*(mesh->np+1) + ip],2) +
                          param->alpha[1]*pow(NormGrd[isol*(mesh->np+1) + ip],2) +
                          param->beta[1]*pow(NormHes[isol*(mesh->np+1) + ip],2);
            OmegaX[ip] = sqrt(OmegaX[ip]); // OK
          }
        }
        if (param->aniso)
          printf("ERROR: You can't use anisotropy with the mixed model\n");
      }
      else if (!param->desSize && param->model!=2) { // Adaptation on physical solution, laplacian (0) or elastic (1) model
        for (ip=1; ip<=mesh->np; ip++) {
          OmegaX[ip] = 1.;
#warning change here to take into account more than one adaptation solution
          if (!param->sumAdapt) {
            printf("ERROR : you can only adapt with one variable\n");
            exit(0);
            if (param->alpha[0]*NormGrd[ip] > param->alpha[1]*NormGrd[mesh->np+1 + ip]) {
              NormGrd[mesh->np + 1 +ip] = 0.;
              cnt1++;
            }
            else
              NormGrd[ip] = 0;
            if (param->beta[0]*NormHes[ip] > param->beta[1]*NormHes[mesh->np+1 + ip]) {
              NormHes[mesh->np + 1 +ip] = 0.;
              cnt2 ++;
            }
            else
              NormHes[ip] = 0;
          }
  
          for (isol=0; isol<sol->size; isol++) {
            if(param->desSize && !param->sumAdapt && !isol) continue; //the first sol is ls function
            if(!param->soladapt[isol]) continue;
            OmegaX[ip] += param->tau[isol]*pow(NSol[isol*(mesh->np+1) + ip],2) +
            param->alpha[isol]*pow(NormGrd[isol*(mesh->np+1) + ip],2) +
            param->beta[isol]*pow(NormHes[isol*(mesh->np+1) + ip],2);
          }
          OmegaX[ip] = sqrt(OmegaX[ip]); // OK
          //printf("\n%d %lf",ip,OmegaX[ip]);
          if (param->aniso)
            OmegaY[ip] = OmegaX[ip]; 
        }
      }
      else { /*desSize==1*/
        if (ijacobi <= (int)(10*param->ijacobi_max)) {// test adapt puis desadapt
          for (ip=1; ip<=mesh->np; ip++) {
            OmegaX[ip] = 1./OmegaX[ip]; 
            if(param->model==2)
              OmegaY[ip] = OmegaX[ip];  
            if (param->aniso)
              OmegaY[ip] = OmegaX[ip];
          }
        } else {
          for (ip=1; ip<=mesh->np; ip++) {
            OmegaX[ip] = 1.;
            if (param->aniso)
              OmegaY[ip] = 1.;
          }
        }
        if(!param->sumAdapt) { /*we have to adapt to physical solution*/
          for (isol=0; isol<sol->size; isol++) {
            if(!isol) continue; //the first sol is ls function
            if(!param->soladapt[isol]) continue;
            for (ip=1; ip<=mesh->np; ip++) {
              OmegaX[ip] += sqrt(1+ param->tau[isol]*pow(NSol[isol*(mesh->np+1) + ip],2) +
                param->alpha[isol]*pow(NormGrd[isol*(mesh->np+1) + ip],2) +
                param->beta[isol]*pow(NormHes[isol*(mesh->np+1) + ip],2));
            }
            if (param->aniso)
              OmegaY[ip] = OmegaX[ip]; 
          }
        } 

      }
      if (param->revert)
        for (ip=1; ip<=mesh->np; ip++) {
          OmegaX[ip] = 1.;//OmegaX[ip];
          if (param->aniso)
            OmegaY[ip] = 1.;//OmegaY[ip];
        }
        
      if (param->verbose >= 3) printf("\n *** Fin calcul Omega : %lf %lf",(double)cnt1/mesh->np,(double)cnt2/mesh->np);

      // Calcul Omega cas elasticite
      if (param->model) {
        a = .500000;
        for (ip=1; ip<=mesh->np; ip++) {
          //      mu[4*ip] =  sqrt(1. + param->tau*pow(NSol[ip],2) + param->alpha*pow(NormGrd[ip],2) + param->beta*pow(NormHes[ip],2));
          mu[4*ip] = 2.0*E/(2.*(1+nu));//a*OmegaX[ip];
          mu[4*ip+1] = 0.;
          mu[4*ip+3] = mu[4*ip];
          mu[4*ip+2] = 0.;
          lambda[ip] = (E*nu)/((1+nu)*(1-2*nu));//(OmegaX[ip] - mu[4*ip])/2.;
          //      lambda[ip] = a*mu[4*ip];
        }
        if (param->verbose >= 3) printf("\n *** Fin calcul mu et lambda");
      }


      // With the mixed model, solve laplacian first
      if(param->model==2){
        // Construction et resolution du systeme
        FMG_computeLinSystSparse2d(KsparseX_aux, belast, mesh, area0, normal0, OmegaY, mu, lambda, neigSparse, data->maxNeig, param->model-2,sol,ijacobi);
        if (param->aniso)
          FMG_computeLinSystSparse2d(KsparseY_aux, belast, mesh, area0, normal0, OmegaY, mu, lambda, neigSparse, data->maxNeig, param->model-2,sol,ijacobi);
        if (param->verbose >= 3) printf("\n *** Fin calcul matrice");
        FMG_iterJacobiSparse2d(mesh, dxk_aux, dyk_aux, KsparseX_aux, KsparseY_aux, belast, neigSparse, data->maxNeig, param->model-2, param->aniso);
       
        /* for slip bdry condition, normal update*/
        // FMG_normalUpdate(mesh,dxk_aux,dyk_aux,xref,yref);

        iinner_max = param->iinner_max;
      }
      else
        iinner_max = 1;
 
      // Construction et resolution du systeme
      for(iinner=1;iinner<=iinner_max;iinner++){
        FMG_computeLinSystSparse2d(KsparseX, belast, mesh, area0, normal0, OmegaX, mu, lambda, neigSparse, data->maxNeig, param->model,sol,ijacobi);
        if (param->aniso)
          FMG_computeLinSystSparse2d(KsparseY, belast, mesh, area0, normal0, OmegaY, mu, lambda, neigSparse, data->maxNeig, param->model,sol,ijacobi);
        if (param->verbose >= 3) printf("\n *** Fin calcul matrice");
        FMG_iterJacobiSparse2d(mesh, dxk, dyk, KsparseX, KsparseY, belast, neigSparse, data->maxNeig, param->model, param->aniso);

        /* for slip bdry condition, normal update*/
        //FMG_normalUpdate(mesh,dxk,dyk,xref,yref);
      }
         
      if (param->verbose >= 3) printf("\n *** Fin calcul deplacements\n");

      // Blend laplacian and elastic model
      if(param->model==2){
        FMG_blendModelsMetric2d(mesh, sol, param, weightModel, Hes, LS, edge_size_min, edge_size_max);
        // Smooth
        for(ip=1; ip<=2; ip++)
          FMG_smoothFunction2d(weightModel,weightModel,mesh,area0);
        for(ip=1; ip<=mesh->np; ip++){
          dxk[ip] = (1.0-weightModel[ip])*dxk_aux[ip]+weightModel[ip]*dxk[ip];
          dyk[ip] = (1.0-weightModel[ip])*dyk_aux[ip]+weightModel[ip]*dyk[ip];
        }
        /*FMG_blendModels2d(Omega_weight,OmegaY,mesh,area0,weightModel);*/
        /*for(ip=1; ip<=mesh->np; ip++){
          dxk[ip] = (1.0-weightModel[ip])*dxk[ip]+weightModel[ip]*dxk_aux[ip];
          dyk[ip] = (1.0-weightModel[ip])*dyk[ip]+weightModel[ip]*dyk_aux[ip];
        }*/
      }
#warning the normal interpolation and point projection is done only at the end, check the precision
      /* for slip bdry condition, normal update*/
      FMG_normalUpdate(mesh,dxk,dyk,xref,yref);
      
      dxmax = 0.;
      dymax = 0.;
      for (ip=1; ip<=mesh->np; ip++) {
        if (fabs(dxk[ip])>dxmax){
          dxmax = fabs(dxk[ip]);
          ixmax = ip;
        }
        if (fabs(dyk[ip])>dymax){
          dymax = fabs(dyk[ip]);
          iymax = ip;
        }
      }


      for (ip=1; ip<=mesh->np; ip++) {
        xprev[ip] = mesh->point[ip].c[0];
        yprev[ip] = mesh->point[ip].c[1];
      }
      /* { */
      /*  FILE* inm; */
      /*  inm = fopen("toto.sol","wb"); */
      /*  fprintf(inm,"MeshVersionFormatted 2 \n Dimension 2\n SolAtVertices \n %d \n 1 2\n",mesh->np); */
      /*  for (ip=1; ip<=mesh->np; ip++) { */
      /*    fprintf(inm,"%e %e\n",dxk[ip],dyk[ip]); */
      /*  } */
      /*  fclose(inm); */

      /* } */
      
      /*try to optimize the mesh in order to avoid relaxation*/

      if (param->maxOptim && !optimized && ijacobi < 3 ) {
        cnt         = FMG_optim2d(mesh,sol,x0,y0,sol0,dxk,dyk,xprev,yprev,ijacobi);
        if (param->verbose >=3) printf(" %d elements optimized to avoid relaxation\n",cnt);
        if(cnt) {
          ijacobi--;
          for (ip=1; ip<=mesh->np; ip++) {
            dxk[ip] = dxkprev[ip];
            dyk[ip] = dykprev[ip];
          }
          optimized = 1;
          break; //exit of the while(1) in order to redo this iteration
        }
      }
      relax_param =  FMG_chkArea2d(mesh, xref, yref, dxk, dyk, xprev, yprev);
//      // Uncomment here for local relaxation
//      MMG5_SAFE_CALLOC(relax_paramArr,mesh->np+1,double,return 0);
//      FMG_chkArea2d_arr(mesh, x0, y0, dxk, dyk, xprev, yprev, relax_paramArr);

      if (param->verbose >= 3) printf("\n *** Fin calcul relax: relax_param = %lf", relax_param);

      if (relax_param > eps) {
        for (ip=1; ip<=mesh->np; ip++) {
          ppt = &mesh->point[ip];
          ppt->c[0] = xprev[ip] + relax_param*(xref[ip] + dxk[ip] - xprev[ip]);
          ppt->c[1] = yprev[ip] + relax_param*(yref[ip] + dyk[ip] - yprev[ip]);
        }
        /* /\*update the normals for slip bdry condition*\/ */
        /* for(ip=1 ; ip<=mesh->nt ; ip++) { */
        /*   pt = &mesh->tria[ip]; */
        /*   if ( !MG_EOK(pt) )  continue; */
        
        /*   adja = &mesh->adja[3*(ip-1)+1]; */
        /*   for (iedg=0; iedg<3; iedg++) { */
        /*     if(adja[iedg]) continue; */
        /*     ref = pt->edg[iedg]; */
        /*     pp1 = &mesh->point[pt->v[MMG2D_iare[iedg][0]]]; */
        /*     pp2 = &mesh->point[pt->v[MMG2D_iare[iedg][1]]]; */
        /*     if(pp1->tag & MG_BDY && !(pp1->tag & MG_CRN) && !(pp1->tag & M_DIRICHLET)) { */
        /*       MMG2D_bezierCurv(mesh,ip,iedg,double s,double *o,double *no) { */
        
        /*   } */
        /* } */
      }
      else {
        for (ip=1; ip<=mesh->np; ip++) {
          mesh->point[ip].c[0] = xprev[ip];
          mesh->point[ip].c[1] = yprev[ip];
        }
        flag = 1;
      }

//// Comment the previous "IF" and uncomment here for local relaxation
//      for (ip=1; ip<=mesh->np; ip++) {
//        if (relax_paramArr[ip] > eps){
//          mesh->point[ip].c[0] = xprev[ip] + relax_paramArr[ip]*(xref[ip] + dxk[ip] - xprev[ip]);
//          mesh->point[ip].c[1] = yprev[ip] + relax_paramArr[ip]*(yref[ip] + dyk[ip] - yprev[ip]);
//        }
//        else {
//          mesh->point[ip].c[0] = xprev[ip];
//          mesh->point[ip].c[1] = yprev[ip];
//          flag = 1;
//        }
//      }
 
      dxavg = 0.;
      dyavg = 0.;
      resid[0] = 0;
      resid[1] = 0;
      for (ip=1;ip<=mesh->np;ip++) {
        dxavg += fabs(dxk[ip]);
        dyavg += fabs(dyk[ip]);
        if(resid[0] < fabs(dxk[ip]-dxkprev[ip]))
           resid[0] = fabs(dxk[ip]-dxkprev[ip]);
        if(resid[1] < fabs(dyk[ip]-dykprev[ip]))
           resid[1] = fabs(dyk[ip]-dykprev[ip]);
      }

      for (ip=1; ip<=mesh->np; ip++) {
        dxkprev[ip] = dxk[ip];
        dykprev[ip] = dyk[ip];
      }


      // FMG_computeJacobiResidual2d(mesh, KsparseX, KsparseY, resid, neigSparse, maxNeig, param->aniso);

      if (param->verbose >= 2) {
        printf("\n *** Fin actualisation des points iteration %d",ijacobi);
        printf("\n              * relax = %lf",relax_param);
        printf("\n              * MAX dx= %e , point %d",relax_param*dxmax,ixmax);
        printf("\n              * MAX dy= %e , point %d",relax_param*dymax,iymax);
        printf("\n              * AVG dx= (%e,%e)",dxavg/mesh->np,dyavg/mesh->np);
        printf("\n              * Res = (%e,%e)\n",resid[0],resid[1]);
      }
      else if (param->verbose>0) // minimal
        fprintf(stdout,"\n *** Iteration %d/%d : relax = %lf, max deplc = (%e,%e)\n",ijacobi,param->ijacobi_max,relax_param,relax_param*dxmax,relax_param*dymax);
      if (param->debug)
        fprintf(resFile,"%d %lf %lf\n",ijacobi,resid[0],resid[1]);


      // Solution sur le nouveau maillage
      if (!param->solCase) {
        FMG_interp2d(mesh, sol, x0, y0, sol0);   // interpoler solution   
        if (param->verbose >= 3) printf("\n *** Fin interpolation\n\n");
      }
      else {
        FMG_analytic(mesh,sol,param);
        if (param->verbose >= 3) printf("\n *** Fin calcul analytique sur nouveau maillage");
      }

      // Save new mesh
      if (param->imprim && !(ijacobi%param->imprim)) {
        tmp = sol->size;
        sol->size = 1;
        sprintf(fname, "dom.%d.mesh", ijacobi/param->imprim);
        if ( !MMG2D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);
        sprintf(fname, "dom.%d.sol", ijacobi/param->imprim);
        if ( !MMG2D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
        sol->size = tmp;
      }

      finaljacobi = ijacobi-1;
      if (flag && ijacobi<param->ijacobi_max) {
        ijacobi = param->ijacobi_max;
        if (param->verbose >= 2) printf("\n\n *** !!! Exécution arrêtée par relaxation complète!!");
      }

      break; // !optimized -> sortir du while
    }
  }

  if (ijacobi == param->ijacobi_max + 1 && !flag) finaljacobi = param->ijacobi_max;
  if (param->verbose >= 1) printf("\n***** Fin Jacobi après %d itérations\n",finaljacobi);


  /* Compute element volume */
  for (iel=1; iel<=mesh->nt; iel++)
    mesh->tria[iel].qual = FMG_computeArea(mesh,iel);

  // Compute solution H1 seminorm
  MMG5_SAFE_CALLOC(NormSolH1,sol->size,double,return);
//  FMG_computeH1Seminorm2d(NormSolH1,mesh,sol);
  NormSolH1[0] = sqrt(FMG_num_quad_2D(mesh,param,&data->listquad[FMG_QUAD_2D_P2],sol->m,FMG_QUAD_GRAD,FMG_QUAD_ERR));
  printf("H1 seminorm: %e\n",NormSolH1[0]);
//  FMG_computeL2Norm2d(NormSolH1,mesh,sol);
  NormSolH1[0] = sqrt(FMG_num_quad_2D(mesh,param,&data->listquad[FMG_QUAD_2D_P2],sol->m,FMG_QUAD_FUNC,FMG_QUAD_ERR));
  printf("L2 norm: %e\n",NormSolH1[0]);
  MMG5_SAFE_FREE(NormSolH1);


  if (param->debug) 
    fclose(resFile);

  // Save final mesh
  if (param->imprim) {
    tmp = sol->size;
    sol->size = 1;
    if (param->nsols == 1) {
      sprintf(fname, "dom.%d.mesh", 9999);
      if ( !MMG2D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);
      sprintf(fname, "dom.%d.sol", 9999);
      if ( !MMG2D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
    }
    else
      for (isol=1;isol<=param->nsols;isol++) {
        sprintf(fname, ".%d.mesh", 9999);
        strcpy(fname2,fileNames[isol-1]);
        strcat(fname2,fname);
        if ( !MMG2D_saveMesh(mesh,fname2) )  exit(EXIT_FAILURE);


        sprintf(fname, ".%d.sol", 9999);
        strcpy(fname2,fileNames[isol-1]);
        strcat(fname2,fname);
        if ( !MMG2D_saveSol(mesh,sol,fname2) )  exit(EXIT_FAILURE);
        for (ip=1; ip<=mesh->np; ip++)
          sol->m[ip] = sol->m[isol*(mesh->np+1) + ip];
      }
    sol->size = tmp;
  }
  if (param->verbose >= 1) {fprintf(stdout,"\n ***************************"); 
    fprintf(stdout,"\n ********* Begin projection *********");
    fprintf(stdout,"\n ***************************\n\n");}

  if(!param->solCase) FMG_ALE_projection(mesh,sol,x0,y0,sol0);
  if (param->verbose >= 1) {fprintf(stdout,"\n ***************************"); 
    fprintf(stdout,"\n ********* End projection *********");
    fprintf(stdout,"\n ***************************\n\n");}

  if (param->verbose >= 1) {fprintf(stdout,"\n ***************************"); 
    fprintf(stdout,"\n ********* End FMG *********");
    fprintf(stdout,"\n ***************************\n\n");}

  if (param->verbose==-10)
    fprintf(stdout,"\n******* End FMG :\n******* fileID: %d; nb iterations: %d\n\n", param->fileID, finaljacobi);

//  // Uncomment here for local relaxation
//  MMG5_SAFE_FREE(relax_paramArr);
}

void FMG_blendModelsMetric2d(MMG5_pMesh mesh, MMG5_pSol sol, FMG_pInfo param, double *sizes, double *Hes, double *LS, double edge_size_min, double edge_size_max){
  double sizeMax, sizeMin, xa = M_PI_4*0.8;
  int ip;

  FMG_computeDesiredSize2d(mesh, sol, param, sizes, Hes, LS, edge_size_min, edge_size_max);

  // Minimum and maximum
  sizeMax=sizes[1];
  sizeMin=sizes[1];
  for(ip=2; ip<=mesh->np; ip++){
    if(sizes[ip]>sizeMax)
      sizeMax=sizes[ip];
    else if(sizes[ip]<sizeMin)
      sizeMin=sizes[ip];
  }

//  // Take min(sizeMin,edge_size_min) and max(sizeMax,edge_size_max)
//  if(sizeMin<edge_size_min) edge_size_min = sizeMin;
//  if(sizeMax>edge_size_max) edge_size_max = sizeMax;
//  printf("edge_size_max edge_size_min %e %e\n",edge_size_max,edge_size_min);
//  printf("sizeMax sizeMin %e %e\n",sizeMax,sizeMin);
  
  // Rescale
  for(ip=1; ip<=mesh->np; ip++){
    sizes[ip] = (sizes[ip]-sizeMin)/(sizeMax-sizeMin); 
    //sizes[ip] = (sizes[ip]-edge_size_min)/(edge_size_max-edge_size_min);
    //sizes[ip] = pow((sizes[ip]-edge_size_min)/(edge_size_max-edge_size_min),0.2);
    //sizes[ip] = pow((sizes[ip]-sizeMin)/(sizeMax-sizeMin),0.4);
    //sizes[ip] = 0.5*(tan(2*xa*((sizes[ip]-edge_size_min)/(edge_size_max-edge_size_min)-0.5))+tan(xa)); // LAST TESTED ON NACA
    //sizes[ip] = 0.5*(tan(2*xa*((sizes[ip]-edge_size_min)/(edge_size_max-edge_size_min)-0.5))/tan(xa)+1.0);
  }

}


void FMG_blendModels2d(double *Omega,MMG5_pMesh mesh,double *area0,double *weight){
  double funcMax,funcMin;
  int ip;
  double func[mesh->np+1];

  // Initialize
  for(ip=1; ip<=mesh->np; ip++)
    func[ip]=Omega[ip];

  // Smooth
  for(ip=1; ip<=5; ip++)
    FMG_smoothFunction2d(func,func,mesh,area0);

  // Minimum and maximum
  funcMax=func[1];
  funcMin=func[1];
  for(ip=2; ip<=mesh->np; ip++){
    if(func[ip]>funcMax)
      funcMax=func[ip];
    else if(func[ip]<funcMin)
      funcMin=func[ip];
  }

  // Rescale
  for(ip=1; ip<=mesh->np; ip++)
    weight[ip] = (func[ip]-funcMin)/(funcMax-funcMin);
}
