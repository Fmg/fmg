/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/



/**
 * \file libfmg.c
 * \brief Private Fortran API functions for FMG library.
 * \version 0
 * \date 07 2015
 
 * Define the private Fortran API functions for FMG library
 * (incompatible functions with the main binary): adds function
 * definitions with upcase, underscore and double underscore to match
 * any fortran compiler.
 *
 */

#include "libfmg.h"

/**
 * \def FORTRAN_NAME(nu,nl,pl,pc)
 * \brief Adds function definitions.
 * \param nu function name in upper case.
 * \param nl function name in lower case.
 * \param pl type of arguments.
 * \param pc name of arguments.
 * \note Macro coming from Scotch library.
 *
 * Adds function definitions with upcase, underscore and double
 * underscore to match any fortran compiler.
 *
 */
#define FORTRAN_NAME(nu,nl,pl,pc)               \
  void nu pl;                                   \
  void nl pl                                    \
  { nu pc; }                                    \
  void nl##_ pl                                 \
  { nu pc; }                                    \
  void nl##__ pl                                \
  { nu pc; }                                    \
  void nu pl


/**
 * See \ref FMG_fmglib function in \ref libfmg.h file.
 */
FORTRAN_NAME(FMG_FMGLIB2D,fmg_fmglib2d,(MMG5_pMesh *mesh,MMG5_pSol *sol,FMG_pData *fmgdata, double *xref, double *yref, int* retval),(
                                        mesh,sol,fmgdata,xref,yref,retval)){
  *retval = FMG_fmglib2d(*mesh,*sol,*fmgdata,xref,yref);

  return;
}

/**
 * See \ref FMG_fmglib function in \ref libfmg.h file.
 */
FORTRAN_NAME(FMG_FMGLIB3D,fmg_fmglib3d,(MMG5_pMesh *mesh,MMG5_pSol *sol,FMG_pInfo *param, double *xref, double *yref, double *zref, int* retval),(
                                        mesh,sol,param,xref,yref,zref,retval)){
  *retval = FMG_fmglib3d(*mesh,*sol,*param,xref,yref,zref);

  return;
}

