/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"
#include "math.h"
#include "string.h"


/**
 * \param argc number of command line arguments.
 * \param argv command line arguments.
 * \return \ref FMG_SUCCESS if success.
 * \return \ref FMG_LOWFAILURE if failed but a conform mesh is saved.
 * \return \ref FMG_STRONGFAILURE if failed and we can't save the mesh.
 *
 * Main program for FMG executable: perform mesh movement.
 *
 */

int main(int argc,char *argv[]) {
  FMG_pData        fmgdata;
  FMG_pInfo        param;
  FMG_pDatastr     data;
  MMG5_pMesh      mmgMesh;
  MMG5_pSol       mmgSol,mmgSol2,mmgSol3;
  double          *xref, *yref, *zref;
  char            *pwd,*inname,*outname,*physname,*lsname;
  int             cas=0, i,is2d;


  fprintf(stdout,"\n  -- FMG, Release %s (%s) \n",FMG_VER,FMG_REL);
  fprintf(stdout,"     %s\n",FMG_CPY);
  fprintf(stdout,"     %s %s\n\n",__DATE__,__TIME__);

  mmgMesh = NULL;
  mmgSol  = NULL;
  fmgdata = NULL;

  is2d = 1;
  for (i=1; i<argc; i++) {
    //--- 2D/3D ---//
    if (!strcmp(argv[i],"-3d")) {
      is2d = 0;
    }
  }

  if (is2d) {
    MMG2D_Init_mesh(MMG5_ARG_start,
                 MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
                 MMG5_ARG_end);
  }
  else
    MMG3D_Init_mesh(MMG5_ARG_start,
                 MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
                 MMG5_ARG_end);

  /* sol allocation */
  MMG5_SAFE_CALLOC(mmgSol2,1,MMG5_Sol,return 0);
  MMG5_SAFE_CALLOC(mmgSol3,1,MMG5_Sol,return 0);

  // verbosity for mmglib
  if (is2d)
    MMG2D_Set_iparameter(mmgMesh,mmgSol,MMG2D_IPARAM_verbose,0);
  else
    MMG3D_Set_iparameter(mmgMesh,mmgSol,MMG3D_IPARAM_verbose,0);



  if ( !MMG5_Set_inputMeshName(mmgMesh,"dom") )
    exit(EXIT_FAILURE);
  if (mmgMesh->dim == 2) {
    if ( !MMG2D_loadMesh(mmgMesh,"dom") )  exit(EXIT_FAILURE);
    FMG_Init_fmg2d(&mmgMesh,&fmgdata);
  }
  else {
    if ( !MMG3D_loadMesh(mmgMesh,"dom") )  exit(EXIT_FAILURE);
    FMG_Init_fmg3d(&mmgMesh,&fmgdata);

  }

  param = (*fmgdata).info;
  data  = (*fmgdata).data;

  /////////////////////////////////
  // FMG PARAMETERS
  ////////////////////////////////
  param->nsols = 1;
  mmgSol->size = param->nsols;

  /*[0/1], Turn on/off adaptation based on desired sizes, default : 1*/
  param->desSize = 1;
  param->sumAdapt=1;
  /* param->imprim = 1;  /*save the mesh at each iter (=1) or not (=0 default)*/
  param->interpolate = 1; /*interpolate the sol (=1 default) or recompute the ls (=0)*/
  if(!param->interpolate) param->imprim = 1;
  param->maxOptim = 0; /*quality mesh improvement default = 0*/


  FMG_Read_commandlineparam(mmgMesh,param,argc,argv);



  if (param->levelSet)
    param->solCase = 0;

  /*read DEFAULT.fmg*/
  FMG_parsop(mmgMesh,fmgdata);

  //
  ///////////////////



  MMG5_SAFE_CALLOC(xref,mmgMesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(yref,mmgMesh->np+1,double,return 0);
  if (mmgMesh->dim == 3) 
    MMG5_SAFE_CALLOC(zref,mmgMesh->np+1,double,return 0);

  if (param->reference){
    if(mmgMesh->dim == 2)
      FMG_readRefMesh2d(mmgMesh, param->refName, xref, yref);
    else
      FMG_readRefMesh3d(mmgMesh, param->refName, xref, yref, zref);
  }
  else
    for (i=1; i<=mmgMesh->np; i++) {
      xref[i] = mmgMesh->point[i].c[0];
      yref[i] = mmgMesh->point[i].c[1];
      if (mmgMesh->dim == 3) 
        zref[i] = mmgMesh->point[i].c[2];
    }

//  ////////////////////////////////////////////////////////////////////////// CONFLICT FORTRAN C
//     for (i=1; i<=mmgMesh->np; i++) {
//      xref[i-1] = xref[i];
//      yref[i-1] = yref[i];
//      if (mmgMesh->dim == 3) 
//        zref[i-1] = zref[i];
//    }
//  //////////////////////////////////////////////////////////////////////////


  if (param->levelSet && param->physAdapt)
    param->nsols = 2;

  /*allocation for sol storage */
  if ( !FMG_Set_solSize(mmgMesh,mmgSol,fmgdata,mmgMesh->np,param->nsols) ) exit(EXIT_FAILURE);

  if (param->solCase > 0 && param->solCase != FMG_UDFUNCTION)
    FMG_analytic(mmgMesh,mmgSol,param);
  else {
    if (mmgMesh->dim == 2) {
      if (param->nsols>1) {
        if ( !MMG2D_loadSol(mmgMesh,mmgSol2,param->physName) ) exit(EXIT_FAILURE);
        if ( !MMG2D_loadSol(mmgMesh,mmgSol3,param->lsName) ) exit(EXIT_FAILURE);
      }
      else {
        //printf("***&&&*** %d %d",mmgSol2->size, mmgMesh->np);
        if(param->levelSet) {
          if ( !MMG2D_loadSol(mmgMesh,mmgSol2,param->lsName) ) exit(EXIT_FAILURE);
          if ( !MMG2D_loadSol(mmgMesh,mmgSol3,param->lsName) ) exit(EXIT_FAILURE);
        } else {
          if(!param->physAdapt) {
            printf("please put -phys or -ls\n");
            exit(0);
          }
          if ( !MMG2D_loadSol(mmgMesh,mmgSol2,param->physName) ) exit(EXIT_FAILURE);
          if ( !MMG2D_loadSol(mmgMesh,mmgSol3,param->physName) ) exit(EXIT_FAILURE);
        }
      }
    }
    else {
      if (param->nsols>1) {
        MMG5_SAFE_CALLOC(lsname,sizeof(param->lsName)+6,char,return 0);
        sprintf(lsname, "%s.sol", param->lsName);
        if ( !MMG3D_loadSol(mmgMesh,mmgSol2,lsname) ) exit(EXIT_FAILURE);
        MMG5_SAFE_CALLOC(physname,sizeof(param->physName)+6,char,return 0);
        sprintf(physname, "%s.sol", param->physName);
        if ( !MMG5_Set_inputSolName(mmgMesh,mmgSol,physname) ) exit(EXIT_FAILURE);
        if ( !MMG3D_loadSol(mmgMesh,mmgSol3,physname) ) exit(EXIT_FAILURE);
        MMG5_SAFE_FREE(lsname);
        MMG5_SAFE_FREE(physname);
      }
      else {
        mmgSol2->size = 1;
        mmgSol3->size = 1;
        mmgSol2->np = mmgMesh->np;
        mmgSol3->np = mmgMesh->np;
        //printf("***ùùù*** %d %d",mmgSol2->size, mmgMesh->np);
        if ( !MMG3D_loadSol(mmgMesh,mmgSol2,"dom.sol") ) exit(EXIT_FAILURE);
        if ( !MMG3D_loadSol(mmgMesh,mmgSol3,"dom.sol") ) exit(EXIT_FAILURE);
      }
    }
  }


  /* mmgSol mem alloc */
  if (param->solCase == 0) {
    mmgSol->size = 2;
    mmgSol->np   = mmgMesh->np;
    MMG5_SAFE_CALLOC(mmgSol->m,2*(mmgMesh->np+1),double,return 0);
    memcpy(&mmgSol->m[0],mmgSol2->m,(mmgMesh->np+1)*sizeof(double));
    memcpy(&mmgSol->m[mmgMesh->np+1],mmgSol3->m,(mmgMesh->np+1)*sizeof(double));
    fmgdata->info->soladapt[0]=1;
    fmgdata->info->soladapt[1]=1;

    MMG5_SAFE_FREE(mmgSol2);
    MMG5_SAFE_FREE(mmgSol3);
  }
  mmgSol->size = param->nsols;
  
  
  mmgSol->size = 1;
  fprintf(stdout,"\n  -- SAVE INITIAL DATA\n");
  if (mmgMesh->dim == 2) {
    if ( !MMG2D_saveMesh(mmgMesh,"init.mesh") )  exit(EXIT_FAILURE);
    if ( !MMG2D_saveSol(mmgMesh,mmgSol,"init.sol") )  exit(EXIT_FAILURE);
  }
  else {
    if ( !MMG3D_saveMesh(mmgMesh,"init.mesh") )  exit(EXIT_FAILURE);
    if ( !MMG3D_saveSol(mmgMesh,mmgSol,"init.sol") )  exit(EXIT_FAILURE);
    
  }
  
  mmgSol->size = param->nsols;
  
  if (mmgMesh->dim == 2) {
    printf("\n *** Accessing fmglib2d ... \n\n");
    printf("alpha %e beta %e\n",param->alpha[0],param->beta[0]); 
    FMG_Start_fmg2d(mmgMesh,mmgSol,fmgdata);
    if ( FMG_fmglib2d(mmgMesh,mmgSol,fmgdata, xref, yref) ) exit(EXIT_FAILURE);
  }
  else {
    printf("\n*** accessing fmglib3d");
    FMG_Start_fmg3d(mmgMesh,mmgSol,fmgdata);
    if ( FMG_fmglib3d(mmgMesh,mmgSol,fmgdata,xref,yref,zref) ) exit(EXIT_FAILURE);
  }
  mmgSol->size = 1;
  
  
  if (mmgMesh->dim == 2) {
    if ( !MMG2D_saveMesh(mmgMesh,"final.mesh") )  exit(EXIT_FAILURE);
    if ( !MMG2D_saveSol(mmgMesh,mmgSol,"final.sol") )  exit(EXIT_FAILURE);
  }
  else {
    if ( !MMG3D_saveMesh(mmgMesh,"final.mesh") )  exit(EXIT_FAILURE);
    if ( !MMG3D_saveSol(mmgMesh,mmgSol,"final.sol") )  exit(EXIT_FAILURE);
  }
  if ( !MMG5_saveMshMesh(mmgMesh,&mmgSol,"final.msh",0) )  exit(EXIT_FAILURE);


  if (mmgMesh->dim == 2) {
    FMG_End_fmg2d(mmgMesh,fmgdata);
    MMG2D_Free_all(MMG5_ARG_start,
                   MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
                   MMG5_ARG_end);
  } else {
    MMG5_SAFE_FREE(zref);
    FMG_End_fmg3d(mmgMesh,fmgdata);
    MMG3D_Free_all(MMG5_ARG_start,
                   MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
                   MMG5_ARG_end);
  }
  
  MMG5_SAFE_FREE(xref);
  MMG5_SAFE_FREE(yref);

  return(0);
}



