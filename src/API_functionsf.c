/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

/**
 * \file API_functionsf.c
 * \brief Fortran API functions for FMG library.
 * \author Cecile Dobrzynski (Inria / IMB, Université de Bordeaux)
 * \version 5
 * \date 07 2015
 * \copyright GNU Lesser General Public License.
 * \note Please, refer to the \ref fmg/libfmg.h file for functions
 * documentation.
 *
 * Define the Fortran API functions for FMG library: adds function
 * definitions with upcase, underscore and double underscore to match
 * any fortran compiler.
 *
 */
#include "fmg.h"
/**
 * See \ref FMG_Set_solSize function in libfmg.h file.
 */
FORTRAN_NAME(FMG_SET_SOLSIZE, fmg_set_solsize,
    (MMG5_pMesh *mesh, MMG5_pSol *sol,FMG_pData *fmgdata,
    int* np, int *nsol,int *retval),
     (mesh,sol,fmgdata,np,nsol,retval )) {
  *retval = FMG_Set_solSize(*mesh,*sol,*fmgdata,*np,*nsol);
  return;
}
/**
 * See \ref FMG_Set_scalarSol function in libfmg.h file.
 */
FORTRAN_NAME(FMG_SET_SCALARSOL,fmg_set_scalarsol,
    (MMG5_pSol *met,FMG_pData *fmgdata, 
    double *s, int *pos,int *isol, int *adapt,int *retval),
    (met,fmgdata,s,pos,isol,adapt,retval)) {
  *retval = FMG_Set_scalarSol(*met,*fmgdata,*s,*pos,*isol,*adapt);
  return;
}
/**
 * See \ref FMG_Get_scalarSol function in libfmg.h file.
 */
FORTRAN_NAME(FMG_GET_SCALARSOL,fmg_get_scalarsol,
    (MMG5_pSol *met, 
    double *s, int *pos,int *isol,int *retval),
    (met,s,pos,isol,retval)) {
  *retval = FMG_Get_scalarSol(*met,s,*pos,*isol);
  return;
}
/**
 * See \ref FMG_Init_fmg2d function in libfmg.h file.
 */
FORTRAN_NAME(FMG_INIT_FMG2D, fmg_init_fmg2d,(MMG5_pMesh *mesh, FMG_pData *fmgdata,int *retval
    ),(mesh,fmgdata,retval)) {

  *retval = FMG_Init_fmg2d(mesh,fmgdata);
  return;
}
/**
 * See \ref FMG_Start_fmg2d function in libfmg.h file.
 */
FORTRAN_NAME(FMG_START_FMG2D, fmg_start_fmg2d,
    (MMG5_pMesh *mesh, MMG5_pSol *sol, FMG_pData *fmgdata,int *retval
    ),(mesh,sol,fmgdata,retval )) {
  /* data allocation */
  //if ( *param )  free(*param);
  *retval = FMG_Start_fmg2d(*mesh,*sol,*fmgdata);
  return;
}
/**
 * See \ref FMG_End_fmg2d function in libfmg.h file.
 */
FORTRAN_NAME(FMG_END_FMG2D, fmg_end_fmg2d,(MMG5_pMesh *mesh, FMG_pData *fmgdata
  ),(mesh,fmgdata
  )) { 
  FMG_End_fmg2d(*mesh,*fmgdata);
  return;
}

/**
 * See \ref FMG_Free_all function in \ref libfmg.h file.
 */
FORTRAN_NAME(FMG_FREE_ALL,fmg_free_all,(MMG5_pMesh *mesh,MMG5_pSol *met,FMG_pInfo *param
  ),(mesh,met,param
                 )){

  FMG_Free_all(*mesh,*met,*param);

  return;
}


FORTRAN_NAME(FMG_SET_IPARAMETER,fmg_set_iparameter,(MMG5_pMesh *mesh,FMG_pData *data, int* iparam, int* val, int* retval
                                                    ),(mesh,data,iparam,val,retval
  )){
  *retval = FMG_Set_iparameter(*mesh,*data,*iparam,*val);

  return;
}

FORTRAN_NAME(FMG_SET_DPARAMETER,fmg_set_dparameter,(MMG5_pMesh *mesh,FMG_pData *data, int* dparam, double* val, int* retval
                                                    ),(mesh,data,dparam,val,retval
                 )){
  *retval = FMG_Set_dparameter(*mesh,*data,*dparam,*val);

  return;
}

//FORTRAN_NAME(FMG_READ_COMMANDLINEPARAM,fmg_read_commandlineparam,(MMG5_pMesh *mesh, FMG_pInfo *param, int* argc, char *argv[], int* retval
//                                                    ),(mesh,param,argc,argv,retval
//                 )){
//  FMG_Read_commandlineparam(*mesh, *param, *argc, *argv); 
//
//  return;
//}


