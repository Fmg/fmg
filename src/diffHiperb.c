/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/



#include "fmg.h"
#include "math.h"

// TEST DE LA RESOLUTION EN UTILISANT UN SYSTÈME HYPERBOLIQUE ÉQUIVALENT

int FMG_diffusionHyperbolique(MMG5_pMesh mesh, MMG5_pSol sol, FMG_pInfo param, double* omega, double* normal,
                               double *Grd, double *Hes, double *NormGrd, double *NormHes, double *NSol,
                               double *x0, double *y0, double *sol0) {

  double *SystSol, *Res, *medArea, *dsol, *hT, *tau, *avgOmegaEl, *dxk, *dyk, *xprev, *yprev;
  double dt,Tr,Lr,kT=1,eps=1e-6, xmax, ymax, minhT,  avgOmegaMesh, relax_param;
  int *list;
  int ip, iel, iloc, isol, ilist, lon,coord, tauOpt = 1, flag, cnt1, cnt2, verbose=1,pas,ixmax,iymax, imprim=100;
  MMG5_pTria ptr;
  char fname[50];

  if (verbose) printf("\n ###### SYSTÈME HYPERBOLIQUE ######\n");

  MMG5_SAFE_CALLOC(SystSol,6*(mesh->np+1),double,return 0);
  MMG5_SAFE_CALLOC(Res,6*(mesh->nt+1),double,return 0);
  MMG5_SAFE_CALLOC(dsol,6*(mesh->np+1),double,return 0);
  MMG5_SAFE_CALLOC(medArea,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(dxk,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(dyk,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(xprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(yprev,mesh->np+1,double,return 0);
  MMG5_SAFE_CALLOC(list,mesh->np+1,int,return 0);
  MMG5_SAFE_CALLOC(hT,mesh->nt+1,double,return 0);
  MMG5_SAFE_CALLOC(tau,mesh->nt+1,double,return 0);
  MMG5_SAFE_CALLOC(avgOmegaEl,mesh->nt+1,double,return 0);

  if (verbose) printf("\n ### Fin allocation");


  for (ip=1; ip<=mesh->np;ip++) {
    iel = mesh->point[ip].s;
    for (iloc=0; iloc<3; iloc++)
      if (mesh->tria[iel].v[iloc] == ip)
        break;
    lon = MMG2D_boulep(mesh,iel,iloc,list);
    medArea[ip] = 0.;
    for (ilist=1; ilist<=lon; ilist++) {
      iel = list[ilist]/3;
      ptr = &mesh->tria[iel];
      medArea[ip] += 1./3.*ptr->qual;      
    }
    
  }
  if (verbose) printf("\n ### Fin calcul aires médiane");



  if (verbose) printf("\n ### Début intégration en temps");
  flag = 0;
  pas=1;
  param->nsols=1;
  while(!flag) {


    if (verbose) printf("\n\n ######## Pas de temps %d",pas);

    for (iel=1; iel<=mesh->nt; iel++)
      mesh->tria[iel].qual = FMG_computeArea(mesh,iel);

    FMG_computeGrd2d(Grd,mesh,sol);
    FMG_computeHes2d(Hes,mesh,sol,Grd);
    if (verbose) printf("\n ## Fin compute Grd et Hes");

    FMG_computeNorm(Grd,NormGrd,mesh->np,2,sol->size);  // OK
    FMG_computeNorm(Hes,NormHes,mesh->np,4,sol->size); // OK
    if (verbose) printf("\n ## Fin compute NormGrd et NormHes");

    for (isol=0; isol<sol->size; isol++)
      for (ip=1; ip<=mesh->np; ip++)
        NSol[isol*mesh->np + ip] = sol->m[isol*mesh->np + ip];

    if(param->normalize) {
      FMG_normalizeVec(NormGrd,mesh->np+1,param->gammaG[0],sol->size);
      FMG_normalizeVec(NormHes,mesh->np+1,param->gammaH[0],sol->size);
      FMG_normalizeVec(NSol,mesh->np+1,param->gammaS[0],sol->size);
    }
    if (verbose) printf("\n ## Fin normalisation");

    cnt1 = 0;
    cnt2 = 0;
    for (ip=1; ip<=mesh->np; ip++) {
      omega[ip] = 1.;

      if (!param->sumAdapt) {
        if (param->alpha[0]*NormGrd[ip] > param->alpha[1]*NormGrd[mesh->np+1 + ip]) {
           NormGrd[mesh->np + 1 +ip] = 0.;
           cnt1++;
        }
        else
           NormGrd[ip] = 0;
        if (param->beta[0]*NormHes[ip] > param->beta[1]*NormHes[mesh->np+1 + ip]) {
           NormHes[mesh->np + 1 +ip] = 0.;
           cnt2 ++;
        }
        else
           NormHes[ip] = 0;
      }

      for (isol=0; isol<sol->size; isol++)
        omega[ip] += param->tau[isol]*pow(NSol[isol*(mesh->np+1) + ip],2) +
                       param->alpha[isol]*pow(NormGrd[isol*mesh->np + ip],2) +
                       param->beta[isol]*pow(NormHes[isol*mesh->np + ip],2);
        omega[ip] = 1./sqrt(omega[ip]); // OK
    }

    if (verbose) printf("\n ## Fin calcul Omega : %lf %lf",(double)cnt1/mesh->np,(double)cnt2/mesh->np);


    minhT = 1e12;
    avgOmegaMesh = 0.;
    for (iel=1;iel<=mesh->nt;iel++) {
      avgOmegaEl[iel] = 0.;
      hT[iel] = FMG_computeMeshSize(mesh,iel);
      if (hT[iel] < minhT)
        minhT = hT[iel];
      
      for (iloc=0;iloc<3;iloc++) {
        avgOmegaEl[iel] += omega[mesh->tria[iel].v[iloc]]/3.;
        avgOmegaMesh += omega[mesh->tria[iel].v[iloc]]/(double)mesh->np;
      }
    }

    if (tauOpt == 1) {
      Lr = minhT/sqrt(3);
      dt = minhT*minhT/4./avgOmegaMesh;
    }
    else {
      Lr = 1;
      dt = minhT*Lr/2./avgOmegaMesh;
    }       
    Tr = Lr*Lr/avgOmegaMesh;
    for (iel=1;iel<=mesh->nt;iel++) {
      if (tauOpt == 1)
        tau[iel] = 2*Tr;
      else
        tau[iel] = kT*hT[iel]/sqrt(avgOmegaEl[iel]/Tr);
    }
    if (verbose) printf("\n ### Fin calcul mesh size et pas de temps: hmin = %e, dt = %e",minhT,dt);


    if (pas==1)
      for (ip=1;ip<=mesh->np;ip++) {
        SystSol[6*ip+3*0] = mesh->point[ip].c[0];      // x0=\xi
        SystSol[6*ip+3*0+1] = omega[ip];               // p0 = \omega*d(x0)/d(\xi)
        SystSol[6*ip+3*0+2] = 0.;                      // q0 = \omega*d(x0)/d(\eta)
        SystSol[6*ip+3*1] = mesh->point[ip].c[1];      // x0=\eta
        SystSol[6*ip+3*1+1] = 0.;                      // p0 = \omega*d(y0)/d(\xi)
        SystSol[6*ip+3*1+2] = omega[ip];               // q0 = \omega*d(y0)/d(\eta)
      }


    FMG_computeResidual(mesh, SystSol, Res, normal, omega, Tr, tau);
    if (verbose) printf("\n ## Fin calcul residus");
    for (ip=1; ip<6*(mesh->np+1); ip++)
      dsol[ip] = 0.;

    for (ip=1; ip<=mesh->np;ip++) {
      iel = mesh->point[ip].s;
      for (iloc=0; iloc<3; iloc++)
        if (mesh->tria[iel].v[iloc] == ip)
          break;

      lon = MMG2D_boulep(mesh,iel,iloc,list);
      for (ilist=1; ilist<=lon; ilist++) {
        for (coord=0; coord<=1; coord++) {
          iel = list[ilist]/3;
          ptr = &mesh->tria[iel];
          for (iloc=0;iloc<=2;iloc++)
            if (ptr->v[iloc] == ip)
              break;
          dsol[6*ip+3*coord] += dt/medArea[ip]*(Res[6*iel+3*coord]/3 - tau[iel]/4./ptr->qual*(
                                      -normal[6*iel+2*iloc]*Res[6*iel+3*coord+1] -
                                      normal[6*iel+2*iloc+1]*Res[6*iel+3*coord+2] ));
          dsol[6*ip+3*coord+1] += dt/medArea[ip]*(+avgOmegaEl[iel]*tau[iel]/4./ptr->qual/Tr*Res[6*iel+3*coord]*normal[6*iel+2*iloc] + 
                                                  Res[6*iel+3*coord+1]/3.);
          dsol[6*ip+3*coord+2] += dt/medArea[ip]*(+avgOmegaEl[iel]*tau[iel]/4./ptr->qual/Tr*Res[6*iel+3*coord]*normal[6*iel+2*iloc+1] + 
                                                  Res[6*iel+3*coord+2]/3.);
        //    if (fabs(dsol[6*ip+3*coord]) > 0.1)
//              printf("\n%d %d %lf %lf %lf %lf %lf",ip,iel,medArea[ip],dsol[6*ip+3*coord],Res[6*ip+3*coord],Res[6*ip+3*coord+1],Res[6*ip+3*coord+2]);
        }
      }


 
      for (coord=0; coord<=1; coord++)
        for (isol=0; isol<3; isol++){
          SystSol[6*ip+3*coord+isol] += dsol[6*ip+3*coord+isol];
////       if (isol == 0)  // x,y
////         mesh->point[ip].c[coord] = SystSol[6*ip+3*coord];
        }
      dxk[ip] = mesh->point[ip].c[0] + dsol[6*ip] - x0[ip];
      dyk[ip] = mesh->point[ip].c[1] + dsol[6*ip+3] - y0[ip];
///////       dxk[ip] = dsol[6*ip] - x0[ip];
//////       dyk[ip] = dsol[6*ip+3] - y0[ip];


      if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_LEFT ||  mesh->point[ip].ref == M_RIGHT)))
        dxk[ip] = 0.;
      else if (((mesh->point[ip].tag & MG_BDY) && (mesh->point[ip].ref == M_BOTTOM ||  mesh->point[ip].ref == M_TOP)))
        dyk[ip] = 0.;
      else if (mesh->point[ip].tag & MG_CRN){
        dxk[ip]  = 0.;
        dyk[ip]  = 0.;
      }
    }

    for (ip=1; ip<=mesh->np;ip++) {
      xprev[ip] = mesh->point[ip].c[0];
      yprev[ip] = mesh->point[ip].c[1];
    }

    relax_param =  FMG_chkArea2d(mesh, x0, y0, dxk, dyk,xprev,yprev);
//    relax_param = 1.;
    printf("\n *** Fin calcul relax: relax_param = %lf", relax_param);

    if (relax_param > eps) {
      for (ip=1; ip<=mesh->np; ip++) {
         mesh->point[ip].c[0] = xprev[ip] + relax_param*(x0[ip] + dxk[ip] - xprev[ip]);
         mesh->point[ip].c[1] = yprev[ip] + relax_param*(y0[ip] + dyk[ip] - yprev[ip]);
      }
    }
    else {
      for (ip=1; ip<=mesh->np; ip++) {
        mesh->point[ip].c[0] = xprev[ip];
        mesh->point[ip].c[1] = yprev[ip];
      }
    }


    for (ip=1; ip<=mesh->np; ip++) {
      SystSol[6*ip] = mesh->point[ip].c[0];
      SystSol[6*ip+3] = mesh->point[ip].c[1];
    }

    if (relax_param<eps)
      break;





    if (verbose) printf("\n ## Fin actualisation de la solution");
  
    FMG_interp2d(mesh, sol, x0, y0, sol0);
    if (verbose) printf("\n ## Fin interpolation");
  
    if (verbose) printf("\n ## Fin actualisation de la solution");
    xmax = 0.;
    ymax = 0.;
    flag=1;
    for (ip=1; ip<=mesh->np; ip++) {
      if (fabs(dsol[6*ip]) > xmax ) {
        xmax = fabs(dsol[6*ip]);    
        ixmax = ip;
      }
      if (fabs(dsol[6*ip+3]) > ymax ) {
        ymax = fabs(dsol[6*ip+3]);    
        iymax = ip;
      }
    }
    if (verbose)printf ("\n ## xmax = %lf, Point %d",xmax,ixmax); 
    if (verbose)printf ("\n ## ymax = %lf, Point %d",ymax,iymax);
  
    if (xmax > eps || ymax > eps) {
      flag = 0; 
      pas++;
    }
  
    if (flag && verbose) printf("\n ## Convergence en %d pas de temps",pas);
  
    if (pas%imprim == 0) {
      sol->size = 1;
      sprintf(fname, "dom.%d.mesh", pas/imprim);
      if ( !MMG2D_saveMesh(mesh,fname) )  exit(EXIT_FAILURE);
      sprintf(fname, "dom.%d.sol", pas/imprim);
      if ( !MMG2D_saveSol(mesh,sol,fname) )  exit(EXIT_FAILURE);
      sol->size = param->nsols;
    }

  }

  for (ip=1; ip <= mesh->np; ip++)
    for (coord=0; coord<=1; coord++)
      mesh->point[ip].c[coord] = SystSol[6*ip+3*coord];
    
  MMG5_SAFE_FREE(SystSol);
  MMG5_SAFE_FREE(Res);
  MMG5_SAFE_FREE(dsol);
  MMG5_SAFE_FREE(medArea);
  MMG5_SAFE_FREE(list);
  MMG5_SAFE_FREE(hT);
  MMG5_SAFE_FREE(tau);
  MMG5_SAFE_FREE(avgOmegaEl);
  
}


void FMG_computeResidual(MMG5_pMesh mesh, double *sol, double *resid, double *normal, double* omega, double Tr, double* tau) {

  int iel, ip, iloc,coord,isol;
  double avgOmega;
  MMG5_pTria ptr;

  for (iel=1; iel<=mesh->nt; iel++) {
    for (coord=0; coord<=1; coord++)
      for (isol=0;isol<=2;isol++)
        resid[6*iel+3*coord+isol] = 0.;
    ptr = &mesh->tria[iel];
    avgOmega = 0.;

    for (iloc=0; iloc<3; iloc++)
      avgOmega += omega[mesh->tria[iel].v[iloc]];

    for (iloc=0; iloc<3; iloc++) {
      ip = ptr->v[iloc];
      for (coord=0; coord<=1; coord++) {
        resid[6*iel+3*coord] -=  normal[6*iel+2*iloc]/2.*sol[6*ip+3*coord+1]  +
                                 normal[6*iel+2*iloc+1]/2.*sol[6*ip+3*coord+2];
        resid[6*iel+3*coord+1] += -tau[iel]*normal[6*iel+2*iloc]/2./Tr*sol[6*ip+3*coord] +
                                  ptr->qual*sol[6*ip+3*coord+1]/3.;
        resid[6*iel+3*coord+2] += -tau[iel]*normal[6*iel+2*iloc+1]/2./Tr*sol[6*ip+3*coord] +
                                  ptr->qual*sol[6*ip+3*coord+2]/3.;
      }
    }
  }
}

double FMG_computeMeshSize(MMG5_pMesh mesh, int iel) {

  int iloc, inext;
  double h, vec[2];

  h = 0;

  for (iloc=0;iloc<=2;iloc++) {
    FMG_computeNormalVector2d(vec, mesh, iel, iloc);
    if (sqrt(vec[0]*vec[0] + vec[1]*vec[1]) > h)
      h = sqrt(vec[0]*vec[0] + vec[1]*vec[1]); 
  }

  h = 2*mesh->tria[iel].qual/h;

//  h = 1e9;
//  for (iel=1; iel<=mesh->nt; iel++) {
//    ptr = &mesh->tria[iel];
//    for (iloc=0;iloc<=2;iloc++) {
//      if (iloc == 2)
//        inext = 0;
//      else
//        inext = iloc+1;
//      ed = sqrt(pow(mesh->point[ptr->v[iloc]].c[0] - mesh->point[ptr->v[inext]].c[0] + 
//                    mesh->point[ptr->v[iloc]].c[1] - mesh->point[ptr->v[inext]].c[1],2));
//      if (ed<h)
//        h = ed;
//    }
//  }

  return 2.*mesh->tria[iel].qual/h;
}

