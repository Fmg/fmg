/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"

/**
 * \param mesh pointer on the mesh
 * \param sol pointer on the solution (defined on mesh)
 * \param LS LevelSet values in each point of mesh
 * 
 * Calculates a binary solution (-1 / 1) based in the LevelSet for defining the object
 */

void FMG_defineObject(MMG5_pMesh mesh, MMG5_pSol sol, double* LS) {

  int ip, jump;

  jump = (mesh->np+1)*(sol->size-1);
 
  for (ip=1;ip<=mesh->np;ip++) {
//    LS[ip] = sol->m[ip];
    if (sol->m[jump+ip] < 0)
      sol->m[jump+ip] = -1;
    else
      sol->m[jump+ip] = 1;
  }
}


/**
 * \param mesh pointer on the mesh
 * \param sol pointer on the solution (defined on mesh)
 * \param LS LevelSet values in each point of mesh
 * \delta layer's thickness around the object's surface
 * 
 * Smooth the object's binary function in a region of thickness 2*delta around the object's surface
 */

void FMG_smoothObject(MMG5_pMesh mesh, MMG5_pSol sol, double* LS, double delta) {
  
  int ip, jump;
  double max, eps=1e-12;

  jump = (mesh->np+1)*(sol->size-1);

  max = 0.;
  for (ip=1; ip<=mesh->np; ip++)
    if (fabs(LS[ip]) <= delta && fabs(LS[ip]) > max)
      max = fabs(LS[ip]);
  
  for (ip=1; ip<=mesh->np; ip++)
    if (fabs(LS[ip]) <= delta) {
//      sol->m[ip] = sol->m[ip]*fabs(LS[ip])/max;
      sol->m[jump+ip] = 1./1.107*atan(2*(LS[ip])/delta);
//      sol->m[ip] = 1./1.3734*atan(5*(LS[ip])/delta);
//      sol->m[ip] = 1./1.4711*atan(10*(LS[ip])/delta);

    }

}
