/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/


#include "fmg.h"
#include "time.h"

#warning remove this quality measure and change it for the new measure in mmg2d
/* from old MMG2D : compute tria quality iso */
double caltri_iso_in(MMG5_pMesh mesh,MMG5_pSol sol,MMG5_pTria pt) {
  double     cal,abx,aby,acx,acy,bcx,bcy;
  double    *a,*b,*c,h1,h2,h3,aire,peri,hm;

  cal = 1e+24;

  a  = mesh->point[pt->v[0]].c;
  b  = mesh->point[pt->v[1]].c;
  c  = mesh->point[pt->v[2]].c;

  abx = b[0] - a[0];
  aby = b[1] - a[1];
  acx = c[0] - a[0];
  acy = c[1] - a[1];
  bcx = c[0] - b[0];
  bcy = c[1] - b[1];

  /* orientation */
  aire = abx*acy - aby*acx;
  if ( aire <= 0 ) return(cal);

  /* edge lengths */
  h1 = abx*abx + aby*aby;
  h1 = sqrt(h1);
  h2 = acx*acx + acy*acy;
  h2 = sqrt(h2);
  h3 = bcx*bcx + bcy*bcy;
  h3 = sqrt(h3);

  peri = 0.5 * (h1 + h2 + h3);
  hm   = M_MAX(h1,M_MAX(h2,h3));
  cal  = hm * peri;
  if ( peri > 1e-10 ) {
    aire = aire * 0.5;//(peri-h1) * (peri-h2) * (peri-h3);
    if ( aire > 0.0 ) {
      cal = cal / aire;//sqrt(aire*peri);
    } else {
      cal = 1e+9;
    }
  } else {
    cal = 1e+9;
  }

  return(cal);
}


void FMG_analytic_grad_point(FMG_pInfo param,double x,double y,double z,double *res) {
  int icas;
  double step,ray=.5,delta, random;

  icas = param->solCase;
  delta = param->delta;

 switch(icas)
  {
    case 20190510:
      delta = pow(x,2.0)+pow(y,2.0)+pow(z,2.0)-pow(0.75,2.0);
      res[0] = exp(-40.0*pow(delta,2.0))*(-80.0*delta)*2*x;
      res[1] = exp(-40.0*pow(delta,2.0))*(-80.0*delta)*2*y;
      res[2] = exp(-40.0*pow(delta,2.0))*(-80.0*delta)*2*z;
      break;
    case 9000:
      res[0] = 0.0; res[1] = 0.0; res[2] = 0.0;
      break;
    case 9100:
      res[0] = 1.0; res[1] = 0.0; res[2] = 0.0;
      break;
    case 9010:
      res[0] = 0.0; res[1] = 1.0; res[2] = 0.0; 
      break;
    case 9001:
      res[0] = 0.0; res[1] = 0.0; res[2] = 1.0;
      break;
  }

}

double FMG_analytic_point(FMG_pInfo param,double x,double y,double z) {
  int icas;
  double step,ray=.5,delta, random;
  double res;

  icas = param->solCase;
  delta = param->delta;

 switch(icas)
  {
    case 1:
      res = exp(-100*pow(y-x*x+0.5,2));
      break;
    case 2:
      res = exp(-8*pow(4*x*x + 9*y*y-1,2));
      break;
    case 3:
      if (x>=y)
        res = 1;
      else
        res = 0;
      break;
    case 4:
      res = cos(M_PI*x) + sin(M_PI*y);
      break;
    case 5:
      while (x >= step/2.)
         x -= step;
      while (x <=-step/2.)
         x += step;
      while (y >= step/2.)
         y -= step;
      while (y <=-step/2.)
         y += step;
      res = 50*exp(-2500*(x*x+y*y));
      break;
    case 6:
      if (x*x+y*y<=ray*ray)
        res = 1;
      else
        res = -1;
      break;
    case 7:
      if (x*x+y*y<=ray*ray)
        if (x*x+y*y >= pow(ray-delta,2))
          res = 1 + (ray-delta-sqrt(x*x+y*y))/delta;
        else
          res = 1;
      else
        if (x*x+y*y <= pow(ray+delta,2))
           res = -1 + (ray+delta-sqrt(x*x+y*y))/delta;
        else
           res = -1;                
      break;
    case 8:
      if (x*x+y*y<=ray*ray)
        if (x*x+y*y >= pow(ray-delta,2))
          res = 1 + (ray-delta-sqrt(x*x+y*y))/delta; 
        else
          res = 1;
      else
        if (x*x+y*y <= pow(ray+delta,2))
           res = -1 + (ray+delta-sqrt(x*x+y*y))/delta;
        else
           res = -1-1*exp(x*x+y*y);                
      break;
    case 9:
      if (x*x+y*y<=ray*ray)
        res = 1 ; 
      else if (x*x+y*y<=pow(ray+delta/2,2))
        res = 0.5;
      else if (x*x+y*y<=pow(ray+delta,2))
        res = 0.;
      else if (x*x+y*y<=pow(ray+3*delta/2,2))
        res = -0.5;
      else
        res = -1.;                
      break;
    case 10:
      if (((sqrt(x*x+y*y)-ray)>=-delta && (sqrt(x*x+y*y)-ray)<0) ||
          ((sqrt(x*x+y*y)-ray)<=delta && (sqrt(x*x+y*y)-ray)>0)) {
        res = 4./3.1415*atan((sqrt(x*x+y*y)-ray)/delta);
        res = 1./1.4711*atan(10*(sqrt(x*x+y*y)-ray)/delta);
        res = 1./1.3734*atan(5*(sqrt(x*x+y*y)-ray)/delta);
        res = 1./1.107*atan(2*(sqrt(x*x+y*y)-ray)/delta);
      } else {
        if (x*x+y*y<=ray*ray)
          res = -1;
        else
          res = 1;   
      }      
      break;
    case 101:
      if (x*x+y*y+z*z<=ray*ray)
        res = 1;
      else
        res = -1;
      break;
    case 102:
      res = exp(-8*pow(4*x*x + 9*y*y + 4*z*z - 1,2));
      break;
    case 401:
      if(x>=0.0)
        res = 1.0;
      else
        res = 0.0;
      break;
    case 40400:
      res = exp(-400*pow(sqrt(x*x + y*y) - 0.5,2));
      break;
    case 40040:
      res = exp(-40*pow(sqrt(x*x + y*y) - 0.5,2));
      break;
    case 40004:
      res = exp(-4*pow(sqrt(x*x + y*y) - 0.5,2));
      break;
    case 40003:
      res = exp(-100*pow(y-x*x-z*z,2));
      break;
    case 20180424:
      res = exp(-400*pow(pow(x-0.5,2.0)+pow(y-0.5,2.0)+pow(z-0.5,2.0)-0.25*3,2));
      break;
    case 20190510:
      res = exp(-40*pow(pow(x,2.0)+pow(y,2.0)+pow(z,2.0)-pow(0.75,2.0),2));
      break;
    case 9000:
      res = 1.0;
      break;
    case 9100:
      res = x;
      break;
    case 9010:
      res = y;
      break;
    case 9001:
      res = z;
      break;
  }

 return res;
}

/**
 * \param mesh mesh 
 * \param sol solution
 * \param icas analytic function
 * 
 * Calculate an analytic solution on the mesh points
 */

void FMG_analytic(MMG5_pMesh mesh,MMG5_pSol sol,FMG_pInfo param) {
  int k,icas;
  double x,y,z;

  /** b) give info for the sol structure: sol applied on vertex entities,
      number of vertices=np, the sol is scalar*/
//  if ( !MMG5_Set_solSize(mesh,sol,MMG5_Vertex,mesh->np,MMG5_Scalar) )
//    exit(EXIT_FAILURE);

  for (k=1;k<=mesh->np;k++)
  {
    x = mesh->point[k].c[0];
    y = mesh->point[k].c[1];
    z = mesh->point[k].c[2];

    sol->m[k] = FMG_analytic_point(param,x,y,z);

  }
    
}


/**
 * \param mmgMesh pointer toward the mesh structure (physical position).
 * \param mmgSol pointer toward the sol structure.
 * \param fmgdata  pointer toward the FMG_data structure
 * \param xref pointer on a tabular of np+1 double (reference position), xref[1] is relative to the vertex 1
 * \param yref pointer on a tabular of np+1 double (reference position)
 * \return Return \ref FMG_SUCCESS if success.
 * \return Return \ref FMG_LOWFAILURE if failed but a conform mesh is saved.
 * \return Return \ref FMG_STRONGFAILURE if failed and we can't save the mesh.
 *
 * Main program for the library .
 *
 */
////int FMG_fmglib(int np, int nt,double* xy,int* elt,MMG5_pSol sol,FMG_pInfo param ) {
int FMG_fmglib2d(MMG5_pMesh mmgMesh ,MMG5_pSol mmgSol,FMG_pData fmgdata,
     double *xref, double *yref ) {
  MMG5_pPoint    ppa,ppb;
  FMG_pDatastr    data;
  FMG_pInfo       param;
  double          dd1;
  int             pa,pb,i,origOUT;
  int             k,ind,ialpha,ib,ref,ie;

  FILE  *f,*f2;

  data = fmgdata->data;
  param = fmgdata->info;

  // CORRECTION PARAMETRES : param->nsols = number of solution to adapt
  param->nsols = 0;
  for(k = 0 ; k< mmgSol->size ; k++)
    param->nsols += param->soladapt[k];
  //mmgSol->size = param->nsols;
  if (param->nsols == 1)
    param->sumAdapt = 1;

  /*put the value of alpha at the good place*/
  ialpha = 0;
  for(k = 0 ; k< mmgSol->size ; k++) {
    if(!param->soladapt[k]) continue;
    param->alpha[k] = param->alpha[ialpha];
    param->beta[k] = param->beta[ialpha];
    param->tau[k] = param->tau[ialpha++];
    if(ialpha > 2) {
      printf("MORE THAN 2 SOLUTION FOR ADAPTATION : this case is not implemented\n");
      printf("ialpha %d solsize %d isol %d soladapt %d\n",ialpha,mmgSol->size,k,param->soladapt[k]);
      printf("here %d %d %d %d\n",param->soladapt[0],param->soladapt[1],
         param->soladapt[2],param->soladapt[3]);
      exit(0);
      exit(0);
    }
  }

  /*put tag on vertex for bdry conditions*/
  FMG_definitiveRef2d(mmgMesh,param);
  
  FMG_movemsh2d(mmgMesh, mmgSol, data,param, xref, yref);

  /*recreate edges*/
  MMG2D_bdryEdge(mmgMesh);
  
  return(FMG_SUCCESS);
}


/**
 * \param mesh pointer toward the mesh structure.
 * \param met pointer toward the sol structure.
 * \return Return \ref FMG_SUCCESS if success.
 * \return Return \ref FMG_LOWFAILURE if failed but a conform mesh is saved.
 * \return Return \ref FMG_STRONGFAILURE if failed and we can't save the mesh.
 *
 * Main program for the library .
 *
 */
int FMG_fmglib3d(MMG5_pMesh mmgMesh ,MMG5_pSol mmgSol,FMG_pData fmgdata,
     double *xref, double *yref, double *zref ) {
  FMG_pDatastr    data;
  FMG_pInfo       param;
 
  int             pa,pb,i,origOUT;
  int             k,ind;

  FILE            *f,*f2;

  data = fmgdata->data;
  param = fmgdata->info;
  
  // CORRECTION PARAMETRES
  mmgSol->size = param->nsols;
  if (param->nsols == 1)
    param->sumAdapt = 1;

  FMG_definitiveRef3d(mmgMesh,param);
  
/*   if (!param->circDom) {  */
/*     FMG_createRef3d(mmgMesh); */
/* //    FMG_correctRef3d(mmgMesh); */
/*   } */


  // if (param->circDom)
  //  FMG_correctRefCirc3d(mmgMesh);

  MMG3D_bdryBuild(mmgMesh);

  if ( !MMG5_Set_outputMeshName(mmgMesh,"initlib.mesh") ) exit(EXIT_FAILURE);
  if ( !MMG3D_saveMesh(mmgMesh,"initlib.mesh"))   return( FMG_STRONGFAILURE);
 
  if ( !MMG5_Set_outputSolName(mmgMesh,mmgSol,"initlib.sol") ) exit(EXIT_FAILURE);
  if ( !MMG3D_saveSol(mmgMesh,mmgSol,"initlib.sol") )   return( FMG_STRONGFAILURE);


  FMG_movemsh3d(mmgMesh, mmgSol, data,param,xref,yref,zref);


  return(FMG_SUCCESS);
}


