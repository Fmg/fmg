/* =============================================================================
**  This file is part of the fmg software package for
**  mesh modification.
**  Copyright (c) Bx INP/Inria, 2016- .
**
**  fmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. 
**
** =============================================================================
*/

#include "fmg.h"




/**
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param filename name of file containg the reference mesh
 * \param x0 x coordinate of initial points
 * \param y0 y coordinate of initial points
 * \param z0 z coordinate of initial points
 * \param normal0 normal vectors in reference mesh
 * \param vol0 elements' volumes in reference mesh
 *
 * Read coordinates of points in reference mesh from an external file and computes its normal vectors and volumes of the elements
 */

int FMG_readRefMesh3d(MMG5_pMesh mesh, char *filename, double *x0, double *y0, double *z0) {

  int ip, iel, iloc, trash;
  double normal[3], *xprev, *yprev,*zprev;
  char trash2[100];
  FILE *inputFile;  

//  MMG5_SAFE_CALLOC(xprev,mesh->np+1,double,return 0);
//  MMG5_SAFE_CALLOC(yprev,mesh->np+1,double,return 0);
//  MMG5_SAFE_CALLOC(zprev,mesh->np+1,double,return 0);
//
//  for (ip=1; ip<=mesh->np; ip++) {
//    xprev[ip] = mesh->point[ip].c[0];
//    yprev[ip] = mesh->point[ip].c[1];
//    zprev[ip] = mesh->point[ip].c[2];
//  }

  if ( !(inputFile = fopen(filename,"r"))) {
    printf("\n REFERENCE MESH %s NOT FOUND!!!\n\n ABORTING FMG \n\n",filename);
    exit(EXIT_FAILURE);
  }

  trash = 0;
  for (ip=1; ip<=10; ip++) {
    fgets(trash2,100,inputFile);
//    printf("\n%s",trash2);
    if (atoi(trash2) == mesh->np) {
      printf("\n*** Reading reference mesh : OK\n");
      break;
    }
  }

  for (ip=1; ip<=mesh->np;ip++) {
    if(fscanf(inputFile,"%lf %lf %lf %d",&x0[ip],&y0[ip],&z0[ip],&trash) != 4) {
      printf("\n !!! Error reading points of regular mesh!!");
      exit(EXIT_FAILURE);
    }
//    printf("\n %d %lf %lf %d",ip,x0[ip],y0[ip],trash);
  }

//  for (ip=1; ip<=mesh->np; ip++) {
//    mesh->point[ip].c[0] = x0[ip];
//    mesh->point[ip].c[1] = y0[ip];
//    mesh->point[ip].c[2] = z0[ip];
//  }
//
//  for (iel=1; iel<=mesh->nt; iel++) {
//    vol0[iel] = FMG_computeVolume(mesh,iel);
//    for (iloc=0;iloc<4;iloc++){
//      FMG_computeNormalVector3d(normal, mesh, iel, iloc);
//      normal0[12*iel+3*iloc] = normal[0]; 
//      normal0[12*iel+3*iloc+1] = normal[1];   
//      normal0[12*iel+3*iloc+2] = normal[2];   
//    }
//  }
// 
//  for (ip=1; ip<=mesh->np; ip++) {
//    mesh->point[ip].c[0] = xprev[ip];
//    mesh->point[ip].c[1] = yprev[ip];
//    mesh->point[ip].c[2] = zprev[ip];
//  }
//
//  MMG5_SAFE_FREE(xprev);
//  MMG5_SAFE_FREE(yprev);
//  MMG5_SAFE_FREE(zprev);
}


/**
 * \param mesh pointer on the mesh in the before the last Jacobi iteration
 * \param filename name of file containg the reference mesh
 * \param x0 x coordinate of initial points
 * \param y0 y coordinate of initial points
 * \param normal0 normal vectors in reference mesh
 * \param area0 elements' areas in reference mesh
 *
 * Read coordinates of points in reference mesh from an external file and computes its normal vectors and areas of the elements
 */



void FMG_readRefMesh2d(MMG5_pMesh mesh, char *filename, double *xref, double *yref) {

  int ip, iel, iloc, trash;
  double normal[2], *xprev, *yprev;
  char trash2[100];
  FILE *inputFile;  

//  MMG5_SAFE_CALLOC(xprev,mesh->np+1,double,return 0);
//  MMG5_SAFE_CALLOC(yprev,mesh->np+1,double,return 0);

/////  for (ip=1; ip<=mesh->np; ip++) {
/////    xprev[ip] = mesh->point[ip].c[0];
/////    yprev[ip] = mesh->point[ip].c[1];
/////  }

  if ( !(inputFile = fopen(filename,"r"))) {
    printf("\n REFERENCE MESH %s NOT FOUND!!!\n\n ABORTING FMG \n\n",filename);
    exit(EXIT_FAILURE);
  }

  trash = 0;
  for (ip=1; ip<=10; ip++) {
    fgets(trash2,100,inputFile);
    if (atoi(trash2) == mesh->np) {
      printf("\n*** Reading reference mesh : OK\n");
      break;
    }
  }

  for (ip=1; ip<=mesh->np;ip++) {
    if(fscanf(inputFile,"%lf %lf %d",&xref[ip],&yref[ip],&trash) != 3) {
      printf("\n !!! Error reading points of regular mesh!!");
      exit(EXIT_FAILURE);
    }
//    printf("\n %d %lf %lf %d",ip,x0[ip],y0[ip],trash);
  }

/////  for (ip=1; ip<=mesh->np; ip++) {
/////    mesh->point[ip].c[0] = x0[ip];
/////    mesh->point[ip].c[1] = y0[ip];
/////  }
/////
/////  for (iel=1; iel<=mesh->nt; iel++) {
/////    area0[iel] = FMG_computeArea(mesh,iel);
/////    for (iloc=0;iloc<3;iloc++){
/////      FMG_computeNormalVector2d(normal, mesh, iel, iloc);
/////      normal0[6*iel+2*iloc] = normal[0]; 
/////      normal0[6*iel+2*iloc+1] = normal[1];   
/////    }
/////  }
///// 
/////  for (ip=1; ip<=mesh->np; ip++) {
/////    mesh->point[ip].c[0] = xprev[ip];
/////    mesh->point[ip].c[1] = yprev[ip];
/////  }
/////
/////  MMG5_SAFE_FREE(xprev);
/////  MMG5_SAFE_FREE(yprev);
}



/**
 * \param filename name of file containg the solution
 *
 * Read the solution values from a .sol file and write then to a .sol.out file
 */

void FMG_readSolValues(char *filename, int dim) {

  char inputName[40], tmpName[40], outputName[40], command[200], c1[100];
  FILE *inputFile, *tmpFile, *outputFile;
  int iso, ip, np;
  double *M, val, a, b, c, l1, l2;

  strcpy(inputName,filename);
  inputFile = fopen(inputName,"r");

  strcpy(tmpName,inputName);
  strcat(tmpName,".tmp");
  strcpy(outputName,inputName);
  strcat(outputName,".out");

  sprintf(command,"awk '/SolAtVertices/,/End/{if ($0==%s) next; else print $0}' %s > %s","\"SolAtVertices\"",inputName,tmpName);


  system(command);

  tmpFile = fopen(tmpName,"r");

  if (fscanf(tmpFile,"%d",&np) != 1) {
    printf("\n !!! Error reading number of points");
    exit(EXIT_FAILURE);
  }
//  else
////    printf("\n *** Number of points : %d",np);

  if (fscanf(tmpFile,"%d %d",&ip,&iso) != 2) {
    printf("\n !!! Error reading type of adaptation (iso/aniso)");
    exit(EXIT_FAILURE);
  }
////  else {
////    if (iso == 1)
////      printf("\n *** Metric type : iso");
////    else if (iso==3)
////      printf("\n *** Metric type : aniso");
////    else {
////      printf("\n !!! Wrong type of adaptation (iso/aniso)");
////      exit(EXIT_FAILURE);
////    }
////  }



  outputFile = fopen(outputName,"w");


  if (dim==2) {
    if (iso == 1){
      for (ip=1; ip<=np; ip++)
        if (fscanf(tmpFile,"%lf",&val) != iso ) {
          printf ("\n !!! Error reading point %d",ip);
          exit(EXIT_FAILURE);
        }
        else
          fprintf(outputFile,"%.16lf %.16lf\n",val,val);
  //    printf("\n *** Last point : %.16lf",val);
    }
    else {
      M = malloc(3*sizeof(double));
      for (ip=1; ip<=np; ip++)
        if (fscanf(tmpFile,"%lf %lf %lf\n",&M[0],&M[1],&M[2]) != iso ) {
          printf ("\n !!! Error reading point %d",ip);
          exit(EXIT_FAILURE);
        }
        else {
          a = 1.;
          b = -M[0] - M[2];
          c = M[0]*M[2] - M[1]*M[1];
          l1 = (-b - sqrt(b*b-4.*a*c))/(2.*a);
          l2 = (-b + sqrt(b*b-4.*a*c))/(2.*a);
          fprintf(outputFile,"%.16lf %.16lf\n",1./sqrt(l1),1./sqrt(l2));
        }
  //    printf("\n *** Last point : %.16lf %.16lf %.16lf",M[0],M[1],M[2]);
    }
  
  }
  else {
    if (iso == 1){
      for (ip=1; ip<=np; ip++)
        if (fscanf(tmpFile,"%lf",&val) != iso ) {
          printf ("\n !!! Error reading point %d",ip);
          exit(EXIT_FAILURE);
        }
        else
          fprintf(outputFile,"%.16lf %.16lf %.16lf\n",val,val,val);
  //    printf("\n *** Last point : %.16lf",val);
    }
    else {
      M = malloc(3*sizeof(double));
      for (ip=1; ip<=np; ip++)
        if (fscanf(tmpFile,"%lf %lf %lf\n",&M[0],&M[1],&M[2]) != iso ) {
          printf ("\n !!! Error reading point %d",ip);
          exit(EXIT_FAILURE);
        }
        else {
          a = 1.;
          b = -M[0] - M[2];
          c = M[0]*M[2] - M[1]*M[1];
          l1 = (-b - sqrt(b*b-4.*a*c))/(2.*a);
          l2 = (-b + sqrt(b*b-4.*a*c))/(2.*a);
          fprintf(outputFile,"%.16lf %.16lf\n",1./sqrt(l1),1./sqrt(l2));
        }
  //    printf("\n *** Last point : %.16lf %.16lf %.16lf",M[0],M[1],M[2]);
    }
  
  
  }

  fclose(inputFile);
  fclose(outputFile);
  fclose(tmpFile);

//  printf ("\n *** %s written (contains metri values)",outputName);
  sprintf(c1,"rm %s",tmpName);
  system(c1);
//  printf("\n\n *** DONE!\n\n");
}

