#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <float.h>

#include "libfmg.h"

int main(int argc,char *argv[]) {
  FMG_pData       fmgdata;
  MMG5_pMesh      mmgMesh;
  MMG5_pSol       mmgSol;
  int             i,k,ier,ind;
  double          x,y;
  char            *pwd,*inname,*outname;
  double          *xref, *yref;

  fprintf(stdout,"  -- TEST FMGLIB \n");
  
  /**** initialisation of the mmg mesh ****/
  mmgMesh = NULL;
  mmgSol  = NULL;
  fmgdata = NULL;

  MMG2D_Init_mesh(MMG5_ARG_start,MMG5_ARG_ppMesh,&mmgMesh,
		  MMG5_ARG_ppMet,&mmgSol,
		  MMG5_ARG_end);

 if ( !MMG2D_Set_inputMeshName(mmgMesh,"dom.mesh") )
    exit(EXIT_FAILURE);
 if ( !MMG2D_loadMesh(mmgMesh,"dom.mesh") )  exit(EXIT_FAILURE);

 /*fmg init*/
 FMG_Init_fmg2d(&mmgMesh,&fmgdata);
  printf("here ok1 %p\n",(fmgdata->info)->alpha);

 /*set sol size : here only one solution*/
 if ( !FMG_Set_solSize(mmgMesh,mmgSol,fmgdata,mmgMesh->np,1) )
    exit(EXIT_FAILURE);
  printf("here ok2 %p\n",(fmgdata->info)->alpha);

  for (k=1;k<=mmgMesh->np;k++)
  {
    x = mmgMesh->point[k].c[0];
    y = mmgMesh->point[k].c[1];
    FMG_Set_scalarSol(mmgSol,fmgdata,exp(-100*pow(y-x*x+0.5,2)),k,0,1);
  }

 

  /** set parameters */
  if ( FMG_Set_iparameter(mmgMesh,fmgdata,FMG_IPARAM_levelSet,1) != 0 ) exit(EXIT_FAILURE); 
  if ( FMG_Set_iparameter(mmgMesh,fmgdata,FMG_IPARAM_circDom,1) != 0) exit(EXIT_FAILURE); 
  if ( FMG_Set_iparameter(mmgMesh,fmgdata,FMG_IPARAM_nsols,1) != 0) exit(EXIT_FAILURE); 
  if ( FMG_Set_dparameter(mmgMesh,fmgdata,FMG_DPARAM_delta,0.02) != 0) exit(EXIT_FAILURE);
  /*adaptation according to an analytic function*/
  if ( FMG_Set_iparameter(mmgMesh,fmgdata,FMG_IPARAM_solCase,FMG_UDFUNCTION) != 0) exit(EXIT_FAILURE); 

  /** library call */
  xref = calloc(mmgMesh->np+1,sizeof(double));
  yref = calloc(mmgMesh->np+1,sizeof(double));
  for (i=1; i<=mmgMesh->np; i++) {
    xref[i] = mmgMesh->point[i].c[0];
    yref[i] = mmgMesh->point[i].c[1];
    
  }

  FMG_Start_fmg2d(mmgMesh,mmgSol,fmgdata);
  printf("here ok %p\n",(fmgdata->info)->alpha);
  ier = FMG_fmglib2d(mmgMesh,mmgSol,fmgdata,xref,yref);
  if ( ier == FMG_STRONGFAILURE ) {
    fprintf(stdout,"BAD ENDING OF FMGLIB: UNABLE TO SAVE MESH\n");
    return(ier);
  } else if ( ier == FMG_LOWFAILURE )
    fprintf(stdout,"BAD ENDING OF FMGLIB\n");

  fprintf(stdout,"RIGHT ENDING\n");


 if ( !MMG2D_saveMesh(mmgMesh,"init.mesh") )  exit(EXIT_FAILURE);


 if ( !MMG2D_saveSol(mmgMesh,mmgSol,"init.sol") )  exit(EXIT_FAILURE);
   
 free(xref);
 free(yref);
 FMG_End_fmg2d(mmgMesh,fmgdata);
 MMG2D_Free_all(MMG5_ARG_start,
                 MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
                 MMG5_ARG_end);
 
}
