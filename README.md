[![Build Status](https://ci.inria.fr/fmg/job/Fmg-multiConf/badge/icon)](https://ci.inria.fr/fmg/job/Fmg-multiConf/)


# Compilation

In a shell:
```
    cd fmg
    mkdir build
    cd build
    cmake ..
    make
    make install
```

If the `make install` command fail, try to run the `sudo make install` command.

The **fmg** application is available under the `fmg_O3` commands. 

### WARNING
* The **mmg** develop branch is needed for the compilation. Last supported commit: 8b8b82edb05badb1abb311a5b0ca6a7cf57b3477
* Compile only with 1 thread (while procrastinating the modification of CMakeList).

# Testing

Supported tests are available under the `ctest` command.

# Documentation

- Some test cases in the `rapports/` folder.
- The most recent publication https://inria.hal.science/hal-03194100v1 for application cases.