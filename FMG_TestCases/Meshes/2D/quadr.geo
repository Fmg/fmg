// Quadrilatère 

Mesh.SaveElementTagType=2; // Pour les références
h = 0.02;

Point(1) = {-0.5,-0.5,0,h};
Point(2) = {-0.5,0.5,0,h};
Point(3) = {2.,0.5,0,h};
Point(4) = {2.,-0.5,0,h};

Line(7) = {1,2};
Line(9) = {2,3};
Line(8) = {3,4};
Line(10) = {4,1};

Line Loop(1) = {7,9,8,10};
Plane Surface(1) = {1}; 

Physical Line(101) = {7}; // ref des points de la ligne 7 est 101 
Physical Line(102) = {8}; // ...
Physical Line(103) = {9};
Physical Line(104) = {10};


Physical Surface(1) = {1};



