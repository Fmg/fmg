// Domaine "infini" plus raffiné au centre
// Gmsh project created on Wed Aug 12 13:39:11 2015

Mesh.Algorithm = 8;

//////// PARAMETRES //////////////////


///// Taille max et min du maillage
lcMax = 5;
lcMin = 0.01;

///// Couche avec les elements les plus petits
distmin = 2.;

///// Domaine circulaire
ray = 20.;
xc = 0.;
yc = 0.;

///// Bounding box de l'objet (circle)
xmin = 0.6;
xmax = .9;
ymin = 0.9;
ymax = 1.1;
yavg = (ymin+ymax)/2.;
//////////////////////////////////////


// Domaine circulaire
Point(1) = {xc-ray, yc, 0, lcMax};
Point(2) = {xc, yc+ray, 0, lcMax};
Point(3) = {xc, yc-ray, 0, lcMax};
Point(4) = {xc+ray, yc, 0, lcMax};
Point(5) = {0, 0, 0, lcMin};
Circle(1) = {2, 5, 2};
Line Loop(5) = {1};
Plane Surface(6) = {5};
Physical Line(8) = {1};  // ref du bord!!!

// Bounding box
xc2 = (xmin+xmax)/2;
yc2 = (ymin+ymax)/2;
Point(9) = {xmin, yc2, 0, lcMin};
Point(10) = {xc2, ymax, 0, lcMin};
Point(11) = {xmax, yc2, 0, lcMin};
Point(12) = {xc2, ymin, 0, lcMin};
Point(13) = {xc2, yc2, 0, lcMin};
Circle(5) = {9,13,9};
Line Loop(6) = {5};
Physical Surface(12) = {6};

Field[1] = Attractor;
Field[1].EdgesList = {5};
Field[1].NNodesByEdge = 100;

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = lcMin;
Field[2].LcMax = lcMax;
Field[2].DistMin = distmin;
Field[2].DistMax = 20.;

Field[3] = Min;
Field[3].FieldsList = {2};
Background Field = 3;

Mesh.CharacteristicLengthExtendFromBoundary = 0;


