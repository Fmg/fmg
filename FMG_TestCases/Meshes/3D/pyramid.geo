// Pyramide de base carrée

l = 0.01;

Point(1) = {0.5, -.5, 0, l};
Point(2) = {0.5, .5, 0, l};
Point(3) = {-.5, .5, 0, l};
Point(4) = {-.5, -.5, 0, l};
Point(5) = {0, 0, 1., l};
Line(1) = {3, 4};
Line(2) = {4, 1};
Line(3) = {2, 1};
Line(4) = {2, 3};
Line(5) = {5, 3};
Line(6) = {4, 5};
Line(7) = {5, 2};
Line(8) = {5, 1};
Line Loop(9) = {2, -3, 4, 1};
Plane Surface(10) = {9};
Line Loop(11) = {6, 5, 1};
Line Loop(12) = {8, -3, -7};
Plane Surface(13) = {11};
Line Loop(14) = {7, 4, -5};
Plane Surface(15) = {14};
Plane Surface(16) = {12};
Line Loop(17) = {8, -2, 6};
Plane Surface(18) = {17};
Physical Surface(19) = {13};
Physical Surface(20) = {10};
Physical Surface(21) = {16};
Physical Surface(22) = {18};
Surface Loop(23) = {13, 18, 16, 10, 15};
Volume(24) = {23};
Physical Volume(25) = {24};
Physical Surface(26) = {15};
