//// Boîte de côté R avec un trou cylindrique de rayon r

l = 0.1;
R = 0.5;
r = 0.25;
z0 = -.5;
zExt = 1.;

Point(1) = {-R, -R, z0, l};
Point(2) = {-R, R, z0, l};
Point(3) = {R, R, z0, l};
Point(4) = {R, -R, z0, l};
Line(1) = {3, 2};
Line(2) = {2, 1};
Line(3) = {1, 4};
Line(4) = {4, 3};
Point(5) = {-0, 0, z0, l};
Point(6) = {-0, r, z0, l};
Point(7) = {r, 0, z0, l};
Point(8) = {-0, -r, z0, l};
Point(9) = {-r, 0, z0, l};
Circle(5) = {6, 5, 7};
Circle(6) = {7, 5, 8};
Circle(7) = {8, 5, 9};
Circle(8) = {9, 5, 6};

Line Loop(1) = {1,2,3,4};
Line Loop(2) = {5,6,7,8};
Plane Surface(9) = {1, 2};
Extrude {0, 0, zExt} {
  Surface{9};
}
