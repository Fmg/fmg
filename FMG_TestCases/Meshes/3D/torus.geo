// Tore de rayon R et section circulaire de rayon r

lc = .025;
R = .5;
r = .1;

Point(1) = {0,0,0,lc};
Point(2) = {R+r,0,0,lc};
Point(3) = {0,R+r,0,lc};
Point(4) = {-R-r,0,0,lc};
Point(5) = {0,-R-r,0,lc};
Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};

Point(6) = {R-r,0,0,lc};
Point(7) = {0,R-r,0,lc};
Point(8) = {-R+r,0,0,lc};
Point(9) = {0,-R+r,0,lc};
Circle(5) = {6,1,7};
Circle(6) = {7,1,8};
Circle(7) = {8,1,9};
Circle(8) = {9,1,6};

Point(11) = {0,0,r,lc};
Point(12) = {R,0,r,lc};
Point(13) = {0,R,r,lc};
Point(14) = {-R,0,r,lc};
Point(15) = {0,-R,r,lc};
Circle(11) = {12,11,13};
Circle(12) = {13,11,14};
Circle(13) = {14,11,15};
Circle(14) = {15,11,12};

Point(21) = {0,0,-r,lc};
Point(22) = {R,0,-r,lc};
Point(23) = {0,R,-r,lc};
Point(24) = {-R,0,-r,lc};
Point(25) = {0,-R,-r,lc};
Circle(21) = {22,21,23};
Circle(22) = {23,21,24};
Circle(23) = {24,21,25};
Circle(24) = {25,21,22};

Point(31) = {R,0,0,lc};
Circle(31) = {6,31,12};
Circle(32) = {12,31,2};
Circle(33) = {2,31,22};
Circle(34) = {22,31,6};

Point(41) = {-R,0,0,lc};
Circle(41) = {8,41,14};
Circle(42) = {14,41,4};
Circle(43) = {4,41,24};
Circle(44) = {24,41,8};

Point(51) = {0,R,0,lc};
Circle(51) = {7,51,13};
Circle(52) = {13,51,3};
Circle(53) = {3,51,23};
Circle(54) = {23,51,7};

Point(61) = {0,-R,0,lc};
Circle(61) = {9,61,15};
Circle(62) = {15,61,5};
Circle(63) = {5,61,25};
Circle(64) = {25,61,9};

Line Loop(65) = {12, 42, -2, -52};
Ruled Surface(66) = {65};
Line Loop(67) = {52, -1, -32, 11};
Ruled Surface(68) = {67};
Line Loop(69) = {32, -4, -62, 14};
Ruled Surface(70) = {69};
Line Loop(71) = {13, 62, -3, -42};
Ruled Surface(72) = {71};
Line Loop(73) = {12, -41, -6, 51};
Ruled Surface(74) = {73};
Line Loop(75) = {51, -11, -31, 5};
Ruled Surface(76) = {75};
Line Loop(77) = {31, -14, -61, 8};
Ruled Surface(78) = {77};
Line Loop(79) = {61, -13, -41, 7};
Ruled Surface(80) = {79};
Line Loop(81) = {43, 23, -63, -3};
Line Loop(82) = {1, 53, -21, -33};
Ruled Surface(83) = {82};
Line Loop(84) = {33, -24, -63, 4};
Ruled Surface(85) = {84};
Ruled Surface(86) = {81};
Line Loop(87) = {43, -22, -53, 2};
Ruled Surface(88) = {87};
Line Loop(89) = {44, 7, -64, -23};
Ruled Surface(90) = {89};
Line Loop(91) = {44, -6, -54, 22};
Ruled Surface(92) = {91};
Line Loop(93) = {54, -5, -34, 21};
Ruled Surface(94) = {93};
Line Loop(95) = {24, 34, -8, -64};
Ruled Surface(96) = {95};

Surface Loop(97) = {74, 66, 72, 80, 78, 76, 68, 83, 88, 86, 90, 92, 94, 96, 85, 70};
Volume(98) = {97};
Physical Surface(99) = {74, 66, 76, 68, 78, 70, 96, 85, 86, 90, 72, 80, 92, 88, 83, 94};
Physical Volume(100) = {98};
