// Cone de hauteur H et rayon R

lc = 0.05;
R = 0.2;
H = 0.5;

Point(1) = {0,0,0,lc};
Point(2) = {R,0,0,lc};
Point(3) = {0,R,0,lc};
Point(4) = {-R,0,0,lc};
Point(5) = {0,-R,0,lc};
Point(6) = {0,0,H,lc};

Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};
Line(5) = {2,6};
Line(6) = {3,6};
Line(7) = {4,6};
Line(8) = {5,6};

Line Loop(1) = {1,6,-5};
Line Loop(2) = {2,7,-6};
Line Loop(3) = {3,8,-7};
Line Loop(4) = {4,5,-8};
Line Loop(5) = {1,2,3,4};
 
Ruled Surface(1) = {1};
Ruled Surface(2) = {2};
Ruled Surface(3) = {3};
Ruled Surface(4) = {4};
Ruled Surface(5) = {5};

Surface Loop (1) = {1,2,3,4,5};

Volume(1) = {1};

Physical Volume(1) = {1};
Physical Surface(1) = {1,2,3,4,5};

//Line Loop(4) = {2,3,1};
//Plane Surface(5) = {4};
//Extrude {{0,0,1}, {0,0,0}, Pi/2} { Surface{5}; }

