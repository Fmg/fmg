// cube [cmin,cmax]^3

l = 0.2;
cmin = -3.;
cmax = 3.;

Point(1) = {cmin, cmin, cmin, l};
Point(2) = {cmin, cmin, cmax, l};
Point(3) = {cmax, cmin, cmax, l};
Point(4) = {cmax, cmin, cmin, l};
Point(5) = {cmax, cmax, cmin, l};
Point(6) = {cmax, cmax, cmax, l};
Point(7) = {cmin, cmax, cmax, l};
Point(8) = {cmin, cmax, cmin, l};
Line(1) = {7, 8};
Line(2) = {8, 5};
Line(3) = {5, 7};
Line(4) = {7, 6};
Line(5) = {5, 6};
Line(6) = {8, 1};
Line(7) = {7, 2};
Line(8) = {1, 2};
//Delete {
//  Line{3};
//}
Line(9) = {1, 4};
Line(10) = {4, 5};
Line(11) = {6, 3};
Line(12) = {2, 3};
Line(13) = {4, 3};
Line Loop(14) = {1, 6, 8, -7};
Plane Surface(15) = {14};
Line Loop(16) = {2, 5, -4, 1};
Plane Surface(17) = {16};
Line Loop(18) = {6, 9, 10, -2};
Plane Surface(19) = {18};
Line Loop(20) = {7, 12, -11, -4};
Plane Surface(21) = {20};
Line Loop(22) = {8, 12, -13, -9};
Plane Surface(23) = {22};
Line Loop(24) = {11, -13, 10, 5};
Plane Surface(25) = {24};
Surface Loop(26) = {15, 17, 19, 23, 21, 25};
Volume(27) = {26};
Physical Volume(28) = {27};
Physical Surface(29) = {15, 19, 17, 25, 21, 23};

