#!/bin/bash

# get arguments
CMAKE_SOURCE_DIR=$1
FMG_SOURCE_DIR=$2

# test if git_log.h is present
if [ ! -f "$FMG_SOURCE_DIR/git_log.h" ]; then
   \touch $FMG_SOURCE_DIR/git_log.h
   echo "#define FMG_GIT_BRANCH \"No git branch found\"" >> "$FMG_SOURCE_DIR/git_log.h"
   echo "#define FMG_GIT_COMMIT \"No git commit found\"" >> "$FMG_SOURCE_DIR/git_log.h"
   echo "#define FMG_GIT_DATE   \"No git commit found\"" >> "$FMG_SOURCE_DIR/git_log.h"
fi

# parameter
git_is_present=1

# test if .git is present
if [ ! -d $CMAKE_SOURCE_DIR/.git ]; then
   git_is_present=0
fi

git_commit=`git rev-parse HEAD 2> /dev/null`
if [ "$git_commit" == "" ]; then
   git_is_present=0
fi

if [ $git_is_present -eq 1 ]; then
   git_branch=`git rev-parse --abbrev-ref HEAD 2> /dev/null`
   echo "   > Found a git branch: $git_branch"
   echo "   > Found a git commit: $git_commit"
   git_date=`git show -s --format="%ci" $git_commit`
   echo "   > Found a git date: $git_date"

   # erase any previous version file
   \rm -f "$FMG_SOURCE_DIR/git_log.h_tmp"
   \touch "$FMG_SOURCE_DIR/git_log.h_tmp"
   echo "#define FMG_GIT_BRANCH \"$git_branch\"" >> "$FMG_SOURCE_DIR/git_log.h_tmp"
   echo "#define FMG_GIT_COMMIT \"$git_commit\"" >> "$FMG_SOURCE_DIR/git_log.h_tmp"
   echo "#define FMG_GIT_DATE   \"$git_date\""   >> "$FMG_SOURCE_DIR/git_log.h_tmp"

   # diff
   diff=`diff "$FMG_SOURCE_DIR/git_log.h_tmp" "$FMG_SOURCE_DIR/git_log.h"`
   if [ "$diff" != "" ]; then
      \cp "$FMG_SOURCE_DIR/git_log.h_tmp" "$FMG_SOURCE_DIR/git_log.h"
   fi

   # clean
   \rm -f "$FMG_SOURCE_DIR/git_log.h_tmp"
fi
